import { Instance, SnapshotOut, types } from "mobx-state-tree"

/**
 * Model description here for TypeScript hints.
 * this modal is for modals and ajouter editer watchlist
 */
export const ValidityModel = types
  .model("Validity")
  .props({
    modalToggle : types.optional(types.boolean,false),
    ajouterWatchList : types.optional(types.boolean,true),
    editerWatchList : types.optional(types.boolean,true),

  })
  .views((self) => ({
    get GetModalToggle(){
      return self.modalToggle
    },
    get GetAjouterWatchList(){
      return self.ajouterWatchList
    },
    get GetEditerWatchList(){
      return self.editerWatchList
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    setModalToggle(value : boolean){
      self.modalToggle=value
    },
    setajouterWatchList(value : boolean){
      self.ajouterWatchList=value
    },
    setEditerWatchList(value : boolean){
      self.editerWatchList=value
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

type ValidityType = Instance<typeof ValidityModel>
export interface Validity extends ValidityType {}
type ValiditySnapshotType = SnapshotOut<typeof ValidityModel>
export interface ValiditySnapshot extends ValiditySnapshotType {}
export const createValidityDefaultModel = () => types.optional(ValidityModel, {})
