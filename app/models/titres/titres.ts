import { flow, Instance, SnapshotOut, types } from "mobx-state-tree"
import { Value } from "react-powerplug"
import { Api } from "../../services/api"

/**
 * Model description here for TypeScript hints.
 */
export const TitresModel = types
  .model("Titres")
  .props({
    resultcode : types.optional(types.number,1111),
    detailcode : types.optional(types.number,1111),
    accountingDate : types.optional(types.number,0),
    plainInventoryElementList:types.optional(types.frozen(),[]),

    loadingTitres:types.optional(types.boolean,false),
  })
  .views((self) => ({
    get GetResultCode(){
      return self.resultcode
    },
    get GetDetailCode(){
      return self.detailcode
    },
    get GetAccountingDate(){
      return self.accountingDate
    },
    get GetLoadingTitres(){
      return self.loadingTitres
    },
    get GetplainInventoryElementList(){
      return self.plainInventoryElementList
    }
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    setResultCode(value : any){
      self.resultcode=value
    },
    setDetailCode(value : any){
      self.detailcode=value
    },
    setAccountingDate(value:any){
      self.accountingDate=value
    },
    setLoadingTitres(value:boolean){
      self.loadingTitres=value
   },
   setplainInventoryElementList(value:any){
     self.plainInventoryElementList=value
   }
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({

    GetTitres: flow(function * (login: string,contextId:string,accountCode:number,valuationType:number){
      const api= new Api()
      api.setup()
      self.setLoadingTitres(true)
      yield api.GetTitresBKBApi(login,contextId,accountCode,valuationType).then((response:any)=>{
       
        if(response.status===200){
          self.setLoadingTitres(false)

          self.setDetailCode(response.detail_code)
          self.setResultCode(response.result_code)
          self.setAccountingDate(response.accountingDate)
          self.setplainInventoryElementList(response.plainInventoryElementList)
          
        

        }
        else{
          //error
          self.setLoadingTitres(false)

          self.setDetailCode(response.detail_code)
          self.setResultCode(response.result_code)


        }
        
      })
    }),
  })) 

type TitresType = Instance<typeof TitresModel>
export interface Titres extends TitresType {}
type TitresSnapshotType = SnapshotOut<typeof TitresModel>
export interface TitresSnapshot extends TitresSnapshotType {}
export const createTitresDefaultModel = () => types.optional(TitresModel, {})
