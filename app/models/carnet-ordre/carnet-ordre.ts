import { flow, Instance, SnapshotOut, types } from "mobx-state-tree"
import { Api } from "../../services/api"

/**
 * Model description here for TypeScript hints.
 */
export const CarnetOrdreModel = types
  .model("CarnetOrdre")
  .props({
    orderList:types.optional(types.frozen(),[]),
    loadingOrderList : types.optional(types.boolean,false),
    resultcode : types.optional(types.number,1111),
    detailcode : types.optional(types.number,1111),

  })
  .views((self) => ({
    get GetOrderList(){
      return self.orderList
    },
    get GetLoadingOrderList(){
      return self.loadingOrderList
    },
    get GetResultCode(){
      return self.resultcode
    },
    get GetDetailCode(){
      return self.detailcode
    },
  })) 
  .actions((self) => ({
    setOrderList(value : any){
      self.orderList=value
    },
    setLoadingOrderList(value : boolean){
      self.loadingOrderList=value
    },
    setResultCode(value : any){
      self.resultcode=value
    },
    setDetailCode(value : any){
      self.detailcode=value
    },
  })) 
  .actions((self) => ({
    GetOrderListBKB: flow(function * (login: string,contextId:string,accountCode:number){
      self.setLoadingOrderList(true)
      const api= new Api()
      api.setup()
      yield api.GetOrderListBKBApi(login,contextId,accountCode).then((response:any)=>{
        if(response.status===200){
          self.setLoadingOrderList(false)
          self.setDetailCode(response.detail_code)
          self.setResultCode(response.result_code)
          self.setOrderList(response.orderList)
          
          
        }
        else{
          self.setLoadingOrderList(false)

          //error
          self.setDetailCode(response.detail_code)
          self.setResultCode(response.result_code)


        }
        
      })
    }),
  })) 


type CarnetOrdreType = Instance<typeof CarnetOrdreModel>
export interface CarnetOrdre extends CarnetOrdreType {}
type CarnetOrdreSnapshotType = SnapshotOut<typeof CarnetOrdreModel>
export interface CarnetOrdreSnapshot extends CarnetOrdreSnapshotType {}
export const createCarnetOrdreDefaultModel = () => types.optional(CarnetOrdreModel, {})
