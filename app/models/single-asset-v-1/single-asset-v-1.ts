import { flow, Instance, SnapshotOut, types } from "mobx-state-tree"
import { Api } from "../../services/api"

/**
 * Model description here for TypeScript hints.
 */
export const SingleAssetV1Model = types
  .model("SingleAssetV1")
  .props({
    resultcode : types.optional(types.number,1111),
    detailcode : types.optional(types.number,1111),
    validityList:types.optional(types.frozen(),[]),
    modalityList :types.optional(types.frozen(),[]),
    conditionList :types.optional(types.frozen(),[]),
    loadingSingleAsset : types.optional(types.boolean,false),
  })
  .views((self) => ({
    get GetResultCode(){
      return self.resultcode
    },
    get GetDetailCode(){
      return self.detailcode
    },
    get GetValidityList(){
      return self.validityList
    },
    get GetModalityList(){
      return self.modalityList
    },
    get GetConditionList(){
      return self.modalityList
    },
    get GetLoadingSingleAsset(){
      return self.loadingSingleAsset
    }
  })) 
  .actions((self) => ({
    setResultCode(value : any){
      self.resultcode=value
    },
    setDetailCode(value : any){
      self.detailcode=value
    },
    setValidityList(value : any){
      return self.validityList=value
    },
    setModalityList(value : any){
      return self.modalityList=value
    },
    setConditionList(value : any){
      return self.conditionList=value
    },
    setLoadingSingleAsset(value : boolean){
      return self.loadingSingleAsset=value
    }

  })) 
  .actions((self) => ({

    GetSignleAssetV1: flow(function * (login: string,contextId:string,accountCode:number,assetCode:string,quotationPlaceCode:number,withQuote:number){
      const api= new Api()
      api.setup()
      self.setLoadingSingleAsset(true)
      yield api.getSingleAssetV1API(login,contextId,accountCode,assetCode,quotationPlaceCode,withQuote).then((response:any)=>{
       
        if(response.status===200){
          self.setLoadingSingleAsset(false)

          self.setDetailCode(response.detail_code)
          self.setResultCode(response.result_code)
          self.setValidityList(response.validityList)
          self.setModalityList(response.modalityList)
          self.setConditionList(response.conditionList)
          

        }
        else{
          //error
          self.setLoadingSingleAsset(false)

          self.setDetailCode(response.detail_code)
          self.setResultCode(response.result_code)


        }
        
      })
    }),

  })) 

type SingleAssetV1Type = Instance<typeof SingleAssetV1Model>
export interface SingleAssetV1 extends SingleAssetV1Type {}
type SingleAssetV1SnapshotType = SnapshotOut<typeof SingleAssetV1Model>
export interface SingleAssetV1Snapshot extends SingleAssetV1SnapshotType {}
export const createSingleAssetV1DefaultModel = () => types.optional(SingleAssetV1Model, {})
