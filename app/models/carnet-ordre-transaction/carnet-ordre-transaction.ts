import { flow, Instance, SnapshotOut, types } from "mobx-state-tree"
import { Api } from "../../services/api"

/**
 * Model description here for TypeScript hints.
 */
export const CarnetOrdreTransactionModel = types
  .model("CarnetOrdreTransaction")
  .props({
    orderListTransaction:types.optional(types.frozen(),[]),
    loadingorderListTransaction : types.optional(types.boolean,false),
    resultcode : types.optional(types.number,1111),
    detailcode : types.optional(types.number,1111),
  })
  .views((self) => ({
    get GetorderListTransaction(){
      return self.orderListTransaction
    },
    get GetLoadingorderListTransaction(){
      return self.loadingorderListTransaction
    },
    get GetResultCode(){
      return self.resultcode
    },
    get GetDetailCode(){
      return self.detailcode
    },
  })) 
  .actions((self) => ({
    setorderListTransaction(value : any){
      self.orderListTransaction=value
    },
    setLoadingorderListTransaction(value : boolean){
      self.loadingorderListTransaction=value
    },
    setResultCode(value : any){
      self.resultcode=value
    },
    setDetailCode(value : any){
      self.detailcode=value
    },
  })) 
  .actions((self) => ({
    GetorderListTransactionBKB: flow(function * (login: string,contextId:string,accountCode:number){
      self.setLoadingorderListTransaction(true)
      const api= new Api()
      api.setup()
      // yield api.GetorderListTransactionBKBApi(login,contextId,accountCode).then((response:any)=>{
      //   if(response.status===200){
      //     self.setLoadingorderListTransaction(false)
      //     self.setDetailCode(response.detail_code)
      //     self.setResultCode(response.result_code)
      //     self.setorderListTransaction(response.orderListTransaction)
          
          
      //   }
      //   else{
      //     self.setLoadingorderListTransaction(false)

      //     //error
      //     self.setDetailCode(response.detail_code)
      //     self.setResultCode(response.result_code)


      //   }
        
      // })
    }),
  })) 

type CarnetOrdreTransactionType = Instance<typeof CarnetOrdreTransactionModel>
export interface CarnetOrdreTransaction extends CarnetOrdreTransactionType {}
type CarnetOrdreTransactionSnapshotType = SnapshotOut<typeof CarnetOrdreTransactionModel>
export interface CarnetOrdreTransactionSnapshot extends CarnetOrdreTransactionSnapshotType {}
export const createCarnetOrdreTransactionDefaultModel = () => types.optional(CarnetOrdreTransactionModel, {})
