import { Instance, SnapshotOut, types } from "mobx-state-tree"

/**
 * Model description here for TypeScript hints.
 */
export const CarnetOrdreModalModel = types
  .model("CarnetOrdreModal")
  .props({
    valeur : types.optional(types.string,""),
    quantite : types.optional(types.number,0),
    validite : types.optional(types.number,0),
    date : types.optional(types.number,0),
    type_ordre : types.optional(types.string,""),
    condition : types.optional(types.string,""),
    condition_execution :types.optional(types.string,""),
    firstlimit :types.optional(types.number,0),
    message_error :types.optional(types.number,0),

  })
  .views((self) => ({
    get GetValeur(){
      return self.valeur
    },
    get GetQuantite(){
      return self.quantite
    },
    get GetValidity(){
      return self.validite
    },
    get GetDate(){
      return self.date
    },
    get GetTypeOrdre(){
      return self.type_ordre
    },
    get GetCondition(){
      return self.condition
    },
    get GetConditionExecution(){
      return self.condition_execution
    },
    get GetFirstLimit(){
      return self.firstlimit
    },
    get GetMessageError(){
      return self.message_error
    }

  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    setValeur(value : string){
      self.valeur=value
    },
    setQuantite(value : number){
      self.quantite=value
    },
    setValidity(value : number){
      self.validite=value
    },
    setDate(value : number){
      self.date=value
    },
    setTypeOrdre(value : string){
      self.type_ordre=value
    },
    setCondition(value : string){
      self.condition=value
    },
    setConditionExecution(value : string){
      self.condition_execution=value
    },
    setFirstLimit(value : number){
      self.firstlimit=value
    },
    setMessageError(value : number){
      self.message_error=value
    }

  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

type CarnetOrdreModalType = Instance<typeof CarnetOrdreModalModel>
export interface CarnetOrdreModal extends CarnetOrdreModalType {}
type CarnetOrdreModalSnapshotType = SnapshotOut<typeof CarnetOrdreModalModel>
export interface CarnetOrdreModalSnapshot extends CarnetOrdreModalSnapshotType {}
export const createCarnetOrdreModalDefaultModel = () => types.optional(CarnetOrdreModalModel, {})
