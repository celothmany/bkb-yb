import { flow, Instance, SnapshotOut, types } from "mobx-state-tree"
import { Api } from "../../services/api"

/**
 * Model description here for TypeScript hints.
 */
export const ProfileUserModel = types
  .model("ProfileUser")
  .props({
    loginCode :types.optional(types.string,""),
    password :types.optional(types.string,""),
    emetteurs:types.optional(types.string,""),
    loadingUser:types.optional(types.boolean,false),
    login :types.optional(types.string,"login"),
    contextId :types.optional(types.string,""),
    resultcode : types.optional(types.number,1111),
    detailcode : types.optional(types.number,1111),
    keepConnected:types.optional(types.boolean,false),
    accountCode : types.optional(types.number,0),
    accountLabel : types.optional(types.string,""),
    accountType : types.optional(types.number,1111),
    mainAccount : types.optional(types.number,1111),
    isLogged:types.optional(types.boolean,false),
    oldPassword :types.optional(types.string,""), 
    NewPassword :types.optional(types.string,""), 
    passwordChangedDone :types.optional(types.boolean,false), 


  })
  .views((self) => ({
    get GetLoginCode(){
      return self.loginCode
    },
    get GetPassword(){
      return self.password
    },
    get GetEmetteurs(){
      return self.emetteurs
    },
    get GetLoadingUser(){
      return self.loadingUser
    },
    get GetLogin(){
      return self.login
    },
    get GetContextId(){
      return self.contextId
    },
    get GetResultCode(){
      return self.resultcode
    },
    get GetDetailCode(){
      return self.detailcode
    },
    get GetkeepConnected(){
      return self.keepConnected
    },
    get GetAccountCode(){
      return self.accountCode
    },
    get GetAccountLabel(){
      return self.accountLabel
    },
    get GetMainAccount(){
      return self.mainAccount
    },
    get GetIslogged(){
      return self.isLogged
    },
    get GetOldPassword(){
      return self.oldPassword
    },
    get GetNewPassword(){
      return self.NewPassword
    },
    get GetPasswordChangedDone(){
      return self.passwordChangedDone
    }
    
  }))
  .actions((self) => ({
    setLoginCode(value : string){
      self.loginCode=value
  },
  setPassword(value : string){
    self.password=value
 },
  setEmetteurs(value : string){
    self.emetteurs=value
  },
  setLoaingUser(value:boolean){
    self.loadingUser=value
  },
  setLogin(value : string){
    self.login=value
  },
  setContextId(value : string){
    self.contextId=value
  },
  setResultCode(value : any){
    self.resultcode=value
  },
  setDetailCode(value : any){
    self.detailcode=value
  },
  setkeepConnected(value : boolean){
    self.keepConnected=value
  },
  setAccountCode(value : number){
    self.accountCode=value 
  },
  setAccountLabel(value : string){
    self.accountLabel=value 
  },
  setAccountType(value : number){
    self.accountType=value 
  },
  setMainAccount(value : number){
    self.mainAccount=value 
  },
  setIsLogged(value : boolean){
    self.isLogged=value
  },
  setOldPassword(value : string){
    self.oldPassword=value
  },
  setNewPassword(value : string){
    self.NewPassword=value
  },
  setPassordChangedDone(value : boolean){
    self.passwordChangedDone=value
  }

  }))
  
   .actions((self)=>({
    LoginUserBKB : flow(function * (loginCode: string,password:string,emetteurs:string){
      self.setLoaingUser(true)
      const api= new Api()
      api.setup()
      yield api.LoginBKBApi(loginCode,password,emetteurs).then((response:any)=>{
        if(response.status===200){
          self.setLoaingUser(false)

          self.setDetailCode(response.detail_code)
          self.setContextId(response.contextId)
          self.setLogin(response.login)
          self.setResultCode(response.result_code)
          if(response.detail_code===0 && response.result_code===0 && response.contextId!=""){
            self.setIsLogged(true)
          }    
        }
        else{
          //error
          self.setLoaingUser(false)

          self.setDetailCode(response.detail_code)
          self.setResultCode(response.result_code)


        }
        
      })
    }),

    DisconnectUserBKB: flow(function * (login: string,contextId:string){
      self.setLoaingUser(true)
      const api= new Api()
      api.setup()
      yield api.DisconnectBKBApi(login,contextId).then((response:any)=>{
        self.setLoaingUser(false)
        if(response.status===200){
          self.setDetailCode(response.detail_code)
          self.setResultCode(response.result_code)
          
        }
        else{
          //error
          self.setDetailCode(response.detail_code)
          self.setResultCode(response.result_code)


        }
        
      })
    }),
    ChangePasswordUser: flow(function * (login: string,contextId:string,loginCode:string,oldPassword:string,newPassword:string){
      self.setLoaingUser(true)
      const api= new Api()
      api.setup()
      yield api.ChangePasswordAPI(login,contextId,loginCode,oldPassword,newPassword).then((response:any)=>{
        self.setLoaingUser(false)
        if(response.status===200){
          self.setDetailCode(response.detail_code)
          self.setResultCode(response.result_code)
          self.setPassordChangedDone(true)
          
        }
        else{
          //error
          self.setDetailCode(response.detail_code)
          self.setResultCode(response.result_code)
          self.setPassordChangedDone(false)



        }
        
      })
    }),
    ListAccountUserBKB: flow(function * (login: string,contextId:string){
      self.setLoaingUser(true)
      const api= new Api()
      api.setup()
      yield api.ListAccountBKBApi(login,contextId).then((response:any)=>{
        self.setLoaingUser(false)
        if(response.status===200){
          self.setDetailCode(response.detail_code)
          self.setResultCode(response.result_code)
          self.setAccountCode(response.accountCode)
          self.setAccountLabel(response.accountLabel)
          self.setAccountType(response.accountType)
          self.setMainAccount(response.mainAccount)
          
        }
        else{
          //error
          self.setDetailCode(response.detail_code)
          self.setResultCode(response.result_code)


        }
        
      })
    }),

   }))

type ProfileUserType = Instance<typeof ProfileUserModel>
export interface ProfileUser extends ProfileUserType {}
type ProfileUserSnapshotType = SnapshotOut<typeof ProfileUserModel>
export interface ProfileUserSnapshot extends ProfileUserSnapshotType {}
export const createProfileUserDefaultModel = () => types.optional(ProfileUserModel, {})
