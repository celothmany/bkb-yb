import { flow, Instance, SnapshotOut, types } from "mobx-state-tree"
import { Api } from "../../services/api"

/**
 * Model description here for TypeScript hints.
 */
export const SyntheseModel = types
  .model("Synthese")
  .props({
    resultcode : types.optional(types.number,1111),
    detailcode : types.optional(types.number,1111),
    accountingDate : types.optional(types.number,0),
    buyingPowerList:types.optional(types.frozen(),[]),
    currency: types.optional(types.string,"BKB"),
    assetValuationAmount:types.optional(types.number,0.0),
    cashValuationAmount:types.optional(types.number,0.0),
    globalValuationAmount :types.optional(types.number,0.0),
    bloquedValuationAmount:types.optional(types.number,0.0),
    assetQuoteMaxDate:types.optional(types.number,1111),
    loadingSynthese:types.optional(types.boolean,false),


  })
  .views((self) => ({
    get GetResultCode(){
      return self.resultcode
    },
    get GetDetailCode(){
      return self.detailcode
    },
    get GetAccountingDate(){
      return self.accountingDate
    },
    get GetBuyingPowerList(){
      return self.buyingPowerList
    },
    get GetCurrency(){
      return self.currency
    },
    get GetassetValuationAmount(){
      return self.assetValuationAmount
    },
    get GetcashValuationAmount(){
      return self.cashValuationAmount
    },
    get GetglobalValuationAmount(){
      return self.globalValuationAmount
    },
    get GetbloquedValuationAmount(){
      return self.globalValuationAmount
    },
    get GetassetQuoteMaxDate(){
      return self.assetQuoteMaxDate
    },
    get GetLoadingSynthese(){
      return self.loadingSynthese
    }
  })) 
  .actions((self) => ({
    setResultCode(value : any){
      self.resultcode=value
    },
    setDetailCode(value : any){
      self.detailcode=value
    },
    setAccountingDate(value:any){
      self.accountingDate=value
    },
    setbuyingPowerList(value : any){
      self.buyingPowerList=value
    },
    setCurrency(value : any){
      self.currency=value
    },
    setAssetValuationAmount(value:any){
      self.assetValuationAmount=value
    },
    setCashValuationAmount(value:any){
      self.cashValuationAmount=value
    },
    setGlobalValuationAmount(value:any){
      self.globalValuationAmount=value
    },
    setBloquedValuationAmount(value:any){
      self.bloquedValuationAmount=value
    },
    setAssetQuoteMaxDate(value:any){
      self.assetQuoteMaxDate=value
    },
    setLoadingSynthese(value:boolean){
      self.loadingSynthese=value
    }

  }))
  .actions((self) => ({

    GetSynthesisinRef: flow(function * (login: string,contextId:string,accountCode:number,synthesisType:number){
      const api= new Api()
      api.setup()
      self.setLoadingSynthese(true)
      yield api.GetSythensisBKBApi(login,contextId,accountCode,synthesisType).then((response:any)=>{
       
        if(response.status===200){
          self.setLoadingSynthese(false)

          self.setDetailCode(response.detail_code)
          self.setResultCode(response.result_code)
          self.setAccountingDate(response.accountingDate)
          self.setbuyingPowerList(response.buyingPowerList)
          self.setCurrency(response.currency)
          self.setAssetValuationAmount(response.assetValuationAmount)
          self.setCashValuationAmount(response.cashValuationAmount)
          self.setGlobalValuationAmount(response.globalValuationAmount)
          self.setBloquedValuationAmount(response.bloquedValuationAmount)
          self.setAssetQuoteMaxDate(response.assetQuoteMaxDate)

        }
        else{
          //error
          self.setLoadingSynthese(false)

          self.setDetailCode(response.detail_code)
          self.setResultCode(response.result_code)


        }
        
      })
    }),
   
  }))
type SyntheseType = Instance<typeof SyntheseModel>
export interface Synthese extends SyntheseType {}
type SyntheseSnapshotType = SnapshotOut<typeof SyntheseModel>
export interface SyntheseSnapshot extends SyntheseSnapshotType {}
export const createSyntheseDefaultModel = () => types.optional(SyntheseModel, {})
