import { flow, Instance, SnapshotOut, types } from "mobx-state-tree"
import { Api } from "../../services/api"

/**
 * Model description here for TypeScript hints.
 */
export const ValuesModel = types
  .model("Values")
  .props({
    resultcode : types.optional(types.number,1111),
    detailcode : types.optional(types.number,1111),
    accountingDate : types.optional(types.number,0),
    plainInventoryElementList:types.optional(types.frozen(),[]),
    currency : types.optional(types.string,""),
    loadingValues:types.optional(types.boolean,false),
    totalPendingPNL:types.optional(types.number,0),
    totalRealizedPNL:types.optional(types.number,0),
    totalSecuritiesValuation:types.optional(types.number,0),


  })
  .views((self) => ({
    get GetResultCode(){
      return self.resultcode
    },
    get GetDetailCode(){
      return self.detailcode
    },
    get GetAccountingDate(){
      return self.accountingDate
    },
    get GetLoadingTitres(){
      return self.loadingValues
    },
    get GetplainInventoryElementList(){
      return self.plainInventoryElementList
    },
    get GetCurrency(){
        return self.currency
    },
    get GetTotalPendingPNL(){
      return self.totalPendingPNL
    },
    get GetTotalRealizedPNL(){
      return self.totalRealizedPNL
    }
    ,
    get GettotalSecuritiesValuation(){
      return self.totalSecuritiesValuation
    }
  }))
  .actions((self) => ({
    setResultCode(value : any){
      self.resultcode=value
    },
    setDetailCode(value : any){
      self.detailcode=value
    },
    setAccountingDate(value:any){
      self.accountingDate=value
    },
    setloadingValues(value:boolean){
      self.loadingValues=value
   },
   setplainInventoryElementList(value:any){
     self.plainInventoryElementList=value
   },
   setTotalPendingPNL(value : any){
     self.totalPendingPNL=value
   },
   setTotalRealizedPNL(value : any){
    self.totalRealizedPNL=value
  },
  setCurrency(value:any){
    self.currency=value
  },
  settotalSecuritiesValuation(value:any){
    self.totalSecuritiesValuation=value
  }
  }))
  .actions((self) => ({

    GetValuesBKB: flow(function * (login: string,contextId:string,accountCode:number,valuationType:number){
      const api= new Api()
      api.setup()
      self.setloadingValues(true)
      yield api.GetValuesBKBApi(login,contextId,accountCode,valuationType).then((response:any)=>{
       
        if(response.status===200){
          self.setloadingValues(false)

          self.setDetailCode(response.detail_code)
          self.setResultCode(response.result_code)
          self.setAccountingDate(response.accountingDate)
          self.setplainInventoryElementList([response.plainInventoryElementList])
          self.setTotalPendingPNL(response.totalPendingPNL)
          self.setTotalRealizedPNL(response.totalRealizedPNL)
          self.setCurrency(response.currency)
          self.settotalSecuritiesValuation(response.totalSecuritiesValuation)
          
        

        }
        else{
          //error
          self.setloadingValues(false)

          self.setDetailCode(response.detail_code)
          self.setResultCode(response.result_code)


        }
        
      })
    }),
  }))

type ValuesType = Instance<typeof ValuesModel>
export interface Values extends ValuesType {}
type ValuesSnapshotType = SnapshotOut<typeof ValuesModel>
export interface ValuesSnapshot extends ValuesSnapshotType {}
export const createValuesDefaultModel = () => types.optional(ValuesModel, {})
