import { flow, Instance, SnapshotOut, types } from "mobx-state-tree"
import { Api } from "../../services/api"

/**
 * Model description here for TypeScript hints.
 */
export const CancelOrderModel = types
  .model("CancelOrder")
  .props({
    resultcode : types.optional(types.number,1111),
    detailcode : types.optional(types.number,1111),
    contextId :types.optional(types.string,""),
    login :types.optional(types.string,"login"),
    accountCode :types.optional(types.number,0),
    orderInternalReference:types.optional(types.string,""),
    issuerCode:types.optional(types.string,""),
    cancelDone:types.optional(types.boolean,true),

  })
  .views((self) => ({
    get GetResultCode(){
      return self.resultcode
    },
    get GetDetailCode(){
      return self.detailcode
    },
    get GetLogin(){
      return self.login
    },
    get GetContextId(){
      return self.contextId
    },
    get GetAccountCode(){
      return self.accountCode
    },
    get GetOrderInternalReference (){
      return self.orderInternalReference
    },
    get GetIssuerCode(){
      return self.issuerCode
    },
    get GetCancelDone(){
      return self.cancelDone
    }
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    setContextId(value : string){
      self.contextId=value
    },
    setResultCode(value : any){
      self.resultcode=value
    },
    setDetailCode(value : any){
      self.detailcode=value
    },
    setLogin(value : string){
      self.login=value
    },
    setAccountCode(value : number){
      self.accountCode=value
    },
    setOrderInternalReference(value : string){
      self.orderInternalReference=value
    },
    setIssuerCode(value : string){
      self.issuerCode=value
    },
    setCancelDone(value : boolean){
      self.cancelDone=value
    }
    
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    CancelOrderFunc : flow(function * (login: string,contextId:string,accountCode:number,orderInternalReference:string,issuerCode:string){
      // self.setLoaingUser(true)
      const api= new Api()
      api.setup()
      yield api.CancelOrderAPI(login,contextId,accountCode,orderInternalReference,issuerCode).then((response:any)=>{
        // self.setLoaingUser(false)
        if(response.status===200){
          self.setDetailCode(response.detail_code)
     
          self.setResultCode(response.result_code)
          self.setCancelDone(true) 
        }
        else{
          //error
          self.setCancelDone(false) 

          self.setDetailCode(response.detail_code)
          self.setResultCode(response.result_code)


        }
        
      })
    }),
  })) // eslint-disable-line @typescript-eslint/no-unused-vars

type CancelOrderType = Instance<typeof CancelOrderModel>
export interface CancelOrder extends CancelOrderType {}
type CancelOrderSnapshotType = SnapshotOut<typeof CancelOrderModel>
export interface CancelOrderSnapshot extends CancelOrderSnapshotType {}
export const createCancelOrderDefaultModel = () => types.optional(CancelOrderModel, {})
