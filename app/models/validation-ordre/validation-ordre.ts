import { flow, Instance, SnapshotOut, types } from "mobx-state-tree"
import { Api } from "../../services/api"

/**
 * Model description here for TypeScript hints.
 */
export const ValidationOrdreModel = types
  .model("ValidationOrdre")
  .props({
    resultcode : types.optional(types.number,1111),
    detailcode : types.optional(types.number,1111),
    detail : types.optional(types.string,""),
  })
  .views((self) => ({
    get GetResultCode(){
      return self.resultcode
    },
    get GetDetailCode(){
      return self.detailcode
    }, 
    get GetDetail(){
      return self.detail
    },
  })) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({
    setResultCode(value : any){
      self.resultcode=value
    },
    setDetailCode(value : any){
      self.detailcode=value
    },
    setDetail(value : string){
      self.detail=value
    },
    
  })) 
  .actions((self) => ({
    ValidateOrdre: flow(function * (login: string,contextId:string,accountCode:number,assetCode:string,quotationPlaceCode:number,marketCode:string,modalityCode : string ,firstLimit : number,quantity : number,validityDate : number ,validityType : number,secondExecutionConditionCode : number,side : number){
      const api= new Api()
      api.setup()
      yield api.ValidateOrdreAPI(login,contextId,accountCode,assetCode,quotationPlaceCode,marketCode,modalityCode,firstLimit,quantity,validityDate,validityType,secondExecutionConditionCode,side).then((response:any)=>{
       
        if(response.status===200){

          self.setDetailCode(response.detail_code)
          self.setResultCode(response.result_code)
          self.setDetail(response.detail)
        
          

        }
        else{
          //error

          self.setDetailCode(response.detail_code)
          self.setResultCode(response.result_code)


        }
        
      })
    }),
  })) 

type ValidationOrdreType = Instance<typeof ValidationOrdreModel>
export interface ValidationOrdre extends ValidationOrdreType {}
type ValidationOrdreSnapshotType = SnapshotOut<typeof ValidationOrdreModel>
export interface ValidationOrdreSnapshot extends ValidationOrdreSnapshotType {}
export const createValidationOrdreDefaultModel = () => types.optional(ValidationOrdreModel, {})
