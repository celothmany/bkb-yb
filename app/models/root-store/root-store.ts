import { Instance, SnapshotOut, types } from "mobx-state-tree"
import { BkbHistoryModel } from "../bkb-history/bkb-history"
import { CancelOrderModel } from "../cancel-order/cancel-order"
import { CarnetOrdreModalModel } from "../carnet-ordre-modal/carnet-ordre-modal"
import { CarnetOrdreModel } from "../carnet-ordre/carnet-ordre"
import { CharacterStoreModel } from "../character-store/character-store"
import { ProfileUserModel } from "../profile-user/profile-user"
import { SingleAssetV1Model } from "../single-asset-v-1/single-asset-v-1"
import { SyntheseModel } from "../synthese/synthese"
import { TitresModel } from "../titres/titres"
import { ValidationOrdreModel } from "../validation-ordre/validation-ordre"
import { ValidityModel } from "../validity/validity"
import { ValuesModel } from "../values/values"

/**
 * A RootStore model.
 */
// prettier-ignore
export const RootStoreModel = types.model("RootStore").props({
  characterStore: types.optional(CharacterStoreModel, {} as any),
  ProfileUserStore : types.optional(ProfileUserModel,{} as any),
  SyntheseStore : types.optional(SyntheseModel,{} as any),
  TitreseStore : types.optional(TitresModel,{} as any),
  ValuesStore : types.optional(ValuesModel,{} as any),
  CarnetOrdreStore : types.optional(CarnetOrdreModel,{} as any),
  CancelOrderStore : types.optional(CancelOrderModel,{} as any),
  CarnetOrdreModalSore : types.optional(CarnetOrdreModalModel,{} as any),
  SingleAssetV1Store : types.optional(SingleAssetV1Model,{} as any),
  ValidateOrdreStore : types.optional(ValidationOrdreModel,{} as any),
  ModalStore : types.optional(ValidityModel,{} as any),
  HistoryStore : types.optional(BkbHistoryModel,{} as any),


}) 

/**
 * The RootStore instance.
 */
export interface RootStore extends Instance<typeof RootStoreModel> {}

/**
 * The data of a RootStore.
 */
export interface RootStoreSnapshot extends SnapshotOut<typeof RootStoreModel> {}
