import { Instance, SnapshotOut, types } from "mobx-state-tree"

/**
 * Model description here for TypeScript hints.
 */
export const BkbHistoryModel = types
  .model("BkbHistory")
  .props({
    accountingDateEnd : types.optional(types.number,0),
    accountingDateStart : types.optional(types.number,0),
    assetCode : types.optional(types.string,"0"),
    assetType : types.optional(types.number,0),
    effectiveDateEnd : types.optional(types.number,0),
    effectiveDateStart : types.optional(types.number,0),
    operationClass : types.optional(types.string,"0"),
    operationSide : types.optional(types.number,0),
    valueDateEnd : types.optional(types.number,0),
    valueDateStart : types.optional(types.number,0),

  })
  .views((self) => ({
    get getAccountingDateEnd(){
        return self.accountingDateEnd
    },
    get getAccountingDateStart(){
      return self.accountingDateStart
  },
  get getAssetCode(){
    return self.assetCode
  },
  get getAssetType(){
    return self.assetType
  },
  get getEffectiveDateEnd () {
      return self.effectiveDateEnd
  },
  get getEffectiveDateStart () {
    return self.effectiveDateStart
 },
 get getOperationClass () {
  return self.operationClass
 },
 get getOperationSide () {
  return self.operationSide
 },
 get getValueDateEnd () {
  return self.valueDateEnd
 },
 get getValueDateStart () {
  return self.valueDateStart
 },


  }))
  .actions((self) => ({
    setAccountingDateEnd(value : number){
      self.accountingDateEnd=value
    },
    setAccountingDateStart(value : number){
      self.accountingDateEnd=value
    },
    setAssetCode(value : string){
      self.assetCode=value
    },
    setAssetType(value : number){
      self.assetType=value
    },
    setEffectiveDateEnd(value : number){
      self.effectiveDateEnd=value
    },
    setEffectiveDateStart(value : number){
      self.effectiveDateStart=value
    },
    setOperationClass(value : string){
      self.operationClass=value
    },
    setOperationSide(value : number){
      self.operationSide=value
    },
    setValueDateEnd(value : number){
      self.valueDateEnd=value
    },
    setValueDateStart(value : number){
      self.valueDateStart=value
    },
    
  })) 
  .actions((self) => ({})) 

type BkbHistoryType = Instance<typeof BkbHistoryModel>
export interface BkbHistory extends BkbHistoryType {}
type BkbHistorySnapshotType = SnapshotOut<typeof BkbHistoryModel>
export interface BkbHistorySnapshot extends BkbHistorySnapshotType {}
export const createBkbHistoryDefaultModel = () => types.optional(BkbHistoryModel, {})
