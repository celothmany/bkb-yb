import { Instance, SnapshotOut, types } from "mobx-state-tree"

/**
 * Model description here for TypeScript hints.
 */
export const ModelFakeModel = types
  .model("ModelFake")
  .props({})
  .views((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars
  .actions((self) => ({})) // eslint-disable-line @typescript-eslint/no-unused-vars

type ModelFakeType = Instance<typeof ModelFakeModel>
export interface ModelFake extends ModelFakeType {}
type ModelFakeSnapshotType = SnapshotOut<typeof ModelFakeModel>
export interface ModelFakeSnapshot extends ModelFakeSnapshotType {}
export const createModelFakeDefaultModel = () => types.optional(ModelFakeModel, {})
