/**
 * The app navigator (formerly "AppNavigator" and "MainNavigator") is used for the primary
 * navigation flows of your app.
 * Generally speaking, it will contain an auth flow (registration, login, forgot password)
 * and a "main" flow which the user will use once logged in.
 */
import React from "react"
import { useColorScheme, ViewStyle } from "react-native"
import { NavigationContainer, DefaultTheme, DarkTheme } from "@react-navigation/native"
import { createNativeStackNavigator } from "@react-navigation/native-stack"
import { SplashScreen ,LoginPageScreen, HomeScreen, PorteFeuilleScreen, PortefeuilleDetailsScreen, TransactionsDetailsScreen, TransactionsScreen, TransactionsAppScreen, BkbProfileScreen, MarchesScreen, IndicesScreen, PalmaresScreen, PublicationsScreen, ActualiteScreen, DevenirClientDeconnecteScreen, ActualiteDeconnecteScreen, PublicationsDeconnecteScreen} from "../screens"
import { navigationRef, useBackButtonHandler } from "./navigation-utilities"
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs"
import { createStackNavigator } from "@react-navigation/stack"
import { WatchListScreen } from "../screens/watch-list/watch-list-screen"
import { NotificationsScreen } from "../screens/notifications/notifications-screen"
import metrics from "../theme/metrics"
import { color } from "../theme"
import { BkbBottomIcon } from "../components"
import { BlurView } from "@react-native-community/blur"
import { HomePageDeconnetceScreen } from "../screens/home-page-deconnetce/home-page-deconnetce-screen"
import { MarcheDeconnecteScreen } from "../screens/marche-deconnecte/marche-deconnecte-screen"
// import { LoginPageScreen } from "../screens/login-page/login-page-screen"

/**
 * This type allows TypeScript to know what routes are defined in this navigator
 * as well as what properties (if any) they might take when navigating to them.
 *
 * If no params are allowed, pass through `undefined`. Generally speaking, we
 * recommend using your MobX-State-Tree store(s) to keep application state
 * rather than passing state through navigation params.
 *
 * For more information, see this documentation:
 *   https://reactnavigation.org/docs/params/
 *   https://reactnavigation.org/docs/typescript#type-checking-the-navigator
 */

//icons & images
const home_focused=require("../../assets/images/home_focused.png")
const home_Infocused=require("../../assets/images/home_Infocused.png")
const portfeuille_focused=require("../../assets/images/portefeuille_focused.png")
const portfeuille_Infocused=require("../../assets/images/portefeuille_Infocused.png")
const transactions_focused=require("../../assets/images/transactions_focused.png")
const transactions_Infocused=require("../../assets/images/transactions_Infocused.png")
const watchList_focused=require("../../assets/images/watchList_focused.png")
const watchList_Infocused=require("../../assets/images/watchList_Infocused.png")
const notification_focused=require("../../assets/images/notification_focused.png")
const notification_Infocused=require("../../assets/images/notification_Infocused.png")

export type NavigatorParamList = {
  
  AppStack:undefined
  BottomBarBKB:undefined
  BottomBarBKBDeconnetce:undefined
  StackMain:undefined
  PortefeuilleMain : undefined
  TransactionMain : undefined
  WatchListmain : undefined
  Homemain : undefined
  Deconnectemain : undefined

  
}

const BottomBarStyle: ViewStyle={
    position: "absolute",
    marginHorizontal:metrics.heightPercentageToDP(1),
    bottom:metrics.heightPercentageToDP(1.5),
    elevation:0,
    borderRadius:metrics.heightPercentageToDP(1),
    height:metrics.heightPercentageToDP(8),
    // backgroundColor:"#ffffff20",
    backgroundColor:"rgba(63,106,145,0.7)",
    borderColor:color.palette.white,
    borderWidth:metrics.heightPercentageToDP(0.01),
    overflow:"hidden"
    
    


}

// Documentation: https://reactnavigation.org/docs/stack-navigator/
const StackRoot = createStackNavigator<NavigatorParamList>()
const RootStack = ()=>{
  return (
    <StackRoot.Navigator
    screenOptions={{
      headerShown: false,
    }}
    >
        <StackRoot.Screen
        name="AppStack"
        component={AppStack}
        options={{
          headerShown: false,
        }}
        />
        <StackRoot.Screen
        name="BottomBarBKB"
        component={BottomBarBKB}
        options={{
          headerShown: false,
        }}
        />
         <StackRoot.Screen
        name="BottomBarBKBDeconnetce"
        component={BottomBarBKBDeconnecte}
        options={{
          headerShown: false,
        }}
        />
        <StackRoot.Screen
        name="StackMain"
        component={AppStackMain}
        options={{
          headerShown: false,
        }}
        />
        <StackRoot.Screen
        name="PortefeuilleMain"
        component={PortefeuilleMainStack}
        options={{
          headerShown: false,
        }}
        />
        <StackRoot.Screen
        name="TransactionMain"
        component={TransactionMainStack}
        options={{
          headerShown: false,
        }}
        />
         <StackRoot.Screen
        name="WatchListmain"
        component={WatchListmainStack}
        options={{
          headerShown: false,
        }}
        />
        <StackRoot.Screen
        name="Homemain"
        component={HomemainStack}
        options={{
          headerShown: false,
        }}
        />
         <StackRoot.Screen
        name="Deconnectemain"
        component={DeconnecteStackMain}
        options={{
          headerShown: false,
        }}
        />

    </StackRoot.Navigator>
  )

}
const Stack = createNativeStackNavigator<any>()

export function AppStack  () {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="splashScreen"
    >
      
      <Stack.Screen name="splashScreen" component={SplashScreen} />
      <Stack.Screen name="loginPage" component={LoginPageScreen} />
      <Stack.Screen name="home" component={HomeScreen} 
      />


    </Stack.Navigator>
    
  )
}

const StackMain = createNativeStackNavigator<any>()

export function AppStackMain  () {
  return (
    <StackMain.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="loginPage"
    >
      
      <StackMain.Screen name="splashScreen" component={SplashScreen} />
      <StackMain.Screen name="loginPage" component={LoginPageScreen} />
      <StackMain.Screen name="home" component={HomeScreen} 
      />


    </StackMain.Navigator>
    
  )
}

//mode deconnecte 
const DeconnecteStack = createNativeStackNavigator<any>()

export function DeconnecteStackMain  () {
  return (
    <DeconnecteStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="homePageDeconnetce"
    >
      
      <DeconnecteStack.Screen name="homePageDeconnetce" component={HomePageDeconnetceScreen} />
      <DeconnecteStack.Screen name="devenirClientDeconnecte" component={DevenirClientDeconnecteScreen} />
      <DeconnecteStack.Screen name="actualiteDeconnecte" component={ActualiteDeconnecteScreen} />
      <DeconnecteStack.Screen name="publicationDeconnecte" component={PublicationsDeconnecteScreen} />
      <DeconnecteStack.Screen name="marcheDeconnecte" component={MarcheDeconnecteScreen} />
      <DeconnecteStack.Screen name="loginPage" component={LoginPageScreen} />



    </DeconnecteStack.Navigator>
    
  )
}

//portefeuille stack
const PortefeuilleMain = createNativeStackNavigator<any>()

export function PortefeuilleMainStack  () {
  return (
    <PortefeuilleMain.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="porte_feuille"
    >
      
      <PortefeuilleMain.Screen name="porte_feuille" component={PorteFeuilleScreen} />
      <PortefeuilleMain.Screen name="porte_feuille_details" component={PortefeuilleDetailsScreen} />



    </PortefeuilleMain.Navigator>
    
  )
}
//transactions stack

const TransactionMain = createNativeStackNavigator<any>()

export function TransactionMainStack  () {
  return (
    <TransactionMain.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="transactions_app"
    >
      
      <TransactionMain.Screen name="transactions_app" component={TransactionsAppScreen} />
      <TransactionMain.Screen name="transactions_details" component={TransactionsDetailsScreen} />


    </TransactionMain.Navigator>
    
  )
}

//watch list stack

const WatchListmain = createNativeStackNavigator<any>()

export function WatchListmainStack  () {
  return (
    <WatchListmain.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="watch_list"
    >
      
      <WatchListmain.Screen name="watch_list" component={WatchListScreen} />


    </WatchListmain.Navigator>
    
  )
}

//watch list stack

const Homemain = createNativeStackNavigator<any>()

export function HomemainStack  () {
  return (
    <Homemain.Navigator
      screenOptions={{
        headerShown: false,
      }}
      initialRouteName="Home"
    >
      
      <Homemain.Screen name="bkbProfile" component={BkbProfileScreen} />
      <Homemain.Screen name="Home" component={HomeScreen} />
      <Homemain.Screen name="marches" component={MarchesScreen} />
      <Homemain.Screen name="indices" component={IndicesScreen} />
      <Homemain.Screen name="palmares" component={PalmaresScreen} />
      <Homemain.Screen name="publications" component={PublicationsScreen} />
      <Homemain.Screen name="actualite" component={ActualiteScreen} />


    </Homemain.Navigator>
    
  )
}



const BKBBottom=createBottomTabNavigator<any>();
export function BottomBarBKB  ()  {
  // const {routeName}=navigation.state;
  return (
    <BKBBottom.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle:BottomBarStyle,
        tabBarBackground : ()=>
        <BlurView
       overlayColor={"transparent"}

       style={{height:metrics.heightPercentageToDP(8),
       borderRadius:metrics.heightPercentageToDP(20),
       marginHorizontal:metrics.heightPercentageToDP(0.1)
       }}      >
         
      </BlurView>
        
        
      }}
      initialRouteName="home"
      
     
      
    >
      
      <BKBBottom.Screen name="home" component={HomemainStack}
      
      options={{
        tabBarShowLabel:false,
        tabBarIcon : ({focused})=>(
          
          <BkbBottomIcon TitleLabel={"Home"} IconFocused={home_focused} IconInFocused={home_Infocused} Focused={focused}/>

        ),
      }}
      />
      <BKBBottom.Screen name="porte_feuille_screen" component={PortefeuilleMainStack}
        options={{
          tabBarShowLabel:false,
          tabBarIcon : ({focused})=>(
            <BkbBottomIcon TitleLabel={"Portefeuille"} IconFocused={portfeuille_focused} IconInFocused={portfeuille_Infocused} Focused={focused}/>
          ),
        }}
      />
      <BKBBottom.Screen name="transactions_screen" component={TransactionMainStack}
        options={{
          tabBarShowLabel:false,
          tabBarIcon : ({focused})=>(
            // <Image source={focused? transactions_focused : transactions_Infocused}/>
            <BkbBottomIcon TitleLabel={"Transactions"} IconFocused={transactions_focused} IconInFocused={transactions_Infocused} Focused={focused}/>
          ),
        }}
      />
      <BKBBottom.Screen name="watch_list_screen" component={WatchListmainStack} 
      options={{
        tabBarShowLabel:false,

        tabBarIcon : ({focused})=>(
          <BkbBottomIcon TitleLabel={"Watch List"} IconFocused={watchList_focused} IconInFocused={watchList_Infocused} Focused={focused}/>

          ),
      }}
      />
      <BKBBottom.Screen name="notifications" component={NotificationsScreen} 
      options={{
        tabBarShowLabel:false,
        tabBarIcon : ({focused})=>(
          <BkbBottomIcon TitleLabel={"Notifications"} IconFocused={notification_focused} IconInFocused={notification_Infocused} Focused={focused}/>
          ),
      }}
      />


    </BKBBottom.Navigator>
  )
}


//bottom bar deconnecte 

const BKBBottomDeconnecte=createBottomTabNavigator<any>();
export function BottomBarBKBDeconnecte  ()  {
  // const {routeName}=navigation.state;
  return (
    <BKBBottom.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle:BottomBarStyle,
        tabBarBackground : ()=>
        <BlurView
       overlayColor={"transparent"}

       style={{height:metrics.heightPercentageToDP(8),
       borderRadius:metrics.heightPercentageToDP(20),
       marginHorizontal:metrics.heightPercentageToDP(0.1)
       }}      >
         
      </BlurView>
        
        
      }}
      initialRouteName="home"
      
     
      
    >
      
      <BKBBottom.Screen name="home" component={HomemainStack}
      
      options={{
        tabBarShowLabel:false,
        tabBarIcon : ({focused})=>(
          
          <BkbBottomIcon TitleLabel={"Home"} IconFocused={home_focused} IconInFocused={home_Infocused} Focused={focused}/>

        ),
      }}
      />
      <BKBBottom.Screen name="porte_feuille_screen" component={PortefeuilleMainStack}
        options={{
          tabBarShowLabel:false,
          tabBarIcon : ({focused})=>(
            <BkbBottomIcon TitleLabel={"Portefeuille"} IconFocused={portfeuille_focused} IconInFocused={portfeuille_Infocused} Focused={focused}/>
          ),
        }}
      />
      <BKBBottom.Screen name="transactions_screen" component={TransactionMainStack}
        options={{
          tabBarShowLabel:false,
          tabBarIcon : ({focused})=>(
            // <Image source={focused? transactions_focused : transactions_Infocused}/>
            <BkbBottomIcon TitleLabel={"Transactions"} IconFocused={transactions_focused} IconInFocused={transactions_Infocused} Focused={focused}/>
          ),
        }}
      />
      <BKBBottom.Screen name="watch_list_screen" component={WatchListmainStack} 
      options={{
        tabBarShowLabel:false,

        tabBarIcon : ({focused})=>(
          <BkbBottomIcon TitleLabel={"Watch List"} IconFocused={watchList_focused} IconInFocused={watchList_Infocused} Focused={focused}/>

          ),
      }}
      />
      <BKBBottom.Screen name="notifications" component={NotificationsScreen} 
      options={{
        tabBarShowLabel:false,
        tabBarIcon : ({focused})=>(
          <BkbBottomIcon TitleLabel={"Deconnecte"} IconFocused={notification_focused} IconInFocused={notification_Infocused} Focused={focused}/>
          ),
      }}
      />


    </BKBBottom.Navigator>
  )
}


interface NavigationProps extends Partial<React.ComponentProps<typeof NavigationContainer>> {}

export const AppNavigator = (props: NavigationProps) => {
  const colorScheme = useColorScheme()
  useBackButtonHandler(canExit)
  return (
    <NavigationContainer
      ref={navigationRef}
      theme={colorScheme === "dark" ? DarkTheme : DefaultTheme}
      {...props}
    >
      <RootStack />
    </NavigationContainer>
  )
}

AppNavigator.displayName = "AppNavigator"

/**
 * A list of routes from which we're allowed to leave the app when
 * the user presses the back button on Android.
 *
 * Anything not on this list will be a standard `back` action in
 * react-navigation.
 *
 * `canExit` is used in ./app/app.tsx in the `useBackButtonHandler` hook.
 */
const exitRoutes = ["splashscreen"]
export const canExit = (routeName: string) => exitRoutes.includes(routeName)
