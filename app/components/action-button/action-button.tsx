import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import { TouchableOpacity } from "react-native-gesture-handler"
import metrics from "../../theme/metrics"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
}

export interface ActionButtonProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  textButton : string
  typeButton? : "Acheter" | "Vendre"
  onPressAction: Function
}

/**
 * Describe your component here
 */
export const ActionButton = observer(function ActionButton(props: ActionButtonProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])

  return (
    <TouchableOpacity  style={[props.typeButton==="Acheter"? BUTTON_STYLE_ACHETER: BUTTON_STYLE_VENDRE ,props.style]} onPress={()=>{props.onPressAction()}}>
      <Text style={TEXT_BUTTON}>{props.textButton}</Text>
    </TouchableOpacity>
  )
})

const BUTTON_STYLE_ACHETER : ViewStyle={
  backgroundColor:color.palette.BKbGreenColor1,
  width:metrics.heightPercentageToDP(10),
  justifyContent:'center',
  alignItems:'center',
  height:metrics.heightPercentageToDP(3.7),
  borderRadius:metrics.heightPercentageToDP(10.5)
}
const BUTTON_STYLE_VENDRE : ViewStyle={
  backgroundColor:color.palette.BKBRedColor1,
  width:metrics.heightPercentageToDP(10),
  justifyContent:'center',
  alignItems:'center',
  height:metrics.heightPercentageToDP(3.7),
  borderRadius:metrics.heightPercentageToDP(10.5)
}
const TEXT_BUTTON : TextStyle={
  fontFamily:'Sora-Bold',
  fontSize:metrics.heightPercentageToDP(1.4),
}