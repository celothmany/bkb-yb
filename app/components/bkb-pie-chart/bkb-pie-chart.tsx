import * as React from "react"
import { Dimensions, StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import { PieChart } from "react-native-chart-kit"
import { Rect, Text as TextSVG, Svg, Circle, Polygon } from "react-native-svg";
import metrics from "../../theme/metrics"
import GlobalFunctions from "../../classes/GlobalFunctions"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
}

export interface BkbPieChartProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
}

/**
 * Describe your component here
 */
export const BkbPieChart = observer(function BkbPieChart(props: BkbPieChartProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])
  const data = [
    {
      name: "Seoul",
      population: 21500000,
      color: "rgba(131, 167, 234, 1)",
      legendFontColor: "#7F7F7F",
      legendFontSize: 15
    },
    {
      name: "Toronto",
      population: 2800000,
      color: "#F00",
      legendFontColor: "#7F7F7F",
      legendFontSize: 15
    },
    {
      name: "Beijing",
      population: 527612,
      color: "red",
      legendFontColor: "#7F7F7F",
      legendFontSize: 15
    },
    {
      name: "New York",
      population: 8538000,
      color: "#ffffff",
      legendFontColor: "#7F7F7F",
      legendFontSize: 15
    },
    {
      name: "Moscow",
      population: 11920000,
      color: "rgb(0, 0, 255)",
      legendFontColor: "#7F7F7F",
      legendFontSize: 15
    }
  ];
  const screenWidth = Dimensions.get("window").width;
  const chartConfig = {
    backgroundGradientFrom: "#1E2923",
    backgroundGradientFromOpacity: 0,
    backgroundGradientTo: "#08130D",
    backgroundGradientToOpacity: 0.5,
    color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
    strokeWidth: 2, // optional, default 3
    barPercentage: 0.5,
    useShadowColorFromDataset: false // optional
  };
  let [tooltipPos, setTooltipPos] = React.useState({ x: 0, y: 0, visible: false, value: 0 })
  const globalFunctions=new GlobalFunctions();
  const Triangle = ({ w = 16, h = 30, direction = 'right', color = '#44a6e8' , x , y  }) => {
    const points = {
      top: [`${w / 2},0`, `0,${h}`, `${w},${h}`],
      right: [`0,0`, `0,${h}`, `${w},${h / 2}`],
      bottom: [`0,0`, `${w},0`, `${w / 2},${h}`],
      left: [`${w},0`, `${w},${h}`, `0,${h / 2}`],
    }
  
    return (
        <Polygon 
        x={x}
        y={y}
        points={points[direction].join(' ')} fill={color} />
      
    )
  }
  return (
    <View>
      <PieChart
      data={data}
      width={screenWidth}
      height={200}
      chartConfig={chartConfig}
      accessor={"population"}
      backgroundColor={"transparent"}
      paddingLeft={"15"}
      // center={[10, 50]}
      // absolute
      
      />
   
    </View>
  )
})
