import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { flatten } from "ramda"
import { BlurView } from "@react-native-community/blur"
import LinearGradient from "react-native-linear-gradient"

const CONTAINER: ViewStyle = {
  // justifyContent: "center",
  backgroundColor:'rgba(255,255,255,0.2)'

}


export interface BkbBlurCardViewProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  styleChildren ?:StyleProp<ViewStyle>
  styleBlur?: StyleProp<ViewStyle>

}

/**
 * Describe your component here
 */
export const BkbBlurCardView : React.FC<BkbBlurCardViewProps> = observer((props)=> {
  const { style } = props
  const styles = flatten([CONTAINER, style])

  return (

      <BlurView
    
   
    overlayColor={"transparent"}
    style={[Blur_Style,props.styleBlur]}

 >
  
  {/* <LinearGradient
   colors={['#ffffff28', '#ffffff28',"#ffffff28","#ffffff28"]}
   // start={{ x: 0.7, y: 0 }}
   // locations={[1,0.88,0]}  

   style={[props.styleChildren,Children_Style,]}> */}
    {/* <View style={[props.styleChildren,Children_Style,props.styleBlur]}> */}
    {props.children}  

    {/* </View> */}

     {/* </LinearGradient> */}

  
    



 </BlurView>

  )
})
//styles 
const Children_Style: ViewStyle={
  // alignItems: 'center', 
  // borderRadius: 5,
  // borderWidth:0.5,
  // borderColor:"white",
  
  // backgroundColor:'#ffffff40'
  

}
const Blur_Style : ViewStyle={
  backgroundColor:'rgba(255,255,255,0.4)'
  // opacity:0.9
}