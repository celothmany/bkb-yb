import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { color } from "../../theme"
import { BkbBlurCardView } from "./bkb-blur-card-view"

storiesOf("BkbBlurCardView", module)
  .addDecorator((fn) => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        <BkbBlurCardView style={{ backgroundColor: color.error }} />
      </UseCase>
    </Story>
  ))
