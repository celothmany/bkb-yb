import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle,Image, Platform, Pressable } from "react-native"
import { observer } from "mobx-react-lite"
import { color } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import { BkbPicker } from "../bkb-picker/bkb-picker"
import { Picker } from "@react-native-picker/picker"
import { BkbButton } from "../bkb-button/bkb-button"
import staticsStrings from "../../localData/staticStrings.json"
import { BkbInput } from "../bkb-input/bkb-input"
import metrics from "../../theme/metrics"
import { BkbTransparentCard } from "../bkb-transparent-card/bkb-transparent-card"
import { useStores } from "../../models"
import { TextInput, TouchableOpacity } from "react-native-gesture-handler"
import DateTimePicker from '@react-native-community/datetimepicker';
import GlobalFunctions from "../../classes/GlobalFunctions"
import { BkbLoading } from "../bkb-loading/bkb-loading"

//images & icons
const date_picer=require("../../../assets/images/date.png")
const x_annuler=require("../../../assets/images/x_annuler.png")

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}


export interface CarnetOrdreModalProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  ValeurOdre: string
  date : number
  AcheterPress:Function
  assetCode : string
  QuotationCodePlace : number
  withQuote :number
  marketCode : string
  side? : number
}

/**
 * Describe your component here
 */
export const CarnetOrdreModal = observer(function CarnetOrdreModal(props: CarnetOrdreModalProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])
  const {CarnetOrdreModalSore,SingleAssetV1Store,ProfileUserStore,ValidateOrdreStore,ModalStore}=useStores();
  const [quantite, setQuantite]=React.useState("");
  const [validite, setValidite]=React.useState("");
  const [validiteList, setValiditeList]=React.useState([]);
  const [date, setDate]=React.useState( new Date());
  const [newDate, setNewDate]=React.useState("");
  const [typeOrdre, settypeOrdre]=React.useState("");
  const [conditions, setConditions]=React.useState("");
  const [firstLimit, setFirstLimit]=React.useState(0);
  const [conditionsExecution, setConditionsExecution]=React.useState("");
  const [show, setShow]=React.useState(false);
  const [orderAccepted, setorderAccepted]=React.useState(false);
  const [orderSteps, setOrderSteps]=React.useState(0);
  const globalFunctions = new GlobalFunctions()
  const onChangeDate=(event , selectedDate)=>{
    const currentDate = selectedDate || date ;
      setShow(Platform.OS==="ios")

    
    
    setDate(currentDate)
    setNewDate(globalFunctions.convertDateToBkbDate(date));
    CarnetOrdreModalSore.setDate(parseInt(newDate))
  }
  const onChangeValues = ()=>{
    //quantity
    CarnetOrdreModalSore.setQuantite(parseInt(quantite))
    //validity
    CarnetOrdreModalSore.setValidity(parseInt(validite))
    //typpe ordre
    CarnetOrdreModalSore.setTypeOrdre(typeOrdre)
    //conditions
    CarnetOrdreModalSore.setCondition(conditions)
    //conditions execution


  }
  const titleModal=(step : number)=>{
      if(step===0){
        return staticsStrings.achat
      }
      if(step===1){
        return staticsStrings.confirmez_ordre
      }
      if(step===2){
        return staticsStrings.order_accepte
      }
      
  }
  const getAssetsFromSignle=async (assetCode,quotationPlaceCode,withQuote)=>{
      await SingleAssetV1Store.GetSignleAssetV1(ProfileUserStore.login,ProfileUserStore.contextId,ProfileUserStore.accountCode,assetCode,quotationPlaceCode,withQuote)
  }
  const newdate = new Date();
  const time = newdate.getHours().toString() +":"+ newdate.getMinutes().toString() ;
  const today = newdate.getDate()+ "/"+newdate.getMonth()+ "/" + newdate.getFullYear();
  CarnetOrdreModalSore.setDate(parseInt(globalFunctions.convertDateToBkbDate(new Date())))
  React.useEffect(   ()=>{
    CarnetOrdreModalSore.setValeur(props.ValeurOdre);
    onChangeValues();
    const getAssets=async ()=>{
      await getAssetsFromSignle(props.assetCode,props.QuotationCodePlace,props.withQuote)
      setValiditeList(SingleAssetV1Store.validityList.validityList)

    }
    getAssets();

  },[])

  return (
    <View>
      {
        !SingleAssetV1Store.loadingSingleAsset ? 
        (
         ( SingleAssetV1Store.resultcode===0 &&  SingleAssetV1Store.detailcode===-1)  ?
        //  modal start 
         
        (<View style={CONTAINER_CARNET}>
          <Pressable onPress={()=>{
            // setEditOrderModal(false)
            ModalStore.setModalToggle(false)
          }} 
          style={{
            right:metrics.widthPercentageToDP(3),
            alignSelf:"flex-end",
            top:metrics.heightPercentageToDP(3)}}>
            <Image  source={x_annuler}/>
            </Pressable>
          <Text style={{alignSelf:"center",fontFamily:'Sora-Bold',fontSize:metrics.heightPercentageToDP(2),margin:metrics.heightPercentageToDP(1),}}>{titleModal(orderSteps) }</Text>
           <View style={ROW_MODAL}>
            <Text style={TEXT_MODAL}>{staticsStrings.valeur}</Text>
    
            {
               orderSteps===0 &&
               (<View style={INPUT_BKB}>
                <Text style={{alignSelf:'flex-start',fontFamily:"Sora-Bold",fontSize:metrics.heightPercentageToDP(1.6)}}>{props.ValeurOdre}</Text>
                </View>)
                
            }
            {
               orderSteps===1 &&
               (<View
                style={INPUT_BKB_EMPTY}
                >
               <Text style={TEXT_EMPTY}>{CarnetOrdreModalSore.valeur}</Text>
                </View>)
                
            }
            {
               orderSteps===2 &&
               (<View
                style={INPUT_BKB_EMPTY}
                >
               <Text style={TEXT_EMPTY}>{CarnetOrdreModalSore.valeur}</Text>
                </View>)
                
            }
    
            </View>
    
            <View style={ROW_MODAL}>
            <Text style={TEXT_MODAL}>{staticsStrings.quantite}</Text>
            <View style={CONTAINER_INPUT}>
            
             
            { orderSteps===0 &&  <BkbInput 
            keyBoardType="numeric"
            inputType={"normal"} 
             
            value={quantite} 
            onChangeText={setQuantite}        
            /> }
             
             
              
              {
                orderSteps===1 && <View
                style={INPUT_BKB_EMPTY}
                >
               <Text style={TEXT_EMPTY}>{CarnetOrdreModalSore.quantite}</Text>
                </View>
              }
              {
                orderSteps===2 && <View
                style={INPUT_BKB_EMPTY}
                >
               <Text style={TEXT_EMPTY}>{CarnetOrdreModalSore.quantite}</Text>
                </View>
              }
    
         
             
            </View>
            </View>
    
            <View style={ROW_MODAL}>
            <Text style={TEXT_MODAL}>{staticsStrings.validite}</Text>
            <View style={CONTAINER_INPUT}>
           
    
             { 
    
             orderSteps===0 &&
             <BkbPicker 
                mode_picker={"dropdown"}
                onFocus={() => {} }
                  onBlur={() => {}} 
                  value={validite}  
                  onChange={(itemvalue)=>{
                    setValidite(itemvalue)
                    CarnetOrdreModalSore.setValidity(parseInt(validite))
                  }}>
                          {
    
                       validiteList.map((maptest,index)=>{
                        return (
                          <Picker.Item  key = {index} color={color.palette.bkbGreyColor} label={globalFunctions.translateValidity(maptest.validityType)} value={maptest.validityType} />
                        );
                    })
    
                    }             
                      
                      </BkbPicker>
                
                }
               
                  
                 { orderSteps===1 &&  <View
               style={INPUT_BKB_EMPTY}
               >
              <Text style={TEXT_EMPTY}>{globalFunctions.translateValidity(CarnetOrdreModalSore.validite) + "  " + today +"  "+ time}</Text>
               </View>
                } 
    
              { orderSteps===2 &&  <View
               style={INPUT_BKB_EMPTY}
               >
              <Text style={TEXT_EMPTY}>{globalFunctions.translateValidity(CarnetOrdreModalSore.validite) + " " + today +" "+ time}</Text>
               </View>
                } 
                
            
            </View>
            </View>
    
            <View style={ROW_MODAL}>
            <Text style={TEXT_MODAL}>{staticsStrings.date}</Text>
           
               {
                 orderSteps===0 &&  <View style={DATE_BKB}>
    
                 <Text style={{color:color.palette.bkbGreyColor}}>{today}</Text>
                 {
                  //  show && <DateTimePicker
                  //  textColor={color.palette.BKBBlueColor1}
                  //  accentColor={color.palette.BKBBlueColor1}
                  //  value={date}
                  //  mode={"date"}
                  //  onChange={onChangeDate}
         
                  //  />
                 }
               {/* <Pressable onPress={()=>{
                 setShow(!show)
                 }}> */}
    
               <Image source={date_picer} />
               {/* </Pressable> */}
               </View>
               }
              
              {
                orderSteps===1 &&
                <View
                style={INPUT_BKB_EMPTY}
                >
               <Text style={TEXT_EMPTY}>{CarnetOrdreModalSore.date}</Text>
                </View>}
                {
                orderSteps===2 &&
                <View
                style={INPUT_BKB_EMPTY}
                >
               <Text style={TEXT_EMPTY}>{CarnetOrdreModalSore.date}</Text>
                </View>}
          
            
            </View>
    
            <View style={ROW_MODAL}>
            <Text style={TEXT_MODAL}>{staticsStrings.type_ordre}</Text>
            <View style={CONTAINER_INPUT}>
                        { 
    
                orderSteps===0 &&
                <BkbPicker 
                  mode_picker={"dropdown"}
                  onFocus={() => {} }
                    onBlur={() => {}} 
                    value={typeOrdre}  
                    onChange={(itemvalue)=>{
                      settypeOrdre(itemvalue.toString())
                      CarnetOrdreModalSore.setTypeOrdre(itemvalue)
    
                    }}>
    
                   {
                        SingleAssetV1Store.modalityList.map((modality,index)=>{
                          return (
                            <Picker.Item key={index}   color={color.palette.bkbGreyColor} label={modality.modalityLabel} value={modality.modalityCode} />
    
                          );
                        })
                      }       
                      
                             </BkbPicker>
                  
                  }
              {         
              orderSteps===1 &&
                 <View
                 style={INPUT_BKB_EMPTY}
                 >
                <Text style={TEXT_EMPTY}>{globalFunctions.translateTypeOrdre(CarnetOrdreModalSore.type_ordre)}</Text>
                 </View>
    
              } 
              {         
              orderSteps===2 &&
                 <View
                 style={INPUT_BKB_EMPTY}
                 >
                <Text style={TEXT_EMPTY}>{globalFunctions.translateTypeOrdre(CarnetOrdreModalSore.type_ordre)}</Text>
                 </View>
    
              } 
              
            </View>
            </View>
    
            <View style={ROW_MODAL}>
            <Text style={TEXT_MODAL}>{staticsStrings.conditions}</Text>
            <View style={CONTAINER_INPUT}>
            
            {/* { orderSteps==0 &&
              <BkbInput inputType={"normal"} 
              keyBoardType={"numeric"}
              value={conditions} 
                onChangeText={setConditions} />
                } */}
    
    
                { 
    
                orderSteps===0 &&
                <BkbPicker 
                  mode_picker={"dropdown"}
                  onFocus={() => {} }
                    onBlur={() => {}} 
                    value={conditions}  
                    onChange={(itemvalue)=>{
                      setConditions(itemvalue)
                      CarnetOrdreModalSore.setCondition(itemvalue)
    
                    }}>

                      {
                        SingleAssetV1Store.conditionList.map((conditionsList,index)=>{
                          return (
                            <Picker.Item   key = {index} color={color.palette.bkbGreyColor} label={conditionsList.conditionExtraDataLabel} value={conditionsList.conditionCode} />
    
                          );
                        })
                      }
                  </BkbPicker>
                  
                  }
    
              { orderSteps==1 &&
                   
                  <View
                 style={INPUT_BKB_EMPTY}
                 >
                <Text style={TEXT_EMPTY}>{globalFunctions.translateConditions(CarnetOrdreModalSore.condition)}</Text>
                 </View>
    
               }
               { orderSteps==2 &&
                   
                   <View
                  style={INPUT_BKB_EMPTY}
                  >
                <Text style={TEXT_EMPTY}>{globalFunctions.translateConditions(CarnetOrdreModalSore.condition)}</Text>
                  </View>
     
                }
            
            </View>
            </View>
            
            <View style={ROW_MODAL}>
            <Text style={TEXT_MODAL}>{staticsStrings.conditions_execution}</Text>
            <View style={CONTAINER_INPUT}>
             
              {
                orderSteps===0 &&
               <BkbPicker 
                mode_picker={"dropdown"}
                onFocus={() => {} }
                  onBlur={() => {}} 
                  value={conditionsExecution}  
                  onChange={(itemvalue)=>{
                    setConditionsExecution(itemvalue)
                    CarnetOrdreModalSore.setConditionExecution(itemvalue)
                  }}>
               {
                        SingleAssetV1Store.conditionList.map((conditionsList,index)=>{
                          return (
                            <Picker.Item key = {index}  color={color.palette.bkbGreyColor} label={conditionsList.conditionExtraDataLabel} value={conditionsList.conditionCode} />
    
                          );
                        })
                      }
                </BkbPicker>
                }
                {
                   orderSteps===1 &&
                  <View
                   style={INPUT_BKB_EMPTY}
                   >
                <Text style={TEXT_EMPTY}>{globalFunctions.translateConditions(CarnetOrdreModalSore.condition_execution)}</Text>
                   </View>
                  }
                  {
                   orderSteps===2 &&
                  <View
                   style={INPUT_BKB_EMPTY}
                   >
                <Text style={TEXT_EMPTY}>{globalFunctions.translateConditions(CarnetOrdreModalSore.condition_execution)}</Text>
                   </View>
                  }
             
            </View>
            </View>
    
            {
              orderSteps===0 &&
              <BkbButton 
            style={{margin:metrics.heightPercentageToDP(1)}}
            ButtonType={"white"} ButtonText={props.side===0 ?staticsStrings.achter : staticsStrings.vendre } onPress={async ()=>{
              props.AcheterPress();
              onChangeValues();
              setOrderSteps(1)
    
            
            }}/>
            }
    
            {
              orderSteps===1 &&
              <View style ={ROW_BUTTONS}>
    
            <BkbButton ButtonType={"white"}
            style={{width:metrics.heightPercentageToDP(20)}}
             ButtonText={staticsStrings.confirmer} onPress={()=>{
              ValidateOrdreStore.ValidateOrdre(ProfileUserStore.login,ProfileUserStore.contextId,
                ProfileUserStore.accountCode,
                props.assetCode,
                props.QuotationCodePlace,
                props.marketCode,
                CarnetOrdreModalSore.type_ordre,
                1,
                CarnetOrdreModalSore.quantite,
                CarnetOrdreModalSore.date,
                CarnetOrdreModalSore.validite,
                1,
                props.side
                )
               if(ValidateOrdreStore.detail==="KO" && ValidateOrdreStore.resultcode===0 && ValidateOrdreStore.detailcode===-1){

                setOrderSteps(2)
                CarnetOrdreModalSore.setMessageError(0)
               }
               
               }}/>
            <BkbButton ButtonType={"transparent"} 
            style={{width:metrics.heightPercentageToDP(20),alignItems:'center'}}
            TextStyle={{textAlign:"center",marginRight:metrics.heightPercentageToDP(2.5)}}
            ButtonText={staticsStrings.annuler} onPress={()=>{setOrderSteps(0)}}/>
            </View>
            }
    
    
            {
              orderSteps===2 &&
                      <BkbButton ButtonType={"white"} ButtonText={staticsStrings.nouvel_ordre} onPress={()=>{
                        
                        setOrderSteps(0)
                      }}/>
    
            }
    
            {/* <BkbTransparentCard   style={GRADIENT_CARD} gradient={true} hexaColorCard={undefined} opacityColorCard={undefined}>
            <Text style={{fontFamily:"Sora-Bold"}}>{staticsStrings.avertissement}</Text>
              <Text style={GRADIENT_TEXT}>{staticsStrings.message_avertissement}</Text>
            </BkbTransparentCard> */}
    
    
        </View>)

        :


        (
          <Text>{"erreur serveur, pas de résultat"}</Text>
        )
        //  modal end 

        )
        :(
          <BkbLoading color={color.palette.white} size={50}/>
        )
      }
    </View>
  )
})


//POP UP style
const CONTAINER_CARNET : ViewStyle={
  // margin:metrics.heightPercentageToDP(2),
  
}

const ROW_MODAL : ViewStyle={
  flexDirection:"row",
  justifyContent:"space-between"
}
const TEXT_MODAL : TextStyle={
  textAlign:"left",
  alignSelf:'center',
  width:metrics.heightPercentageToDP(10),
  fontFamily:"Sora-Bold",
  fontSize:metrics.heightPercentageToDP(1.6),

}
const CONTAINER_INPUT : ViewStyle={
  marginBottom:metrics.heightPercentageToDP(1.5)
}
const INPUT_BKB : ViewStyle={
  width:metrics.heightPercentageToDP(30),
  height:metrics.heightPercentageToDP(6),
  borderRadius:metrics.heightPercentageToDP(5.1),
  borderWidth:metrics.heightPercentageToDP(0),
  borderColor:color.palette.white,
  // backgroundColor:'#ffffff90',
  backgroundColor:'rgba(250,250,250,0.3)',
  // backgroundColor:'rgba(55,93,129,0.7)',
  marginBottom:metrics.heightPercentageToDP(1.5),
  justifyContent:"center",
  alignItems:'center',
  paddingLeft:metrics.heightPercentageToDP(1.5),

  
}
const DATE_BKB : ViewStyle={
  width:metrics.heightPercentageToDP(30),
  height:metrics.heightPercentageToDP(6),
  borderRadius:metrics.heightPercentageToDP(5.1),
  borderWidth:metrics.heightPercentageToDP(0.1),
  borderColor:color.palette.white,
  // backgroundColor:'#ffffff90',
  marginBottom:metrics.heightPercentageToDP(1.5),
  justifyContent:"space-between",
  alignItems:'center',
  paddingHorizontal:metrics.heightPercentageToDP(2),
  flexDirection:'row',

  
}
const GRADIENT_CARD : ViewStyle={
  height:metrics.heightPercentageToDP(15),
  padding:metrics.heightPercentageToDP(2),
  width:metrics.heightPercentageToDP(40),
  marginHorizontal:metrics.heightPercentageToDP(1.6),
}
const GRADIENT_TEXT : TextStyle={
  textAlign:"center",
  fontSize:metrics.heightPercentageToDP(1.69),
  fontFamily:"Sora-Regular"
}

const INPUT_BKB_EMPTY : ViewStyle={
  width:metrics.heightPercentageToDP(30),
  height:metrics.heightPercentageToDP(6),
  borderRadius:metrics.heightPercentageToDP(5.1),
  // borderWidth:metrics.heightPercentageToDP(0.1),
  borderColor:color.palette.white,
  backgroundColor:'transparent',
  marginBottom:metrics.heightPercentageToDP(1.5),
  justifyContent:"center",
  alignItems:'center',
  paddingLeft:metrics.heightPercentageToDP(1.5),

  
}
const TEXT_EMPTY : TextStyle={
  alignSelf:'flex-start',
  fontFamily:"Sora-Regular",
  fontSize:metrics.heightPercentageToDP(1.6),
  paddingTop:metrics.heightPercentageToDP(2),
  color:color.palette.bkbGreyColor
}
//styles of confirmez l'ordre
const ROW_BUTTONS : ViewStyle={
  flexDirection:'row-reverse',
  justifyContent:"space-between"
}