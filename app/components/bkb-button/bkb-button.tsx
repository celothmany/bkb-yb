import * as React from "react"
import { Image, Pressable, StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
// import { Pressable } from "react-native-gesture-handler"
import metrics from "../../theme/metrics"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
}

export interface BkbButtonProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  TextStyle?: StyleProp<TextStyle>

  ButtonType : "white" | "transparent"
  ButtonText : string
  onPress : Function
  Icon ? : any
}

/**
 * Describe your component here
 */
export const BkbButton = observer(function BkbButton(props: BkbButtonProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])

  return (
    <View >
        {
          props.ButtonType==="white" && 
          <Pressable 
          onPress={()=>{props.onPress()}}
          style={[WhiteButton,props.style]}>
            <Text style={[WhiteButtonText,props.TextStyle]}>{props.ButtonText}</Text>
          </Pressable>

        }
        {
          props.ButtonType==="transparent" && 
          <Pressable 
          onPress={()=>{props.onPress()}}

          style={[TransparentButton,props.style]}>
            <View style={{flexDirection:'row-reverse'}}>
            <Text style={[TransparentButtonText,props.TextStyle]} >{props.ButtonText}</Text>
              <Image source={props.Icon}/>
            </View>
          </Pressable>

        }
    </View>
  )
})
//styles of buttons
const BKB_Button : ViewStyle={
  borderRadius:metrics.heightPercentageToDP(3),
  width:metrics.heightPercentageToDP(28),
  height:metrics.heightPercentageToDP(6),
  alignContent:'center',
  alignItems:'center',
  alignSelf:'center',
  padding:metrics.heightPercentageToDP(1.3),
  justifyContent:'center'
  
}
const WhiteButton : ViewStyle={
  ...BKB_Button,
  backgroundColor:color.palette.white

}
const WhiteButtonText : TextStyle={

  color:color.palette.BKBBlue,
  textAlign: 'center',
  fontFamily:"Sora-Bold"



}
const TransparentButton : ViewStyle={
  ...BKB_Button,
  borderWidth:metrics.heightPercentageToDP(0.2),
  borderColor: color.palette.white,
  justifyContent:'center',
  
  
  

}
const TransparentButtonText : TextStyle={

  textAlign: 'center',
  marginLeft:metrics.heightPercentageToDP(3),

}