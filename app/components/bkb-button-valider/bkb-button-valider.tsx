import * as React from "react"
import { Image, StyleProp, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
}

export interface BkbButtonValiderProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  TextStyle?: StyleProp<TextStyle>

  ButtonType : "white" | "transparent"
  ButtonText : string
  onPress : Function
  Icon ? : any
}

/**
 * Describe your component here
 */
export const BkbButtonValider = observer(function BkbButtonValider(props: BkbButtonValiderProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])

  return (
    <View style={styles}>
      {
          props.ButtonType==="white" && 
          <TouchableOpacity 
          onPress={()=>{props.onPress()}}
          style={[WHITE_BUTTON,props.style]}>
            <Text style={[WHITE_BUTTON_TEXT,props.TextStyle]}>{props.ButtonText}</Text>
          </TouchableOpacity>

        }
        {
          props.ButtonType==="transparent" && 
          <TouchableOpacity 
          onPress={()=>{props.onPress()}}

          style={[TRANSPARANT_BUTTON,props.style]}>
            <View style={{flexDirection:'row-reverse'}}>
            <Text style={[TRANSPARANT_BUTTON_TEXT,props.TextStyle]} >{props.ButtonText}</Text>
              <Image source={props.Icon}/>
            </View>
          </TouchableOpacity>

        }
    </View>
  )
})


const BKB_BUTTON : TextStyle={
  padding : metrics.heightPercentageToDP(1.4),
  margin : metrics.heightPercentageToDP(2),
  borderRadius : metrics.heightPercentageToDP(4),
  minWidth : metrics.widthPercentageToDP(40),
}
const WHITE_BUTTON: TextStyle={
  ...BKB_BUTTON,
  backgroundColor : color.palette.BKBBlueColor,
}
const WHITE_BUTTON_TEXT : TextStyle={
  
  textAlign : 'center',

}
const TRANSPARANT_BUTTON : TextStyle = {
  ...BKB_BUTTON,
  backgroundColor : color.palette.BKBBlueColor1,

}
const TRANSPARANT_BUTTON_TEXT : TextStyle = {

  textAlign : 'center'
}