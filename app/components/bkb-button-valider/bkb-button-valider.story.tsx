import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { color } from "../../theme"
import { BkbButtonValider } from "./bkb-button-valider"

storiesOf("BkbButtonValider", module)
  .addDecorator((fn) => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        <BkbButtonValider style={{ backgroundColor: color.error }} />
      </UseCase>
    </Story>
  ))
