import * as React from "react"
import { Image, StyleProp, TextStyle, View, ViewStyle,TextInput } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"
//images & icons 
// const lock=require("../../../assets/images/lock.png")
// const user=require("../../../assets/images/user.png")
// const valide=require("../../../assets/images/valide_login.png")
// const error=require("../../../assets/images/error_login.png")


//
const CONTAINER: ViewStyle = {
  justifyContent: "center",
}



export interface BkbInputProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  placeholder?:any
  inputType: "login" | "normal"
  iconRight ? : any
  iconLeft ? : any
  value : any
  onChangeText: Function
  isPassword ?: boolean
  // onFocus: Function
  // onBlur: Function
  keyBoardType? : string


}

/**
 * Describe your component here
 */
export const BkbInput = observer(function BkbInput(props: BkbInputProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])

  return (
    <View style={styles}>
      {props.inputType==="normal" && 
      <View style={[INPUT_BKB,props.style]}>
        <TextInput
      placeholderTextColor={color.palette.white}
      keyboardType={props.keyBoardType}
      value={props.value}
      placeholder={props.placeholder}
      underlineColorAndroid="transparent"

      style={{color:"white",fontFamily:"Sora-Bold"}}
      onChangeText={(text)=>{
        props.onChangeText(text)

      }}
      />
      <Image source={props.iconRight}/>

      </View>
      }
      {props.inputType==="login"&&
      <View
      style={[LOGIN_BKB,props.style]}
      >
        <Image  source={props.iconLeft}/>
        <TextInput
          secureTextEntry={props.isPassword}
          
          value={props.value}
          placeholder={props.placeholder}
          underlineColorAndroid="transparent"
          style={LOGIN_BKB_INPUT}
          placeholderTextColor={"#ffffff90"}
          onChangeText={(text)=>{
          props.onChangeText(text)
          }}

          />
          <Image source={props.iconRight}/>


      </View>
      }
    </View>
  )
})

//styles of inputs
const INPUT_BKB : ViewStyle={
  width:metrics.heightPercentageToDP(30),
  height:metrics.heightPercentageToDP(6),
  borderRadius:metrics.heightPercentageToDP(5.1),
  borderWidth:metrics.heightPercentageToDP(0.1),
  borderColor:color.palette.white,
  
  
}
const LOGIN_BKB :ViewStyle={
  ...INPUT_BKB,
  borderWidth:metrics.heightPercentageToDP(0.12),
  flexDirection:'row',
  maxWidth:metrics.heightPercentageToDP(37),
  height:metrics.heightPercentageToDP(6),
  alignItems:'center',
  paddingHorizontal:metrics.heightPercentageToDP(1),
  margin:metrics.heightPercentageToDP(1),
  borderColor:color.palette.white



}
const LOGIN_BKB_INPUT : TextStyle={
  width:metrics.heightPercentageToDP(25),
  color:color.palette.white,
  marginLeft:metrics.heightPercentageToDP(0.6),
  fontFamily:"Sora-Bold"
  


}