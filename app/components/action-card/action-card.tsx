import * as React from "react"
import { Image, StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"
import { TouchableOpacity } from "react-native-gesture-handler"
import { ActionButton } from "../action-button/action-button"
import staticsStrings from "../../localData/staticStrings.json"
import LinearGradient from "react-native-linear-gradient"

//images & icons 
const add_watch=require("../../../assets/images/add_watch.png")
const notif_small=require("../../../assets/images/notif_small.png")
const down_action=require("../../../assets/images/down_action.png")
const up_action=require("../../../assets/images/up_action.png")


const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
}

export interface ActionCardProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  idTitre : String
  nameTitre : String
  valueTitre :any
  onPressAddWatch ? : Function
  onPressNotifications ? : Function
  onPressActionAcheter  : Function
  onPressActionVendre  : Function
  percentTitre1:any
  percentTitre2:any
  currency : string




}

/**
 * Describe your component here
 */
export const ActionCard = observer(function ActionCard(props: ActionCardProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])
  const  ParseFloat=(str,val)=> {
    str = str.toString();
    str = str.slice(0, (str.indexOf(".")) + val + 1); 
    return Number(str);   
 
  }
  const colorLatente=(numberLatente)=>{
    if(numberLatente<0){
      return color.palette.BKBRedColor

    }
    else{
      return color.palette.BKbGreenColor1
    }
    
  }

  return (
    // <LinearGradient
    //        colors={['rgba(63,106,145,0.7)','rgba(55,93,129,0.7)','rgba(53,91,126,0.7)']}

    //        >
             <View style={CARD_STYLE}>
        {/* Row 1 */}
        <View style={ROW_TITRE_NAME}>
              {/* Titre Name */}
            <View style={TITRE_NAME_CONTAINER}>
            <Text style={ID_TITRE}>{props.idTitre}</Text>
            <Text style={TITRE_NAME}>{props.nameTitre}</Text>
            </View>

            {/* Notification */}
            <View style={NOTIFICATION_STYLE}>
              <TouchableOpacity onPress={()=>{props.onPressAddWatch()}}>
              <Image   source={add_watch}/>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>{props.onPressNotifications()}}>
              <Image style={{marginTop:metrics.heightPercentageToDP(0.5)}} source={notif_small}/>
              </TouchableOpacity>

            </View>
          
        </View>
        <View style={ACTION_UP_DOWN}>
            <Text style={{
              color:colorLatente(props.valueTitre),
              marginRight:metrics.heightPercentageToDP(1),
              fontFamily:"Sora-Bold",
              fontSize:metrics.heightPercentageToDP(1.7)
              }}>{ParseFloat(props.valueTitre,2)+ " "+props.currency}</Text>
            <Image source={colorLatente(props.valueTitre)===color.palette.BKBRedColor ?   down_action : up_action }/>
          </View>
        {/* Row 2 */}
        <View style={ROW_TITRE_ACTION}>
                  <View style={ACTION_PERCENT}>
                    <Text style={ACTION_PERCENT_TEXT}>{props.percentTitre1 + " "+ props.currency}</Text>
                  </View>
                  <View style={ACTION_PERCENT}>
                    <Text style={ACTION_PERCENT_TEXT}>{ParseFloat(props.percentTitre2,2)+"%"}</Text>
                  </View>
                  <ActionButton   typeButton="Acheter" textButton={staticsStrings.achter} onPressAction={()=>{props.onPressActionAcheter()}}/>
                  <ActionButton  typeButton="Vendre" textButton={staticsStrings.vendre} onPressAction={()=>{props.onPressActionVendre()}}/>

        </View>
    </View>
          //  </LinearGradient>
    
  )
})
//styles of card

const CARD_STYLE : ViewStyle={
  height:metrics.heightPercentageToDP(15),
  // width:metrics.heightPercentageToDP(40),
  flexDirection:'column',

  margin:metrics.heightPercentageToDP(2),
  // backgroundColor:'#ffffff30',
  backgroundColor:"rgba(63,106,145,0.7)",
  borderWidth:metrics.heightPercentageToDP(0.022),
  borderColor:color.palette.white,
  borderRadius:metrics.heightPercentageToDP(1),
  padding:metrics.heightPercentageToDP(1.5)

}
//styles of rows
const ROW_TITRE_NAME : ViewStyle={
  flexDirection:'row',
  justifyContent:'space-between',
}
const ROW_TITRE_ACTION : ViewStyle={
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'flex-end',
    paddingBottom:metrics.heightPercentageToDP(1),
}
const TITRE_NAME_CONTAINER : ViewStyle={
  
}
const NOTIFICATION_STYLE : ViewStyle={
  flexDirection :'row',
  width:metrics.heightPercentageToDP(15),
  justifyContent:'space-between'
}
const ACTION_UP_DOWN : ViewStyle={
  flexDirection:'row',
  alignContent:'center',
  alignItems:'center',

}
const ACTION_PERCENT : ViewStyle={
  backgroundColor:"#FFFFFF35",
  maxWidth:metrics.heightPercentageToDP(20),
  borderRadius:metrics.heightPercentageToDP(2),
  width:metrics.heightPercentageToDP(9.5),
  height:metrics.heightPercentageToDP(2),
}
//styles of textes
const ID_TITRE : TextStyle={
  fontFamily:'Sora-Light',
  color:color.palette.white,
  fontSize:metrics.heightPercentageToDP(1.4)

}
const TITRE_NAME : TextStyle={
  fontFamily:"Sora-Bold",
  fontSize:metrics.heightPercentageToDP(2),
  
}
const ACTION_PERCENT_TEXT : TextStyle={
  fontSize:metrics.heightPercentageToDP(1.6),
  // fontFamily:'Sora-Regular',
  fontWeight:"bold",
  textAlign:'center',
  maxWidth:metrics.heightPercentageToDP(20)

}