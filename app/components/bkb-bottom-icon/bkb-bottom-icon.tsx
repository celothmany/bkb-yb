import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle,Image } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"
import LinearGradient from "react-native-linear-gradient"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
}

export interface BkbBottomIconProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  TitleLabel : string
  IconFocused : any
  IconInFocused : any
  Focused : boolean

}

/**
 * Describe your component here
 */
export const BkbBottomIcon = observer(function BkbBottomIcon(props: BkbBottomIconProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])

  return (
    <View>
      {
        props.Focused ? 
        (
                  <LinearGradient
                  colors={[color.palette.BKBBlue,"transparent"]}
                  start={{x: 0, y: 1}} end={{x: 0, y: 0}}
                      >
            <View style={BottomBarContainer}>

            <Image source={props.IconFocused } style={{ marginTop:metrics.heightPercentageToDP(1)}}/>
            <Text style={Title_Style_Focused}>{props.TitleLabel}</Text>
            <View
            style={Focused_Line_Bottom}
            />

        </View>
                </LinearGradient>
          
        )
        :
        (
        <View style={BottomBarContainer}>
           <Image source={props.IconInFocused } style={{ marginTop:metrics.heightPercentageToDP(1)}}/>
           <Text style={Title_Style_InFocused}>{props.TitleLabel}</Text>
           <View
           style={InFocused_Line_Bottom}
           />

          </View>
        )
      }
    </View>
  )
})

//style of bottom bar 
const BottomBarContainer : ViewStyle={
  height:metrics.heightPercentageToDP(8),
  // borderWidth:1,
  // paddingTop:metrics.heightPercentageToDP(1),
  flexDirection:"column",
  alignContent:"center",
  alignItems:"center",
  justifyContent:"space-between"
  }

//styles of focused
const Title_Style_Focused : TextStyle={
  color: color.palette.white,
  fontSize:metrics.heightPercentageToDP(1.27),
  fontFamily:'Sora-Bold'

  
}
const Focused_Line_Bottom : ViewStyle={
  //  marginTop:metrics.heightPercentageToDP(2.5),
   height:metrics.heightPercentageToDP(0.65),
   width:metrics.heightPercentageToDP(9),
   borderRadius:metrics.heightPercentageToDP(2),
   backgroundColor:color.palette.BKBOffBlue
 }
// styles of in focused
const Title_Style_InFocused : TextStyle={
  color: color.palette.white,
  fontSize:metrics.heightPercentageToDP(1.27),
  fontFamily:'Sora-Regular',

  
}
const InFocused_Line_Bottom : ViewStyle={
  //  marginTop:metrics.heightPercentageToDP(2.5),
   height:metrics.heightPercentageToDP(0.65),
   width:metrics.heightPercentageToDP(9),
   borderRadius:metrics.heightPercentageToDP(2),
   backgroundColor:"transparent"
 }