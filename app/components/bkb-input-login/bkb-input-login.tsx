import * as React from "react"
import { Image, StyleProp, TextInput, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
}

export interface BkbInputLoginProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>

  placeholder?: any
  inputType: "login" | "normal"
  iconRight?: any
  iconLeft?: any
  value: any
  onChangeText: Function
}

/**
 * Describe your component here
 */
export const BkbInputLogin = observer(function BkbInputLogin(props: BkbInputLoginProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])

  return (
    <View style={styles}>
      <View
        style={LOGIN_BKB}
      >
        <Image source={props.iconLeft} />
        <TextInput
          placeholder={props.placeholder}
          style={LOGIN_BKB_INPUT}
          placeholderTextColor={color.palette.white}
        />
        <Image  source={props.iconRight}/>
      </View>

    </View>
  )
})


const LOGIN_BKB: ViewStyle = {
  borderWidth: metrics.heightPercentageToDP(0.12),
  flexDirection: 'row',
  maxWidth: metrics.heightPercentageToDP(37),
  height: metrics.heightPercentageToDP(6),
  alignItems: 'center',
  paddingHorizontal: metrics.heightPercentageToDP(1),
  margin: metrics.heightPercentageToDP(1),
  borderColor: color.palette.white,
  borderRadius: metrics.widthPercentageToDP(5)


}


const LOGIN_BKB_INPUT: TextStyle = {

  borderColor: color.palette.white,
  width: metrics.heightPercentageToDP(23),
  color: color.palette.white,
  marginLeft: metrics.heightPercentageToDP(0.6),
}

