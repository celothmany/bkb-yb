import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"
import { BlurView } from "@react-native-community/blur";
import LinearGradient from "react-native-linear-gradient"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
}

export interface BkbTransparentCardProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  hexaColorCard:any
  opacityColorCard:any
  gradient? : boolean
  gradientColor? : any
}

/**
 * Describe your component here
 */
export const BkbTransparentCard  : React.FC<BkbTransparentCardProps>= observer((props)=> {
  const { style } = props
  const styles = flatten([CONTAINER, style])
  const hexToRgbA = (hex, opacity) => {
    let c;
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
      c = hex.substring(1).split('');
      if (c.length === 3) {
        c = [c[0], c[0], c[1], c[1], c[2], c[2]];
      }
      c = `0x${c.join('')}`;
      return `rgba(${[(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',')},${opacity})`;
    }
    throw new Error('Bad Hex');
  };
  return (
    
     
      <View>
        {
          props.gradient  ?  (<LinearGradient 
            colors={[props.gradientColor,"transparent"]}
            start={{x: 0, y: 0}} end={{x: 1, y: 0}}
      
            style={[TRANSPARENT_BACKGROUND,props.style,
              // #FB8A8A {backgroundColor:hexToRgbA(props.hexaColorCard,props.opacityColorCard)}
          ]}>
            {props.children}
          </LinearGradient>):
          (
            <View
            style={[TRANSPARENT_BACKGROUND,props.style,
              // {backgroundColor:hexToRgbA(props.hexaColorCard,props.opacityColorCard)}
              // {backgroundColor:"rgba(255, 255, 255, 0.3)",}
          ]}>
            {props.children}
              </View>
          )
        }
      </View>

  )
})

//styles of background
const TRANSPARENT_BACKGROUND : ViewStyle={
    borderWidth:metrics.heightPercentageToDP(0.01),
    alignContent:'center',
    alignItems:"center",
    height:metrics.heightPercentageToDP(43),
    marginHorizontal:metrics.heightPercentageToDP(4),
    borderRadius:metrics.heightPercentageToDP(1),
    borderColor:color.palette.white,
    // backgroundColor: '#ffffff20',
    backgroundColor:'rgba(63,106,145,0.7)'
   
    
}