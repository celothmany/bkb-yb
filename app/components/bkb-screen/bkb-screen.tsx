import * as React from "react"
import { ImageBackground, ImageStyle, StyleProp, TextStyle, View, ViewStyle,Image, Platform, Dimensions } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"
import { FC } from "react"
import { TouchableOpacity } from "react-native-gesture-handler"
import moment from "moment"
import staticsStrings from "../../localData/staticStrings.json"
import { useNavigation } from "@react-navigation/native"
import { useStores } from "../../models"
import LinearGradient from "react-native-linear-gradient"
import UserInactivity from "react-native-user-inactivity"
import BackgroundTimer from 'react-native-user-inactivity/lib/BackgroundTimer';;


//images & icons 
const backgroundImage=require("../../../assets/images/back_ground_image.png")
const bkb_logo=require("../../../assets/images/bkb_logo.png")
const back=require("../../../assets/images/icon_arrow_left.png")
const search=require("../../../assets/images/search.png")


//
const CONTAINER: ViewStyle = {
  justifyContent: "center",
}



export interface BkbScreenProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  styleParent?:StyleProp<ViewStyle>
  headerShow ?:boolean
  navigation ?:any
  onPressSearch ? : Function
  goBackButton ? : boolean 
}

/**
 * Describe your component here
 */
export const BkbScreen :FC<BkbScreenProps>= observer((props)=> {
  const { style } = props
  const [timeNow,setTimeNow]=React.useState("");
  const [seance,setSeance]=React.useState(false);
  const {ProfileUserStore}=useStores()
  const styles = flatten([CONTAINER, style])
  const hexToRgbA = (hex, opacity) => {
    let c;
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
      c = hex.substring(1).split('');
      if (c.length === 3) {
        c = [c[0], c[0], c[1], c[1], c[2], c[2]];
      }
      c = `0x${c.join('')}`;
      return `rgba(${[(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',')},${opacity})`;
    }
    throw new Error('Bad Hex');
  };
  //get the date 

  const getDateString=(date) =>{
    var hours = new Date().getHours(); //To get the Current Hours
    var min = new Date().getMinutes(); //To get the Current Minutes
    var sec = new Date().getSeconds(); //To get the Current Seconds
    var currentTime = moment().utcOffset('+00:00').format(' hh:mm ');

    if (Platform.OS === 'ios')
        return date.toLocaleDateString('fr-FR', {
            weekday: 'short',
            day: 'numeric',
            month: 'long',
            year: 'numeric',
        });
    else {

        var
            monthName = ["Janvier", "Février", "Mars", "Avril","Mai", "Juin", "Juillet","Août", "Septembre", "Octobre", "Novembre", "Décembre"],
            utc = date.getTime() + date.getTimezoneOffset() * 60000,
            US_time = utc + (3600000 * -4),
            US_date = new Date(US_time);
            // setTimeNow(monthName[US_date.getMonth()] +
            // " " + US_date.getDate() + " - " + currentTime)

        return   US_date.getDate() + " "+ monthName[US_date.getMonth()]  + " - " + currentTime;
    }
}
  const seanceOuverte=()=>{
    var startTime = moment(staticsStrings.seance_start, 'HH:mm:ss a');
    // var endTime = staticsStrings.ramadan==="non" ? moment('04:00:00 pm', 'HH:mm:ss a') : moment('02:00:00 pm', 'HH:mm:ss a') ;
    var endTime = moment(staticsStrings.seance_end,'HH:mm:ss a')
    var date = moment().utcOffset('+00:00').format('hh:mm:ss a'); 
    var currnetTime= moment(date,'HH:mm:ss a');
    var dayOfweek=new Date().getDay()


   

    if(currnetTime>=startTime && currnetTime<=endTime  && (dayOfweek>=1 && dayOfweek<=5 )){
      return color.palette.BKBGreenColor
      //seance ouverte 
    }else{
      //seance fermé 
      return color.palette.BKBRedColor1
   

    }
    
    
  }
  // Pull in navigation via hook
  const navigation = useNavigation()
  const LogOutAutomaticAfterMin=()=>{
    if(ProfileUserStore.isLogged===true){
      setTimeout(()=>{
        navigation.reset({
          index : 0,
          routes : [{name :"StackMain"}]
        })
    },900000);
    //navigate to login page after 15 min 
    ProfileUserStore.setIsLogged(false)
    }else{

    }
  }
 

  React.useEffect(()=>{
    // LogOutAutomaticAfterMin()
  },[])
  return (
    // <UserInactivity
    // timeForInactivity={900000}
    //         timeoutHandler={BackgroundTimer}
    //         onAction={() => { 
    //           // navigation.reset({
    //           //   index : 0,
    //           //   routes : [{name :"StackMain"}]
    //           // })
    //           ; }}
    // >
    <ImageBackground
    source={backgroundImage}
    style={[BACKGROUNDSCREEN,props.style]}
    >
      {
        props.headerShow ? 
        (
          
          //  <LinearGradient
          //  colors={['rgba(63,106,145,0.7)','rgba(55,93,129,0.7)','rgba(53,91,126,0.7)']}

          //  >
                            <View>

              <View style={[HEADER_CONTAINER_PARENT,props.styleParent]}>
              <View style={HEADER_CONTAINER}>

              {
              props.goBackButton ?
              ( <TouchableOpacity 

              onPress={()=>{props.navigation.goBack(null)}}
              style={{ 
              alignSelf:'center',
              marginTop:metrics.heightPercentageToDP(3),
              borderWidth:metrics.heightPercentageToDP(0.3),  
              borderColor:hexToRgbA("#ffffff",0.25),
              height:metrics.heightPercentageToDP(4.5),
              width:metrics.heightPercentageToDP(4.5),
              alignItems:'center',
              alignContent:'center',
              justifyContent:'center',
              borderRadius:metrics.heightPercentageToDP(1),}}>
              <Image  source={back}/>

              </TouchableOpacity>)
              :(
                <View style={
                  {
                    alignSelf:'center',
              marginTop:metrics.heightPercentageToDP(3),
              borderWidth:metrics.heightPercentageToDP(0),  
              borderColor:hexToRgbA("#ffffff",0.25),
              height:metrics.heightPercentageToDP(4.5),
              width:metrics.heightPercentageToDP(4.5),
              alignItems:'center',
              alignContent:'center',
              justifyContent:'center',
              borderRadius:metrics.heightPercentageToDP(1),
                  }
                }/>
              )
              }

              <Image style={LOGO_BKB} source={bkb_logo}/>
              <TouchableOpacity
              onPress={()=>{props.onPressSearch(),seanceOuverte()}}
              style={{
              marginTop:metrics.heightPercentageToDP(3),

              }}>
              <Image source={search} />
              </TouchableOpacity>

              </View>
              <View style={TIME_DATE_ROW}>
              <View style={SEANCE_STYLE}>
              <View style={[CIRCLE,{backgroundColor:seanceOuverte()}]}/>
              <Text style={SEANCE_TEXT_STYLE}>{seanceOuverte()===color.palette.BKBGreenColor? staticsStrings.seance + " "+ staticsStrings.ouverte : staticsStrings.seance + " "+ staticsStrings.ferme}</Text>
              </View>
              <Text style={TIME_DATE_ROW_STYLE}>{getDateString(new Date())}</Text>

              </View>
              </View>
              </View>
          //  </LinearGradient>
        )
        :
        (
        <View style={No_HEADER_CONTAINER}>
          <Image style={LOGO_BKB} source={bkb_logo}/>
  
        </View>
        )
      }
      {props.children}
    </ImageBackground>
    // </UserInactivity>
  )
})
//style of back ground image
const BACKGROUNDSCREEN : ImageStyle={
  // height:metrics.heightPercentageToDP(100),
  // width:metrics.widthPercentageToDP(100),
  width:Dimensions.get("window").width,
  height:Dimensions.get("window").height

}

//styles of header
const HEADER_CONTAINER : ViewStyle={
  alignContent:"center",
  alignSelf:"center",
  padding:metrics.heightPercentageToDP(1),
  flexDirection:'row',
  justifyContent:"space-between",
  alignItems:'center',
  width:metrics.heightPercentageToDP(49),
  // height:metrics.heightPercentageToDP(10),
  // backgroundColor:'#ffffff20'
  

}
const HEADER_CONTAINER_PARENT : ViewStyle={
  // backgroundColor:'rgba(59,101,140,0.7)',
  backgroundColor:'rgba(63,106,145,0.7)'
  
}
const No_HEADER_CONTAINER : ViewStyle={
  alignContent:"center",
  alignSelf:"center",
  padding:metrics.heightPercentageToDP(1),
  flexDirection:'row',
  justifyContent:'center',
  alignItems:'center',

}
const LOGO_BKB : ImageStyle={
  alignSelf:'center',
  marginTop:metrics.heightPercentageToDP(3),
}
const TIME_DATE_ROW : ViewStyle={

  flexDirection:"row",
  justifyContent:"space-between",
  marginHorizontal:metrics.heightPercentageToDP(2),
}
const CIRCLE : ViewStyle={
   height:metrics.heightPercentageToDP(1.3),
   width:metrics.heightPercentageToDP(1.3),
   borderRadius:metrics.heightPercentageToDP(1.3/2),
   backgroundColor:color.palette.BKBRedColor1,
   marginRight:metrics.heightPercentageToDP(1),

}
const SEANCE_STYLE: ViewStyle=
{flexDirection:"row",justifyContent:'center',alignItems:'center'}
//styles of textes

const SEANCE_TEXT_STYLE: TextStyle={
    fontFamily:'Sora-Medium',
    fontSize:metrics.heightPercentageToDP(1.6),
}


const TIME_DATE_ROW_STYLE : TextStyle={
  fontFamily:'Sora-Medium',
  fontSize:metrics.heightPercentageToDP(1.6),
}
