import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"
import LinearGradient from "react-native-linear-gradient"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
}

export interface BkbTransparantViewProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
}

/**
 * Describe your component here
 */
  export const BkbTransparantView  : React.FC<BkbTransparantViewProps>= observer((props)=> {
  const { style } = props
  const styles = flatten([CONTAINER, style])
console.log(props.children);

  return (
    <View 
      style={[TRANSPARENT_BACKGROUND, props.style]}
      
    >{props.children}
      </View>
  )
})

const TRANSPARENT_BACKGROUND : ViewStyle={
  borderWidth:metrics.heightPercentageToDP(0.01),
  alignContent:'center',
  alignItems:"center",
  height:metrics.heightPercentageToDP(43),
  marginHorizontal:metrics.heightPercentageToDP(4),
  borderRadius:metrics.heightPercentageToDP(1),
  borderColor:color.palette.white,
  backgroundColor:'rgba(63,106,145,0.7)'
  
}