import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle,Image } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"

//images & icons
const publication=require("../../../assets/images/publication.png")

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}



export interface BkbPublicationCardProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  dateTime : string 
  asselabel : string
}

/**
 * Describe your component here
 */
export const BkbPublicationCard = observer(function BkbPublicationCard(props: BkbPublicationCardProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])

  return (
    <View style={CARD_STYLE}>
      <View>
      <Text style={{fontFamily:"Sora-Medium",fontSize:metrics.heightPercentageToDP(1.4)}}>{props.dateTime}</Text>
      <Text style={{fontFamily:"Sora-Bold",fontSize:metrics.heightPercentageToDP(1.5)}} >{props.asselabel}</Text>
      </View>
      <Image  source={publication} style={{alignSelf:'center'}}/>
    </View>
  )
})


//styles
const CARD_STYLE : ViewStyle={
  flexDirection:'row',
  justifyContent:"space-between",
  backgroundColor:"rgba(63,106,145,0.6)",
  marginHorizontal:metrics.heightPercentageToDP(2),
  padding:metrics.heightPercentageToDP(1),
  borderRadius:metrics.heightPercentageToDP(1),
  marginVertical:metrics.heightPercentageToDP(1),


}