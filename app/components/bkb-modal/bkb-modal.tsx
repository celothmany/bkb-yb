import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import Modal from "react-native-modal";
import metrics from "../../theme/metrics"
import { BkbBlurCardView } from "../bkb-blur-card-view/bkb-blur-card-view"
import LinearGradient from "react-native-linear-gradient"

const CONTAINER: ViewStyle = {
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
}

export interface BkbModalProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  isVisibleState : boolean
}

/**
 * Describe your component here
 */
export const BkbModal: React.FC<BkbModalProps> = observer((props)=> {
  const { style } = props
  const styles = flatten([CONTAINER, style])

  return (
      <Modal 
      
      style={{justifyContent:"center"}}

    backdropColor={color.palette.BKbBackDropColor}
    isVisible={props.isVisibleState}

    >
      <LinearGradient
           colors={['#ffffff60',"#ffffff10","rgba(63,106,145,0.99)"]}
          style={[props.style,POPUP_CONTAINER]} 
          locations={[0, 0.5, 1]} 
          useAngle={true} angle={35} angleCenter={{ x: 0.5, y: 0.5 }}
           >
             {props.children}
           </LinearGradient>
      
    </Modal>
 
  )
})
const POPUP_CONTAINER : ViewStyle={
  paddingVertical:metrics.heightPercentageToDP(2),
  width: metrics.widthPercentageToDP(85),
  maxHeight: metrics.heightPercentageToDP(90),
  alignSelf: 'center',
  marginTop: metrics.heightPercentageToDP(25.5),
  // backgroundColor: "#ffffff30",
  backgroundColor:'rgba(63,106,145,0.99)',
  borderRadius: metrics.heightPercentageToDP(2),
  
  // alignContent:'center',
  // alignItems:'center',
}
