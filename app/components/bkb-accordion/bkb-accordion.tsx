import * as React from "react"
import { StyleProp, TextStyle, TouchableOpacity, View, ViewStyle,Image, LayoutAnimation, UIManager, Platform } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import LinearGradient from "react-native-linear-gradient"
import metrics from "../../theme/metrics"

//images & icons
const drop_right=require("../../../assets/images/drop_right.png")
const drop_down=require("../../../assets/images/drop_down.png")

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}


export interface BkbAccordionProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  title : string 
  icon : any
}

/**
 * Describe your component here
 */
export const BkbAccordion  : React.FC<BkbAccordionProps>= observer((props)=> {
  const { style } = props
  const styles = flatten([CONTAINER, style])
  const [expanded, setExpanded] = React.useState(false);
  const handlePress = () => {setExpanded(!expanded);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);

  };
  React.useEffect(()=>{
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental(true);
  }
  },[])
  return (
    
    <View>
      <TouchableOpacity   onPress={()=>{
                          handlePress()}}>
        <LinearGradient
                        colors={['rgba(63,106,145,0.99)',"rgba(63,106,145,0.99)","#04CAFC53"]}
                        locations={[0, 0.5, 1]} 
                        style={expanded ?  ROW_EXPANDED : ROW_UNEXPANDED}
                        useAngle={true} angle={35} angleCenter={{ x: 0.5, y: 0.5 }}
                        >
                          

                          <View style={ICON_CONTAINER}>
                          <Image  source={props.icon}/>

                          <Text style={TITLE_STYLE} >{props.title}</Text>
                          </View>

                          <Image source={expanded ?drop_down : drop_right } />
                        
        </LinearGradient>
        </TouchableOpacity>
        
        <LinearGradient
                        colors={['rgba(63,106,145,0.99)',"rgba(63,106,145,0.99)","#04CAFC95"]}
                        locations={[0, 0.5, 1]} 
                        useAngle={true} angle={35} angleCenter={{ x: 0.5, y: 0.5 }}
                        style={parentHr}
                        >
                        <View >
                          {
                            expanded && 
                            <View  
                            // style={child}
                            >
                              {props.children}
                            
                              </View>
                          }
                        </View>
            </LinearGradient>
    </View>
  )
})


const ROW_UNEXPANDED : ViewStyle={

    flexDirection: 'row',
    justifyContent:'space-between',
    height:metrics.heightPercentageToDP(8),
    paddingLeft:metrics.heightPercentageToDP(2),
    paddingRight:metrics.heightPercentageToDP(2),
    alignItems:'center',
    backgroundColor: "rgba(63,106,145,0.99)",
    margin:metrics.heightPercentageToDP(1),
    borderRadius:metrics.heightPercentageToDP(1),

}
const ROW_EXPANDED : ViewStyle={

  flexDirection: 'row',
  justifyContent:'space-between',
  height:metrics.heightPercentageToDP(8),
  paddingLeft:metrics.heightPercentageToDP(2),
  paddingRight:metrics.heightPercentageToDP(2),
  alignItems:'center',
  backgroundColor: "rgba(63,106,145,0.99)",
  marginHorizontal:metrics.heightPercentageToDP(1),
  // borderRadius:metrics.heightPercentageToDP(1),
  borderTopRightRadius:metrics.heightPercentageToDP(1),
  borderTopLeftRadius:metrics.heightPercentageToDP(1)

}

const parentHr : ViewStyle={
  // width:'100%',
  marginHorizontal:metrics.heightPercentageToDP(1),
  borderBottomRightRadius:metrics.heightPercentageToDP(1),
  borderBottomLeftRadius:metrics.heightPercentageToDP(1)


}
const child : ViewStyle={
  backgroundColor:"green",
  padding:16,
}
const ICON_CONTAINER : ViewStyle={
  flexDirection:"row",
}
const TITLE_STYLE : TextStyle={
  alignSelf:'center',
  marginLeft:metrics.heightPercentageToDP(2),
  fontFamily:"Sora-Bold"

}