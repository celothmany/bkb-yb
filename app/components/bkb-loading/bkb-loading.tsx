import * as React from "react"
import { ActivityIndicator, StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
}

export interface BkbLoadingProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  size? : number
  color? : any
}

/**
 * Describe your component here
 */
export const BkbLoading = observer(function BkbLoading(props: BkbLoadingProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])

  return (
    <View style={[LOADING,props.style]}>
          <ActivityIndicator color={props.color} size={props.size} />
    </View>
  )
})
const LOADING : ViewStyle={
  position:"absolute",alignSelf:'center',
  top:metrics.heightPercentageToDP(8)
}