import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle,Image, TouchableHighlight } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import { TouchableOpacity } from "react-native-gesture-handler"
import metrics from "../../theme/metrics"
import staticsStrings from "../../localData/staticStrings.json"
import Swipeable from 'react-native-gesture-handler/Swipeable';
import LinearGradient from "react-native-linear-gradient"
import { useStores } from "../../models"

//icons & images
const pto_watch=require("../../../assets/images/pto_watch.png")
const watch_list_min=require("../../../assets/images/watch_list_min.png")

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}


export interface WatchListCardProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  assetCode : any 
  assetLabel : any 
  value : any 
  up_down : string
  pto_show : boolean
  onPressWatchMin: Function
}

/**
 * Describe your component here
 */
export const WatchListCard = observer(function WatchListCard(props: WatchListCardProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])
  const [showEdit,setShowEdit]=React.useState(true);
  const [widthCard,setWidthCard]=React.useState(40);
  const {ModalStore}=useStores();
  const upDown =(num : string)=>{
    if (num.substring(0,1)==="+"){
      return color.palette.BKbGreenColor1
    }
    if (num.substring(0,1)==="-"){
      return color.palette.BKBRedColor1
    }
  }
  const hexToRgbA = (hex, opacity) => {
    let c;
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
      c = hex.substring(1).split('');
      if (c.length === 3) {
        c = [c[0], c[0], c[1], c[1], c[2], c[2]];
      }
      c = `0x${c.join('')}`;
      return `rgba(${[(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',')},${opacity})`;
    }
    throw new Error('Bad Hex');
  };
  const renderLeftActions = (progress, dragX) => {
    const trans = dragX.interpolate({
      inputRange: [0, 50, 100, 101],
      outputRange: [-20, 0, 0, 1],
    });

    React.useEffect(()=>{
      setShowEdit(ModalStore.editerWatchList)
    },[])
    return (
      <TouchableHighlight onPress={()=>{props.onPressWatchMin()}} style={{
        
        // backgroundColor:hexToRgbA("#ffffff",0.25),
        backgroundColor:'rgba(63,106,145,0.7)',
        height:metrics.heightPercentageToDP(5),
        width:metrics.heightPercentageToDP(5),
        alignItems:'center',
        alignContent:'center',
        justifyContent:'center',
        borderRadius:metrics.heightPercentageToDP(1),
        alignSelf:'center',
        margin:metrics.heightPercentageToDP(0.3),
        marginRight:metrics.heightPercentageToDP(3),
        marginLeft:metrics.heightPercentageToDP(3),
        marginTop:metrics.heightPercentageToDP(2.5),

        
      }}>
        <Image style={{alignSelf:'center'}} source={watch_list_min}/>
      </TouchableHighlight>
    );
  };
  return (
    // <Swipeable
    // renderLeftActions={renderLeftActions}
    // >

    
    <View
    style={ROW_CARD}
    >
      {
        showEdit && <LinearGradient
        colors={['#ffffff60',"#ffffff10","rgba(63,106,145,0.99)"]}
        locations={[0, 0.5, 1]} 
        useAngle={true} angle={35} angleCenter={{ x: 0.5, y: 0.5 }}          
        style={[EDITER,{}]}
           
               >
       <TouchableOpacity
       onPress={()=>{props.onPressWatchMin();
         setWidthCard(46);setShowEdit(false)
       }}
       >
         <Image  source={watch_list_min}/>
       </TouchableOpacity>
       </LinearGradient>
      }


      <View style={[CARD_STYLE,{width:metrics.heightPercentageToDP(widthCard)}]}>
      <Text style={assetCodeStyle} >{props.assetCode}</Text>
      <View style={ROW_1}>
      <Text style={assetLabelStyle}>{props.assetLabel}</Text>
      <Text style={assetLabelStyle}>{props.value}</Text>

      <View style={UP_DOWN_STYLE}>
        <Text style={[UP_DOWN_TEXT,{color:upDown(props.up_down)}]}>{props.up_down}</Text>

      </View>
      </View>
     {
       props.pto_show===true &&  
       <View  style={ROW_2}>
       <Image source={pto_watch}/>
       <Text style={PTO_STYLE}>{staticsStrings.pto}</Text>
   </View>
     }
    </View>
    </View>


    // </Swipeable>
  )
})
//styles 

const ROW_CARD : ViewStyle={
  flexDirection:"row",
}
const CARD_STYLE : ViewStyle={
  maxHeight:metrics.heightPercentageToDP(30),
  // width:metrics.heightPercentageToDP(40),
  flexDirection:'column',

  margin:metrics.heightPercentageToDP(1),
  // backgroundColor:'#ffffff30',
  backgroundColor:"rgba(63,106,145,0.55)",
  borderWidth:metrics.heightPercentageToDP(0.022),
  borderColor:color.palette.white,
  borderRadius:metrics.heightPercentageToDP(1),
  padding:metrics.heightPercentageToDP(1.5)

}
const assetCodeStyle : TextStyle={
  fontFamily:'Sora-Bold',
  fontSize:metrics.heightPercentageToDP(1.4),
}
const ROW_1 : ViewStyle={
  flexDirection:'row',
  justifyContent:'space-between',
}
const assetLabelStyle : TextStyle={
  fontFamily:'Sora-Bold'
}
const UP_DOWN_STYLE : ViewStyle={
  backgroundColor:color.palette.white,
  maxWidth:metrics.heightPercentageToDP(20),
  width:metrics.heightPercentageToDP(7),
  justifyContent:"center",
  alignContent:'center',
  alignItems:'center',
  borderRadius:metrics.heightPercentageToDP(1.6),
  padding:metrics.heightPercentageToDP(0.1),
}
const UP_DOWN_TEXT : TextStyle={
  color:color.palette.BKbGreenColor1,
  fontFamily:"Sora-Bold",
  fontSize:metrics.heightPercentageToDP(1.5),
  
}

const ROW_2 : ViewStyle={
  flexDirection:'row',
  // justifyContent:'space-between',
  justifyContent: 'center',
  alignSelf:'flex-start',
  alignContent:'center',
  alignItems:'center',
  marginTop:metrics.heightPercentageToDP(0.3),

}
const PTO_STYLE : TextStyle={
  color:color.palette.BKBBlueColor,
  fontFamily:"Sora-Bold",
  fontSize:metrics.heightPercentageToDP(1.3),
  marginLeft:metrics.heightPercentageToDP(0.5),
}

const EDITER : ViewStyle={
  // backgroundColor:hexToRgbA("#ffffff",0.25),
  backgroundColor:'rrgba(63,106,145,0.55)',
  height:metrics.heightPercentageToDP(5),
  width:metrics.heightPercentageToDP(5),
  alignItems:'center',
  alignContent:'center',
  justifyContent:'center',
  borderRadius:metrics.heightPercentageToDP(1.5),
  alignSelf:'center',
  // margin:metrics.heightPercentageToDP(1.5),
  // marginRight:metrics.heightPercentageToDP(3),
  marginLeft:metrics.heightPercentageToDP(1.2),
  marginTop:metrics.heightPercentageToDP(-0.5),
  // borderTopWidth:metrics.heightPercentageToDP(0.1),
  borderColor:color.palette.white


  

}