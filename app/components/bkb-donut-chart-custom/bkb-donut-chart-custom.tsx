import * as React from "react"
import { StyleProp, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { flatten } from "ramda"
import { PieChart } from 'react-native-svg-charts'
import Svg, { G, Text as TextSVG,Polygon, Rect, Circle } from "react-native-svg"
import metrics from "../../theme/metrics"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}


export interface BkbDonutChartCustomProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  dataSet : any
}

/**
 * Describe your component here
 */
export const BkbDonutChartCustom = observer(function BkbDonutChartCustom(props: BkbDonutChartCustomProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])
  const [tooltip,setTooltip]=React.useState(false);
  const [indexTooltip,setIndexTooltip]=React.useState(0);
  const Triangle = ({ w = 16, h = 15, direction = 'right', color = '#44a6e8' , x , y  }) => {
    const points = {
      top: [`${w / 2},0`, `0,${h}`, `${w},${h}`],
      right: [`0,0`, `0,${h}`, `${w},${h / 2}`],
      bottom: [`0,0`, `${w},0`, `${w / 2},${h}`],
      left: [`${w},0`, `${w},${h}`, `0,${h / 2}`],
    }
  
    return (
        <Polygon 
        x={x}
        y={y}
        points={points[direction].join(' ')} fill={color} />
    )
  }
  const data = [50, 10, 40]
  const randomColor = () => ('#' + ((Math.random() * 0xffffff) << 0).toString(16) + '000000').slice(0, 7)
  const mapdataSetNew =props.dataSet.map((value,index)=>({
    key :index ,
    amount :index+1,
    
    svg: { fill :randomColor(),onPress:(event)=>{setTooltip(!tooltip);setIndexTooltip(index)} },
  
  }))
  const Labels = ({ slices, height, width }) => {
    return slices.map((slice, index) => {
        const { labelCentroid, pieCentroid, data } = slice;
        return (
            <G
                key={index}
                x={labelCentroid[ 0 ]}
                y={labelCentroid[ 1 ]}
            >
             
              
             
  
    
                  {index===indexTooltip && 
                    <>
                {/* <Triangle direction="top" color={"white"}
                 x={35} y={-33} /> */}
                <Circle
                x={-8}
               r={5}
               fill={'white'}
               />
                <Rect
                x={0} 
                y={-20}
                width={metrics.heightPercentageToDP(12)}
                height={metrics.heightPercentageToDP(6)}
                fill={"white"}
                rx={metrics.heightPercentageToDP(1)} />
                
                <TextSVG
                              x={48}
                              y={0}
                              fill={color.palette.BKBBlueColor}
                              fontSize={metrics.heightPercentageToDP(1.5)}
                              fontFamily={"Sora-Medium"}
                              textAnchor="middle"
                              >
                              {"Etat marocain"}

                </TextSVG>
                <TextSVG
                              x={46}
                              y={16}
                              fill={color.palette.BKBBlueColor1}
                              fontSize={metrics.heightPercentageToDP(1.5)}
                              fontFamily={"Sora-Bold"}
                              textAnchor="middle"
                              >
                              {"60,00%"}

                </TextSVG>
                          
                </>
                              
                              }
                          
            </G>
        )
    })
  }
  return (
    <View style={{}}>
              <PieChart
              padAngle={0}
                style={{ height: metrics.heightPercentageToDP(30),}}
                valueAccessor={({ item }) => item.amount}
                data={mapdataSetNew}
                spacing={0}
                outerRadius={'100%'}
                innerRadius={"43%"}
            >
                <Labels slices={undefined} height={undefined} width={undefined}/>
            </PieChart>
  </View>
  )
})
