import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { flatten } from "ramda"
import { PieChart } from "react-native-gifted-charts";
import metrics from "../../theme/metrics"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
}

export interface BkbDonutChartProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  dataSetDonut:any
}

/**
 * Describe your component here
 */
export const BkbDonutChart = observer(function BkbDonutChart(props: BkbDonutChartProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])
  const pieData = [
    {value: 64, color: "#00A8FF", text: '54%'+"\n"+"A"},
    {value: 40, color: "green", text: '30%'},
    {value: 20, color: "white", text: '26%'},
    {value: 20, color: "white", text: '26%'},
  ];
  return (
    <View>
            <PieChart
            showText
            textColor={color.palette.BKBBlueColor1}
            fontWeight="bold"
            radius={110}
            textSize={20}
            innerRadius={metrics.heightPercentageToDP(6)}
            backgroundColor={"transparent"}
            showTextBackground={true}
            textBackgroundRadius={30}
            innerCircleBorderColor={"#1B486D"}
            innerCircleBorderWidth={metrics.heightPercentageToDP(6)}
            data={props.dataSetDonut}
            donut
            
            />
    </View>
  )
})
