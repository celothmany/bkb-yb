import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { color } from "../../theme"
import { BkbInfoMailPhone } from "./bkb-info-mail-phone"

storiesOf("BkbInfoMailPhone", module)
  .addDecorator((fn) => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        <BkbInfoMailPhone style={{ backgroundColor: color.error }} />
      </UseCase>
    </Story>
  ))
