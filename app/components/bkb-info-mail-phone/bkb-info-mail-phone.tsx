import * as React from "react"
import { StyleProp, View, ViewStyle,Image, Platform, Linking } from "react-native"
import { observer } from "mobx-react-lite"
import { color } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"
import { TouchableOpacity } from "react-native-gesture-handler"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}
//images & icons
const message=require("../../../assets/images/icon_message.png")
const phone=require("../../../assets/images/icon_phone.png")


export interface BkbInfoMailPhoneProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  mail : string 
  phone : string
}

/**
 * Describe your component here
 */
export const BkbInfoMailPhone = observer(function BkbInfoMailPhone(props: BkbInfoMailPhoneProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])
  const callNumber = phone => {
    console.log('phone ', phone);
    let phoneNumber = phone;
    if (Platform.OS !== 'android') {
      phoneNumber = `telprompt:${phone}`;
    }
    else  {
      phoneNumber = `tel:${phone}`;
    }
    Linking.canOpenURL(phoneNumber)
    .then(supported => {
      if (!supported) {
      } else {
        return Linking.openURL(phoneNumber);
      }
    })
  };
  return (
    <View >
      <TouchableOpacity onPress={()=>{callNumber(props.phone)}}>
      <View style={ROW_INFO} >
                <View style={BACKGROUND_CIRCLE}>
                  <Image source={phone}/>
                 </View>
                 <Text>{props.phone}</Text>
        </View>
      </TouchableOpacity>
        <TouchableOpacity onPress={()=>{Linking.openURL(`mailto:${props.mail}`)}}>
        <View style={ROW_INFO} >
                <View style={BACKGROUND_CIRCLE}>
                  <Image source={message}/>
                 </View>
                 <Text>{props.mail}</Text>
        </View>
        </TouchableOpacity>
    </View>
  )
})

//styles 
const ROW_INFO : ViewStyle={
  flexDirection:'row',
  margin:metrics.heightPercentageToDP(1),
}

const BACKGROUND_CIRCLE : ViewStyle={
  backgroundColor:'#ffffff35',
  height:metrics.heightPercentageToDP(2.5),
  width:metrics.heightPercentageToDP(2.5),
  alignItems:'center',
  alignContent:'center',
  justifyContent:'center',
  borderRadius:metrics.heightPercentageToDP(2),
  marginRight:metrics.heightPercentageToDP(1),
}