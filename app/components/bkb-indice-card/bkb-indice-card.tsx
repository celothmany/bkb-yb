import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle ,Image} from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"
import staticsStrings from "../../localData/staticStrings.json"
import LinearGradient from "react-native-linear-gradient"

//images & icons 
const up_indice=require("../../../assets/images/up_indice.png")
const down_indice=require("../../../assets/images/down_indice.png")
const CONTAINER: ViewStyle = {
  justifyContent: "center",
}



export interface BkbIndiceCardProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  assetStatus :"Achat" | "Vente" 
}

/**
 * Describe your component here
 */
export const BkbIndiceCard = observer(function BkbIndiceCard(props: BkbIndiceCardProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])

  return (
    <View style={TRANSPARENT_BACKGROUND} >
      <View style={PART_LEFT}>
      <View style={ACHAT_VENTE_CONTAINER}>
        <Text style={{fontFamily:"Sora-Medium" ,alignSelf:"center",marginRight:metrics.heightPercentageToDP(1), color: props.assetStatus==="Achat" ? color.palette.BKbGreenColor1 : color.palette.BKBRedColor1}}>1103,51</Text>
        <Image  style={{alignSelf:"center"}} source={props.assetStatus==="Achat" ? up_indice : down_indice} />
      </View>
      <View>

      </View>
      <View style={{flexDirection:"row"}}>
      <LinearGradient
      colors={[props.assetStatus==="Achat" ? color.palette.BKbGreenColor1 : color.palette.BKBRedColor1,"rgba(63,106,145,0.99)","rgba(63,106,145,0.99)"]}
      locations={[0, 0.5, 1]} 
      useAngle={true} angle={35} angleCenter={{ x: 0.5, y: 0.5 }}          
      style={POINTS}
      >
        <Text>-0.05%</Text>

      </LinearGradient>

      <LinearGradient
      colors={[props.assetStatus==="Achat" ? color.palette.BKbGreenColor1 : color.palette.BKBRedColor1,"rgba(63,106,145,0.99)","rgba(63,106,145,0.99)"]}
      locations={[0, 0.5, 1]} 
      useAngle={true} angle={35} angleCenter={{ x: 0.5, y: 0.5 }}          
      style={POINTS}
      >
        <Text>180 pts</Text>

      </LinearGradient>
      </View>

      </View>

      <View  style={PART_RIGHT} >
       <View>
         {/* plus haut & ouverture */}
       <View>
        <Text style={TEXT_INDICE} >{staticsStrings.plus_haut_indices}</Text>
        <Text style={TEXT_INDICE_NUMBER} >11111,11</Text>
        </View>

        <View>
        <Text style={TEXT_INDICE} >{staticsStrings.ouverture}</Text>
        <Text style={TEXT_INDICE_NUMBER} >11111,11</Text>
        </View>
       </View>

       <View>
         {/* plus bas & cloture */}
       <View>
       <Text style={TEXT_INDICE} >{staticsStrings.plus_bas_indices}</Text>
        <Text style={TEXT_INDICE_NUMBER} >11111,11</Text>
        </View>
        <View>
        <Text style={TEXT_INDICE} >{staticsStrings.cloture}</Text>
        <Text style={TEXT_INDICE_NUMBER} >11111,11</Text>
        </View>

       </View>
       <View>
         {/* YID*/}
       <View>
       <Text style={TEXT_INDICE} >{staticsStrings.YID}</Text>
        <Text style={TEXT_INDICE_NUMBER} >11111,11</Text>
        </View>
       

       </View>


      </View>


    </View>

  )
})


const TRANSPARENT_BACKGROUND : ViewStyle={
  borderWidth:metrics.heightPercentageToDP(0.01),
  alignContent:'center',
  alignItems:"center",
  alignSelf:'center',
  // height:metrics.heightPercentageToDP(43),
  borderRadius:metrics.heightPercentageToDP(1),
  borderColor:color.palette.white,
  backgroundColor:'rgba(63,106,145,0.7)',
  width:metrics.heightPercentageToDP(45),
  flexDirection:"row",
  justifyContent:"space-between",
  padding:metrics.heightPercentageToDP(0.5),
  marginTop:metrics.heightPercentageToDP(2),
 
  
}

const PART_LEFT : ViewStyle={
  width:metrics.heightPercentageToDP(18),
  justifyContent:"space-between",

  
 
}
const PART_RIGHT : ViewStyle={
  width:metrics.heightPercentageToDP(27),
  flexDirection:'row',
  justifyContent:"space-around",

}
//styles of textes
const TEXT_INDICE: TextStyle={
  fontFamily:"Sora-Light",
  fontSize:metrics.heightPercentageToDP(1.4)
}
const TEXT_INDICE_NUMBER  : TextStyle={
  fontSize:metrics.heightPercentageToDP(1.5),
  fontFamily:"Sora-Bold",

}
const ACHAT_VENTE_CONTAINER : ViewStyle={
  flexDirection:"row",
  margin:metrics.heightPercentageToDP(1),
}
const POINTS : ViewStyle={
  height:metrics.heightPercentageToDP(2.5),
  width:metrics.heightPercentageToDP(7.5),
  borderRadius:metrics.heightPercentageToDP(2),
  alignItems:"center",
  margin:metrics.heightPercentageToDP(1)
}