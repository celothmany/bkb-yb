import * as React from "react"
import { StyleProp, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"
import staticsStrings from "../../localData/staticStrings.json"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
}

export interface BkbPaginationProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  onPressPagination : Function 
}

/**
 * Describe your component here
 */
export const BkbPagination = observer(function BkbPagination(props: BkbPaginationProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])

  return (
    <TouchableOpacity onPress={()=>{props.onPressPagination()}} style={{alignSelf:'center',margin:metrics.heightPercentageToDP(1)}}>
            <Text style={{fontFamily:'Sora-ExtraBold',color:color.palette.white}}>{staticsStrings.afficher_plus}</Text>
    </TouchableOpacity>
  )
})
