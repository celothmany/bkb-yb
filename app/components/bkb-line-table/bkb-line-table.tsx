import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
}

export interface BkbLineTableProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  isBlurLine ? : boolean
}

/**
 * Describe your component here
 */
export const BkbLineTable : React.FC<BkbLineTableProps> = observer((props)=> {
  const { style } = props
  const styles = flatten([CONTAINER, style])

  return (
    <View style={[props.isBlurLine ? isBlur_LINE_STYLE : isNotBlur_LINE_STYLE,props.style]}>
      {props.children}
    </View>
  )
})
//styles of blured line
const isBlur_LINE_STYLE : ViewStyle={
  flexDirection:'row',
  justifyContent:"space-between",
  width:metrics.heightPercentageToDP(42),
  padding:metrics.heightPercentageToDP(1),
  backgroundColor:'#ffffff20',
  borderRadius:metrics.heightPercentageToDP(0.9),
  maxHeight:metrics.heightPercentageToDP(100)
  
}
//styles of unblured line
const isNotBlur_LINE_STYLE : ViewStyle={
  flexDirection:'row',
  justifyContent:"space-between",
  width:metrics.heightPercentageToDP(42),
  padding:metrics.heightPercentageToDP(1),
  maxHeight:metrics.heightPercentageToDP(100),
  alignItems:'center',


  
}