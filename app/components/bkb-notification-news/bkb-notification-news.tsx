import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle,Image } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import { TouchableOpacity } from "react-native-gesture-handler"
import metrics from "../../theme/metrics"
import LinearGradient from "react-native-linear-gradient"


//images & icons
const news=require("../../../assets/images/news.png")

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}



export interface BkbNotificationNewsProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
}

/**
 * Describe your component here
 */
export const BkbNotificationNews = observer(function BkbNotificationNews(props: BkbNotificationNewsProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])
  const [detailNews,setDetailNews]=React.useState(false)
  return (
    <View>
    <TouchableOpacity onPress={()=>{setDetailNews(!detailNews)}} >
      {
        detailNews===false && <View style={CARD_STYLE}>
        <View style={{flexDirection:"row"}}>
        <LinearGradient
        colors={['#ffffff60',"#ffffff10","rgba(63,106,145,0.99)"]}
        locations={[0, 0.5, 1]} 
        useAngle={true} angle={35} angleCenter={{ x: 0.5, y: 0.5 }}          
        style={NEWS}>
          <Image source={news}  /> 
          </LinearGradient>
          <Text style={{fontFamily:"Sora-Bold",fontSize:metrics.heightPercentageToDP(1.6),alignSelf:'center'}} >hellooo</Text>
        </View>
  
        <Text style={{fontFamily:"Sora-Bold",fontSize:metrics.heightPercentageToDP(1.6),alignSelf:'center',marginRight:metrics.heightPercentageToDP(1)}} >1h</Text>
  
        </View>
      }
    </TouchableOpacity>

      {
        detailNews===true &&
        <TouchableOpacity onPress={()=>{setDetailNews(!detailNews)}} >
          <View style={[CARD_STYLE,{flexDirection:"column"}]}>
          <View><Text style={{color:"#ffffff85",fontFamily:"Sora-Bold",fontSize:metrics.heightPercentageToDP(1.5)}} >14/12/2021    10:48</Text></View>
          <Text  style={{fontFamily:"Sora-Bold",fontSize:metrics.heightPercentageToDP(2)}} >news news news</Text>
          <Text  style={{fontFamily:"Sora-Regular",fontSize:metrics.heightPercentageToDP(1.6),width:metrics.heightPercentageToDP(43)}} >ssssssssfffffgfggggggggggfffffffffffffffffffffffffffffffffffffffsssssssssssssssssssssnews news newsnews news newsnews news newsnews news newsnews news newsnews news newsnews news newsnews news newsnews news newsnews news newsnews news newsnews news newsnews news newsnews news newsnews news newsnews news newsnews news newsnews news newsnews news newsnews news newsnews news newsnews news newsnews news newsnews news newsnews news newsnews news newsnews news news</Text>
          <Text style={{color:"#ffffff85",fontFamily:"Sora-Bold",fontSize:metrics.heightPercentageToDP(1.5)}} >Source</Text>
          <Text style={{fontFamily:"Sora-Regular",fontSize:metrics.heightPercentageToDP(1.6)}} >AMMC</Text>
          </View>
        </TouchableOpacity>
      }

    </View>
  )
})

//styles
const CARD_STYLE : ViewStyle={
  flexDirection:'row',
  justifyContent:"space-between",
  backgroundColor:"rgba(63,106,145,0.6)",
  marginVertical:metrics.heightPercentageToDP(1),
  padding:metrics.heightPercentageToDP(0.6),
  borderRadius:metrics.heightPercentageToDP(0.7),
  marginHorizontal:metrics.heightPercentageToDP(2),
  paddingLeft:metrics.heightPercentageToDP(1),



}
const NEWS : ViewStyle={
  // backgroundColor:hexToRgbA("#ffffff",0.25),
  backgroundColor:'rrgba(63,106,145,0.55)',
  height:metrics.heightPercentageToDP(5),
  width:metrics.heightPercentageToDP(5),
  alignItems:'center',
  alignContent:'center',
  justifyContent:'center',
  borderRadius:metrics.heightPercentageToDP(1.5),
  alignSelf:'center',
  // margin:metrics.heightPercentageToDP(1.5),
  // marginRight:metrics.heightPercentageToDP(3),
  marginLeft:metrics.heightPercentageToDP(0.5),
  marginRight:metrics.heightPercentageToDP(0.7),

  // marginTop:metrics.heightPercentageToDP(-0.5),
  // borderTopWidth:metrics.heightPercentageToDP(0.1),
  borderColor:color.palette.white


}