import * as React from "react"
import { Dimensions, StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color } from "../../theme"
import { flatten } from "ramda"
import { Rect, Text as TextSVG, Svg, Circle, Polygon } from "react-native-svg";
import metrics from "../../theme/metrics"
import { LineChart } from "react-native-chart-kit"
import GlobalFunctions from "../../classes/GlobalFunctions"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}



export interface BkbLineChartProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  dataSet : any
  labelsSet:any
  toolTipText1:any
  toolTipText2:any

}

/**
 * Describe your component here
 */
export const BkbLineChart = observer(function BkbLineChart(props: BkbLineChartProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])
  let [tooltipPos, setTooltipPos] = React.useState({ x: 0, y: 0, visible: false, value: 0 })
  const Triangle = ({ w = 16, h = 15, direction = 'right', color = '#44a6e8' , x , y  }) => {
    const points = {
      top: [`${w / 2},0`, `0,${h}`, `${w},${h}`],
      right: [`0,0`, `0,${h}`, `${w},${h / 2}`],
      bottom: [`0,0`, `${w},0`, `${w / 2},${h}`],
      left: [`${w},0`, `${w},${h}`, `0,${h / 2}`],
    }
  
    return (
        <Polygon 
        x={x}
        y={y}
        points={points[direction].join(' ')} fill={color} />
    )
  }
  const globalFunctions=new GlobalFunctions();
  return (
    <View>
        <LineChart
        withDots={true}
        withInnerLines={true}
        withOuterLines={false}
        withVerticalLines={true}
        withHorizontalLines={false}
          data={{
            labels: props.labelsSet,
            datasets: [
                {
                    data: props.dataSet
                }
            ]
        }}
          width={Dimensions.get("window").width} 
          height={metrics.heightPercentageToDP(45)}
          yAxisInterval={1} 
          chartConfig={{
            backgroundGradientFrom: "white",
            backgroundGradientFromOpacity: 0,
            backgroundGradientTo: color.palette.white,
            backgroundGradientToOpacity: 0,
            backgroundColor:"white",
            color: (opacity = 1) => `#ffffff`,
            labelColor: () => color.palette.white,
            
            strokeWidth: metrics.heightPercentageToDP(0.3), // optional, default 3
            barPercentage: 10,
            useShadowColorFromDataset: false // optional
          }}
    
    // bezier
    style={{
      marginVertical: 8,
      borderRadius: 16
    }}
    decorator={() => {
      return tooltipPos.visible ? <View>
             <Svg 
                  style={{height:metrics.heightPercentageToDP(80),}}
                  >
                  <Rect 
                          x={tooltipPos.x-1} 
                          // y={tooltipPos.y + 20} 
                          width={metrics.heightPercentageToDP(0.18)} 
                          height={metrics.heightPercentageToDP(35.6)}
                          fill={color.palette.BKBBlueColor1} 
                          
                  />
                  <Triangle  x={tooltipPos.x-8} y={tooltipPos.y+13} direction="top" color={color.palette.BKBBlueColor1}/>
                    <Circle cx={tooltipPos.x} cy={tooltipPos.y} r="12" fill={color.palette.BKBBlueColor1} />
                    <Circle cx={tooltipPos.x} cy={tooltipPos.y} r="6" fill={color.palette.white} />

                      <Rect 
                          x={tooltipPos.x - 55} 
                          y={tooltipPos.y + 20} 
                          width={metrics.heightPercentageToDP(13)} 
                          height={metrics.heightPercentageToDP(6)}
                          fill={color.palette.BKBBlueColor1} 
                          rx={metrics.heightPercentageToDP(1)}        
                          />
                         
                          <TextSVG
                              x={tooltipPos.x }
                              y={tooltipPos.y + 35}
                              fill={color.palette.white}
                              fontSize={metrics.heightPercentageToDP(1.5)}
                              fontFamily={"Sora-Regular"}
                              textAnchor="middle"
                              >
                              {props.toolTipText1}

                          </TextSVG>
                          <TextSVG
                              x={tooltipPos.x}
                              y={tooltipPos.y +55}
                              fill={color.palette.white}
                              fontSize={metrics.heightPercentageToDP(1.8)}
                              fontFamily={"Sora-ExtraBold"}
                              textAnchor="middle"
                              >
                              {globalFunctions.ParseFloat(tooltipPos.value,2)+ props.toolTipText2}

                          </TextSVG>
                          
                          
                           
                  </Svg>
                </View> : null
            }}
            onDataPointClick={(data) => {

              let isSamePoint = (tooltipPos.x === data.x 
                                  && tooltipPos.y === data.y)

              isSamePoint ? setTooltipPos((previousState) => {
                  return { 
                            ...previousState,
                            value: data.value,
                            visible: !previousState.visible
                        }
              })
                  : 
              setTooltipPos({ x: data.x, value: data.value, y: data.y, visible: true });

          }}


            />
          </View>
  )
})
