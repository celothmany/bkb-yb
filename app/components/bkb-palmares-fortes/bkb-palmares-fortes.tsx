import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle,Image } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"
import staticsStrings from "../../localData/staticStrings.json"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

//icons & images
const pto_watch=require("../../../assets/images/pto_watch.png")

export interface BkbPalmaresFortesProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  assetStatus : "Haut" | "Bas"
  assetLabel : string 
  assetCode : string
  assetValue : any
  assetHautBas: any
  pto_show? : boolean

}

/**
 * Describe your component here
 */
export const BkbPalmaresFortes = observer(function BkbPalmaresFortes(props: BkbPalmaresFortesProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])

  return (
    <View style={CARD_STYLE}>
      <View style={{alignSelf:"flex-start"}}>
      <Text style={{fontFamily:"Sora-Medium",fontSize:metrics.heightPercentageToDP(1.6)}} >{props.assetCode}</Text>
      <Text style={{fontFamily:"Sora-Bold",fontSize:metrics.heightPercentageToDP(1.9)}} >{props.assetLabel}</Text>
      {
       props.pto_show===true &&  
       <View  style={ROW_2}>
       <Image source={pto_watch}/>
       <Text style={PTO_STYLE}>{staticsStrings.pto}</Text>
   </View>
     }
      </View>
      <Text style={{alignSelf:"center",fontFamily:"Sora-Bold",fontSize:metrics.heightPercentageToDP(1.9)}} >{props.assetValue}</Text>
      <View style={{alignSelf:"center",backgroundColor:color.palette.white,borderRadius:metrics.heightPercentageToDP(2),padding:metrics.heightPercentageToDP(0.2)}} >
        <Text style={{fontFamily:"Sora-Bold",fontSize:metrics.heightPercentageToDP(1.9),color:props.assetStatus==="Haut" ? color.palette.BKbGreenColor1 : color.palette.BKBRedColor1}} >{props.assetHautBas}</Text>
      </View>
      
    </View>
  )
})

//styles
const CARD_STYLE : ViewStyle={
  flexDirection:'row',
  justifyContent:"space-between",
  backgroundColor:"rgba(63,106,145,0.6)",
  marginHorizontal:metrics.heightPercentageToDP(2),
  padding:metrics.heightPercentageToDP(0.6),
  borderRadius:metrics.heightPercentageToDP(1),
  marginVertical:metrics.heightPercentageToDP(1),


}
const ROW_2 : ViewStyle={
  flexDirection:'row',
  // justifyContent:'space-between',
  justifyContent: 'center',
  alignSelf:'flex-start',
  alignContent:'center',
  alignItems:'center',
  marginTop:metrics.heightPercentageToDP(0.3),

}
const PTO_STYLE : TextStyle={
  color:color.palette.BKBBlueColor,
  fontFamily:"Sora-Bold",
  fontSize:metrics.heightPercentageToDP(1.3),
  marginLeft:metrics.heightPercentageToDP(0.5),
}