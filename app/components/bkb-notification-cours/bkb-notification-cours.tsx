import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle , Image } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"
import { TouchableOpacity } from "react-native-gesture-handler"
import LinearGradient from "react-native-linear-gradient"



//images & icons
const notification_vente=require("../../../assets/images/notification_vente.png")
const notification_achat=require("../../../assets/images/notification_achat.png")
const trash=require("../../../assets/images/trash.png")

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}
export interface BkbNotificationCoursProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  assetlabel : string 
  assetCode : string
  seuil : any
  date : any
  assetStatus: "Achat" | "Vente"
  time: any

}

/**
 * Describe your component here
 */
export const BkbNotificationCours = observer(function BkbNotificationCours(props: BkbNotificationCoursProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])
  const [removeShow , setRemoveShow]=React.useState(false)
  const [widthShow , setWidthShow]=React.useState(45)
  return (
    <View style={{flexDirection:'row'}}>
    <TouchableOpacity   onPress={()=>{setRemoveShow(true);setWidthShow(40)}}>
      <View style={[props.style,CARD_STYLE,{width:metrics.heightPercentageToDP(widthShow)}]} >
      <View style={{flexDirection:"row"}}>
        
      <Image style={{marginRight:metrics.heightPercentageToDP(1),alignSelf:"center"}} source={props.assetStatus==="Achat"?notification_achat : notification_vente} />
      <View>
      <Text style={{fontFamily:"Sora-Bold" , fontSize:metrics.heightPercentageToDP(1.3)}} >{props.assetCode}</Text> 
      <Text style={{fontFamily:"Sora-Bold"}}>{props.assetlabel}</Text> 
      <View style={{flexDirection:'row'}}>
        <Text style={{fontFamily:"Sora-Bold" , fontSize:metrics.heightPercentageToDP(1.3),alignSelf:'center'}} >Seuil de   </Text>
      <Text style={{color : props.assetStatus==="Achat" ? color.palette.BKBGreenColor : color.palette.BKBRedColor,fontSize:metrics.heightPercentageToDP(1.4),alignSelf:'center',fontFamily:"Sora-Bold"}} >{props.seuil+"  "}</Text>
      <Text style={{fontFamily:"Sora-Bold" , fontSize:metrics.heightPercentageToDP(1.3),alignSelf:'center'}} >atteint le </Text>
      <Text style={{fontFamily:"Sora-Bold" , fontSize:metrics.heightPercentageToDP(1.3),alignSelf:'center'}} >{props.date}</Text>
      </View>
      </View>
      </View>   
      <Text style={{fontFamily:"Sora-Bold" , fontSize:metrics.heightPercentageToDP(1.3),alignSelf:'center',marginRight:metrics.heightPercentageToDP(1)}}>{props.time}</Text> 
    </View>
    </TouchableOpacity>
    {
      removeShow===true && 
      <LinearGradient
      colors={['#ffffff60',"#ffffff10","rgba(63,106,145,0.99)"]}
      locations={[0, 0.5, 1]} 
      useAngle={true} angle={35} angleCenter={{ x: 0.5, y: 0.5 }}          
      style={TRASH}>
          <TouchableOpacity onPress={()=>{setRemoveShow(false);setWidthShow(47)}}>
          <Image source={trash} />
          </TouchableOpacity>
 
      </LinearGradient>
    }
   
    </View>
  )
})


//styles
const CARD_STYLE : ViewStyle={
  flexDirection:'row',
  justifyContent:"space-between",
  backgroundColor:"rgba(63,106,145,0.6)",
  marginHorizontal:metrics.heightPercentageToDP(2),
  padding:metrics.heightPercentageToDP(0.6),
  borderRadius:metrics.heightPercentageToDP(1),
  marginVertical:metrics.heightPercentageToDP(1),


}
const TRASH : ViewStyle={
  // backgroundColor:hexToRgbA("#ffffff",0.25),
  backgroundColor:'rrgba(63,106,145,0.55)',
  height:metrics.heightPercentageToDP(5),
  width:metrics.heightPercentageToDP(5),
  alignItems:'center',
  alignContent:'center',
  justifyContent:'center',
  borderRadius:metrics.heightPercentageToDP(1.5),
  alignSelf:'center',
  // margin:metrics.heightPercentageToDP(1.5),
  // marginRight:metrics.heightPercentageToDP(3),
  marginLeft:metrics.heightPercentageToDP(0.5),
  marginTop:metrics.heightPercentageToDP(-0.5),
  // borderTopWidth:metrics.heightPercentageToDP(0.1),
  borderColor:color.palette.white


}