import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
  alignSelf:"stretch",
  alignItems:'center',
  alignContent:'center',
  top:metrics.heightPercentageToDP(92),
  // marginHorizontal: metrics.heightPercentageToDP(9),
  right:metrics.heightPercentageToDP(2),
  left:metrics.heightPercentageToDP(2),
  position:"absolute",
  
}

const TEXT: TextStyle = {
  fontFamily:"Sora-Medium",
  fontSize: metrics.heightPercentageToDP(1.5),
  color: color.palette.white,
}

export interface BkbFooterProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
}

/**
 * Describe your component here
 */
export const BkbFooter = observer(function BkbFooter(props: BkbFooterProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])

  return (
    <View style={styles}>
      <Text style={TEXT}>© Copyright BMCE Capital Bourse 2022</Text>
    </View>
  )
})
