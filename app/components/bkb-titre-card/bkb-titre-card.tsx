import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import { BkbTransparentCard } from "../bkb-transparent-card/bkb-transparent-card"
import metrics from "../../theme/metrics"
import staticsStrings from "../../localData/staticStrings.json"
import { TouchableOpacity } from "react-native-gesture-handler"
import GlobalFunctions from "../../classes/GlobalFunctions"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
}

export interface BkbTitreCardProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  titreLabel:any
  quantite:any,
  px_rev:any
  pmv_latente:any
  cours:any
  valorisation:any
  poids:any
  typeCard? : "Titres" | "Values"
  FilterType?:"Realises" | "Latentes" | "Toutes"
  onPress ? : Function
}

/**
 * Describe your component here
 */
export const BkbTitreCard = observer(function BkbTitreCard(props: BkbTitreCardProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])
  const globalFunctions=new GlobalFunctions();


  const colorLatente=(numberLatente)=>{
    if(numberLatente<0){
      return color.palette.BKBRedColor

    }
    else{
      return "#6AC76A"
    }
    
  }
  const filterValues=(filter: string)=>{
      if(filter==="Realises"){
        return (
          <View style={TITRE_ITEM}>
          <Text style={TITRE_STYLE}>{"                             "}</Text>
          <Text style={TITRE_VALEUR}>{"                            "}</Text>
          </View>
          )
        
      }
      if(filter==="Latentes"){
        return (
          <View style={TITRE_ITEM}>
          <Text style={TITRE_STYLE}>{staticsStrings.PMV_latente_percent}</Text>
          <Text style={TITRE_VALEUR}>{globalFunctions.ParseFloat(props.poids,2)+" %"}</Text>
          </View>
        )
      }
      if(filter==="Toutes"){
        return (
          <View style={TITRE_ITEM}>
          <Text style={TITRE_STYLE}>{staticsStrings.PMV_latente_percent}</Text>
          <Text style={TITRE_VALEUR}>{globalFunctions.ParseFloat(props.poids,2)+" %"}</Text>
          </View>
        )
      }
  }
  return (
    <TouchableOpacity  onPress={()=>{props.onPress()}}>
      <BkbTransparentCard style={[CARD_TITRE, props.style]} hexaColorCard={undefined} opacityColorCard={undefined}>
        <View style={titreLABEL_CONTAINER}>
          <Text style={TITRE_LABEL}>{props.titreLabel}</Text>
          </View>
      {/* row 1 */}
        <View style={TITRE_ROW}>
        <View style={TITRE_ITEM}>
          <Text style={TITRE_STYLE}>{staticsStrings.quantite}</Text>
          <Text style={TITRE_VALEUR}>{props.quantite}</Text>
        </View>

        <View style={TITRE_ITEM}>
          <Text style={TITRE_STYLE}>{staticsStrings.px_rev}</Text>
          <Text style={TITRE_VALEUR}>{globalFunctions.ParseFloat(props.px_rev,2)}</Text>
        </View>
        <View style={TITRE_ITEM}>
          <Text style={TITRE_STYLE}>{staticsStrings.PMV_latente}</Text>
          <Text style={[TITRE_VALEUR,{color:colorLatente(props.pmv_latente)}]}>{globalFunctions.ParseFloat(props.pmv_latente,2)}</Text>
        </View>

        </View>
        {/* row 2 */}

        <View style={TITRE_ROW}>
        <View style={TITRE_ITEM}>
          <Text style={TITRE_STYLE}>{staticsStrings.cours}</Text>
          <Text style={TITRE_VALEUR}>{props.cours}</Text>
        </View>

        <View style={TITRE_ITEM}>
          <Text style={TITRE_STYLE}>{staticsStrings.valorisation}</Text>
          <Text style={TITRE_VALEUR}>{props.valorisation}</Text>
        </View>
        {
          props.typeCard==="Titres" && 
          <View style={TITRE_ITEM}>
          <Text style={TITRE_STYLE}>{staticsStrings.poids}</Text>
          <Text style={TITRE_VALEUR}>{globalFunctions.ParseFloat(props.poids,2)+" %"}</Text>
        </View>
        }
        {
          props.typeCard==="Values" && 

          // <View style={TITRE_ITEM}>
          // <Text style={TITRE_STYLE}>{staticsStrings.PMV_latente_percent}</Text>
          // <Text style={TITRE_VALEUR}>{globalFunctions.ParseFloat(props.poids,2)+" %"}</Text>
          // </View>
          filterValues(props.FilterType)

        }
       

        </View>

    </BkbTransparentCard>
    </TouchableOpacity>
  )
})

//styles of card
const CARD_TITRE: ViewStyle={
  marginTop:metrics.heightPercentageToDP(2),
  marginHorizontal:metrics.heightPercentageToDP(2),
  height:metrics.heightPercentageToDP(20),
  borderTopWidth:metrics.heightPercentageToDP(0.03),
  borderRightWidth:metrics.heightPercentageToDP(0.04),
  

}
const titreLABEL_CONTAINER: ViewStyle={
  width:metrics.heightPercentageToDP(40),
  marginBottom:metrics.heightPercentageToDP(1),
  marginTop:metrics.heightPercentageToDP(1)

}
const TITRE_ITEM : ViewStyle={

}
const TITRE_ROW : ViewStyle={
  flexDirection:'row',
  justifyContent:'space-between',
  width:metrics.heightPercentageToDP(40),
  marginBottom:metrics.heightPercentageToDP(2),
}
//styles of textes
const TITRE_LABEL : TextStyle={
  textAlign:'left',
  fontFamily:'Sora-Bold'
}
const TITRE_STYLE : TextStyle={
  fontFamily:'Sora-Regular',
  fontSize:metrics.heightPercentageToDP(1.8)
}
const TITRE_VALEUR : TextStyle={
  fontFamily:'Sora-Bold'
}