import React, { FC, useState } from "react";
import { StyleProp, TextStyle, View, ViewStyle,Image ,Text} from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { flatten } from "ramda"
import { Picker } from "@react-native-picker/picker";
import metrics from "../../theme/metrics"


const CONTAINER: ViewStyle = {
  justifyContent: "center",
}


//images & icons
const drop_up=require("../../../assets/images/drop_up.png")
const drop_down=require("../../../assets/images/drop_down.png")

export interface BkbPickerProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  mode_picker:"dialog" | "dropdown"
  onFocus?: Function,
  onBlur?: Function,
  value : any
  onChange : Function
}

/**
 * Describe your component here
 */
export const BkbPicker:FC<BkbPickerProps> = observer((props)=>{
  const { style } = props
  const styles = flatten([CONTAINER, style])
  const [DropDownIcon,setDropDownIcon]=useState(false);

  return (
    <View style={INPUT_CONTAINER}>
      <Picker
      enabled={true}
      style={PICKER}
      mode={props.mode_picker}
      onFocus={() => {
        props.onFocus(true);setDropDownIcon(true)
      }}
      onBlur={() => {
          props.onBlur(false);setDropDownIcon(false)
      }}

      collapsable={false}
      dropdownIconRippleColor={"white"}
      dropdownIconColor={'white'}
      selectedValue={props.value}
    
      itemStyle={PICKER_CHILDREN}
      onValueChange={(itemValue, _) =>
        props.onChange(itemValue)
    }
      
      >
      
      {props.children}
            </Picker>
      {/* <Image source={DropDownIcon ?drop_up:drop_down} style={{
        marginRight:metrics.heightPercentageToDP(1),
        position: "absolute",
        right:metrics.heightPercentageToDP(1),
        }}/> */}

    </View>
  )
})

const PICKER: ViewStyle = {
  width: metrics.widthPercentageToDP(55),
  borderWidth:1,
  // backgroundColor:'red',
  

}
const INPUT_BKB : ViewStyle={
  width:metrics.heightPercentageToDP(30),
  height:metrics.heightPercentageToDP(6),
  borderRadius:metrics.heightPercentageToDP(5.1),
  borderWidth:metrics.heightPercentageToDP(0.1),
  borderColor:color.palette.white,
  backgroundColor:'#ffffff90',
  marginBottom:metrics.heightPercentageToDP(1.5),
  justifyContent:"center",
  alignItems:'center',
  paddingLeft:metrics.heightPercentageToDP(1.5),

  
}
const INPUT_CONTAINER: ViewStyle = {
  flexDirection: 'row',
  justifyContent: 'flex-end',
  alignItems: 'center',
  backgroundColor: "rgba(63,106,145,0.1)",
  marginTop: metrics.heightPercentageToDP(3),
  marginHorizontal: metrics.heightPercentageToDP(3),
  borderRadius: metrics.heightPercentageToDP(5),
  borderColor: color.palette.white,
  borderWidth:metrics.widthPercentageToDP(0.2),
  width:metrics.heightPercentageToDP(30),
  height: metrics.heightPercentageToDP(6),
  paddingLeft:metrics.heightPercentageToDP(10),
  

}
//picker children style
const PICKER_CHILDREN: TextStyle = {
  fontSize: metrics.heightPercentageToDP(10),
  textAlign: 'center',
  alignSelf:'center',
  backgroundColor:'black'

  
}