import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"
import staticsStrings from "../../localData/staticStrings.json"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
}

export interface BkbPalmaresVolumeProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  assetLabel : string 
  assetCode : string
  assetVolume : string 
  assetCours : string
}

/**
 * Describe your component here
 */
export const BkbPalmaresVolume = observer(function BkbPalmaresVolume(props: BkbPalmaresVolumeProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])

  return (
    <View style={CARD_STYLE}>
      <View style={{alignSelf:"flex-start"}}>
      <Text style={{fontFamily:"Sora-Medium",fontSize:metrics.heightPercentageToDP(1.6)}} >{props.assetCode}</Text>
      <Text style={{fontFamily:"Sora-Bold",fontSize:metrics.heightPercentageToDP(1.9)}} >{props.assetLabel}</Text>
      </View>
      <View style={{alignSelf:"flex-start"}}>
      <Text style={{fontFamily:"Sora-Medium",fontSize:metrics.heightPercentageToDP(1.6)}} >{staticsStrings.volume_mmd}</Text>
      <Text style={{fontFamily:"Sora-Bold",fontSize:metrics.heightPercentageToDP(1.9)}} >{props.assetVolume}</Text>
      </View>
      <View style={{alignSelf:"flex-start"}}>
      <Text style={{fontFamily:"Sora-Medium",fontSize:metrics.heightPercentageToDP(1.6)}} >{"COURS"}</Text>
      <Text style={{fontFamily:"Sora-Bold",fontSize:metrics.heightPercentageToDP(1.9)}} >{props.assetCours}</Text>
      </View>
      
    </View>
  )
})

//styles
const CARD_STYLE : ViewStyle={
  flexDirection:'row',
  justifyContent:"space-between",
  backgroundColor:"rgba(63,106,145,0.6)",
  marginHorizontal:metrics.heightPercentageToDP(2),
  padding:metrics.heightPercentageToDP(0.9),
  borderRadius:metrics.heightPercentageToDP(1),
  marginVertical:metrics.heightPercentageToDP(1),


}