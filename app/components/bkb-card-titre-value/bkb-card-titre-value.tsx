import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import LinearGradient from "react-native-linear-gradient"
import metrics from "../../theme/metrics"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
}

export interface BkbCardTitreValueProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
}

/**
 * Describe your component here
 */
export const BkbCardTitreValue :React.FC<BkbCardTitreValueProps>= observer((props)=>{
  const { style } = props
  const styles = flatten([CONTAINER, style])

  return (
    <LinearGradient
    colors={['#ffffff10', '#ffffff20',"#ffffff20","#6AC76A60"]}
    // start={{ x: 0, y: 0 }}
    useAngle={true} angle={35} angleCenter={{ x: 0.5, y: 0.5 }}

    style={CARD_STYLE}
    >
      {props.children}

    </LinearGradient>
  )
})
//styles of card
const CARD_STYLE : ViewStyle={
  width:metrics.heightPercentageToDP(30),
  // height:metrics.heightPercentageToDP(10),
  maxHeight:metrics.heightPercentageToDP(50),
  margin:metrics.heightPercentageToDP(1.5),
  borderRadius:metrics.heightPercentageToDP(1.5),
  padding:metrics.heightPercentageToDP(1.5),
  borderColor:color.palette.white,
  borderRightWidth:metrics.heightPercentageToDP(0.04),
  borderTopWidth:metrics.heightPercentageToDP(0.04)

}