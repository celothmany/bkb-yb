import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"
import { TouchableOpacity } from "react-native-gesture-handler"
import staticsStrings from "../../localData/staticStrings.json"
import GlobalFunctions from "../../classes/GlobalFunctions"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
}

export interface BkbCardTransactionDetailsProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  onPress : Function
  creation : any
  quantite : any
  modalite : any
  cours_traite : any
  date_de_validite : any
  id_carnet : any 
  name_carnet : any 
  type_titre:string
  status_carnet:string
 

}

/**
 * Describe your component here
 */
export const BkbCardTransactionDetails = observer(function BkbCardTransactionDetails(props: BkbCardTransactionDetailsProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])
  const globalFunctions=new GlobalFunctions()
  return (
    <TouchableOpacity onPress={()=>props.onPress()} style={CARD_STYLE}>
      <View style={ROW_HEADER}>
          <View>
            <Text>{props.id_carnet}</Text>
            <Text style={{fontFamily:'Sora-Bold'}}>{props.name_carnet}</Text>

          </View>
          <View>
          <Text style={globalFunctions.translateAchatVente(props.type_titre)==="Achat"?ACHAT_TEXT:VENTE_TEXT}>{globalFunctions.translateAchatVente(props.type_titre)}</Text>

          </View>
          <View>
          <Text style={[TEXT_STATUS,{color:props.status_carnet==="En attente"?  color.palette.BKBBlueColor : color.palette.white}]}>{props.status_carnet}</Text>

          </View>
      </View>
      <View style={DIVIDER}/>
      <View style={ROW_CARNET}>
          <Text style={TEXT_CARENT}>{staticsStrings.creation+":                     "}</Text>
          <Text style={TEXT_CARENT_VALUE} >{props.creation}</Text>
        {/* 10 spaces */}
      </View>
      <View style={ROW_CARNET}>
          <Text style={TEXT_CARENT} >{staticsStrings.quantite+":                     "}</Text>
                    <Text style={TEXT_CARENT_VALUE} >{props.quantite}</Text>

      </View>
      <View style={ROW_CARNET}>
          <Text style={TEXT_CARENT} >{staticsStrings.modalité+":                     "}</Text>
          <Text style={TEXT_CARENT_VALUE} >{props.modalite}</Text>

      </View>
      <View style={ROW_CARNET}>
          <Text style={TEXT_CARENT} >{staticsStrings.cours_traite+":               "}</Text>
          <Text style={TEXT_CARENT_VALUE} >{props.cours_traite}</Text>

      </View>
      <View style={ROW_CARNET}>
          <Text style={TEXT_CARENT} >{staticsStrings.date_de_validte+":                        "}</Text>
          <Text style={[TEXT_CARENT_VALUE,{alignSelf:'flex-end'}]} >{props.date_de_validite}</Text>

      </View>
      
    </TouchableOpacity>
  )
})

//styles 
const CARD_STYLE : ViewStyle={
  marginRight:metrics.heightPercentageToDP(1.5),
  marginLeft:metrics.heightPercentageToDP(1.5),
  borderColor:"transparent",
  // backgroundColor:"#ffffff20",
  backgroundColor:"rgba(63,106,145,0.7)",
  borderRadius:metrics.heightPercentageToDP(1),
  padding:metrics.heightPercentageToDP(1),
  // flexDirection:'row',
  justifyContent:"space-between",
  marginTop:metrics.heightPercentageToDP(1.5),


}
const ROW_HEADER : ViewStyle={
  flexDirection:'row',
  justifyContent:"space-between",
}
const DIVIDER : ViewStyle={
  width:metrics.heightPercentageToDP(42),
  height:metrics.heightPercentageToDP(0.3),
  backgroundColor:"rgba(200,200,200,0.2)",
  borderRadius:metrics.heightPercentageToDP(5),
  alignSelf:'center',
  margin:metrics.heightPercentageToDP(1),
}
const ROW_CARNET : ViewStyle={
  flexDirection:'row',
  marginBottom:metrics.heightPercentageToDP(1),
}
const TEXT_CARENT : TextStyle={
  textAlign:"left",
  fontFamily:'Sora-Regular',
  fontSize:metrics.heightPercentageToDP(1.5),
  // marginRight:metrics.heightPercentageToDP(4)


}
const TEXT_CARENT_VALUE : TextStyle={
  textAlign:"left",
  fontFamily:"Sora-Bold",
  fontSize:metrics.heightPercentageToDP(1.5),

}
const VENTE_TEXT : TextStyle={
  color:color.palette.BKBRedColor,
  textAlign:'right',
  alignSelf:'flex-start'

}
const ACHAT_TEXT : TextStyle={
  color:color.palette.BKbGreenColor1,
  textAlign:'right',
  alignSelf:'flex-end'


}
const TEXT_STATUS : TextStyle={
  fontFamily:"Sora-Bold",
  fontSize:metrics.heightPercentageToDP(1.6),
  maxWidth:metrics.heightPercentageToDP(12),
}