import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { color } from "../../theme"
import { BkbCardTransaction } from "./bkb-card-transaction"

storiesOf("BkbCardTransaction", module)
  .addDecorator((fn) => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        <BkbCardTransaction style={{ backgroundColor: color.error }} />
      </UseCase>
    </Story>
  ))
