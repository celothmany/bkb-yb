import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import { BkbTransparentCard } from "../bkb-transparent-card/bkb-transparent-card"
import metrics from "../../theme/metrics"
import LinearGradient from "react-native-linear-gradient"
import GlobalFunctions from "../../classes/GlobalFunctions"
import { TouchableOpacity } from "react-native-gesture-handler"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}


export interface BkbCardTransactionProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  idTransactions : any
  heureTransaction:any
  nameTransaction:any
  dateTransaction : any
  typeTransaction : "Vente" | "Achat"
  // statusTransaction : "En attente" | "Annulé" |"Expiré" | "En anomalie" | "Exécuté partiellement" | "Comptabilisé" | "Ordre Exécuté" | "Annulation refusée" | "Rejeté" | "Modifie" | "Non acquitté" | "Exécuté non comptabilisée"
  statusTransaction :String
  onPress : Function
}

/**S
 * Describe your component here
 */
export const BkbCardTransaction = observer(function BkbCardTransaction(props: BkbCardTransactionProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])
  const linearGradientColor=(type)=>{
      if(type==="Vente"){
        return "#FB5F4F30"
      }
      if(type==="Achat"){
        return  "#6AC76A60"

      }
  }
  const globalFunctions= new GlobalFunctions();
  return (
    
      
      <TouchableOpacity onPress={()=>{props.onPress()}} >
          <LinearGradient
        colors={['#ffffff10', '#ffffff10',"#ffffff20",linearGradientColor(props.typeTransaction)]}
        // start={{ x: 0, y: 0.1 }}
        useAngle={true} angle={35} angleCenter={{ x: 0.5, y: 0.5 }}

            style={CARD_STYLE}
    >

        <View>
          <Text>{props.idTransactions}</Text>
          <Text style={TEXT_ROW_TWO}>{props.nameTransaction}</Text>

        </View>
        <View>
          <Text>{globalFunctions.getHeure(props.heureTransaction)}</Text>
          <Text style={TEXT_ROW_TWO}>{globalFunctions.getMaxdate(props.dateTransaction)}</Text>

        </View>
        <View>
          <Text style={props.typeTransaction==="Vente" ? VENTE_TEXT : ACHAT_TEXT}>{props.typeTransaction}</Text>
          <Text style={[TEXT_ROW_TWO,{color:props.statusTransaction==="En attente"?  color.palette.BKBBlueColor : color.palette.white}]}>{props.statusTransaction}</Text>

        </View>
    </LinearGradient>
      </TouchableOpacity>
  
  )
})

//styles 
const CARD_STYLE : ViewStyle={
  marginRight:metrics.heightPercentageToDP(1.5),
  marginLeft:metrics.heightPercentageToDP(1.5),
  borderColor:"transparent",
  backgroundColor:"#ffffff20",
  borderRadius:metrics.heightPercentageToDP(1),
  padding:metrics.heightPercentageToDP(1),
  flexDirection:'row',
  justifyContent:"space-between",
  marginTop:metrics.heightPercentageToDP(1.5),
  borderTopWidth:metrics.heightPercentageToDP(0.04),
  borderLeftWidth:metrics.heightPercentageToDP(0.04),

}
//styles of columns

const TEXT_ROW_TWO : TextStyle={
  fontFamily:"Sora-Bold",
  fontSize:metrics.heightPercentageToDP(1.6),
  maxWidth:metrics.heightPercentageToDP(12),
}

const VENTE_TEXT : TextStyle={
  color:color.palette.BKBRedColor,
  textAlign:'right'

}
const ACHAT_TEXT : TextStyle={
  color:color.palette.BKbGreenColor1,
  textAlign:'right'

}