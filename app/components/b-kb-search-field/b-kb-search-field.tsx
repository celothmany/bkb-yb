import * as React from "react"
import { FlatList, StyleProp, TextInput, TextStyle, View, ViewStyle ,Image} from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import staticsStrings from "../../localData/staticStrings.json"
import metrics from "../../theme/metrics"
import { TouchableOpacity } from "react-native-gesture-handler"
import { WatchListCardFiltered } from "../watch-list-card-filtered/watch-list-card-filtered"


//icons & images

const search_icon=require("../../../assets/images/search.png")

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}



export interface BKbSearchFieldProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  dataSource : any 

}

/**
 * Describe your component here
 */
export const BKbSearchField = observer(function BKbSearchField(props: BKbSearchFieldProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])
  const [search, setSearch] = React.useState('');
  const [filteredDataSource, setFilteredDataSource] = React.useState([]);
  const [masterDataSource, setMasterDataSource] = React.useState([]);
  const searchFilterFunction = (text) => {
    if (text) {

      const newData = masterDataSource.filter(function (item) {
        const itemData = item.title
          ? item.title.toUpperCase()
          : ''.toUpperCase();
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setFilteredDataSource(newData);
      setSearch(text);
    } else {
      setFilteredDataSource(masterDataSource);
      setSearch(text);
    }
  };

  const ItemView = ({ item }) => {
    return (
      <Text style={{padding:10}} onPress={() => getItem(item)}>
        {item.id}
        {'.'}
        {item.title.toUpperCase()}
      </Text>
    );
  };

  const ItemSeparatorView = () => {
    return (
      // Flat List Item Separator
      <View
        style={{
          height: 0.5,
          width: '100%',
          backgroundColor: '#C8C8C8',
        }}
      />
    );
  };

  const getItem = (item) => {
    // Function for click on an item
    alert('Id : ' + item.id + ' Title : ' + item.title);
  };

  React.useEffect(() => {
    
    setFilteredDataSource(props.dataSource);
    setMasterDataSource(props.dataSource);
  }, []);
  
  return (
    <View >
      <View style={TEXT_INPUT_STYLE} >
        <TouchableOpacity style={SEARCH_BUTTON}>
        <Image source={search_icon}/>
        </TouchableOpacity>
      <TextInput
        style={TEXT_INPUT}
        onChangeText={(text) => searchFilterFunction(text)}
        value={search}
        underlineColorAndroid="transparent"
        placeholder={"    "+staticsStrings.isin_valeur}
        placeholderTextColor={"#ffffff50"}
        keyboardType={"web-search"}
        
      />
      </View>
      {/* <FlatList
        data={filteredDataSource}
        keyExtractor={(item, index) => index.toString()}
        ItemSeparatorComponent={ItemSeparatorView}
        renderItem={ItemView}
      /> */}
      {
        filteredDataSource.map((dataFiletred,index)=>{
            return (
              <View>
                <WatchListCardFiltered 
                assetCode={"MA1923829346"} 
                assetLabel={"FMASVC"} 
                value={199.111} 
                up_down={"-19.1"} 
                pto_show={true} 
                onPressWatchPlus={()=>{}}/>
                {/* <ItemView item={dataFiletred}/> */}
              </View>
            );
        })
      }
    </View>
  )
})


//styles 

const TEXT_INPUT : TextStyle={
  // borderWidth:1,
  width:metrics.heightPercentageToDP(41),
  color:color.palette.white

  

}
const TEXT_INPUT_STYLE : ViewStyle={
  height: metrics.heightPercentageToDP(6),
  borderWidth: metrics.heightPercentageToDP(0.2),
  // paddingLeft: metrics.heightPercentageToDP(2),
  margin: metrics.heightPercentageToDP(1),
  borderColor: "#ffffff50",
  backgroundColor: 'transparent',
  borderRadius:metrics.heightPercentageToDP(5),
  flexDirection:'row-reverse',
  justifyContent:'space-between'
}
const SEARCH_BUTTON : ViewStyle={
  backgroundColor:'rgba(63,106,145,0.7)',
  width:metrics.heightPercentageToDP(6),
  height:metrics.heightPercentageToDP(5.6),
  justifyContent:"center",
  alignContent:'center',
  alignItems:'center',
  borderTopEndRadius:metrics.heightPercentageToDP(4.1),
  borderBottomEndRadius:metrics.heightPercentageToDP(4.1),
}