import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle,Image, ImageStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"
import staticsStrings from "../../localData/staticStrings.json"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}
//images & icons
const actualites_img=require("../../../assets/images/actualites_img.png")



export interface BkbActualitesViewProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  title : string 
  time_date: any
  description : string
}

/**
 * Describe your component here
 */
export const BkbActualitesView = observer(function BkbActualitesView(props: BkbActualitesViewProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])
  const [numLines, setNumLines] = React.useState(2);
  const [showMoreButton, setShowMoreButton] = React.useState(false);
  const [textShown, setTextShown] = React.useState(false);
  const toggleTextShown = () => {
    setTextShown(!textShown);
  };

  React.useEffect(() => {
    setNumLines(textShown ? undefined : 1);
  }, [textShown]);

  const onTextLayout = React.useCallback(
    (e) => {
      if (e.nativeEvent.lines.length > 3 && !textShown) {
        setShowMoreButton(true);
        setNumLines(1);
      }
    },
    [textShown],
  );
  return (
    <View style={ACTUALITE}>
      {/* <Image source={actualites_img} style={IMAGE_STYLE} /> */}
      <View>
      <Text style={TEXT_TITRE} >{props.title}</Text>
      <Text style={TEXT_TIME_DATE} >{props.time_date}</Text>
     <View style={{marginRight:metrics.heightPercentageToDP(2)}} >
     <Text style={TEXT_DESCRIPTION} onTextLayout={onTextLayout} numberOfLines={numLines} ellipsizeMode="tail" >{props.description}</Text>

       </View> 
      {showMoreButton ? (
        <Text onPress={toggleTextShown}  style={{fontFamily:"Sora-Medium",fontSize:metrics.heightPercentageToDP(1.5)}}>
          {textShown ? staticsStrings.lire_moins : staticsStrings.lire_plus}
        </Text>
      ) : null}
      </View>
    </View>
  )
})

const ACTUALITE :ViewStyle={
  flexDirection:'row',
  padding:metrics.heightPercentageToDP(1),
  maxHeight:metrics.heightPercentageToDP(40)
}
const IMAGE_STYLE : ImageStyle={
  height:68,width:83,
  resizeMode: 'contain',
  margin:metrics.heightPercentageToDP(1),
}
const TEXT_TITRE : TextStyle={
  fontFamily:"Sora-Bold",
  width:metrics.heightPercentageToDP(35)
}

const TEXT_TIME_DATE : TextStyle={
  fontFamily:"Sora-Medium",
  fontSize:metrics.heightPercentageToDP(1.3)

}
const TEXT_DESCRIPTION : TextStyle={
  
  width:metrics.heightPercentageToDP(33),
  fontFamily:"Sora-Bold",
  fontSize:metrics.heightPercentageToDP(1.3),
  textAlign:"left",

  
}