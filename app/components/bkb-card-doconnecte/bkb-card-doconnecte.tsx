import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle,Image } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import { BkbTransparentCard } from "../bkb-transparent-card/bkb-transparent-card"
import { BkbButton } from "../bkb-button/bkb-button"
import staticsStrings from "../../localData/staticStrings.json"
import metrics from "../../theme/metrics"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

//images & icons 
const exlamation_deconnecte=require("../../../assets/images/exlamation_deconnecte.png")

export interface BkbCardDoconnecteProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  onPressConnecter : Function 
  onPressClient : Function 
}

/**
 * Describe your component here
 */
export const BkbCardDoconnecte = observer(function BkbCardDoconnecte(props: BkbCardDoconnecteProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])

  return (
    <BkbTransparentCard hexaColorCard={undefined} opacityColorCard={undefined} style={PROFILE_CARD} >
          <Image  source={exlamation_deconnecte}/>
          <Text style={TEXT_DECONNECTE}>{staticsStrings.connectez_vous_fonctionnalites_deconnecte}</Text>
          <BkbButton ButtonType={"transparent"} ButtonText={staticsStrings.se_connecter_deconnecte}  TextStyle={ {fontFamily:"Sora-Bold",marginRight:metrics.heightPercentageToDP(1.8),}} onPress={()=>{props.onPressConnecter()}}/>
          <BkbButton ButtonType={"white"} ButtonText={staticsStrings.devenir_client_deconnecte} onPress={()=>{props.onPressClient()}} />
      </BkbTransparentCard>
  )
})


const PROFILE_CARD: ViewStyle = {
  height:metrics.heightPercentageToDP(26),
  // maxHeight:metrics.heightPercentageToDP(26),
  marginHorizontal:metrics.heightPercentageToDP(3),
  marginRight:metrics.heightPercentageToDP(3),
  marginLeft:metrics.heightPercentageToDP(3),
  width:metrics.heightPercentageToDP(43),
  borderWidth:metrics.heightPercentageToDP(0.05),
  borderColor:color.palette.white,
  alignItems:'center',
  alignContent:"center",
  justifyContent:"space-evenly",
  marginBottom:metrics.heightPercentageToDP(4),
  // marginTop:metrics.heightPercentageToDP(5),


}
const TEXT_DECONNECTE : TextStyle={
  textAlign:"center",
  alignSelf:"center",
  fontFamily:"Sora-Regular",
  marginHorizontal:metrics.heightPercentageToDP(2),
  color:color.palette.white
 }