import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { color } from "../../theme"
import { BkbNavButton } from "./bkb-nav-button"

storiesOf("BkbNavButton", module)
  .addDecorator((fn) => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        <BkbNavButton style={{ backgroundColor: color.error }} />
      </UseCase>
    </Story>
  ))
