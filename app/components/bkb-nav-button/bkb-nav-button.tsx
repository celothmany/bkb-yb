import * as React from "react"
import { Image, StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import { TouchableOpacity } from "react-native-gesture-handler"
import { BkbTransparentCard } from "../bkb-transparent-card/bkb-transparent-card"
import metrics from "../../theme/metrics"
//iamges & icons
const marche=require("../../../assets/images/marche.png")
const arrow_right=require("../../../assets/images/icon_arrow_right.png")

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}


export interface BkbNavButtonProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  Title : string
  Icon : any
  onPress : Function
}

/**
 * Describe your component here
 */
export const BkbNavButton = observer(function BkbNavButton(props: BkbNavButtonProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])
  const hexToRgbA = (hex, opacity) => {
    let c;
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
      c = hex.substring(1).split('');
      if (c.length === 3) {
        c = [c[0], c[0], c[1], c[1], c[2], c[2]];
      }
      c = `0x${c.join('')}`;
      return `rgba(${[(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',')},${opacity})`;
    }
    throw new Error('Bad Hex');
  };
  return (
    <TouchableOpacity style={[NAV_CONTAINER,{
      backgroundColor:hexToRgbA("#ffffff",0.15),
      // backgroundColor:'rgba(63,106,145,0.7)',
      },props.style]} onPress={()=>props.onPress()}>
        <View style={{flexDirection:'row',justifyContent:'center',alignContent:'center',alignItems:'center'}}>
        <View style={{
          backgroundColor:hexToRgbA("#ffffff",0.25),
          // backgroundColor:'rgba(63,106,145,0.7)',
          height:metrics.heightPercentageToDP(3.7),
          width:metrics.heightPercentageToDP(3.7),
          alignItems:'center',
          alignContent:'center',
          justifyContent:'center',
          borderRadius:metrics.heightPercentageToDP(1),
          
        }}>
        <Image source={props.Icon}/>

        </View>
      
        <View 
        style={Title_Container_Style}>
      <Text  style={Title_Style}>
        {props.Title}
      </Text>
      </View>
        </View>

      <Image source={arrow_right}/>
     
    </TouchableOpacity>
  )
})
//styles
const NAV_CONTAINER : ViewStyle={
  height:metrics.heightPercentageToDP(5.6),
  // maxHeight:metrics.heightPercentageToDP(100),
  width:metrics.heightPercentageToDP(43),
  
  alignSelf:'center',
  alignItems:'center',
  borderRadius:metrics.heightPercentageToDP(1),
  flexDirection:'row',
  justifyContent:"space-between",
  paddingHorizontal:metrics.heightPercentageToDP(1),
  marginBottom:metrics.heightPercentageToDP(3),
  // marginRight:metrics.heightPercentageToDP(20),
  // marginLeft:metrics.heightPercentageToDP(20)
}
const Title_Container_Style: ViewStyle={
  marginLeft:metrics.heightPercentageToDP(1),
  width:metrics.heightPercentageToDP(30),
   alignItems:'flex-start',
   justifyContent:'center'
  // marginRight:metrics.heightPercentageToDP(12)
}
const Title_Style: TextStyle={
  textAlign:'right',maxWidth:metrics.heightPercentageToDP(40),
  fontSize:metrics.heightPercentageToDP(2.6),
}