import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"
import staticsStrings from "../../localData/staticStrings.json"
import GlobalFunctions from "../../classes/GlobalFunctions"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
}

export interface BkbHistoriqueCardProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  date_effet : any 
  montant_net : any 
  cours : any 
  operation : any 
  quantite : any 
  

}

/**
 * Describe your component here
 */
export const BkbHistoriqueCard = observer(function BkbHistoriqueCard(props: BkbHistoriqueCardProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])
  const globalFunctions= new GlobalFunctions();
  return (
    <View style={CARD_STYLE}>
      <Text style={{fontFamily:"Sora-Bold",marginBottom:metrics.heightPercentageToDP(1)}}>Hello</Text>

      <View style={CARD_CONTAINER}>

      <View style={COLUMN}>
      <View style={TEXTES_CONTAINER}>
      <Text style={TEXT_ONE}>{staticsStrings.date_effet}</Text>
      <Text style={TEXT_TWO}>{props.date_effet}</Text>
      </View>
      <View style={TEXTES_CONTAINER}>
      <Text style={TEXT_ONE}>{staticsStrings.operation}</Text>
      <Text style={globalFunctions.translateAchatVente(props.operation)==="Achat"?ACHAT_TEXT:VENTE_TEXT}>{globalFunctions.translateAchatVente(props.operation)}</Text>
      </View>
      </View>


      <View style={COLUMN}>
      <View style={TEXTES_CONTAINER}>
      <Text style={TEXT_ONE}>{staticsStrings.montant_net_two}</Text>
      <Text style={TEXT_TWO}>{props.montant_net}</Text>
      </View>
      <View style={TEXTES_CONTAINER}>
      <Text style={TEXT_ONE}>{staticsStrings.quantite}</Text>
      <Text style={TEXT_TWO}>{props.quantite}</Text>
      </View>
      </View>

      <View style={COLUMN}>
      <View style={TEXTES_CONTAINER}>
      <Text style={TEXT_ONE}>{staticsStrings.cours}</Text>
      <Text style={TEXT_TWO}>{props.cours}</Text>
      </View>
    
      </View>

      </View>
      
    </View>
  )
})

//styles 
const CARD_STYLE : ViewStyle={
  marginRight:metrics.heightPercentageToDP(1.5),
  marginLeft:metrics.heightPercentageToDP(1.5),
  borderColor:"transparent",
  backgroundColor:"rgba(63,106,145,0.7)",
  borderRadius:metrics.heightPercentageToDP(1),
  padding:metrics.heightPercentageToDP(1),
  justifyContent:"space-between",
  marginTop:metrics.heightPercentageToDP(1.5),
}
const CARD_CONTAINER : ViewStyle={
  flexDirection:'row',
  justifyContent:"space-between",
  paddingRight:metrics.heightPercentageToDP(3)

}
const TEXTES_CONTAINER : ViewStyle={
  marginBottom:metrics.heightPercentageToDP(2),

}
const COLUMN : ViewStyle={
  // flexDirection:'row',
  justifyContent:"space-between",
}
const TEXT_ONE : TextStyle={
  
}
const TEXT_TWO : TextStyle={
  fontFamily:"Sora-Bold",
  fontSize:metrics.heightPercentageToDP(1.5),
}
const VENTE_TEXT : TextStyle={
  color:color.palette.BKBRedColor,
  textAlign:'left'

}
const ACHAT_TEXT : TextStyle={
  color:color.palette.BKbGreenColor1,
  textAlign:'left'

}