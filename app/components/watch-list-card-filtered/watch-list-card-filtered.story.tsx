import * as React from "react"
import { storiesOf } from "@storybook/react-native"
import { StoryScreen, Story, UseCase } from "../../../storybook/views"
import { color } from "../../theme"
import { WatchListCardFiltered } from "./watch-list-card-filtered"

storiesOf("WatchListCardFiltered", module)
  .addDecorator((fn) => <StoryScreen>{fn()}</StoryScreen>)
  .add("Style Presets", () => (
    <Story>
      <UseCase text="Primary" usage="The primary.">
        <WatchListCardFiltered style={{ backgroundColor: color.error }} />
      </UseCase>
    </Story>
  ))
