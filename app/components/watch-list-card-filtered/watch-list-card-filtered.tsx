import * as React from "react"
import { StyleProp, TextStyle, View, ViewStyle,Image, Pressable } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Text } from "../text/text"
import { flatten } from "ramda"
import metrics from "../../theme/metrics"
import staticsStrings from "../../localData/staticStrings.json"
import { TouchableOpacity } from "react-native-gesture-handler"
import LinearGradient from "react-native-linear-gradient"
import { BkbModal } from "../bkb-modal/bkb-modal"
import { BkbInput } from "../bkb-input/bkb-input"
import { BkbButton } from "../bkb-button/bkb-button"


//icons & images
const pto_watch=require("../../../assets/images/pto_watch.png")
const watch_list_plus=require("../../../assets/images/watch_list_plus.png")
const x_annuler=require("../../../assets/images/x_annuler.png")
const notification_modal=require("../../../assets/images/notification_modal.png")
const watchlist_modal=require("../../../assets/images/watchlist_modal.png")


const CONTAINER: ViewStyle = {
  justifyContent: "center",
}



export interface WatchListCardFilteredProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
  assetCode : any 
  assetLabel : any 
  value : any 
  up_down : string
  pto_show : boolean
  onPressWatchPlus: Function
}

/**
 * Describe your component here
 */
export const WatchListCardFiltered = observer(function WatchListCardFiltered(props: WatchListCardFilteredProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])
  const [showAdd,setShowAdd]=React.useState(true);
  const [widthCard,setWidthCard]=React.useState(40);
  const [ajouterSeuil, setAjouterSeuil] = React.useState(false);
  const [modalNotif, setModalNotif] = React.useState(false);
  const [modalWatchList, setModalWatchList] = React.useState(false);
  const [seuil, setSeuil] = React.useState("");

  const upDown =(num : string)=>{
    if (num.substring(0,1)==="+"){
      return color.palette.BKbGreenColor1
    }
    if (num.substring(0,1)==="-"){
      return color.palette.BKBRedColor1
    }
  }
  const hexToRgbA = (hex, opacity) => {
    let c;
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
      c = hex.substring(1).split('');
      if (c.length === 3) {
        c = [c[0], c[0], c[1], c[1], c[2], c[2]];
      }
      c = `0x${c.join('')}`;
      return `rgba(${[(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',')},${opacity})`;
    }
    throw new Error('Bad Hex');
  };
  return (
   
    <View 
    style={ROW_CARD}
    >
      <View style={[CARD_STYLE,{width:metrics.heightPercentageToDP(widthCard),}]}>
      <Text style={assetCodeStyle} >{props.assetCode}</Text>
      <View style={ROW_1}>
      <Text style={assetLabelStyle}>{props.assetLabel}</Text>
      <Text style={assetLabelStyle}>{props.value}</Text>

      <View style={UP_DOWN_STYLE}>
        <Text style={[UP_DOWN_TEXT,{color:upDown(props.up_down)}]}>{props.up_down}</Text>

      </View>
      </View>
     {
       props.pto_show===true &&  
       <View  style={ROW_2}>
       <Image source={pto_watch}/>
       <Text style={PTO_STYLE}>{staticsStrings.pto}</Text>
   </View>
     }
    </View>
   {
     showAdd && <LinearGradient
     colors={['#ffffff60',"#ffffff10","rgba(63,106,145,0.99)"]}
     locations={[0, 0.5, 1]} 
     useAngle={true} angle={35} angleCenter={{ x: 0.5, y: 0.5 }}          
     style={AJOUTER}
        
            >
    <TouchableOpacity
    onPress={()=>{props.onPressWatchPlus();
      // setWidthCard(46);setShowAdd(false);
      setAjouterSeuil(true)}}
    >
      {/* <TouchableOpacity onPress={()=>{props.onPressWatchPlus()}}> */}
      <Image  source={watch_list_plus}/>
      {/* </TouchableOpacity> */}
    </TouchableOpacity>
    </LinearGradient>
   }

   {/* modal 1 add seuil */}
   <BkbModal isVisibleState={ajouterSeuil}  style={{height:metrics.heightPercentageToDP(36)}}>
   <Pressable onPress={()=>{setAjouterSeuil(false)}} style={{right:metrics.widthPercentageToDP(4),alignSelf:"flex-end",top:metrics.heightPercentageToDP(-1)}}>
        <Image  source={x_annuler}/>
        </Pressable>
        <View style={NOTFICATION_CONTAINER}>
              <Image source={notification_modal}  />
            <Text style={TITRE}>{props.assetLabel}</Text>
            <Text style={MESSAGE}>{staticsStrings.indiquez_le_seuil}</Text>
            <BkbInput style={{width:metrics.heightPercentageToDP(35),borderColor:"#ffffff90"}} placeholder={"XXXXXXX Dhs"} inputType={"login"} value={seuil} onChangeText={setSeuil}/>

            <BkbButton  style={{width:metrics.heightPercentageToDP(15),height:metrics.heightPercentageToDP(6),margin:metrics.heightPercentageToDP(2)}} ButtonType={"white"} ButtonText={staticsStrings.enregistrer} onPress={()=>{setWidthCard(46);setShowAdd(false);setModalNotif(true)}}/>
        </View>
   </BkbModal>

   {/* modal 2 notifications  */}
   <BkbModal isVisibleState={modalNotif}  style={{height:metrics.heightPercentageToDP(20)}}>
   <Pressable onPress={()=>{setModalNotif(false)}} style={{right:metrics.widthPercentageToDP(4),alignSelf:"flex-end",top:metrics.heightPercentageToDP(-1)}}>
        <Image  source={x_annuler}/>
        </Pressable>
        <View style={NOTFICATION_CONTAINER}>
              <Image source={notification_modal}  />
            <Text style={TITRE}>Atlanta Sanad</Text>
            <Text style={MESSAGE}>{staticsStrings.notification_succes}</Text>
            
        </View>
   </BkbModal>
   {/* modal 3 watchlist  */}
   <BkbModal isVisibleState={modalWatchList}  style={{height:metrics.heightPercentageToDP(20)}}>
   <Pressable onPress={()=>{setModalWatchList(false)}} style={{right:metrics.widthPercentageToDP(4),alignSelf:"flex-end",top:metrics.heightPercentageToDP(-1)}}>
        <Image  source={x_annuler}/>
        </Pressable>
        <View style={NOTFICATION_CONTAINER}>
              <Image source={watchlist_modal}  />
            <Text style={TITRE}>{props.assetLabel}</Text>
            <Text style={MESSAGE}>{staticsStrings.watchlist_succes}</Text>
            
        </View>
   </BkbModal>



    </View>
  )
})

//styles 

const ROW_CARD : ViewStyle={
  flexDirection:"row",
}
const CARD_STYLE : ViewStyle={
  maxHeight:metrics.heightPercentageToDP(30),
  width:metrics.heightPercentageToDP(40),
  flexDirection:'column',

  margin:metrics.heightPercentageToDP(1),
  // backgroundColor:'#ffffff30',
  backgroundColor:"rgba(63,106,145,0.55)",
  borderWidth:metrics.heightPercentageToDP(0.022),
  borderColor:color.palette.white,
  borderRadius:metrics.heightPercentageToDP(1),
  padding:metrics.heightPercentageToDP(1.5),
  borderRightWidth:metrics.heightPercentageToDP(0.03),
  borderTopWidth:metrics.heightPercentageToDP(0.023),
  // maxWidth:metrics.heightPercentageToDP(60)
  marginLeft:metrics.heightPercentageToDP(1.5)

}
const assetCodeStyle : TextStyle={
  fontFamily:'Sora-Bold',
  fontSize:metrics.heightPercentageToDP(1.4),
}
const ROW_1 : ViewStyle={
  flexDirection:'row',
  justifyContent:'space-between',
}
const assetLabelStyle : TextStyle={
  fontFamily:'Sora-Bold'
}
const UP_DOWN_STYLE : ViewStyle={
  backgroundColor:color.palette.white,
  maxWidth:metrics.heightPercentageToDP(20),
  width:metrics.heightPercentageToDP(7),
  justifyContent:"center",
  alignContent:'center',
  alignItems:'center',
  borderRadius:metrics.heightPercentageToDP(1.6),
  padding:metrics.heightPercentageToDP(0.1),
}
const UP_DOWN_TEXT : TextStyle={
  color:color.palette.BKbGreenColor1,
  fontFamily:"Sora-Bold",
  fontSize:metrics.heightPercentageToDP(1.5),
  
}

const ROW_2 : ViewStyle={
  flexDirection:'row',
  // justifyContent:'space-between',
  justifyContent: 'center',
  alignSelf:'flex-start',
  alignContent:'center',
  alignItems:'center',
  marginTop:metrics.heightPercentageToDP(0.3),

}
const PTO_STYLE : TextStyle={
  color:color.palette.BKBBlueColor,
  fontFamily:"Sora-Bold",
  fontSize:metrics.heightPercentageToDP(1.3),
  marginLeft:metrics.heightPercentageToDP(0.5),
}

const AJOUTER : ViewStyle={
  // backgroundColor:hexToRgbA("#ffffff",0.25),
  backgroundColor:'rrgba(63,106,145,0.55)',
  height:metrics.heightPercentageToDP(5),
  width:metrics.heightPercentageToDP(5),
  alignItems:'center',
  alignContent:'center',
  justifyContent:'center',
  borderRadius:metrics.heightPercentageToDP(1.5),
  alignSelf:'center',
  // margin:metrics.heightPercentageToDP(1.5),
  // marginRight:metrics.heightPercentageToDP(3),
  marginLeft:metrics.heightPercentageToDP(0.5),
  marginTop:metrics.heightPercentageToDP(-0.5),
  // borderTopWidth:metrics.heightPercentageToDP(0.1),
  borderColor:color.palette.white


  

}
//styles of modals
const NOTFICATION_CONTAINER : ViewStyle={
    alignSelf:'center',
    alignContent:'center',
    alignItems:'center',
}
const TITRE : TextStyle={
  fontFamily:"Sora-Bold",
  fontSize:metrics.heightPercentageToDP(2.6)
}
const MESSAGE : TextStyle={
  textAlign:"center",
  fontFamily:"Sora-Regular",
  fontSize:metrics.heightPercentageToDP(1.66)
}
