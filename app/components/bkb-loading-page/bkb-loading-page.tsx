import * as React from "react"
import { Dimensions, StyleProp, TextStyle, View, ViewStyle } from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { flatten } from "ramda"
import { BkbLoading } from "../bkb-loading/bkb-loading"
import metrics from "../../theme/metrics"

const CONTAINER: ViewStyle = {
  justifyContent: "center",
}



export interface BkbLoadingPageProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: StyleProp<ViewStyle>
}

/**
 * Describe your component here
 */
export const BkbLoadingPage = observer(function BkbLoadingPage(props: BkbLoadingPageProps) {
  const { style } = props
  const styles = flatten([CONTAINER, style])

  return (
    <View  style={CONTAINER_PAGE}>
      <BkbLoading color={color.palette.BKBBlueColor1} size={60}/>
    </View>
  )
})

const CONTAINER_PAGE : ViewStyle = {
  marginTop:metrics.heightPercentageToDP(15),
  alignSelf:"center",
  alignItems:'center',
  alignContent:'center',
  width:Dimensions.get("window").width/2,
  height:Dimensions.get("window").height/3

  
}