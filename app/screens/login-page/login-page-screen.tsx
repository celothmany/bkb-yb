import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { ImageBackground, ImageStyle, TextStyle, TouchableOpacity, View, ViewStyle, Image } from "react-native"
import { BkbButton, BkbButtonValider, BkbFooter, BkbInput, BkbInputLogin, BkbLoading, BkbTransparantView, BkbTransparentCard, ComponentFake, Text } from "../../components"
import { color } from "../../theme"
import { useStores } from "../../models"
import { NavigationInjectedProps } from "react-navigation"
import metrics from "../../theme/metrics"
import staticsStrings from "../../localData/staticStrings.json"


//icons & images
const pwd = require("../../../assets/images/lock.png")
const userMail = require("../../../assets/images/user.png")
const valide = require("../../../assets/images/valide_login.png")
const error = require("../../../assets/images/error_login.png")
const backgroundImage = require("../../../assets/images/back_ground_image.png")
const bkb_logo = require("../../../assets/images/bkb_logo.png")



//login compte
// "TESTWEB","TESTPATIOWFG","W"

export interface LoginPageProps extends NavigationInjectedProps<{}> {
}
export const LoginPageScreen: FC<LoginPageProps> = observer((props) => {
  const { ProfileUserStore } = useStores()


  const [loginCode, setloginCode] = useState("");
  const [password, setPassword] = useState("");

  const [messageError, setMessageError] = useState("")
  const handleLoginError = (detail_code: number, resultcode: number) => {
    if (detail_code === -1 && resultcode === 1) {
      return error
    }
    if (detail_code === 3 && resultcode === 3) {
      return error
    }
    if (detail_code === 1 && resultcode === 3) {
      return error
    }
    if (detail_code === 1111 || resultcode === 1111) {
      return null
    }
    if (detail_code === 0 && resultcode === 0) {
      return valide
    }

  }
  const handlePasswordError = (detail_code: number, resultcode: number) => {
    if (detail_code === -1 && resultcode === 1) {
      return error
    }
    if (detail_code === 3 && resultcode === 3) {
      return error
    }
    if (detail_code === 2 && resultcode === 3) {
      return error
    }
    if (detail_code === 1111 || resultcode === 1111) {
      return null
    }
    if (detail_code === 0 && resultcode === 0) {
      return valide
    }

  }
  const messageErrorLogin = "Le nom  d'utilisateur  saisie ne correspond pas à l’identifiant saisi. Veuillez réessayer ou contactez-nous pour de l’assistance";
  const messageErrorPassword = "Le mot de passe saisie ne correspond pas à l’identifiant saisi. Veuillez réessayer ou contactez-nous pour de l’assistance";
  const messageErrorIdentifiant = "Identifiant ou mot de passe incorrect. Veuillez réessayer ou contactez-nous pour de l’assistance";
  const handleMessageError = (detail_code: number, resultcode: number) => {
    if (detail_code === 1 && resultcode === 3) {
      // setMessageError(messageErrorLogin)

      return true
    }
    if (detail_code === -1 && resultcode === 1) {
      // setMessageError(messageErrorIdentifiant)

      return true
    }
    if (detail_code === 3 && resultcode === 3) {
      // setMessageError(messageErrorIdentifiant)

      return true
    }
    if (detail_code === 2 && resultcode === 3) {
      // setMessageError(messageErrorPassword)

      return true
    }
    if (detail_code === 1111 || resultcode === 1111) {
      return null
    }
    if (detail_code === 0 && resultcode === 0) {
      return false
    }
  }
  const handleMessageErrorIdentifant = (detail_code: number, resultcode: number) => {
    if (detail_code === 1 && resultcode === 3) {
      // setMessageError(messageErrorLogin)

    }
    if (detail_code === -1 && resultcode === 1) {
      // setMessageError(messageErrorIdentifiant)

    }
    if (detail_code === 3 && resultcode === 3) {
      // setMessageError(messageErrorIdentifiant)

    }
    if (detail_code === 2 && resultcode === 3) {
      // setMessageError(messageErrorPassword)

    }
    if (detail_code === 1111 || resultcode === 1111) {
      // setMessageError("")

    }
    if (detail_code === 0 && resultcode === 0) {

      // setMessageError("")

    }
    return messageError
  }
  const StoreIdentifiant = () => {
    if (ProfileUserStore.keepConnected === true) {
      // setloginCode(ProfileUserStore.loginCode)
      // setPassword(ProfileUserStore.password)
    }
  }

  useEffect(() => {
    if (ProfileUserStore.keepConnected === true) {
      setloginCode(ProfileUserStore.loginCode)
      setPassword(ProfileUserStore.password)
    }
  }, [])


  return (

    <ImageBackground
      source={backgroundImage}
      style={BACKGROUNDSCREEN}
    >

      <Image style={LOGO_BKB} source={bkb_logo} />

      <BkbTransparantView
        style={TRANSPARANT_CARD}
      >

        <Text style={TITLE_BKB}>{staticsStrings.seconnecter}</Text>
        <BkbInputLogin
          inputType={"login"}
          value={undefined}
          onChangeText={undefined}
          placeholder={"Email"}
          iconLeft={userMail}
          iconRight={handleLoginError(ProfileUserStore.detailcode, ProfileUserStore.resultcode)}

        />
        <BkbInputLogin
          inputType={"login"}
          value={undefined}
          onChangeText={undefined}
          placeholder={"Password"}
          iconLeft={pwd}
          iconRight={handleLoginError(ProfileUserStore.detailcode, ProfileUserStore.resultcode)}

        />

        <BkbButtonValider
          ButtonType={"white"}
          ButtonText={"Sign in"}
          onPress={undefined}
        />

        <TouchableOpacity style={FORGET_PASSWORD_TOUCH}>
          <Text style={FORGET_PASSWORD_TEXT}>{"Mot de passe oublié ?"}</Text>
        </TouchableOpacity>
      </BkbTransparantView>
      {
        //  handleMessageError(ProfileUserStore.detailcode,ProfileUserStore.resultcode) &&
        <BkbTransparentCard
          gradientColor={"#fff"}
          gradient={true}
          style={GRADIENT_CARD}
          hexaColorCard={color.palette.redGradient} opacityColorCard={0.6}
        >
          <Text style={{ fontFamily: "Sora-Bold" }}>{staticsStrings.cher_client}</Text>
          <Text style={GRADIENT_TEXT}>{staticsStrings.message_error_cher_client}</Text>
        </BkbTransparentCard>
      }



      <BkbFooter />
    </ImageBackground>

  )
})

//style of back ground image
const BACKGROUNDSCREEN: ImageStyle = {
  height: metrics.heightPercentageToDP(100),
  width: metrics.widthPercentageToDP(100),

}
//styles of cards
const TRANSPARANT_CARD: ViewStyle = {
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  marginTop: metrics.heightPercentageToDP(10),
  marginBottom: metrics.heightPercentageToDP(10),
}
const GRADIENT_CARD: ViewStyle = {
  height: metrics.heightPercentageToDP(15),
  padding: metrics.heightPercentageToDP(2)
}
const GRADIENT_TEXT: TextStyle = {
  textAlign: "center",
  fontSize: metrics.heightPercentageToDP(1.69),
  fontFamily: "Sora-Regular",
  color: color.palette.BKbBackDropColor
}
const LOGO_BKB: ImageStyle = {
  alignSelf: 'center',
  marginTop: metrics.heightPercentageToDP(4),
}
const TITLE_BKB: TextStyle = {
  fontFamily: "Sora-SemiBold",
  textAlign: "center",
  marginBottom: metrics.heightPercentageToDP(4)
}
const FORGET_PASSWORD_TEXT: TextStyle = {
  textDecorationLine: "underline",
  fontSize: metrics.heightPercentageToDP(2),
}
const FORGET_PASSWORD_TOUCH: TextStyle = {
  marginTop: metrics.heightPercentageToDP(2),


}