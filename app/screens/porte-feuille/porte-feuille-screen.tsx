import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { View, ViewStyle,TextStyle, RefreshControl } from "react-native"
import { BkbCardTitreValue, BkbLineTable, BkbLoading, BkbLoadingPage, BkbPagination, BkbScreen, BkbTitreCard, BkbTransparentCard, Text } from "../../components"
import { color } from "../../theme"
import { NavigationInjectedProps } from "react-navigation"
import metrics from "../../theme/metrics"
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler"
import { useIsFocused } from "@react-navigation/native"
import { useStores } from "../../models"
import staticsStrings from "../../localData/staticStrings.json"
import GlobalFunctions from "../../classes/GlobalFunctions"
//images & icons 


export interface PorteFeuilleProps extends NavigationInjectedProps<{}> {
  route
}

export const PorteFeuilleScreen:FC<PorteFeuilleProps> = observer((props)=> {
  const { ProfileUserStore,SyntheseStore,TitreseStore,ValuesStore} = useStores()
  const [refreshing, setRefreshing] = useState(false);
  const [paginationTitres, setPaginationTitres] = useState(10);
  const [paginationValues, setPaginationValues] = useState(1);
  const [paginationHeight, setPaginationHeight] = useState(100);

  const [tab,setTab]=useState(0)
  const [filter,setFilter]=useState(0)

  const isFocused = useIsFocused();
  const GetSynthesisinRef=async ()=>{
   await  SyntheseStore.GetSynthesisinRef(ProfileUserStore.login,ProfileUserStore.contextId,ProfileUserStore.accountCode,3)

  }
  const GetTitres=async ()=>{
    await  TitreseStore.GetTitres(ProfileUserStore.login,ProfileUserStore.contextId,ProfileUserStore.accountCode,10)
 
   }
   const GetValues=async ()=>{
    await  ValuesStore.GetValuesBKB(ProfileUserStore.login,ProfileUserStore.contextId,ProfileUserStore.accountCode,3)
 
   }
  const getMaxdate=(date)=>{
        var year=date.substring(0,4)
        var month=date.substring(4,6)
        var day=date.substring(6,8)
        return day + "/"+month+"/"+year

  }
  //refreshing synthese
  const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
  }
  const onRefreshSynthese = React.useCallback(() => {
    GetSynthesisinRef()
    wait(5000).then(() => setRefreshing(false));
  }, []);

  const onRefreshTitres = React.useCallback(() => {
    GetTitres()
    wait(5000).then(() => setRefreshing(false));
  }, []);

  const onRefreshValues = React.useCallback(() => {
    GetValues()
    wait(5000).then(() => setRefreshing(false));
  }, []);

  // function ParseFloat(str,val) {
  //   str = str.toString();
  //   str = str.slice(0, (str.indexOf(".")) + val + 1); 
  //   return Number(str);   
 
  // }
  const globalFunctions=new GlobalFunctions();

  const filterTabFunc=(filterTab)=>{
      if(filterTab===0){
        return "Toutes"
      }
      if(filterTab===1){
        return "Realises"
      }
      if(filterTab===2){
        return "Latentes"
      }
  }
  useEffect(()=>{
    GetSynthesisinRef()
    GetTitres()
    GetValues()
  },[isFocused])
  const pourcentagePoids=(a,b)=>{
    if( a<0 || b <0){
      return 0.0
    }
    else{
      if (b===0){
        return 0.0
      }
      else{
          return (a*100/b).toFixed(2)
      
      }
    }
      
  }
  useEffect(()=>{
  //  console.log(pourcentagePoids(SyntheseStore.assetValuationAmount,SyntheseStore.globalValuationAmount))
 
  },[isFocused])
  return (
    <BkbScreen
    onPressSearch={()=>{
      // console.log("search")
    }}
    navigation={props.navigation}
    headerShow
    styleParent={{
      height:metrics.heightPercentageToDP(15),

    }}
     style={ROOT}>

     <ScrollView style={{height:metrics.heightPercentageToDP(paginationHeight)}}>
     <View style={TABS}>
         {/* Tabs */}
          <TouchableOpacity 
          style={TAB_STYLE}
          onPress={()=>{setTab(0),GetSynthesisinRef()}}><Text style={tab===0 ? isTabStyle : isNotTabStyle}>{staticsStrings.synthese}</Text>
          {
          tab===0 && <View style={underlineTab}/> 
          }
          </TouchableOpacity>

          <TouchableOpacity  onPress={()=>{setTab(1),GetSynthesisinRef()}}><Text style={tab===1 ? isTabStyle : isNotTabStyle} >{staticsStrings.couverture}</Text>
          {
          tab===1 && <View style={underlineTab}/> 
          }
          </TouchableOpacity>

          <TouchableOpacity  
           style={TAB_STYLE}
          onPress={()=>{setTab(2),GetTitres()}}><Text style={tab===2 ? isTabStyle : isNotTabStyle}>{staticsStrings.titres}</Text>
          {
          tab===2 && <View style={underlineTab}/> 
          }
          </TouchableOpacity>

          <TouchableOpacity  
           style={TAB_STYLE}
          onPress={()=>{setTab(3),GetValues()}}><Text style={tab===3 ? isTabStyle : isNotTabStyle}>{staticsStrings.values}</Text>
          {
          tab===3 && <View style={underlineTab}/> 
          }
          </TouchableOpacity>
       </View>


      {
        // Synthese
        tab===0 &&  
        <ScrollView
        refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefreshSynthese} tintColor ={"red"}  />}
        >
          {SyntheseStore.loadingSynthese && <BkbLoading color={color.palette.white} size={60}/>}
        <BkbTransparentCard 
        style={SYNTHESE_TABLE}
        hexaColorCard={undefined} opacityColorCard={undefined}>
          <BkbLineTable isBlurLine={true} style={{width:metrics.heightPercentageToDP(45)}} >
                  <Text style={TITRE_TABLE_STYLE}>{staticsStrings.Positions}</Text>
                  <Text style={TITRE_TABLE_STYLE}>{SyntheseStore.currency}</Text>
                  <Text style={TITRE_TABLE_STYLE}>{staticsStrings.dontBloques}</Text>
                  <Text style={TITRE_TABLE_STYLE}>{staticsStrings.poids+" %"}</Text>
          </BkbLineTable>
          <BkbLineTable isBlurLine={false} style={{width:metrics.heightPercentageToDP(45)}}>
                  <Text style={[CELL_TABLE_STYLE,COLUMN_TABLE_STYLE]}>{staticsStrings.evaluation_titres}</Text>
                  <Text style={CELL_TABLE_STYLE}>{SyntheseStore.assetValuationAmount}</Text>
                  <Text style={CELL_TABLE_STYLE}>{SyntheseStore.bloquedValuationAmount}</Text>
                  <Text style={[CELL_TABLE_STYLE,{}]}>{pourcentagePoids(SyntheseStore.assetValuationAmount,SyntheseStore.globalValuationAmount)+"%"}</Text>
          </BkbLineTable>
          <BkbLineTable isBlurLine={true}  style={{width:metrics.heightPercentageToDP(45)}}>
                  <Text style={[CELL_TABLE_STYLE,COLUMN_TABLE_STYLE]}>{staticsStrings.solde_especes}</Text>
                  <Text style={CELL_TABLE_STYLE}>{SyntheseStore.cashValuationAmount}</Text>
                  <Text style={CELL_TABLE_STYLE}>{"-"}</Text>
                  <Text style={[CELL_TABLE_STYLE,{width:metrics.heightPercentageToDP(6)}]}>{pourcentagePoids(SyntheseStore.cashValuationAmount,SyntheseStore.assetValuationAmount)+"%"}</Text>
          </BkbLineTable>
          <BkbLineTable isBlurLine={false} style={{width:metrics.heightPercentageToDP(45)}}>
                  {/* <Text style={[TITRE_TABLE_STYLE,COLUMN_TABLE_STYLE]}>{"Total au "+getMaxdate(SyntheseStore.assetQuoteMaxDate.toString())}</Text>
                  <Text style={FOOTER_TABLE_STYLE}>{SyntheseStore.globalValuationAmount}</Text>
                  <Text style={FOOTER_TABLE_STYLE}>{"-"}</Text>
                  <Text style={FOOTER_TABLE_STYLE}>{"-"}</Text> */}
                  <Text style={[CELL_TABLE_STYLE,COLUMN_TABLE_STYLE,{fontFamily:"Sora-Bold"}]}>{"Total au "+getMaxdate(SyntheseStore.assetQuoteMaxDate.toString())}</Text>
                  <Text style={[CELL_TABLE_STYLE,{fontFamily:"Sora-Bold"}]}>{SyntheseStore.globalValuationAmount}</Text>
                  <Text style={[CELL_TABLE_STYLE,{fontFamily:"Sora-Bold"}]}>{"-"}</Text>
                  <Text style={[CELL_TABLE_STYLE,{fontFamily:"Sora-Bold"}]}>{"-"}</Text>
          </BkbLineTable>       
        </BkbTransparentCard>
        

        
      </ScrollView>
      }
      {
        // Couverture
        tab===1 &&  
        <ScrollView
        refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefreshSynthese} tintColor ={"red"}  />}

        >
          {SyntheseStore.loadingSynthese && <BkbLoading color={color.palette.white} size={60}/>}

          <BkbTransparentCard style={COUVERTURE_TABLE} hexaColorCard={undefined} opacityColorCard={undefined}>
          <BkbLineTable isBlurLine={true}>
                  <Text style={TITRE_TABLE_STYLE}>{staticsStrings.marche}</Text>
                  <Text style={TITRE_TABLE_STYLE}>{staticsStrings.engagement}</Text>
                  <Text style={TITRE_TABLE_STYLE}>{staticsStrings.disponible}</Text>
          </BkbLineTable>

          {
            SyntheseStore.buyingPowerList.map((buyingPowerList,index)=>{
              return(
                <BkbLineTable isBlurLine={!(index %2===0)}>
                  <Text style={CELL_TABLE_STYLE}>{buyingPowerList.marketCode}</Text>
                  <Text style={CELL_TABLE_STYLE}>{buyingPowerList.engagedAmount}</Text>
                  <Text style={CELL_TABLE_STYLE}>{buyingPowerList.availlableAmount}</Text>
              </BkbLineTable>              
              )
            })
          }
          </BkbTransparentCard>

      </ScrollView>
      }
      {
        // Titres
        tab===2 &&      
        <ScrollView
        style={{height:metrics.heightPercentageToDP(100+paginationTitres*30)}}
        refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefreshValues} tintColor ={"red"}  />}
        >
        {TitreseStore.loadingTitres && <BkbLoading color={color.palette.white} size={60}/>}
        {
          TitreseStore.plainInventoryElementList.slice(0,paginationTitres).map((plainInventoryElementList,index)=>{
            if(index>3){
              return(
                <BkbTitreCard
                onPress={()=>{}}
                typeCard="Titres"
                key={index} 
                quantite={plainInventoryElementList.quantity}
                px_rev={plainInventoryElementList.buyingCostPrice}
                pmv_latente={plainInventoryElementList.pendingProfitAndLossValuation}
                cours={plainInventoryElementList.assetPrice}
                valorisation={plainInventoryElementList.valuationAmount}
                poids={0} 
                titreLabel={plainInventoryElementList.assetLabel}/>
              );
            }
          })
        }
        <BkbPagination onPressPagination={()=>{setPaginationTitres(paginationTitres+1);setPaginationHeight(paginationHeight+paginationTitres*10)}}/>
      </ScrollView>
      }
      {
        // +/-Values
        tab===3 && 
         
          <View>
            {
              ValuesStore.loadingValues  ? (
                <BkbLoadingPage/>

              ):(<View>
              {/* Latentes & Realisées & Total Cards */}
  
          <Text style={{fontFamily:'Sora-Bold',margin:metrics.heightPercentageToDP(1)}}>{staticsStrings.values+" en YTD au :  "+getMaxdate(ValuesStore.accountingDate.toString())}</Text>
          <ScrollView horizontal={true}>
            
           <BkbCardTitreValue >
              <View style={TOTAL_COANTAINER}><Text style={TOTAL_TEXT}>{staticsStrings.latentes}</Text></View>
              <View style={TOTAL_COANTAINER_VALUE}><Text style={TOTAL_VALUE}>{globalFunctions.ParseFloat(ValuesStore.totalPendingPNL,2)+" MAD" }</Text></View>
            </BkbCardTitreValue>
  
            <BkbCardTitreValue  >
              <View style={TOTAL_COANTAINER}><Text style={TOTAL_TEXT}>{staticsStrings.realisees}</Text></View>
              <View style={TOTAL_COANTAINER_VALUE}><Text style={TOTAL_VALUE}>{globalFunctions.ParseFloat(ValuesStore.totalRealizedPNL,2)+" MAD"}</Text></View>
            </BkbCardTitreValue>
  
            <BkbCardTitreValue >
              <View style={TOTAL_COANTAINER}><Text style={TOTAL_TEXT}>{"Total"+staticsStrings.values}</Text></View>
              <View style={TOTAL_COANTAINER_VALUE}><Text style={TOTAL_VALUE}>{globalFunctions.ParseFloat(ValuesStore.totalPendingPNL+ValuesStore.totalRealizedPNL,2)+" MAD"}</Text></View>
            </BkbCardTitreValue>
  
            {/* <BkbTransparentCard  style={VALUE_CARD} hexaColorCard={undefined} opacityColorCard={undefined}>
              <View style={TOTAL_COANTAINER}><Text style={TOTAL_TEXT}>{"Total"+staticsStrings.values}</Text></View>
              <View style={TOTAL_COANTAINER_VALUE}><Text style={TOTAL_VALUE}>{globalFunctions.ParseFloat(ValuesStore.totalSecuritiesValuation,2)+" "+ValuesStore.plainInventoryElementList[0].assetCurrency}</Text></View>
            </BkbTransparentCard> */}
  
          </ScrollView>
  
              {/* Latentes & Realisées & Toutes Filter */}
  
            <View style={FILTER_VALUES}>
              <TouchableOpacity onPress={()=>{setFilter(0)}} style={filter===0 ? FILTER_TABBED : FILTER_NOTABBED}>
                <Text style={FILTER_TEXT}>{staticsStrings.toutes}</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>{setFilter(1)}} style={filter===1 ? FILTER_TABBED : FILTER_NOTABBED}>
                <Text style={FILTER_TEXT}>{staticsStrings.realisees}</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>{setFilter(2)}} style={filter===2 ? FILTER_TABBED : FILTER_NOTABBED}>
                <Text style={FILTER_TEXT}>{staticsStrings.latentes}</Text>
              </TouchableOpacity>
  
            </View>
  
          <ScrollView
          refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefreshTitres} tintColor ={"red"}  />}
  
          >
          {ValuesStore.loadingValues && <BkbLoading color={color.palette.white} size={60}/>}
          {
            ValuesStore.plainInventoryElementList.slice(0,paginationValues).map((plainInventoryElementList,index)=>{
                return(
                  <BkbTitreCard
                  onPress={()=>{filter===2?
                    props.navigation.navigate("porte_feuille_details",{indexTitre:index})
                    // {}
                    :null
                  }}
                  FilterType={filterTabFunc(filter)}
                  typeCard="Values"
                  key={index} 
                  quantite={plainInventoryElementList.quantity}
                  px_rev={plainInventoryElementList.buyingCostPrice}
                  pmv_latente={plainInventoryElementList.pendingProfitAndLossValuation}
                  cours={plainInventoryElementList.assetPrice}
                  valorisation={plainInventoryElementList.valuationAmount}
                  poids={plainInventoryElementList.pendingProfitAndLossPercent} 
                  titreLabel={plainInventoryElementList.assetLabel}/>
                );
            })
          }
          <BkbPagination onPressPagination={()=>{setPaginationValues(paginationValues+1);setPaginationHeight(100);setPaginationHeight(paginationHeight+paginationTitres*10)}}/>  
        </ScrollView>
         </View>) 
              
            }
          </View>


      }

     
      
      
     </ScrollView>

    </BkbScreen>
  )
})
//styles
const ROOT: ViewStyle = {
 
}
//tabs styles
const TABS: ViewStyle = {
  // backgroundColor:'#ffffff20',
  backgroundColor:'rgba(63,106,145,0.7)',
  flexDirection:'row',
  justifyContent:'space-around',
  height:metrics.heightPercentageToDP(3.52),
  borderBottomEndRadius:metrics.heightPercentageToDP(2),
  borderBottomStartRadius:metrics.heightPercentageToDP(2),
  paddingHorizontal:metrics.heightPercentageToDP(1),

}
const underlineTab :ViewStyle={
  height:metrics.heightPercentageToDP(0.25),
  backgroundColor:color.palette.white,
  borderRadius:metrics.heightPercentageToDP(2),
  marginTop:metrics.heightPercentageToDP(0.5),
}
const TAB_STYLE : ViewStyle={
  width:metrics.heightPercentageToDP(11),
 
}

//styles of textes
const isTabStyle : TextStyle={
  fontFamily:'Sora-ExtraBold',
  textAlign:'center',


}
const isNotTabStyle : TextStyle={
  fontFamily:'Sora-Light',
  textAlign:'center',
  

  
}
//styles of table synthese
const SYNTHESE_TABLE : ViewStyle={
  marginTop:metrics.heightPercentageToDP(3),
  padding:metrics.heightPercentageToDP(2),
  width:metrics.heightPercentageToDP(47),
  marginHorizontal:metrics.heightPercentageToDP(1),
  alignSelf:'center',
  height:metrics.heightPercentageToDP(25),
  maxHeight:metrics.heightPercentageToDP(100),
  borderWidth:metrics.heightPercentageToDP(0.03),
}
const TITRE_TABLE_STYLE : TextStyle={
  width:metrics.heightPercentageToDP(11.5),
  fontFamily:'Sora-ExtraBold',
  fontSize:metrics.heightPercentageToDP(1.5),
  textAlign:'left'
}
const FOOTER_TABLE_STYLE : TextStyle={
  // maxWidth:metrics.heightPercentageToDP(11),
  width:metrics.heightPercentageToDP(11.51),
  fontFamily:'Sora-Bold',
  fontSize:metrics.heightPercentageToDP(1.5),
  textAlign:'center',


}

const CELL_TABLE_STYLE : TextStyle={
  //maxWidth:metrics.heightPercentageToDP(15),
  width:metrics.heightPercentageToDP(14),
  fontSize:metrics.heightPercentageToDP(1.3),
  fontFamily:"Sora-Regular",
  textAlign:'left',
 
}
const COLUMN_TABLE_STYLE : TextStyle={
  maxWidth:metrics.heightPercentageToDP(9),  
  textAlign:'left',
  fontSize:metrics.heightPercentageToDP(1.3),

}
//styles of couverture 
const COUVERTURE_TABLE : ViewStyle={
  marginTop:metrics.heightPercentageToDP(3),
  padding:metrics.heightPercentageToDP(2),
  width:metrics.heightPercentageToDP(45),
  marginHorizontal:metrics.heightPercentageToDP(1),
  alignSelf:'center',
  height:metrics.heightPercentageToDP(18),
  maxHeight:metrics.heightPercentageToDP(100),
  borderWidth:metrics.heightPercentageToDP(0.03),

}
//styles of values 
const VALUE_CARD : ViewStyle={
    marginHorizontal:metrics.heightPercentageToDP(2),
    marginVertical:metrics.heightPercentageToDP(2),
    width:metrics.heightPercentageToDP(30),
    height:metrics.heightPercentageToDP(15),
    padding:metrics.heightPercentageToDP(1)
    
}
const TOTAL_COANTAINER :ViewStyle={
  width:metrics.heightPercentageToDP(25)

}
const TOTAL_COANTAINER_VALUE :ViewStyle={
  width:metrics.heightPercentageToDP(25)

}
const TOTAL_TEXT : TextStyle={
  textAlign:'left',
  fontFamily:'Sora-Bold',
  fontSize:metrics.heightPercentageToDP(1.5)

}

const TOTAL_VALUE : TextStyle={
  textAlign:'left',
  fontFamily:'Sora-ExtraBold',
  color:color.palette.BKBGreenColor,
  marginTop:metrics.heightPercentageToDP(1.5),
  fontSize:metrics.heightPercentageToDP(2.5),
  maxWidth:metrics.heightPercentageToDP(50)
}
//styles of filter
const FILTER_VALUES : ViewStyle={
  flexDirection:'row'
}
const FILTER_TABBED: ViewStyle={
  marginHorizontal:metrics.heightPercentageToDP(1),
  borderRadius:metrics.heightPercentageToDP(3),
  padding:metrics.heightPercentageToDP(0.5),
  width:metrics.heightPercentageToDP(10),
  backgroundColor:color.palette.BKBBlueColor


}
const FILTER_NOTABBED: ViewStyle={
  marginHorizontal:metrics.heightPercentageToDP(1),
  borderRadius:metrics.heightPercentageToDP(3),
  padding:metrics.heightPercentageToDP(0.5),
  width:metrics.heightPercentageToDP(10),
  backgroundColor:"#FFFFFF35"
  
}
const FILTER_TEXT : TextStyle={
  fontFamily:'Sora-Bold',
  fontSize:metrics.heightPercentageToDP(1.5),
  textAlign:"center",

}