import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { TextStyle, TouchableOpacity, View, ViewStyle,Text, RefreshControl } from "react-native"
import { BkbCardTransaction, BkbHistoriqueCard, BkbLoading, BkbScreen } from "../../components"

import { color } from "../../theme"
import { NavigationInjectedProps } from "react-navigation"
import metrics from "../../theme/metrics"
import staticsStrings from "../../localData/staticStrings.json"
import { ScrollView } from "react-native-gesture-handler"
import { useStores } from "../../models"
import { useIsFocused } from "@react-navigation/native"
import GlobalFunctions from "../../classes/GlobalFunctions"

export interface TransactionsAppProps extends NavigationInjectedProps<{}> {
  route
}
export const TransactionsAppScreen : FC<TransactionsAppProps>= observer((props)=> {
  const {ProfileUserStore,CarnetOrdreStore,HistoryStore} = useStores()
  const [tabTransaction,setTabTransaction]=useState(0);
  const [tabCarnet,setTabCarnet]=useState(0);
  const [tabFilterCarnet,setFilterTabCarnet]=useState(0);
  const [filterHistory,setfilterHistory]=useState(0);
  const [paginationOne,setPaginationOne]=useState(2);
  const [paginationTwo,setPaginationTwo]=useState(2);
  const [heightScrollOne,setHeightScrollOne]=useState(100);
  const [heightScrollTwo,setHeightScrollTwo]=useState(100);
  const [refreshing, setRefreshing] = useState(false);
 
  const isFocused = useIsFocused();

  const GetOrderList=async ()=>{
    await  CarnetOrdreStore.GetOrderListBKB(ProfileUserStore.login,ProfileUserStore.contextId,ProfileUserStore.accountCode)
 
   }
  const onRefreshTransactions = React.useCallback(() => {
    GetOrderList()
    globalFunctions.wait(5000).then(() => setRefreshing(false));
  }, []);
   const translateAchatVente=(translate)=>{
        if(translate==="BUY"){
          return "Achat"
        }
        if(translate==="SELL"){
          return "Vente"
        }
   }
  
 const globalFunctions=new GlobalFunctions();
 const filterStatusLabel=(tabNumber: number)=>{
   if(tabNumber===1){
     return globalFunctions.translateStatusLabelEnglish(staticsStrings.en_attente)
   }
   if(tabNumber===2){
    return globalFunctions.translateStatusLabelEnglish(staticsStrings.execute)
  }
  if(tabNumber===3){
    return globalFunctions.translateStatusLabelEnglish(staticsStrings.en_attente_annulation)
  }
  if(tabNumber===4){
    return globalFunctions.translateStatusLabelEnglish(staticsStrings.annule)
  }
  if(tabNumber===5){
    return globalFunctions.translateStatusLabelEnglish(staticsStrings.expire)
  }
  if(tabNumber===6){
    return globalFunctions.translateStatusLabelEnglish(staticsStrings.en_anomalie)
  }
  if(tabNumber===7){
    return globalFunctions.translateStatusLabelEnglish(staticsStrings.execute_partiellement)
  }
  if(tabNumber===8){
    return globalFunctions.translateStatusLabelEnglish(staticsStrings.comptablise)
  }
  if(tabNumber===9){
    return globalFunctions.translateStatusLabelEnglish(staticsStrings.ordre_execute_annule)
  }
  if(tabNumber===10){
    return globalFunctions.translateStatusLabelEnglish(staticsStrings.annulation_refusée)
  }
  if(tabNumber===11){
    return globalFunctions.translateStatusLabelEnglish(staticsStrings.rejetee)
  }
  if(tabNumber===12){
    return globalFunctions.translateStatusLabelEnglish(staticsStrings.modifié)
  }
  if(tabNumber===13){
    return globalFunctions.translateStatusLabelEnglish(staticsStrings.non_acquitté)
  }
  if(tabNumber===14){
    return globalFunctions.translateStatusLabelEnglish(staticsStrings.execute_non_comptabilisée)
  }

 }
   useEffect(()=>{
    GetOrderList()
   },[isFocused])
  return (
    <BkbScreen
    navigation={props.navigation}
    headerShow={true}
    styleParent={{
      height:metrics.heightPercentageToDP(15),

    }}
    >

      <ScrollView style={{height:metrics.heightPercentageToDP("100%")}}
        refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefreshTransactions} tintColor ={"red"}  />}

      >
      <View style={TABS}>
              {/* Tabs */}
                <TouchableOpacity 
                style={TAB_STYLE}
                onPress={()=>{setTabTransaction(0)}}><Text style={tabTransaction===0 ? isTabStyle : isNotTabStyle}>{staticsStrings.carnet_dordres}</Text>
                {
                tabTransaction===0 && <View style={underlineTab}/> 
                }
                </TouchableOpacity>

                <TouchableOpacity  onPress={()=>{setTabTransaction(1)}}><Text style={tabTransaction===1 ? isTabStyle : isNotTabStyle} >{staticsStrings.historique}</Text>
                {
                tabTransaction===1 && <View style={underlineTab}/> 
                }
                </TouchableOpacity>

              
      </View>
      {CarnetOrdreStore.loadingOrderList && <BkbLoading color={color.palette.white} size={60}/>}

     {
       tabTransaction===0 &&  
       <ScrollView>
       {/* carnet d'ordres */}
         {/* Tabs carnet d'ordres  */}
      <View style={TABS_CARNET}>
      <ScrollView  horizontal={true}>

        {/* <TouchableOpacity 
        style={TAB_STYLE}
        onPress={()=>{setTabCarnet(0)}}><Text style={tabCarnet===0 ? isTabStyleGraphic : isNotTabCarnetStyle}>{staticsStrings.en_cours}</Text>
        {
        tabCarnet===0 ? (<View style={underlinetabCarnet}/> ):(<View style={underlinetabCarnetInvisible}/> )
      }
        </TouchableOpacity> */}


        <TouchableOpacity 
        style={TAB_STYLE}
        onPress={()=>{setTabCarnet(0)}}><Text style={tabCarnet===0 ? isTabStyleGraphic : isNotTabCarnetStyle}>{staticsStrings.tous}</Text>
        {
        tabCarnet===0 ? (<View style={underlinetabCarnet}/> ):(<View style={underlinetabCarnetInvisible}/> )
      }
        </TouchableOpacity>

        <TouchableOpacity  onPress={()=>{setTabCarnet(1)}}><Text style={tabCarnet===1 ? isTabStyleGraphic : isNotTabCarnetStyle} >{staticsStrings.j_j_un}</Text>
        {
        tabCarnet===1 ? (<View style={underlinetabCarnet}/> ):(<View style={underlinetabCarnetInvisible}/> )
      }
        </TouchableOpacity>

        {/* <TouchableOpacity  
         style={TAB_STYLE}
        onPress={()=>{setTabCarnet(2)}}><Text style={tabCarnet===2 ? isTabStyleGraphic : isNotTabCarnetStyle}>{staticsStrings.veille}</Text>
        {
        tabCarnet===2 ? (<View style={underlinetabCarnet}/> ):(<View style={underlinetabCarnetInvisible}/> )
      }
        </TouchableOpacity> */}

        <TouchableOpacity  
         style={TAB_STYLE}
        onPress={()=>{setTabCarnet(3)}}><Text style={tabCarnet===3 ? isTabStyleGraphic : isNotTabCarnetStyle}>{staticsStrings.un_mois}</Text>
        {
        tabCarnet===3 ? (<View style={underlinetabCarnet}/> ):(<View style={underlinetabCarnetInvisible}/> )
      }
        </TouchableOpacity>

        <TouchableOpacity  
         style={TAB_STYLE}
        onPress={()=>{setTabCarnet(4)}}><Text style={tabCarnet===4 ? isTabStyleGraphic : isNotTabCarnetStyle}>{staticsStrings.trois_mois}</Text>
        {
        tabCarnet===4 ? (<View style={underlinetabCarnet}/> ):(<View style={underlinetabCarnetInvisible}/> )
        }
        </TouchableOpacity>
        <TouchableOpacity  
         style={TAB_STYLE}
        onPress={()=>{setTabCarnet(5)}}><Text style={tabCarnet===5 ? isTabStyleGraphic : isNotTabCarnetStyle}>{staticsStrings.six_mois}</Text>
        {
        tabCarnet===5 ? (<View style={underlinetabCarnet}/> ):(<View style={underlinetabCarnetInvisible}/> )
        }
        </TouchableOpacity>
        <TouchableOpacity  
         style={TAB_STYLE}
        onPress={()=>{setTabCarnet(6)}}><Text style={tabCarnet===6 ? isTabStyleGraphic : isNotTabCarnetStyle}>{staticsStrings.un_an}</Text>
        {
        tabCarnet===6 ? (<View style={underlinetabCarnet}/> ):(<View style={underlinetabCarnetInvisible}/> )
        }
        </TouchableOpacity>
        </ScrollView>
         </View>       
          {/* tabs data filter carnet  */}

          <ScrollView horizontal={true} style={FILTER_TABS_CARNET} contentContainerStyle={{justifyContent:"space-between",flexDirection:'row'}}>

          <TouchableOpacity onPress={()=>{setFilterTabCarnet(0)}}>
            <Text style={tabFilterCarnet===0 ?  FILTER_TABBED_CARNET : FILTER_NOT_TABBED_CARNET}>{staticsStrings.tous}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{setFilterTabCarnet(1)}}>
            <Text style={tabFilterCarnet===1 ?  FILTER_TABBED_CARNET : FILTER_NOT_TABBED_CARNET}>{staticsStrings.en_attente}</Text>
            </TouchableOpacity>   
            <TouchableOpacity onPress={()=>{setFilterTabCarnet(2)}}>
            <Text style={tabFilterCarnet===2 ?  FILTER_TABBED_CARNET : FILTER_NOT_TABBED_CARNET}>{staticsStrings.execute}</Text>
            </TouchableOpacity>  
            <TouchableOpacity onPress={()=>{setFilterTabCarnet(3)}}>
            <Text style={tabFilterCarnet===3 ?  FILTER_TABBED_CARNET : FILTER_NOT_TABBED_CARNET}>{staticsStrings.en_attente_annulation}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{setFilterTabCarnet(4)}}>
            <Text style={tabFilterCarnet===4 ?  FILTER_TABBED_CARNET : FILTER_NOT_TABBED_CARNET}>{staticsStrings.annule}</Text>
            </TouchableOpacity> 
            <TouchableOpacity onPress={()=>{setFilterTabCarnet(5)}}>
            <Text style={tabFilterCarnet===5 ?  FILTER_TABBED_CARNET : FILTER_NOT_TABBED_CARNET}>{staticsStrings.expire}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{setFilterTabCarnet(6)}}>
            <Text style={tabFilterCarnet===6 ?  FILTER_TABBED_CARNET : FILTER_NOT_TABBED_CARNET}>{staticsStrings.en_anomalie}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{setFilterTabCarnet(7)}}>
            <Text style={tabFilterCarnet===7 ?  FILTER_TABBED_CARNET : FILTER_NOT_TABBED_CARNET}>{staticsStrings.execute_partiellement}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{setFilterTabCarnet(8)}}>
            <Text style={tabFilterCarnet===8 ?  FILTER_TABBED_CARNET : FILTER_NOT_TABBED_CARNET}>{staticsStrings.comptablise}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{setFilterTabCarnet(9)}}>
            <Text style={tabFilterCarnet===9 ?  FILTER_TABBED_CARNET : FILTER_NOT_TABBED_CARNET}>{staticsStrings.ordre_execute_annule}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{setFilterTabCarnet(10)}}>
            <Text style={tabFilterCarnet===10 ?  FILTER_TABBED_CARNET : FILTER_NOT_TABBED_CARNET}>{staticsStrings.annulation_refusée}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{setFilterTabCarnet(11)}}>
            <Text style={tabFilterCarnet===11 ?  FILTER_TABBED_CARNET : FILTER_NOT_TABBED_CARNET}>{staticsStrings.rejetee}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{setFilterTabCarnet(12)}}>
            <Text style={tabFilterCarnet===12 ?  FILTER_TABBED_CARNET : FILTER_NOT_TABBED_CARNET}>{staticsStrings.modifié}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{setFilterTabCarnet(13)}}>
            <Text style={tabFilterCarnet===13 ?  FILTER_TABBED_CARNET : FILTER_NOT_TABBED_CARNET}>{staticsStrings.non_acquitté}</Text>
            </TouchableOpacity>
            {/* <TouchableOpacity onPress={()=>{setFilterTabCarnet(14)}}>
            <Text style={tabFilterCarnet===14 ?  FILTER_TABBED_CARNET : FILTER_NOT_TABBED_CARNET}>{staticsStrings.execute_non_comptabilisée}</Text>
            </TouchableOpacity> */}
          </ScrollView>
          {/* </ScrollView> */}
       
       {
        //  Tous carnet
        tabFilterCarnet===0 &&
        <ScrollView style={{height:metrics.heightPercentageToDP(heightScrollOne),paddingBottom:metrics.heightPercentageToDP(20)}}
        refreshControl={<RefreshControl refreshing={refreshing} onRefresh={onRefreshTransactions} tintColor ={"red"}  />}

        >
        {CarnetOrdreStore.loadingOrderList && <BkbLoading size={60} color={color.palette.white}/>}
        {
            CarnetOrdreStore.orderList.filter(item=>globalFunctions.filterDateRange(item.creationDate,item.creationHour,item.executionDate,tabCarnet)===true).slice(0,paginationOne).map((orderList,index)=>{
                return(
                  <BkbCardTransaction 
                  onPress={()=>{
                    props.navigation.navigate("transactions_details",
                  {
                    internalReference:orderList.internalReference,
                    assetLabel:orderList.assetLabel,
                    assetCode:orderList.assetCode,
                    statusLabel:orderList.statusLabel

                  })}} 
                  key={index} idTransactions={orderList.assetCode} heureTransaction={orderList.creationHour} 
                  nameTransaction={orderList.assetLabel} dateTransaction={orderList.creationDate} typeTransaction={translateAchatVente(orderList.sideLabel)} 
                  statusTransaction={globalFunctions.translateStatusLabelFrench(orderList.statusLabel)}/>
                );
            })
          }
          <TouchableOpacity onPress={()=>{setPaginationOne(paginationOne+3);setHeightScrollOne(heightScrollOne+30)}} style={{alignSelf:'center',margin:metrics.heightPercentageToDP(1)}}>
            <Text style={{fontFamily:'Sora-ExtraBold',color:color.palette.white}}>{staticsStrings.afficher_plus}</Text>
          </TouchableOpacity>
        </ScrollView>
       }
       {
            (tabFilterCarnet>0 && tabFilterCarnet!==4 &&tabFilterCarnet!==14 && tabFilterCarnet!==1 && tabFilterCarnet!==7) &&
            <ScrollView style={{height:metrics.heightPercentageToDP(heightScrollTwo),paddingBottom:metrics.heightPercentageToDP(20)}}>
            {CarnetOrdreStore.loadingOrderList && <BkbLoading size={60} color={color.palette.white}/>}
                  {
                CarnetOrdreStore.orderList.filter(item=>item.statusLabel===filterStatusLabel(tabFilterCarnet) && globalFunctions.filterDateRange(item.creationDate,item.creationHour,item.executionDate,tabCarnet)===true).slice(0,paginationTwo).map((orderList,index)=>{
                    return(
                      <BkbCardTransaction 
                      onPress={()=>{props.navigation.navigate("transactions_details",
                      {
                        internalReference:orderList.internalReference,
                        assetLabel:orderList.assetLabel,
                        assetCode:orderList.assetCode,
                        statusLabel:orderList.statusLabel

                      })}} 
                      key={index} idTransactions={orderList.assetCode} heureTransaction={orderList.creationHour} 
                      nameTransaction={orderList.assetLabel} dateTransaction={orderList.creationDate} typeTransaction={translateAchatVente(orderList.sideLabel)} 
                      statusTransaction={globalFunctions.translateNumberToStatusLabel(tabFilterCarnet)}/>
                    );
                })
                }
            <TouchableOpacity onPress={()=>{setPaginationTwo(paginationTwo+2);setHeightScrollTwo(heightScrollTwo+10)}} style={{alignSelf:'center',margin:metrics.heightPercentageToDP(1)}}>
            <Text style={{fontFamily:'Sora-ExtraBold',color:color.palette.white}}>{staticsStrings.afficher_plus}</Text>
          </TouchableOpacity>
              </ScrollView>
          }
        {/* annule={annule,execute non comptablise}   tabFilterCarnet===4 and tabFilterCarnet===14*/}
         {
            tabFilterCarnet===4  &&
            <ScrollView style={{height:metrics.heightPercentageToDP(heightScrollTwo),paddingBottom:metrics.heightPercentageToDP(20)}}>
            {CarnetOrdreStore.loadingOrderList && <BkbLoading size={60} color={color.palette.white}/>}
                  {
                CarnetOrdreStore.orderList.filter(item=>item.statusLabel===filterStatusLabel(4) || item.statusLabel===filterStatusLabel(14) && globalFunctions.filterDateRange(item.creationDate,item.creationHour,item.executionDate,tabCarnet)===true).slice(0,paginationTwo).map((orderList,index)=>{
                    return(
                      <BkbCardTransaction 
                      onPress={()=>{props.navigation.navigate("transactions_details",
                      {
                        internalReference:orderList.internalReference,
                        assetLabel:orderList.assetLabel,
                        assetCode:orderList.assetCode,
                        statusLabel:orderList.statusLabel

                      })}} 
                      key={index} idTransactions={orderList.assetCode} heureTransaction={orderList.creationHour} 
                      nameTransaction={orderList.assetLabel} dateTransaction={orderList.creationDate} typeTransaction={translateAchatVente(orderList.sideLabel)} 
                      statusTransaction={globalFunctions.translateStatusLabelFrench(orderList.statusLabel)}/>
                    );
                })
                }
            <TouchableOpacity onPress={()=>{setPaginationTwo(paginationTwo+2);setHeightScrollTwo(heightScrollTwo+20)}} style={{alignSelf:'center',margin:metrics.heightPercentageToDP(1)}}>
            <Text style={{fontFamily:'Sora-ExtraBold',color:color.palette.white}}>{staticsStrings.afficher_plus}</Text>
          </TouchableOpacity>
              </ScrollView>
          }


    {/* en cours={en cours,Exécuté partiellement }   tabFilterCarnet===1 and tabFilterCarnet===?*/}
     {
            tabFilterCarnet===1  &&
            <ScrollView style={{height:metrics.heightPercentageToDP(heightScrollTwo),paddingBottom:metrics.heightPercentageToDP(20)}}>
            {CarnetOrdreStore.loadingOrderList && <BkbLoading size={60} color={color.palette.white}/>}
                  {
                CarnetOrdreStore.orderList.filter(item=>item.statusLabel===filterStatusLabel(1) || item.statusLabel===filterStatusLabel(7) && globalFunctions.filterDateRange(item.creationDate,item.creationHour,item.executionDate,tabCarnet)===true).slice(0,paginationTwo).map((orderList,index)=>{
                    return(
                      <BkbCardTransaction 
                      onPress={()=>{props.navigation.navigate("transactions_details",
                      {
                        internalReference:orderList.internalReference,
                        assetLabel:orderList.assetLabel,
                        assetCode:orderList.assetCode,
                        statusLabel:orderList.statusLabel

                      })}} 
                      key={index} idTransactions={orderList.assetCode} heureTransaction={orderList.creationHour} 
                      nameTransaction={orderList.assetLabel} dateTransaction={orderList.creationDate} typeTransaction={translateAchatVente(orderList.sideLabel)} 
                      statusTransaction={globalFunctions.translateStatusLabelFrench(orderList.statusLabel)}/>
                    );
                })
                }
            <TouchableOpacity onPress={()=>{setPaginationTwo(paginationTwo+2);setHeightScrollTwo(heightScrollTwo+10)}} style={{alignSelf:'center',margin:metrics.heightPercentageToDP(1)}}>
            <Text style={{fontFamily:'Sora-ExtraBold',color:color.palette.white}}>{staticsStrings.afficher_plus}</Text>
          </TouchableOpacity>
              </ScrollView>
          }
     </ScrollView>
     }

     {
       tabTransaction===1 && 
      //  Historique
       <ScrollView  style={{paddingBottom:metrics.heightPercentageToDP(20)}} >

          {/* tous & especes & titres Filter */}

          <View style={FILTER_VALUES}>
            <TouchableOpacity onPress={()=>{setfilterHistory(0)}} style={filterHistory===0 ? FILTER_TABBED : FILTER_NOTABBED}>
              <Text style={FILTER_TEXT}>{staticsStrings.tous}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{setfilterHistory(1)}} style={filterHistory===1 ? FILTER_TABBED : FILTER_NOTABBED}>
              <Text style={FILTER_TEXT}>{staticsStrings.especes}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>{setfilterHistory(2)}} style={filterHistory===2 ? FILTER_TABBED : FILTER_NOTABBED}>
              <Text style={FILTER_TEXT}>{staticsStrings.titres}</Text>
            </TouchableOpacity>

          </View>
          <BkbHistoriqueCard 
          date_effet={"undefined"} 
          montant_net={"undefined"} 
          cours={"undefined"} 
          operation={"BUY"} 
          quantite={"undefined"}/>
          
          <BkbHistoriqueCard 
          date_effet={"undefined"} 
          montant_net={"undefined"} 
          cours={"undefined"} 
          operation={"SELL"} 
          quantite={"undefined"}/>
          <BkbHistoriqueCard 
          date_effet={"undefined"} 
          montant_net={"undefined"} 
          cours={"undefined"} 
          operation={"BUY"} 
          quantite={"undefined"}/>
      

      <TouchableOpacity onPress={()=>{}} style={{alignSelf:'center',margin:metrics.heightPercentageToDP(1)}}>
            <Text style={{fontFamily:'Sora-ExtraBold',color:color.palette.white}}>{staticsStrings.afficher_plus}</Text>
          </TouchableOpacity>

       </ScrollView>
     }
      </ScrollView>
    </BkbScreen>
  )
})
//styles
//tabs styles
const TABS: ViewStyle = {
  // backgroundColor:'#ffffff20',
  backgroundColor:"rgba(63,106,145,0.7)",
  flexDirection:'row',
  justifyContent:'space-around',
  height:metrics.heightPercentageToDP(3.5),
  borderBottomEndRadius:metrics.heightPercentageToDP(2),
  borderBottomStartRadius:metrics.heightPercentageToDP(2),
  paddingHorizontal:metrics.heightPercentageToDP(0),

}
const TABS_CARNET : ViewStyle={
  flexDirection:'row',
  height:metrics.heightPercentageToDP(4.5),
  marginTop:metrics.heightPercentageToDP(1),
  marginRight:metrics.heightPercentageToDP(1),
  marginLeft:metrics.heightPercentageToDP(1),

}
const underlineTab :ViewStyle={
  height:metrics.heightPercentageToDP(0.25),
  backgroundColor:color.palette.white,
  borderRadius:metrics.heightPercentageToDP(2),
  marginTop:metrics.heightPercentageToDP(0.5),
  width:metrics.heightPercentageToDP(20),

}
const TAB_STYLE : ViewStyle={
  width:metrics.heightPercentageToDP(11),
 
}
//styles of textes
const isTabStyle : TextStyle={
  fontFamily:'Sora-ExtraBold',
  textAlign:'center',
  color:color.palette.white,
  width:metrics.heightPercentageToDP(20),


}
const isNotTabStyle : TextStyle={
  fontFamily:'Sora-Light',
  textAlign:'center',
  color:color.palette.white,
  width:metrics.heightPercentageToDP(20),


  
}
const isNotTabCarnetStyle : TextStyle={
  fontFamily:'Sora-Light',
  textAlign:'center',
  color:"#ffffff90"

  
}
const underlinetabCarnet :ViewStyle={
  height:metrics.heightPercentageToDP(0.25),
  backgroundColor:color.palette.BKBBlueColor1,
  borderRadius:metrics.heightPercentageToDP(2),
  marginTop:metrics.heightPercentageToDP(0.5),

}
const underlinetabCarnetInvisible :ViewStyle={
  height:metrics.heightPercentageToDP(0.27),
  backgroundColor:"#ffffff20",
  borderRadius:metrics.heightPercentageToDP(0),
  marginTop:metrics.heightPercentageToDP(0.5),
  // width:metrics.heightPercentageToDP(13)
}
const isTabStyleGraphic : TextStyle={
  fontFamily:'Sora-ExtraBold',
  textAlign:'center',
  color:color.palette.BKBBlueColor1


}
//styles of filter carnet d'ordres
const FILTER_TABBED_CARNET : TextStyle={
  // backgroundColor:"#ffffff40",
  backgroundColor:"rgba(63,106,145,0.7)",
  padding:metrics.heightPercentageToDP(0.5),
  borderRadius:metrics.heightPercentageToDP(0.6),
  color:color.palette.white,
  fontFamily:'Sora-Regular',
  marginRight:metrics.heightPercentageToDP(1),
}
const FILTER_NOT_TABBED_CARNET : TextStyle={
  padding:metrics.heightPercentageToDP(0.5),
  borderRadius:metrics.heightPercentageToDP(0.6),
  color:color.palette.white,
  fontFamily:'Sora-Regular',
  marginRight:metrics.heightPercentageToDP(0.5),

}
const FILTER_TABS_CARNET : ViewStyle={
  margin:metrics.heightPercentageToDP(1.5),
  // width:metrics.heightPercentageToDP(100),
  // flexDirection:'row',
}

//styles of filter
const FILTER_VALUES : ViewStyle={
  flexDirection:'row',
  alignSelf:'center',
  margin:metrics.heightPercentageToDP(2.5)
}
const FILTER_TABBED: ViewStyle={
  marginHorizontal:metrics.heightPercentageToDP(1),
  borderRadius:metrics.heightPercentageToDP(3),
  padding:metrics.heightPercentageToDP(0.5),
  width:metrics.heightPercentageToDP(10),
  backgroundColor:color.palette.BKBBlueColor


}
const FILTER_NOTABBED: ViewStyle={
  marginHorizontal:metrics.heightPercentageToDP(1),
  borderRadius:metrics.heightPercentageToDP(3),
  padding:metrics.heightPercentageToDP(0.5),
  width:metrics.heightPercentageToDP(10),
  backgroundColor:"rgba(63,106,145,0.7)"
  
}
const FILTER_TEXT : TextStyle={
  fontFamily:'Sora-Bold',
  fontSize:metrics.heightPercentageToDP(1.5),
  textAlign:"center",
  color:color.palette.white

}