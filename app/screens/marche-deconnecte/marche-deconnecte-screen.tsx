import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle,Image,Text, TextStyle, ScrollView, TouchableOpacity, View } from "react-native"
import { BkbCardDoconnecte, BkbPalmaresFortes, BkbScreen, Switch } from "../../components"
import { color } from "../../theme"
import { NavigationInjectedProps } from "react-navigation"
import metrics from "../../theme/metrics"
import staticsStrings from "../../localData/staticStrings.json"

//images & icons 
const pto_white=require("../../../assets/images/pto_white.png")
const a_z_right=require("../../../assets/images/a_z_right.png")

export interface MarcheDeconnecteProps extends NavigationInjectedProps<{}> {
  route
}

export const MarcheDeconnecteScreen  : FC<MarcheDeconnecteProps>= observer((props)=> {
  const [tab ,setTab]=useState(0)
  const [switchPTO,setSwitchPTO]=useState(true)
  const [alphabet,setAlphabet]=useState(true)
  const onToggle =()=>{
        setSwitchPTO(!switchPTO)
  }
  const fromAToZ=(a)=>{
      if(a===true){
        return staticsStrings.a
      }
      else{
        return staticsStrings.z
      }
  }
 
 
  
  
  

  useEffect(()=>{
    
  
  },[])
  return (
    <BkbScreen
    navigation={props.navigation}
    headerShow={true}
    goBackButton={true}
  
    >
      
{/* 
      <View style={TABS} >
        <TouchableOpacity style={TAB_CONTAINER}  onPress={()=>{setTab(0)}}>
        <Text style ={tab===0 ? TEXT_isTab : TEXT_isNotTab}>{staticsStrings.actions}</Text>
        <View  style={tab===0 ? underLine_isTab : underLine_isNotTab}/>
        </TouchableOpacity>
      
        <TouchableOpacity style={TAB_CONTAINER} onPress={()=>{setTab(1)}}>
        <Text style ={tab===1 ? TEXT_isTab : TEXT_isNotTab}  >{staticsStrings.droits}</Text>
        <View  style={tab===1 ? underLine_isTab : underLine_isNotTab}/>
        </TouchableOpacity>

        <TouchableOpacity style={TAB_CONTAINER} onPress={()=>{setTab(2)}}>
        <Text style ={tab===2 ? TEXT_isTab : TEXT_isNotTab}  >{staticsStrings.obligations}</Text>
        <View  style={tab===2 ? underLine_isTab : underLine_isNotTab}/>
        </TouchableOpacity>

      
      </View>

      <View style={FILTER}>
          <View style={SWITCH_CONTAINER}>
          <View style={{flexDirection:"row",alignContent:"center",alignItems:"center",marginHorizontal:metrics.heightPercentageToDP(2)}}>
          <Image  source={pto_white}/>
          <Text style={{fontFamily:"Sora-Bold",color:color.palette.white}} >PTO</Text>
          </View>
          <Switch
          value={switchPTO}
          thumbOnStyle={{height:metrics.heightPercentageToDP(3.5),width:metrics.heightPercentageToDP(3.5)}}
          thumbOffStyle={{height:metrics.heightPercentageToDP(3.6),width:metrics.heightPercentageToDP(3.5)}}
          trackOffStyle={{height:metrics.heightPercentageToDP(3.6),width:metrics.heightPercentageToDP(6.6),backgroundColor:"#2e4d6a",borderColor:"#ffffff",borderWidth:metrics.heightPercentageToDP(0.03)}}
          trackOnStyle={{height:metrics.heightPercentageToDP(3.6),width:metrics.heightPercentageToDP(6.6),backgroundColor:"#2e4d6a",borderColor:"#ffffff",borderWidth:metrics.heightPercentageToDP(0.03)}}
          onToggle={onToggle}
          
           />

          </View>

        

         <TouchableOpacity onPress={()=>{setAlphabet(!alphabet)}} >
         <View style={[HAUT_BAS_CONTAINER,{backgroundColor:color.palette.BKBBlueColor,flexDirection:"row",justifyContent:"space-evenly"}]} >
         <Text style={{fontFamily:"Sora-Medium",color:color.palette.white,fontSize:metrics.heightPercentageToDP(1.5)}} >{fromAToZ(alphabet)}</Text>
         <Image  source={a_z_right}/>
         <Text style={{fontFamily:"Sora-Medium",color:color.palette.white,fontSize:metrics.heightPercentageToDP(1.5)}} >{fromAToZ(!alphabet)}</Text>
          </View>
         </TouchableOpacity>
         <TouchableOpacity>
         <View style={HAUT_BAS_CONTAINER} >
            <Text style={{fontFamily:"Sora-Medium",color:color.palette.white,fontSize:metrics.heightPercentageToDP(1.5)}} >{staticsStrings.plus_haut}</Text>
          </View>
         </TouchableOpacity>
         <TouchableOpacity>
         <View style={HAUT_BAS_CONTAINER} >
            <Text style={{fontFamily:"Sora-Medium",color:color.palette.white,fontSize:metrics.heightPercentageToDP(1.5)}} >{staticsStrings.plus_bas}</Text>
            
          </View>
         </TouchableOpacity>

          
      </View>

      <ScrollView>
        {
          tab===0 &&       <BkbPalmaresFortes pto_show={true} assetStatus={"Haut"} assetLabel={"SNA"} assetCode={"MA 00003923"} assetValue={19.23} assetHautBas={"+12,67%"} />

        }
        {
          tab===1 &&       <BkbPalmaresFortes assetStatus={"Bas"} assetLabel={"SNA"} assetCode={"MA 00003923"} assetValue={19.23} assetHautBas={"+12,67%"}/>

        }
        {
          tab===2 &&       <BkbPalmaresFortes assetStatus={"Bas"} assetLabel={"SNA"} assetCode={"MA 00003923"} assetValue={19.23} assetHautBas={"+12,67%"}/>

        }

      </ScrollView> */}
      {/* <BkbCardDoconnecte onPressClient={()=>{console.log("client")}} onPressConnecter={()=>{}} /> */}


    </BkbScreen>
  )
})


//styles 
const TABS : ViewStyle={
  paddingTop:metrics.heightPercentageToDP(1),
  flexDirection:'row',
  height:metrics.heightPercentageToDP(4),
  backgroundColor:'rgba(63,106,145,0.7)'}
const TEXT_isTab: TextStyle={
  fontFamily:"Sora-Regular",
  color:color.palette.white
}
const TEXT_isNotTab: TextStyle={
    color:"#ffffff85"

}
const underLine_isTab:ViewStyle={
  height:metrics.heightPercentageToDP(0.3),
  backgroundColor:color.palette.white,
  borderRadius:metrics.heightPercentageToDP(3),
  marginTop:metrics.heightPercentageToDP(0.5),
  width:metrics.heightPercentageToDP(16)
}

const underLine_isNotTab:ViewStyle={
  height:metrics.heightPercentageToDP(0.3),
  // backgroundColor:"#ffffff85",
  // borderRadius:metrics.heightPercentageToDP(3),
  marginTop:metrics.heightPercentageToDP(0.7),
  width:metrics.heightPercentageToDP(16)
}

const TAB_CONTAINER : ViewStyle={
  alignItems:"center",
  alignSelf:'center',
  alignContent:'center',
}

const FILTER : ViewStyle={
  flexDirection:"row",
  alignItems:"center",

}
const SWITCH_CONTAINER : ViewStyle={
  flexDirection:"row",
  margin:metrics.heightPercentageToDP(1)
}
const HAUT_BAS_CONTAINER : ViewStyle={
  borderRadius:metrics.heightPercentageToDP(3),
  width:metrics.heightPercentageToDP(9),
  height:metrics.heightPercentageToDP(3.6),
  alignContent:'center',
  alignItems:"center",
  justifyContent:"center",
  backgroundColor:'#2e4d6a',
}