import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { TouchableOpacity, View, ViewStyle,Image, TextStyle } from "react-native"
import { BkbScreen, BKbSearchField, Text, WatchListCard } from "../../components"
import { NavigationInjectedProps } from "react-navigation"
import staticsStrings from "../../localData/staticStrings.json"
import metrics from "../../theme/metrics"
import { useStores } from "../../models"

//icons & images
const ajouter=require("../../../assets/images/ajouter.png")
const editer=require("../../../assets/images/editer.png")


export interface WatchListProps extends NavigationInjectedProps<{}> {
  route
}

export const WatchListScreen :FC<WatchListProps>= observer((props)=> {
  const [ajouterState,setAjouterState]=useState(false);
  const [editerState,setEditerState]=useState(false);
  const {ModalStore}=useStores();

  const dataJson=[
    {
      "userId": 1,
      "id": 1,
      "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
      "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
    },
    {
      "userId": 2,
      "id": 2,
      "title": "qui est esse",
      "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
    },
    {
      "userId": 3,
      "id": 3,
      "title": "qui est esse",
      "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
    },
   
   
 

  ]
  useEffect(() => {
    

  }, []);


  return (
    <BkbScreen
    headerShow={true}
    styleParent={HEADER_STYLE}
    navigation={props.navigation}
    >
    {/* <View style={WATCH_LIST_HEADER}>
        <Text style={MAWATCHLIST_TEXT}>{staticsStrings.ma_watchlist}</Text>
       <View style={AJOUTER_EDITER_STYLE}>


       <TouchableOpacity  style={TOUCHABLE_BTN} onPress={()=> {setAjouterState(true); setEditerState(false)}}>
        <Image source={ajouter}/>
          <Text style={ajouterState? FILTER_TEXT:FILTER_TEXT_HIDE   }>{staticsStrings.ajouter}</Text>
        </TouchableOpacity>


        <TouchableOpacity   style={TOUCHABLE_BTN} onPress={()=> {setEditerState(true); setAjouterState(false);ModalStore.setEditerWatchList(true)}}>
        <Image source={editer}/>
          <Text style={editerState? FILTER_TEXT:FILTER_TEXT_HIDE   }>{staticsStrings.editer}</Text>
        </TouchableOpacity>
       </View>

        
    </View>

          {
            editerState===true && 
            <View>
              <WatchListCard assetCode={"MA 1939383838"}
            assetLabel={"undefined"} value={193.37} up_down={"+19.1%"} pto_show={false} onPressWatchMin={()=>{console.log("hello1")}}/>
            <WatchListCard assetCode={"MA 1939383838"}
            assetLabel={"undefined"} value={193.37} up_down={"-10.1%"} pto_show={true} onPressWatchMin={()=>{console.log("hello2")}}/>

          <WatchListCard assetCode={"MA 1939383838"}
            assetLabel={"undefined"} value={193.37} up_down={"-13.1%"} pto_show={true} onPressWatchMin={()=>{console.log("hello3")}}/>
               <WatchListCard assetCode={"MA 1939383838"}
            assetLabel={"undefined"} value={193.37} up_down={"-13.1%"} pto_show={true} onPressWatchMin={()=>{console.log("hello3")}}/>
               <WatchListCard assetCode={"MA 1939383838"}
            assetLabel={"undefined"} value={193.37} up_down={"-13.1%"} pto_show={true} onPressWatchMin={()=>{console.log("hello3")}}/>
            </View>
          }

          {
            ajouterState===true &&
            
            
            <BKbSearchField dataSource={dataJson}/>


          } */}

  
    </BkbScreen>
  )
})
//styles of watch list
const HEADER_STYLE : ViewStyle={
  borderBottomEndRadius:metrics.heightPercentageToDP(1.5),
  borderBottomStartRadius:metrics.heightPercentageToDP(1.5),
  height:metrics.heightPercentageToDP(14),
}

const WATCH_LIST_HEADER : ViewStyle={
  flexDirection:"row",
  justifyContent:'space-between',
  margin:metrics.heightPercentageToDP(1),
}
const MAWATCHLIST_TEXT : TextStyle={
  fontFamily:'Sora-Bold'

}
const FILTER_TEXT : TextStyle={
  fontFamily:'Sora-Bold',
  fontSize:metrics.heightPercentageToDP(1.5),
  textAlign:"center",
  marginLeft:metrics.heightPercentageToDP(1),
  alignSelf:'flex-end',

}
const FILTER_TEXT_HIDE : TextStyle={
  fontFamily:'Sora-Bold',
  fontSize:metrics.heightPercentageToDP(1.5),
  textAlign:"center",
  marginLeft:metrics.heightPercentageToDP(1),
  alignSelf:'flex-end',
  color:"#ffffff25"

}
const TOUCHABLE_BTN: ViewStyle={
  marginHorizontal:metrics.heightPercentageToDP(1),
  borderRadius:metrics.heightPercentageToDP(3),
  padding:metrics.heightPercentageToDP(0.5),
  width:metrics.heightPercentageToDP(10),
  // backgroundColor:"#FFFFFF35",
  backgroundColor:'rgba(63,106,145,0.7)',
  flexDirection:'row',
  justifyContent:'center',
  alignContent:"center",
  alignItems:"center",
  
}
const AJOUTER_EDITER_STYLE : ViewStyle={
  flexDirection:'row',

}


