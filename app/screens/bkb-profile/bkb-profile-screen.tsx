import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { View, ViewStyle ,Image, TextStyle} from "react-native"
import { BkbAccordion, BkbButton, BkbInfoMailPhone, BkbInput, BkbScreen, Text } from "../../components"
import { color } from "../../theme"
import { NavigationInjectedProps } from "react-navigation"
import { TouchableOpacity } from "react-native-gesture-handler"
import metrics from "../../theme/metrics"
import LinearGradient from "react-native-linear-gradient"
import staticsStrings from "../../localData/staticStrings.json"
import { useStores } from "../../models"
import { useIsFocused } from "@react-navigation/native"


//images & icons

const profile_pwd=require("../../../assets/images/profile_pwd.png")
const casque_pwd=require("../../../assets/images/casque_pwd.png")
const lock_pwd=require("../../../assets/images/lock_pwd.png")
const message=require("../../../assets/images/icon_message.png")
const drop_right=require("../../../assets/images/drop_right.png")
const icon_eye=require("../../../assets/images/icon_eye.png")
export interface BKBProfileProps extends NavigationInjectedProps<{}> {
  route
}
export const BkbProfileScreen : FC<BKBProfileProps> = observer((props)=> {
  const {ProfileUserStore}=useStores()
  const [pwdShow , setPwdShow]=useState(false)
  const [profileShow , setProfileShow]=useState(true)
  const [oldPwd , setOldPwd]=useState("")
  const [newPwd , setNewPwd]=useState("")
  const [confirmPwd , setConfirmPwd]=useState("")
  const isFocused = useIsFocused();

  const comparePasswords=(o,n)=>{
    if(n===o || o===n){
      return color.palette.white
    }
    else {
      return color.palette.BKBRedColor
    } 
  }

  useEffect(()=>{
    

  },[isFocused])
  return (
    <BkbScreen
    headerShow={true}
    navigation={props.navigation}
    goBackButton={true}
    >
      {/* {
         profileShow===true && 
         <View>
          <BkbAccordion title={ProfileUserStore.accountLabel} icon={profile_pwd}>
                  <View style={{marginLeft:metrics.heightPercentageToDP(7)}}>
                  <BkbInfoMailPhone mail={"bkb@bmce.com"} phone={"11111111"}/>
                  </View>
                
          </BkbAccordion>
          <TouchableOpacity onPress={()=>{setPwdShow(true);setProfileShow(false)}} >
          <LinearGradient
                          colors={['rgba(63,106,145,0.1)',"rgba(63,106,145,0.1)","#04CAFC53"]}
                          locations={[0, 0.6, 1]} 
                          style={ ROW_UNEXPANDED}
                          useAngle={true} angle={35} angleCenter={{ x: 0.5, y: 0.5 }}
                          >
                            

                            <View style={ICON_CONTAINER}>
                            <Image  source={lock_pwd}/>

                            <Text style={TITLE_STYLE} >{staticsStrings.modifier_mon_mot_de_passe}</Text>
                            </View>

                            <Image source={drop_right } />
                          
          </LinearGradient>

          </TouchableOpacity>
                  <BkbAccordion title={staticsStrings.assistance} icon={casque_pwd}>
                  <View style={{marginLeft:metrics.heightPercentageToDP(7)}}>
                  <Text style={{marginLeft:metrics.heightPercentageToDP(1)}}>{staticsStrings.besoin_d_assistance}</Text>
                  <BkbInfoMailPhone mail={"bkb@bmce.com"} phone={"11111111"}/>
                  </View>
                
                  </BkbAccordion>
         </View>
      }

      {
        pwdShow===true && 
        <LinearGradient
                          colors={['rgba(63,106,145,0.1)',"rgba(63,106,145,0.1)","#04CAFC53"]}
                          locations={[0, 0.6, 1]} 
                          style={PWD_EDIT}
                          useAngle={true} angle={35} angleCenter={{ x: 0.5, y: 0.5 }}
                          >
                            
                        <View style={HEADER_PWD}>
                          
                        <View style={ICON_CONTAINER}>
                            <Image  source={lock_pwd}/>

                            <Text style={TITLE_STYLE} >{staticsStrings.modifier_mon_mot_de_passe}</Text>
                            </View>
                        </View>
              <View style={INPUTS_CONTAINER}>
                <BkbInput iconRight={icon_eye} style={{width:metrics.heightPercentageToDP(40),justifyContent:"space-evenly",}} inputType={"login"} placeholder={staticsStrings.mot_de_passe_actuel}  value={oldPwd} onChangeText={setOldPwd}/>
                <TouchableOpacity style={FORGET_PASSWORD_TOUCH}>
                  <Text style={FORGET_PASSWORD_TEXT}>{staticsStrings.j_ai_oublie_mon_mot_de_passe}</Text>
                </TouchableOpacity>
                <BkbInput style={{width:metrics.heightPercentageToDP(40),justifyContent:"space-evenly"}} inputType={"login"} placeholder={staticsStrings.nouveau_mot_de_passe} value={newPwd} onChangeText={setNewPwd}/>
              
                <BkbInput style={{width:metrics.heightPercentageToDP(40),justifyContent:"space-evenly",borderColor:comparePasswords(newPwd,confirmPwd)}} isPassword={true} inputType={"login"} placeholder={staticsStrings.confirmer_le_mot_de_passe} value={confirmPwd} onChangeText={setConfirmPwd}/>
              </View>
              <View style={{margin:metrics.heightPercentageToDP(1)}} > 
              <BkbButton style={{width:metrics.heightPercentageToDP(37)}} ButtonType={"white"} ButtonText={staticsStrings.sauvegarder} onPress={async ()=>{
                
                await ProfileUserStore.ChangePasswordUser(ProfileUserStore.login,ProfileUserStore.contextId,ProfileUserStore.loginCode,oldPwd,newPwd);
                if(ProfileUserStore.passwordChangedDone===true){
                  setProfileShow(true)
                  setPwdShow(false)
                }
                else{
                }
                
                
                }}/>
  
              </View>                            
                          
          </LinearGradient>
      } */}
    </BkbScreen>
  )
})
//styles
const ROW_UNEXPANDED : ViewStyle={

  flexDirection: 'row',
  justifyContent:'space-between',
  height:metrics.heightPercentageToDP(8),
  paddingLeft:metrics.heightPercentageToDP(2),
  paddingRight:metrics.heightPercentageToDP(2),
  alignItems:'center',
  backgroundColor: "rgba(63,106,145,0.99)",
  margin:metrics.heightPercentageToDP(1),
  borderRadius:metrics.heightPercentageToDP(1),

}
const ICON_CONTAINER : ViewStyle={
  flexDirection:"row",
}
const TITLE_STYLE : TextStyle={
  alignSelf:'center',
  marginLeft:metrics.heightPercentageToDP(2),
  fontFamily:"Sora-Bold"

}
const PWD_EDIT : ViewStyle={

  // flexDirection: 'row',
  // justifyContent:'space-between',
  height:metrics.heightPercentageToDP(45),
  // paddingLeft:metrics.heightPercentageToDP(2),
  // paddingRight:metrics.heightPercentageToDP(2),
  backgroundColor: "rgba(63,106,145,0.99)",
  margin:metrics.heightPercentageToDP(2),
  borderRadius:metrics.heightPercentageToDP(1),
  

}
const HEADER_PWD : ViewStyle={
  flexDirection: 'row',
  justifyContent:'space-between',
  height:metrics.heightPercentageToDP(8),
  paddingLeft:metrics.heightPercentageToDP(2),
  paddingRight:metrics.heightPercentageToDP(2),
  alignItems:'center',
  margin:metrics.heightPercentageToDP(1),
  borderRadius:metrics.heightPercentageToDP(1),
}
const INPUTS_CONTAINER : ViewStyle={
  justifyContent:'center',
  alignItems:'center',
  alignContent:'center'
}
const FORGET_PASSWORD_TEXT : TextStyle={
  textDecorationLine:"underline",
  fontSize:metrics.heightPercentageToDP(1.6),
  fontFamily:"Sora-Bold",
  color:"#04CAFC",
}
const FORGET_PASSWORD_TOUCH : TextStyle={
// marginTop:metrics.heightPercentageToDP(2),


}