import React, { FC, useRef, useState } from "react"
import { observer } from "mobx-react-lite"
import { TextStyle, TouchableOpacity, View, ViewStyle,Image, Animated } from "react-native"
import { BkbButton, BkbInput, BkbScreen, BkbTransparentCard, Text } from "../../components"
import { color } from "../../theme"
import { NavigationInjectedProps } from "react-navigation"
import metrics from "../../theme/metrics"
import staticsStrings from "../../localData/staticStrings.json"
import CheckBox from '@react-native-community/checkbox';
import { ScrollView } from "react-native-gesture-handler"

//icons & images
const valide_deconnecte=require("../../../assets/images/valide_deconnecte.png")
export interface DevenirClientDeconnecteProps extends NavigationInjectedProps<{}> {
}


export const DevenirClientDeconnecteScreen :FC<DevenirClientDeconnecteProps> = observer((props)=> {
  const [tab,setTab]=useState(0);
  const [nom,setNom]=useState("");
  const [prenom,setPrenom]=useState("");
  const [email,setEmail]=useState("");
  const [telephone,setTelephone]=useState("");
  const [isSelected, setSelection] = useState(false);

  const [completeScrollBarHeight, setCompleteScrollBarHeight] = useState(1);
  const [visibleScrollBarHeight, setVisibleScrollBarHeight] = useState(0);

  const scrollIndicator = useRef(new Animated.Value(0)).current;

  const scrollIndicatorSize =
    completeScrollBarHeight > visibleScrollBarHeight
      ? (visibleScrollBarHeight * visibleScrollBarHeight) /
        completeScrollBarHeight
      : visibleScrollBarHeight;

  const difference =
    visibleScrollBarHeight > scrollIndicatorSize
      ? visibleScrollBarHeight - scrollIndicatorSize
      : 1;

  const scrollIndicatorPosition = Animated.multiply(
    scrollIndicator,
    visibleScrollBarHeight / completeScrollBarHeight
  ).interpolate({
    inputRange: [0, difference],
    outputRange: [0, difference],
    extrapolate: 'clamp'
  });

  return (
    <BkbScreen
    headerShow={true}
    styleParent={tab===3 ?  {}: HEADER_PARENT }
    >
        {/* {
          tab===3 && <View style={HEADER}>
          <Text style={{fontFamily:"Sora-Bold"}}>{staticsStrings.conditions_generales_header_deconnecte}</Text>
      </View>
        }

        {tab===0 &&
        <BkbTransparentCard style={CARD_ZERO} hexaColorCard={undefined} opacityColorCard={undefined}>

        <Text style={OUVRIR_COMPTE_DECONNECTE} >{staticsStrings.ouvrir_compte_deconnecte}</Text>
        <Text style={DESORMAIS_DECONNECTE} >{staticsStrings.desormais_deconnecte}</Text>
       
        <BkbButton ButtonType={"transparent"} ButtonText={staticsStrings.ouvrir_mon_compte_en_ligne} style={{width:metrics.heightPercentageToDP(35),}} TextStyle={{fontFamily:"Sora-Bold",marginRight:metrics.heightPercentageToDP(1.2),alignSelf:'center'}} onPress={()=>{setTab(1)}}/>
        <TouchableOpacity style={FORGET_PASSWORD_TOUCH}  onPress={()=>{props.navigation.navigate("Deconnectemain")}} >
        <Text style={FORGET_PASSWORD_TEXT}>{"Des questions ? Contactez-nous"}</Text>
        </TouchableOpacity>
        
        <BkbButton ButtonType={"white"} style={{width:metrics.heightPercentageToDP(35)}} ButtonText={staticsStrings.pride_de_contact_deconnecte} onPress={()=>{}}/>
        </BkbTransparentCard>
        }
        {tab===1 &&
        <BkbTransparentCard  style={CARD_ONE} hexaColorCard={undefined} opacityColorCard={undefined}>
          <Text style={REMPLIR_TEXT} >{staticsStrings.merci_de_remplir_deconnecte}</Text>
         
          <BkbInput inputType={"login"} placeholder={staticsStrings.nom_deconnecte} value={nom} onChangeText={setNom}/>
          <BkbInput inputType={"login"} placeholder={staticsStrings.prenom_deconnecte} value={prenom} onChangeText={setPrenom}/>
          <BkbInput inputType={"login"} placeholder={staticsStrings.email_deconnecte} value={email} onChangeText={setEmail}/>
          <BkbInput inputType={"login"} placeholder={staticsStrings.telephone_deconnecte} value={telephone} onChangeText={setTelephone}/>
          
          <View style={CONDITIONS} >
          <CheckBox
          value={isSelected}
          onValueChange={setSelection}
          onFillColor={"red"}
          onTintColor={"red"}
           tintColors={{ true: color.palette.BKBBlueColor1 , false :"white"}}
          style={{borderWidth:metrics.heightPercentageToDP(1),borderColor:"white"}}
          
        />
        <View style={CONDITIONS_CONTAINER}>
          <Text style={text_parte_one} >{staticsStrings.lire_accepter_deconnecte}</Text>
         <TouchableOpacity onPress={()=>{setTab(3)}} >  
           <Text style={conditions_generales} >{staticsStrings.conditions_generales_deconnecte}</Text>
         </TouchableOpacity>
          <Text style={text_parte_two} >{staticsStrings.protection_deconnecte}</Text>
        </View> 
          </View>
          <BkbButton ButtonType={"white"} ButtonText={staticsStrings.continuer_deconnecte} style={{width:metrics.heightPercentageToDP(15),marginTop:metrics.heightPercentageToDP(3)}} onPress={()=>{

            if(isSelected===true){
              setTab(2)
            }
          }}/>
          
            
        </BkbTransparentCard>
        }
         
        {tab===2 &&
        <BkbTransparentCard style={CARD_TWO} hexaColorCard={undefined} opacityColorCard={undefined}>
          <Image  source={valide_deconnecte} />
          <Text style={{fontFamily:"Sora-Bold",textAlign:'center',alignSelf:"center"}}>{staticsStrings.merci_deconnecte}</Text>
          <Text style={{fontSize:metrics.heightPercentageToDP(1.6),fontFamily:"Sora-Bold",textAlign:'center',alignSelf:"center"}}>{staticsStrings.demande_recu_deconnecte}</Text>
          <Text style={{fontSize:metrics.heightPercentageToDP(1.6),fontFamily:"Sora-Regular",textAlign:'center',alignSelf:"center"}}>{staticsStrings.conseiller_deconnecte}</Text>
          <Text style={{fontSize:metrics.heightPercentageToDP(1.6),fontFamily:"Sora-Regular",textAlign:'center',alignSelf:"center"}}>{staticsStrings.prochainement_deconnecte}</Text>

          <BkbButton ButtonType={"white"} style={{marginTop:metrics.heightPercentageToDP(9)}} ButtonText={staticsStrings.ecran_principal_deconnecte} onPress={()=>{setTab(0)}}/>
        </BkbTransparentCard>
        }

        {
          tab===3 && 
          
          <View style={{ flex: 1,  }}>
        
        <View style={{ flex: 3, marginVertical: metrics.heightPercentageToDP(1) }}>
          <View
            style={{ flex: 1, flexDirection: 'row', paddingHorizontal: metrics.heightPercentageToDP(1) }}
          >
            <ScrollView
              contentContainerStyle={{ paddingRight: metrics.heightPercentageToDP(1) }}
              showsVerticalScrollIndicator={false}
              scrollEventThrottle={16}
              onContentSizeChange={(_,height) => {
                setCompleteScrollBarHeight(height);
              }}
              onLayout={({
                nativeEvent: {
                  layout: { height }
                }
              }) => {
                setVisibleScrollBarHeight(height);
              }}
              onScroll={Animated.event(
                [{ nativeEvent: { contentOffset: { y: scrollIndicator } } }],
                { useNativeDriver: false }
              )}
            >
              
              <Text style={{fontFamily:"Sora-Regular",textAlign:"left",width:metrics.heightPercentageToDP(40),margin:metrics.heightPercentageToDP(2)}} >Le site www.bmcecapitalbourse.com a été optimisé pour une utilisation Firefox 10+ ainsi qu' Internet Explorer 8+. De plus, certaines parties du site font appel aux plugins Macromedia Flash. </Text>
              <Text style={{fontFamily:"Sora-Regular",textAlign:"left",width:metrics.heightPercentageToDP(40),margin:metrics.heightPercentageToDP(2)}} >Le site www.bmcecapitalbourse.com a été optimisé pour une utilisation Firefox 10+ ainsi qu' Internet Explorer 8+. De plus, certaines parties du site font appel aux plugins Macromedia Flash. </Text>
              <Text style={{fontFamily:"Sora-Regular",textAlign:"left",width:metrics.heightPercentageToDP(40),margin:metrics.heightPercentageToDP(2)}} >Le site www.bmcecapitalbourse.com a été optimisé pour une utilisation Firefox 10+ ainsi qu' Internet Explorer 8+. De plus, certaines parties du site font appel aux plugins Macromedia Flash. </Text>
              <Text style={{fontFamily:"Sora-Regular",textAlign:"left",width:metrics.heightPercentageToDP(40),margin:metrics.heightPercentageToDP(2)}} >Le site www.bmcecapitalbourse.com a été optimisé pour une utilisation Firefox 10+ ainsi qu' Internet Explorer 8+. De plus, certaines parties du site font appel aux plugins Macromedia Flash. </Text>
              <Text style={{fontFamily:"Sora-Regular",textAlign:"left",width:metrics.heightPercentageToDP(40),margin:metrics.heightPercentageToDP(2)}} >Le site www.bmcecapitalbourse.com a été optimisé pour une utilisation Firefox 10+ ainsi qu' Internet Explorer 8+. De plus, certaines parties du site font appel aux plugins Macromedia Flash. </Text>
              <Text style={{fontFamily:"Sora-Regular",textAlign:"left",width:metrics.heightPercentageToDP(40),margin:metrics.heightPercentageToDP(2)}} >Le site www.bmcecapitalbourse.com a été optimisé pour une utilisation Firefox 10+ ainsi qu' Internet Explorer 8+. De plus, certaines parties du site font appel aux plugins Macromedia Flash. </Text>
              <Text style={{fontFamily:"Sora-Regular",textAlign:"left",width:metrics.heightPercentageToDP(40),margin:metrics.heightPercentageToDP(2)}} >Le site www.bmcecapitalbourse.com a été optimisé pour une utilisation Firefox 10+ ainsi qu' Internet Explorer 8+. De plus, certaines parties du site font appel aux plugins Macromedia Flash. </Text>
             
            </ScrollView>
            <View
              style={{
                height: '100%',
                width: metrics.heightPercentageToDP(1.5),
                backgroundColor: 'rgba(63,106,145,0.3)',
                borderRadius: 8
              }}
            >
              <Animated.View
                style={{
                  width: metrics.heightPercentageToDP(1.5),
                  borderRadius: 8,
                  backgroundColor: 'rgba(63,106,145,0.7)',
                  height: scrollIndicatorSize,
                  transform: [{ translateY: scrollIndicatorPosition }]
                }}
              />
            </View>
          </View>
        </View>
      </View>
        }
         */}

    </BkbScreen>
  )
})

//styles 
const HEADER: ViewStyle = {
  // backgroundColor:'#ffffff20',
  backgroundColor:'rgba(63,106,145,0.7)',
  flexDirection:'row',
  justifyContent:'space-around',
  height:metrics.heightPercentageToDP(4.5),
  borderBottomEndRadius:metrics.heightPercentageToDP(2),
  borderBottomStartRadius:metrics.heightPercentageToDP(2),
  paddingHorizontal:metrics.heightPercentageToDP(1),
  paddingTop:metrics.heightPercentageToDP(1),

}
const HEADER_PARENT : ViewStyle={
  height:metrics.heightPercentageToDP(14),
      borderBottomEndRadius:metrics.heightPercentageToDP(1),
      borderBottomStartRadius:metrics.heightPercentageToDP(1),
}

const CARD_ZERO : ViewStyle={
  marginTop:metrics.heightPercentageToDP(2),
  borderWidth:metrics.heightPercentageToDP(0.04),
  borderColor:color.palette.white,
  padding:metrics.heightPercentageToDP(1),
  height:metrics.heightPercentageToDP(45),
}
const FORGET_PASSWORD_TEXT : TextStyle={
  textDecorationLine:"underline",
  fontSize:metrics.heightPercentageToDP(1.6),
  fontFamily:"Sora-Regular"
}
const FORGET_PASSWORD_TOUCH : TextStyle={
  marginTop:metrics.heightPercentageToDP(1),
  marginBottom:metrics.heightPercentageToDP(2),


}
const OUVRIR_COMPTE_DECONNECTE :TextStyle={
  fontFamily:"Sora-Bold",
  textAlign:'center',
  fontSize:metrics.heightPercentageToDP(2.4),
}
const DESORMAIS_DECONNECTE : TextStyle={
  textAlign:'center',
  fontFamily:"Sora-Regular",
  marginVertical:metrics.heightPercentageToDP(1.5),

}
const CARD_ONE : ViewStyle={
  marginTop:metrics.heightPercentageToDP(2),
  borderWidth:metrics.heightPercentageToDP(0.04),
  borderColor:color.palette.white,
  padding:metrics.heightPercentageToDP(1),
  height:metrics.heightPercentageToDP(60),
  marginHorizontal:metrics.heightPercentageToDP(6.5),
  
}
const REMPLIR_TEXT : TextStyle={
  textAlign:'center',
  fontFamily:"Sora-Bold",
  fontSize:metrics.heightPercentageToDP(2.3),

}

const CONDITIONS : ViewStyle={
  flexDirection:"row",
  alignSelf:"center",
  alignContent:"center",
}
const CONDITIONS_CONTAINER : ViewStyle={

}
const text_parte_one: TextStyle={
  fontFamily:"Sora-Regular",
  color:"#ffffff90",
  fontSize:metrics.heightPercentageToDP(1.6)
}
const conditions_generales : TextStyle={
  textDecorationLine:"underline",
  fontFamily:"Sora-Bold",
  color:"#ffffff",
  fontSize:metrics.heightPercentageToDP(1.5)

}
const text_parte_two: TextStyle={
  fontFamily:"Sora-Regular",
  color:"#ffffff90",
  fontSize:metrics.heightPercentageToDP(1.6)
}
const CARD_TWO : ViewStyle={
  
  marginTop:metrics.heightPercentageToDP(10),
  borderTopWidth:metrics.heightPercentageToDP(0.7),
  borderColor:color.palette.white,
  padding:metrics.heightPercentageToDP(1),
  height:metrics.heightPercentageToDP(40),
  marginHorizontal:metrics.heightPercentageToDP(6),
  alignSelf:'center',
  
}
