import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle,View, TextStyle } from "react-native"
import {  BkbDonutChartCustom, BkbNotificationCours, BkbNotificationNews, BkbScreen, Text } from "../../components"
import { color } from "../../theme"
import { NavigationInjectedProps } from "react-navigation"
import { useStores } from "../../models"
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler"
import metrics from "../../theme/metrics"
import staticsStrings from "../../localData/staticStrings.json"
import GlobalFunctions from "../../classes/GlobalFunctions"


//images & icons

export interface NotificationsProps extends NavigationInjectedProps<{}> {
  route
}
export const NotificationsScreen  : FC<NotificationsProps>= observer((props)=> {
//  const {CarnetOrdreModalSore,SingleAssetV1Store,ProfileUserStore,ValidateOrdreStore}=useStores();
  const globalFunctions = new GlobalFunctions();
  const [tab,setTab]=useState(0);
  const dateDay=(inputDate)=>{
    let today = new Date();
    let yesterday = new Date();
    yesterday.setDate(today.getDate() - 1);
    if(inputDate.getTime() === today.getTime()){
        return "Aujourd'hui";
    }
    else if(inputDate.getTime() === yesterday.getTime()){
      return "Hier";
    } else{
      return   globalFunctions.getMaxdate(globalFunctions.convertDateToBkbDate(inputDate));
    }

  }

  useEffect(()=>{

  },[])
  return (
    <BkbScreen
    navigation={props.navigation}
    headerShow={true}
    >
      <View style={HEADER}>
          <Text style={{fontFamily:"Sora-Bold"}}>{"Notifications"}</Text>
      </View>
      <View style={TABS} >
        <TouchableOpacity style={TAB_CONTAINER}  onPress={()=>{setTab(0)}}>
        <Text style ={tab===0 ? TEXT_isTab : TEXT_isNotTab}>{staticsStrings.cours}</Text>
        <View  style={tab===0 ? underLine_isTab : underLine_isNotTab}/>
        </TouchableOpacity>
      
        <TouchableOpacity style={TAB_CONTAINER} onPress={()=>{setTab(1)}}>
        <Text style ={tab===1 ? TEXT_isTab : TEXT_isNotTab}  >{staticsStrings.news}</Text>
        <View  style={tab===1 ? underLine_isTab : underLine_isNotTab}/>

        </TouchableOpacity>

      
      </View>

      {
        tab===0 &&
        <ScrollView style={{padding:metrics.heightPercentageToDP(0)}}  >
          <Text  style={{marginHorizontal:metrics.heightPercentageToDP(2.5),marginTop:metrics.heightPercentageToDP(1)}} >{dateDay(new Date('December 25, 2017 01:30:00'))}</Text>
          <BkbNotificationCours assetlabel={"Addoha"} seuil={22.1} date={"25/17/2021"} assetStatus={"Achat"} assetCode={"MA 1000000"} time={"1h"}/>
          <BkbNotificationCours assetlabel={"lafarage"} seuil={22.1} date={"25/17/2021"} assetStatus={"Vente"} assetCode={"MA 1000000"} time={"1h"}/>
        </ScrollView>
      }
      {
        tab===1 &&
        <ScrollView>
          <BkbNotificationNews/>
          <BkbNotificationNews/>

        </ScrollView>
      }
      

    {/* <BkbDonutChartCustom dataSet={[10,80,5]}/> */}

    </BkbScreen>
  )
})


//styles 
const HEADER: ViewStyle = {
  // backgroundColor:'#ffffff20',
  backgroundColor:'rgba(63,106,145,0.7)',
  flexDirection:'row',
  justifyContent:'space-around',
  height:metrics.heightPercentageToDP(4.5),
  borderBottomEndRadius:metrics.heightPercentageToDP(2),
  borderBottomStartRadius:metrics.heightPercentageToDP(2),
  paddingHorizontal:metrics.heightPercentageToDP(1),
  paddingTop:metrics.heightPercentageToDP(1),

}
const TABS : ViewStyle={
  marginTop:metrics.heightPercentageToDP(1),
  flexDirection:'row',
  // justifyContent:"space-around",
  alignSelf:'center',
  marginHorizontal:metrics.heightPercentageToDP(1),
  // width:metrics.heightPercentageToDP(50)
}
const TEXT_isTab: TextStyle={
  fontFamily:"Sora-Regular",
}
const TEXT_isNotTab: TextStyle={
    color:"#ffffff85"

}
const underLine_isTab:ViewStyle={
  height:metrics.heightPercentageToDP(0.5),
  backgroundColor:color.palette.white,
  borderRadius:metrics.heightPercentageToDP(3),
  marginTop:metrics.heightPercentageToDP(0.5),
  width:metrics.heightPercentageToDP(22)
}

const underLine_isNotTab:ViewStyle={
  height:metrics.heightPercentageToDP(0.5),
  backgroundColor:"#ffffff85",
  borderRadius:metrics.heightPercentageToDP(3),
  

  marginTop:metrics.heightPercentageToDP(0.7),
  width:metrics.heightPercentageToDP(22)
}

const TAB_CONTAINER : ViewStyle={
  alignItems:"center",
  alignSelf:'center',
  alignContent:'center',
}