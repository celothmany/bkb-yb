import React, { FC } from "react"
import { observer } from "mobx-react-lite"
import { View, ViewStyle } from "react-native"
import { BkbActualitesView, BkbScreen, Screen, Text } from "../../components"
import { color } from "../../theme"
import { NavigationInjectedProps } from "react-navigation"
import staticsStrings from "../../localData/staticStrings.json"
import metrics from "../../theme/metrics"
import { ScrollView } from "react-native-gesture-handler"


export interface ActualiteProps extends NavigationInjectedProps<{}> {
  route
}

export const ActualiteScreen : FC<ActualiteProps> = observer((props)=> {
  
  return (
    <BkbScreen
    navigation={props.navigation}
    headerShow={true}
    goBackButton={true}
    >
      {/* <View style={HEADER}>
          <Text style={{fontFamily:"Sora-Bold"}}>{staticsStrings.actualite+"s"}</Text>
      </View>

    <ScrollView>
      <BkbActualitesView title={"MANAGEM : Indicateurs semestriels à fin juin 2021"} time_date={"27-09-2021 10:03"} description={"description dhd dhd dhbhdh dhbdhdh dhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizo ijlsuencjf "}/>

    </ScrollView> */}
      
    </BkbScreen>
  )
})

const HEADER: ViewStyle = {
  // backgroundColor:'#ffffff20',
  backgroundColor:'rgba(63,106,145,0.7)',
  flexDirection:'row',
  justifyContent:'space-around',
  height:metrics.heightPercentageToDP(4.5),
  borderBottomEndRadius:metrics.heightPercentageToDP(2),
  borderBottomStartRadius:metrics.heightPercentageToDP(2),
  paddingHorizontal:metrics.heightPercentageToDP(1),
  paddingTop:metrics.heightPercentageToDP(1),

}