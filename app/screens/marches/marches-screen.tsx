import React, { FC } from "react"
import { observer } from "mobx-react-lite"
import { BkbDonutChartCustom, BkbScreen } from "../../components"
import { NavigationInjectedProps } from "react-navigation"
import { ScrollView } from "react-native-gesture-handler"


export interface MarchesProps extends NavigationInjectedProps<{}> {
  route
}

export const MarchesScreen : FC<MarchesProps> = observer((props)=> {

  return (
    <BkbScreen
    navigation={props.navigation}
    headerShow={true}
    goBackButton={true}>
      {/* <ScrollView>
      <BkbDonutChartCustom dataSet={[10,80,5,5,0,0]}/>
      </ScrollView> */}


      </BkbScreen>
  )
})
