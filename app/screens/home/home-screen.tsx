import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { Image, ViewStyle, View, TextStyle, Pressable } from "react-native"
import { BkbButton, BkbLoading, BkbLoadingPage, BkbModal, BkbScreen, BkbTransparentCard, Text } from "../../components"
import { color } from "../../theme"
import { NavigationInjectedProps } from "react-navigation"
import metrics from "../../theme/metrics"
import { BkbNavButton } from "../../components/bkb-nav-button/bkb-nav-button"
import { useStores } from "../../models"
import { useIsFocused } from "@react-navigation/native"
import staticsStrings from "../../localData/staticStrings.json"
import { TouchableOpacity } from "react-native-gesture-handler"

//images & icons 
 const marche=require("../../../assets/images/marche.png")
 const indice=require("../../../assets/images/indice.png")
 const palmares=require("../../../assets/images/palmares.png")
 const publications=require("../../../assets/images/publications.png")
 const actualite=require("../../../assets/images/actualite.png")
 const user_profile=require("../../../assets/images/user_profile.png")
 const logout=require("../../../assets/images/logout.png")
 const x_annuler=require("../../../assets/images/x_annuler.png")


export interface HomeProps extends NavigationInjectedProps<{}> {
  route
}
export const HomeScreen : FC<HomeProps> = observer((props)=> {
  // Pull in one of our MST stores
  const {ProfileUserStore} = useStores()
  const [disconnect,setDisconnect]=useState(false);
  const isFocused = useIsFocused();

  const hexToRgbA = (hex, opacity) => {
    let c;
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
      c = hex.substring(1).split('');
      if (c.length === 3) {
        c = [c[0], c[0], c[1], c[1], c[2], c[2]];
      }
      c = `0x${c.join('')}`;
      return `rgba(${[(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',')},${opacity})`;
    }
    throw new Error('Bad Hex');
  };
  const Disconnect =async ()=>{
    await ProfileUserStore.DisconnectUserBKB(ProfileUserStore.login,ProfileUserStore.contextId)
    if(ProfileUserStore.detailcode===-1 || ProfileUserStore.resultcode===0){
      props.navigation.reset({
            index : 0,
            routes : [{name :"Deconnectemain"}]
          })
    }
    
  }
  useEffect(()=>{
    ProfileUserStore.setkeepConnected(true)
    ProfileUserStore.ListAccountUserBKB(ProfileUserStore.login,ProfileUserStore.contextId)
  },[isFocused])
  return (
    <BkbScreen>
      {
         ProfileUserStore.loadingUser ? 
         (<BkbLoadingPage/>)
         : 
         ( <View>
           <View style={NAVS_CONTAINER}>
      <BkbNavButton Title={staticsStrings.marches} Icon={marche} onPress={()=>{
        //props.navigation.navigate("marches")
      }}/>
      <BkbNavButton Title={staticsStrings.Indices}Icon={indice} onPress={()=>{
        //props.navigation.navigate("indices")
        }}/>
      <BkbNavButton Title={staticsStrings.palmares} Icon={palmares} onPress={()=>{
        //props.navigation.navigate("palmares")
        }}/>
      <BkbNavButton Title={staticsStrings.publications} Icon={publications} onPress={()=>{
        //props.navigation.navigate("publications")
        }}/>
      <BkbNavButton Title={staticsStrings.actualite} Icon={actualite} onPress={()=>{
        //props.navigation.navigate("actualite")
        }}/>
      </View>  
      <BkbTransparentCard 
        style={PROFILE_CARD} hexaColorCard={undefined} opacityColorCard={undefined}      // hexaColorCard={undefined} opacityColorCard={undefined}

      >
          <View style={USER_ROW}>
          <TouchableOpacity onPress={()=>{
        props.navigation.navigate("bkbProfile")
      }}>
          <View style={{ backgroundColor:hexToRgbA("#ffffff",0.25),
          height:metrics.heightPercentageToDP(3.7),
          width:metrics.heightPercentageToDP(3.7),
          alignItems:'center',
          alignContent:'center',
          justifyContent:'center',
          borderRadius:metrics.heightPercentageToDP(1),}}>
            <Image source={user_profile}/>
          </View>
          </TouchableOpacity>
          <Text style={PROFILE_TEXT}>{ProfileUserStore.accountLabel}</Text>
          </View>
        <View style={DIVIDER}/>
        <BkbButton
        Icon={logout}
        style={{marginTop:metrics.heightPercentageToDP(3)}}
         ButtonType={"transparent"}
         TextStyle={{fontFamily:"Sora-Bold"}}
         ButtonText={staticsStrings.deconnexion} onPress={()=>
          {
          // Disconnect();
          // ProfileUserStore.setIsLogged(false)
          setDisconnect(true)
         }}/>
      </BkbTransparentCard>
      {/* modal for disconnect  */}
      <BkbModal isVisibleState={disconnect}  style={CANCEL_POP_UP}>
      <Pressable onPress={()=>{setDisconnect(false)}} style={{right:metrics.widthPercentageToDP(4),alignSelf:"flex-end",top:metrics.heightPercentageToDP(-1)}}>
        <Image  source={x_annuler}/>
        </Pressable>
        <Text style={{fontFamily:"Sora-Regular",textAlign:"center",fontSize:metrics.heightPercentageToDP(2.5)}}>{staticsStrings.se_deconnecter}</Text>

        <View style={BUTTONS_ROW}>
        <BkbButton style={OUI_BTN} ButtonType={"white"} ButtonText={"oui"} onPress={()=>{
           Disconnect();
          ProfileUserStore.setIsLogged(false)
        }}/>
        <BkbButton style={OUI_BTN}  ButtonType={"transparent"}  TextStyle={{textAlign:'center',fontFamily:"Sora-Bold",marginRight:metrics.heightPercentageToDP(1.9)}} ButtonText={"non"} onPress={()=>{
          setDisconnect(false)
        }}/>
        </View>
      </BkbModal>
         </View>)
      }
     
    </BkbScreen>
  )
})
const NAVS_CONTAINER: ViewStyle = {
  marginTop:metrics.heightPercentageToDP(5),
}
const PROFILE_CARD: ViewStyle = {
  marginTop:metrics.heightPercentageToDP(1),
  // height:metrics.heightPercentageToDP(22),
  maxHeight:metrics.heightPercentageToDP(26),
  marginHorizontal:metrics.heightPercentageToDP(3),
  marginRight:metrics.heightPercentageToDP(3),
  marginLeft:metrics.heightPercentageToDP(3),
  width:metrics.heightPercentageToDP(43),
  borderWidth:metrics.heightPercentageToDP(0),
  borderColor:color.palette.white,
  alignSelf:'center',
}
const USER_ROW : ViewStyle={
    flexDirection:'row',
    alignSelf:'center',
    alignContent:'center',
    alignItems:'center',
    justifyContent:'space-around',
    width:metrics.heightPercentageToDP(35),
    margin:metrics.heightPercentageToDP(3),
}
const DIVIDER : ViewStyle={
      width:metrics.heightPercentageToDP(35),
      height:metrics.heightPercentageToDP(0.3),
      backgroundColor:"rgba(200,200,200,0.2)",
      borderRadius:metrics.heightPercentageToDP(5),
      alignSelf:'center',
}
const PROFILE_TEXT : TextStyle={
    textAlign:"left",
    marginRight:metrics.heightPercentageToDP(1),
    marginLeft:metrics.heightPercentageToDP(1),
    alignSelf:'center',
    fontSize:metrics.heightPercentageToDP(2.5),
    maxWidth:metrics.heightPercentageToDP(70),
}
//POP UP style
const CANCEL_POP_UP : ViewStyle={
  marginBottom:metrics.heightPercentageToDP(35), 
}
const BUTTONS_ROW : ViewStyle={
  flexDirection:'row-reverse',
  margin:metrics.heightPercentageToDP(1.7),
  justifyContent:"space-around"
}
const OUI_BTN : ViewStyle={
  width:metrics.heightPercentageToDP(16)
}