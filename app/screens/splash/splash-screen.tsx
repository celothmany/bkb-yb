import React, { FC, useEffect } from "react"
import { observer } from "mobx-react-lite"
import { ImageBackground, ImageStyle ,Image, Dimensions, Text} from "react-native"
import { NavigationInjectedProps } from "react-navigation";

import metrics from "../../theme/metrics";

//icons & images

const backgroundImage=require("../../../assets/images/back_ground_image.png")
const bkb_logo=require("../../../assets/images/bkb_logo.png")
// props interface
export interface Splashprops extends NavigationInjectedProps<{}> {
}
export const SplashScreen : FC<Splashprops>= observer( ({navigation}) =>{


  useEffect(()=>{
      setTimeout(()=>{
          navigation.navigate("Deconnectemain")
          // navigation.reset({
          //   index : 0,
          //   routes : [{name :"loginPage"}]
          // })
      },5000);
  },[])
  return (
    <ImageBackground
    source={backgroundImage}
    style={BACKGROUNDSCREEN}
    >  
        <Image style={LOGO_BKB} source={bkb_logo}/>
        <Text >hello</Text>

    </ImageBackground>
  )
})

//style of back ground image
const BACKGROUNDSCREEN : ImageStyle={
  height:metrics.heightPercentageToDP(100),
  width:metrics.widthPercentageToDP(100),
  alignItems:'center',
  alignContent:"center",
  

}
//styles of logo BKB
const LOGO_BKB : ImageStyle={
  alignSelf:'center',
  marginTop:Dimensions.get("window").height/2.9,
}
