import React, { FC } from "react"
import { observer } from "mobx-react-lite"
import { ScrollView, View, ViewStyle } from "react-native"
import { BkbActualitesView, BkbScreen, Screen, Text } from "../../components"
import { color } from "../../theme"
import staticsStrings from "../../localData/staticStrings.json"
import metrics from "../../theme/metrics"
import { NavigationInjectedProps } from "react-navigation"

export interface ActualiteDeconnecteProps extends NavigationInjectedProps<{}> {
  route
}

export const ActualiteDeconnecteScreen : FC<ActualiteDeconnecteProps> = observer( (props)=> {

  return (
    <BkbScreen
    headerShow={true}
    goBackButton={true}

    navigation={props.navigation}

    >
      {/* <View style={HEADER}>
          <Text style={{fontFamily:"Sora-Bold"}}>{staticsStrings.actualite+"s"}</Text>
      </View>

      <ScrollView>
      <BkbActualitesView title={"MANAGEM : Indicateurs semestriels à fin juin 2021"} time_date={"27-09-2021 10:03"} description={"description dhd dhd dhbhdh dhbdhdh dhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizodhhiajoizo ijlsuencjf "}/>

    </ScrollView> */}

    </BkbScreen>
  )
})


//styles 
const HEADER: ViewStyle = {
  // backgroundColor:'#ffffff20',
  backgroundColor:'rgba(63,106,145,0.7)',
  flexDirection:'row',
  justifyContent:'space-around',
  height:metrics.heightPercentageToDP(4.5),
  borderBottomEndRadius:metrics.heightPercentageToDP(2),
  borderBottomStartRadius:metrics.heightPercentageToDP(2),
  paddingHorizontal:metrics.heightPercentageToDP(1),
  paddingTop:metrics.heightPercentageToDP(1),

}