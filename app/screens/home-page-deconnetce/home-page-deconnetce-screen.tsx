import React, { FC } from "react"
import { observer } from "mobx-react-lite"
import { BkbButton, BkbFooter, BkbNavButton, BkbTransparentCard, Text } from "../../components"
import { color } from "../../theme"
import { NavigationInjectedProps } from "react-navigation"
import { Dimensions, ImageBackground, ImageStyle, View, ViewStyle,Image, TouchableOpacity, TextStyle } from "react-native"
import metrics from "../../theme/metrics"
import staticsStrings from "../../localData/staticStrings.json"

//images & icons 
const backgroundImage=require("../../../assets/images/back_ground_image.png")
const bkb_logo=require("../../../assets/images/bkb_logo.png")
const back=require("../../../assets/images/icon_arrow_left.png")
const marche=require("../../../assets/images/marche.png")
const indice=require("../../../assets/images/indice.png")
const palmares=require("../../../assets/images/palmares.png")
const publications=require("../../../assets/images/publications.png")
const actualite=require("../../../assets/images/actualite.png")
const deconnecte=require("../../../assets/images/deconnecte.png")

export interface HomePageDeconnecteProps extends NavigationInjectedProps<{}> {
}


export const HomePageDeconnetceScreen : FC<HomePageDeconnecteProps> = observer((props)=> {
  const hexToRgbA = (hex, opacity) => {
    let c;
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
      c = hex.substring(1).split('');
      if (c.length === 3) {
        c = [c[0], c[0], c[1], c[1], c[2], c[2]];
      }
      c = `0x${c.join('')}`;
      return `rgba(${[(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',')},${opacity})`;
    }
    throw new Error('Bad Hex');
  };
  return (
    <ImageBackground
    source={backgroundImage}
    style={[BACKGROUNDSCREEN,]}
    >
      <View style={HEADER}  >
        <TouchableOpacity 

              onPress={()=>{props.navigation.navigate("AppStack")}}
              style={{ 
              alignSelf:'center',
              marginTop:metrics.heightPercentageToDP(3),
              borderWidth:metrics.heightPercentageToDP(0.3),  
              borderColor:hexToRgbA("#ffffff",0.25),
              height:metrics.heightPercentageToDP(4.5),
              width:metrics.heightPercentageToDP(4.5),
              alignItems:'center',
              alignContent:'center',
              justifyContent:'center',
              borderRadius:metrics.heightPercentageToDP(1),}}>
              <Image  source={back}/>

      </TouchableOpacity>
              <Image source={bkb_logo} style={LOGO_BKB} />


      </View>

      <View style={NAVS_CONTAINER}>
      <BkbNavButton Title={staticsStrings.marches} Icon={marche} onPress={()=>{
        //props.navigation.navigate("marcheDeconnecte")
      }}/>
      <BkbNavButton Title={staticsStrings.Indices}Icon={indice} onPress={()=>{
        // props.navigation.navigate("indices")
        }}/>
      <BkbNavButton Title={staticsStrings.palmares} Icon={palmares} onPress={()=>{
        // props.navigation.navigate("palmares")

        }}/>
      <BkbNavButton Title={staticsStrings.publications} Icon={publications} onPress={()=>{
        //props.navigation.navigate("publicationDeconnecte")

        }}/>
      <BkbNavButton Title={staticsStrings.actualite} Icon={actualite} onPress={()=>{
        //props.navigation.navigate("actualiteDeconnecte")

        }}/>

      </View>
  
      <BkbTransparentCard hexaColorCard={undefined} opacityColorCard={undefined} style={PROFILE_CARD} >
          <Text style={TEXT_DECONNECTE}>{staticsStrings.connectez_vous_fonctionnalites_deconnecte}</Text>
          <BkbButton ButtonType={"transparent"} ButtonText={staticsStrings.se_connecter_deconnecte}  TextStyle={ {fontFamily:"Sora-Bold",marginRight:metrics.heightPercentageToDP(1.8),}} onPress={()=>{
            props.navigation.navigate("loginPage")
            }}/>
          <BkbButton ButtonType={"white"} ButtonText={staticsStrings.devenir_client_deconnecte} onPress={()=>{
            //props.navigation.navigate("devenirClientDeconnecte")
            }} />
      </BkbTransparentCard>

      
     
      <BkbTransparentCard 
        gradient={true}
        gradientColor={"#FB5F4F"}
        style={GRADIENT_CARD}
        hexaColorCard={"#FB5F4F"} opacityColorCard={0.6} 
      >
       <View style={GRADIENT_CARD_DECONNECTE}>
         <Image source={deconnecte} style={{margin:metrics.heightPercentageToDP(1)}} />
       <Text>{staticsStrings.en_mode_deconnecte}</Text>
       </View>
      </BkbTransparentCard>

    <BkbFooter/>
      

      
    </ImageBackground>
  )
})


//style of back ground image
const BACKGROUNDSCREEN : ImageStyle={
  // height:metrics.heightPercentageToDP(100),
  // width:metrics.widthPercentageToDP(100),
  width:Dimensions.get("window").width,
  height:Dimensions.get("window").height

}

const HEADER : ViewStyle={
  flexDirection:"row",
  justifyContent:"space-between",
  paddingHorizontal:metrics.heightPercentageToDP(2),
  
}

const LOGO_BKB : ImageStyle={
  alignSelf:'center',
  marginTop:metrics.heightPercentageToDP(3),
}

const NAVS_CONTAINER: ViewStyle = {
  marginTop:metrics.heightPercentageToDP(5),
}

const PROFILE_CARD: ViewStyle = {
  height:metrics.heightPercentageToDP(22),
  // maxHeight:metrics.heightPercentageToDP(26),
  marginHorizontal:metrics.heightPercentageToDP(3),
  marginRight:metrics.heightPercentageToDP(3),
  marginLeft:metrics.heightPercentageToDP(3),
  width:metrics.heightPercentageToDP(43),
  borderWidth:metrics.heightPercentageToDP(0),
  borderColor:color.palette.white,
  alignItems:'center',
  alignContent:"center",
  justifyContent:"space-evenly",
  marginBottom:metrics.heightPercentageToDP(4),
  // marginTop:metrics.heightPercentageToDP(5),


}
const TEXT_DECONNECTE : TextStyle={
 textAlign:"center",
 alignSelf:"center",
 fontFamily:"Sora-Regular",
 marginHorizontal:metrics.heightPercentageToDP(2),
}
const GRADIENT_CARD : ViewStyle={
  height:metrics.heightPercentageToDP(7),
  padding:metrics.heightPercentageToDP(1),
  width:metrics.heightPercentageToDP(42),
  marginRight:metrics.heightPercentageToDP(3),
  marginLeft:metrics.heightPercentageToDP(3),
  
}
const GRADIENT_CARD_DECONNECTE : ViewStyle={
  flexDirection:"row",
  alignContent:"center",
  alignItems:"center",

}