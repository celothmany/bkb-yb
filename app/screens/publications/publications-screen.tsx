import React, { FC, useState } from "react"
import { observer } from "mobx-react-lite"
import { ScrollView, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { BkbPicker, BkbPublicationCard, BkbScreen, Text } from "../../components"
import { color } from "../../theme"
import { NavigationInjectedProps } from "react-navigation"
import metrics from "../../theme/metrics"
import staticsStrings from "../../localData/staticStrings.json"
import { Picker } from "@react-native-picker/picker"



export interface PublicationsProps extends NavigationInjectedProps<{}> {
  route
}

export const PublicationsScreen : FC<PublicationsProps> = observer( (props)=> {
  const [tab ,setTab]=useState(0)
  const [valuePicker,setValuePicker]=useState("")

  return (
    <BkbScreen
    navigation={props.navigation}
    headerShow={true}
    goBackButton={true}>
      <View style={HEADER}>
          <Text style={{fontFamily:"Sora-Bold"}}>{staticsStrings.publications}</Text>
      </View>
      <View style={TABS} >
        <TouchableOpacity style={TAB_CONTAINER}  onPress={()=>{setTab(0)}}>
        <Text style ={tab===0 ? TEXT_isTab : TEXT_isNotTab}>{staticsStrings.toutes}</Text>
        <View  style={tab===0 ? underLine_isTab : underLine_isNotTab}/>
        </TouchableOpacity>
      
        <TouchableOpacity style={TAB_CONTAINER} onPress={()=>{setTab(1)}}>
        <Text style ={tab===1 ? TEXT_isTab : TEXT_isNotTab}  >{staticsStrings.marche}</Text>
        <View  style={tab===1 ? underLine_isTab : underLine_isNotTab}/>
        </TouchableOpacity>

        <TouchableOpacity style={TAB_CONTAINER} onPress={()=>{setTab(2)}}>
        <Text style ={tab===2 ? TEXT_isTab : TEXT_isNotTab}  >{staticsStrings.valeur+"s"}</Text>
        <View  style={tab===2 ? underLine_isTab : underLine_isNotTab}/>
        </TouchableOpacity>

      
      </View>
    <ScrollView>

      {
        tab===0 && 
        <BkbPublicationCard dateTime={"22-01-22"} asselabel={"Flash : ADDOHA"}/>
      }
      {
        tab===1 && 
        <View>
          <View  style={{alignItems:'center'}} >
          <BkbPicker mode_picker={"dropdown"} onFocus={()=>{}} onBlur={()=>{}} value={valuePicker} onChange={setValuePicker}>
          <Picker.Item  color={color.palette.bkbGreyColor} label={"Toutes"} value={"Toutes"} />
          <Picker.Item  color={color.palette.bkbGreyColor} label={"Toutses"} value={"Toutses"} />

            </BkbPicker>
          </View>
          <BkbPublicationCard dateTime={"22-01-22"} asselabel={"Flash : ADDOHA"}/>
        </View>
      }
      {
        tab===2 && 
        <BkbPublicationCard dateTime={"22-01-22"} asselabel={"Flash : ADDOHA"}/>
      }

    </ScrollView>
    </BkbScreen>
  )
})

const HEADER: ViewStyle = {
  // backgroundColor:'#ffffff20',
  backgroundColor:'rgba(63,106,145,0.7)',
  flexDirection:'row',
  justifyContent:'space-around',
  height:metrics.heightPercentageToDP(4.5),
  borderBottomEndRadius:metrics.heightPercentageToDP(2),
  borderBottomStartRadius:metrics.heightPercentageToDP(2),
  paddingHorizontal:metrics.heightPercentageToDP(1),
  paddingTop:metrics.heightPercentageToDP(1),

}
const TABS : ViewStyle={
  marginTop:metrics.heightPercentageToDP(1),
  flexDirection:'row',
  // justifyContent:"space-around",
  alignSelf:'center',
  marginHorizontal:metrics.heightPercentageToDP(1),
  // width:metrics.heightPercentageToDP(50)
}
const TEXT_isTab: TextStyle={
  fontFamily:"Sora-Regular",
}
const TEXT_isNotTab: TextStyle={
    color:"#ffffff85"

}
const underLine_isTab:ViewStyle={
  height:metrics.heightPercentageToDP(0.5),
  backgroundColor:color.palette.white,
  // borderRadius:metrics.heightPercentageToDP(3),
  marginTop:metrics.heightPercentageToDP(0.5),
  width:metrics.heightPercentageToDP(16)
}

const underLine_isNotTab:ViewStyle={
  height:metrics.heightPercentageToDP(0.5),
  backgroundColor:"#ffffff85",
  // borderRadius:metrics.heightPercentageToDP(3),
  

  marginTop:metrics.heightPercentageToDP(0.7),
  width:metrics.heightPercentageToDP(16)
}

const TAB_CONTAINER : ViewStyle={
  alignItems:"center",
  alignSelf:'center',
  alignContent:'center',
}
