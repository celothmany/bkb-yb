import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import { Pressable, TextStyle, TouchableOpacity, View, ViewStyle,Image } from "react-native"
import { ActionCard, BkbDonutChart, BkbDonutChartCustom, BkbLineChart, BkbLineTable, BkbLoading, BkbLoadingPage, BkbPieChart, BkbScreen, BkbTransparentCard, CarnetOrdreModal, Text } from "../../components"

import { color } from "../../theme"
import metrics from "../../theme/metrics"
import { NavigationInjectedProps } from "react-navigation"
import { useStores } from "../../models"
import { useIsFocused } from "@react-navigation/native"
import { ScrollView } from "react-native-gesture-handler"
import staticsStrings from "../../localData/staticStrings.json"
import Modal from "react-native-modal";

//images & icons
const x_annuler=require("../../../assets/images/x_annuler.png")


export interface PorteFeuilleDetailsProps extends NavigationInjectedProps<{}> {
  route
}

export const PortefeuilleDetailsScreen : FC<PorteFeuilleDetailsProps> = observer((props)=> {
  const { ProfileUserStore,SyntheseStore,ValuesStore,CarnetOrdreStore,CarnetOrdreModalSore} = useStores()

  const GetValues=async ()=>{
    await  ValuesStore.GetValuesBKB(ProfileUserStore.login,ProfileUserStore.contextId,ProfileUserStore.accountCode,3)
 
   }
   const GetOrderList=async ()=>{
    await  CarnetOrdreStore.GetOrderListBKB(ProfileUserStore.login,ProfileUserStore.contextId,ProfileUserStore.accountCode)
 
   }
   // we are still displaying local data set for graphic , until we get the link to the market dataset
   const pieData = [
    {value: 64, color: color.palette.BKBBlueColor, text: `64%`},
    {value: 40, color: color.palette.offWhite, text: '30%'},
    {value: 20, color: color.palette.BKbGreenColor1, text: '26%'},
  ];
  //get data set for graphic
  const getDataSetGraphic=(filterData :number)=>{
    if(filterData===0){
      //aujourd'hui
      return [Math.random() * 10,Math.random() * 10,Math.random() * 10,Math.random() * 10,Math.random() * 10,Math.random() * 10]
    }
    if(filterData===1){
      //6 mois
      return [100,200,172,420,246,689]
    }
    if(filterData===2){
      // 1 an
      return [110,223,90,130,341,600,555]
    }
    if(filterData===3){
      //3 ans
      return [110,203,90,132,341,600,505]
    }
    if(filterData===4){
      //5 ans
      return [110,203,190,132,341,231,585]
    }
   
  }
  //get labels for graphic 

  const getLabelsGraphic=(filterLabel :number)=>{
    if(filterLabel===0){
      //aujourd'hui
      return ["Jan","Fév","Mars","Avr","Mai","Juin"]
    }
    if(filterLabel===1){
      //6 mois
      return ["Jan","Fév","Mars","Avr","Mai","Juin"]
    }
    if(filterLabel===2){
      // 1 an
      return ["Jan","Fév","Mars","Avr","Mai","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
    }
    if(filterLabel===3){
      // 3 ans
      return ["Jan","Fév","Mars","Avr","Mai","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
    }
    if(filterLabel===4){
      // 5 ans
      return ["Jan","Fév","Mars","Avr","Mai","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
    }
   
    
  }
 
  
  const isFocused = useIsFocused();
  const [tab,setTab]=useState(0);
  const [tabGraphic,setTabGraphic]=useState(0);
  const [tabStatistic,setTabStatistic]=useState(0);
  const [tabPerformance,setTabPerformance]=useState(0);
  const [tabVolatilite,setTabVolatilite]=useState(0);
  const [modalAchat,setModalAchat]=useState(false);
  const [side,setSide]=useState(0);


   useEffect(()=>{
    GetValues()
    GetOrderList()
    // console.log(ValuesStore.plainInventoryElementList[props.route.params.indexTitre].assetCode)
   },[isFocused])
  return (
    <BkbScreen
    goBackButton={true}
    headerShow={true}
    styleParent={HEADER_STYLE}
    navigation={props.navigation}
    >
      {
        ValuesStore.loadingValues ? (<BkbLoadingPage/>)
        :
        (<ScrollView>
          <ActionCard  
          onPressAddWatch={()=>{
          }}
          
            onPressActionAcheter={() => { 
              setSide(0)
              setModalAchat(true)
              console.log("acheter") 
            } }
            onPressActionVendre={() => { 
              console.log("vendre")
              setSide(1)
     
              setModalAchat(true)
    
            } }
            idTitre={ValuesStore.plainInventoryElementList[props.route.params.indexTitre].assetCode}
            nameTitre={ValuesStore.plainInventoryElementList[props.route.params.indexTitre].assetLabel}
            valueTitre={ValuesStore.plainInventoryElementList[props.route.params.indexTitre].pendingProfitAndLossValuation}
            percentTitre1={ValuesStore.plainInventoryElementList[props.route.params.indexTitre].assetCoupon}
            percentTitre2={ValuesStore.plainInventoryElementList[props.route.params.indexTitre].pendingProfitAndLossPercent} 
            currency={ValuesStore.plainInventoryElementList[props.route.params.indexTitre].assetCurrency}      />
    
            {/* Tabs  */}
            <View style={TABS}>
            <ScrollView  horizontal={true}>
    
              <TouchableOpacity 
              style={TAB_STYLE}
              onPress={()=>{setTab(0)}}><Text style={tab===0 ? isTabStyle : isNotTabStyle}>{staticsStrings.cotation}</Text>
              {
              tab===0 && <View style={underlineTab}/> 
              }
              </TouchableOpacity>
    
              <TouchableOpacity  onPress={()=>{setTab(1),GetOrderList()}}><Text style={tab===1 ? isTabStyle : isNotTabStyle} >{staticsStrings.carnet_dordres}</Text>
              {
              tab===1 && <View style={underlineTab}/> 
              }
              </TouchableOpacity>
    
              <TouchableOpacity  
               style={TAB_STYLE}
              onPress={()=>{setTab(2)}}><Text style={tab===2 ? isTabStyle : isNotTabStyle}>{staticsStrings.graphique}</Text>
              {
              tab===2 && <View style={underlineTab}/> 
              }
              </TouchableOpacity>
    
              <TouchableOpacity  
               style={TAB_STYLE}
              onPress={()=>{setTab(3)}}><Text style={tab===3 ? isTabStyle : isNotTabStyle}>{staticsStrings.statistique}</Text>
              {
              tab===3 && <View style={underlineTab}/> 
              }
              </TouchableOpacity>
              </ScrollView>
    
           </View>
            {
              // cotation
              tab===0 && 
                <BkbTransparentCard style={COTATION_CONTAINER} hexaColorCard={undefined} opacityColorCard={undefined}>
                   <ScrollView>
                      {/* seance */}
                    <View style={TITRE_HEADER}>
                    <Text style={TITRE_TEXT}>{staticsStrings.seance}</Text>
                    </View>
                    <BkbLineTable isBlurLine={true}>
                      <Text style={[CELL_TABLE_STYLE,{width:metrics.heightPercentageToDP(10)}]}>{staticsStrings.dontBloques}</Text>
                      <Text style={[TITRE_TABLE_STYLE,{marginLeft:metrics.heightPercentageToDP(1),width:metrics.heightPercentageToDP(9)}]}>{SyntheseStore.currency}</Text>
                      <Text style={[CELL_TABLE_STYLE,,{width:metrics.heightPercentageToDP(10)}]}>{staticsStrings.dontBloques}</Text>
                      <Text style={[TITRE_TABLE_STYLE,{marginLeft:metrics.heightPercentageToDP(1),width:metrics.heightPercentageToDP(9)}]}>{SyntheseStore.currency}</Text>
                    </BkbLineTable>
                    <BkbLineTable isBlurLine={false}>
                      <Text style={[CELL_TABLE_STYLE,{width:metrics.heightPercentageToDP(10)}]}>{staticsStrings.dontBloques}</Text>
                      <Text style={[TITRE_TABLE_STYLE,{marginLeft:metrics.heightPercentageToDP(1),width:metrics.heightPercentageToDP(9)}]}>{SyntheseStore.currency}</Text>
                      <Text style={[CELL_TABLE_STYLE,,{width:metrics.heightPercentageToDP(10)}]}>{staticsStrings.dontBloques}</Text>
                      <Text style={[TITRE_TABLE_STYLE,{marginLeft:metrics.heightPercentageToDP(1),width:metrics.heightPercentageToDP(9)}]}>{SyntheseStore.currency}</Text>
                    </BkbLineTable>
                   
                    
               
    
                    {/* derniere_transactions */}
                    <View style={TITRE_HEADER}>
                    <Text style={TITRE_TEXT}>{staticsStrings.derniere_transactions}</Text>
                    </View> 
                    <BkbLineTable isBlurLine={true}>
                      <Text style={[CELL_TABLE_STYLE,{fontFamily:"Sora-Bold"}]}>{"Heure"}</Text>
                      <Text style={[CELL_TABLE_STYLE,{fontFamily:"Sora-Bold"}]}>{"Cours"}</Text>
                      <Text style={[CELL_TABLE_STYLE,{fontFamily:"Sora-Bold"}]}>{"Quantité"}</Text>
                    </BkbLineTable>
                    <BkbLineTable isBlurLine={false}>
                      <Text style={[CELL_TABLE_STYLE,{fontFamily:"Sora-ExtraBold"}]}>{"15:08"}</Text>
                      <Text style={[CELL_TABLE_STYLE,{fontFamily:"Sora-ExtraBold"}]}>{"1,2222,22"}</Text>
                      <Text style={[CELL_TABLE_STYLE,{fontFamily:"Sora-ExtraBold"}]}>{"16"}</Text>
                    </BkbLineTable>
                
                   
                  
                   
                   
                 
                   
                   
                   </ScrollView>
                    
                        
                </BkbTransparentCard>
            }
            {
              // carnet d'ordres
              tab===1 && 
              <BkbTransparentCard style={[COTATION_CONTAINER,{padding:metrics.heightPercentageToDP(1.5)}]} hexaColorCard={undefined} opacityColorCard={undefined}>
              {CarnetOrdreStore.loadingOrderList && <BkbLoading color={color.palette.white} size={60}/>}
    
              <View style={CARNET_STYLE}>
                  <View style={{marginRight:metrics.heightPercentageToDP(1)}}>
                  <BkbLineTable  isBlurLine={true} style={ACHAT}>
                  <Text style ={ACHAT_VENTE_TEXT}>{staticsStrings.achat}</Text>
                </BkbLineTable>
    
                  {/* nombre qte cours achat */}
    
                  <BkbLineTable style={ACHAT}>
                      <Text style={CELL_COTATION_STYLE}>{staticsStrings.nombre}</Text>
                      <Text style={CELL_COTATION_STYLE} >{staticsStrings.qte}</Text>
                      <Text style={CELL_COTATION_STYLE} >{staticsStrings.cours}</Text>
    
                  </BkbLineTable>
                  {/* map data of achat */}
    
                  <ScrollView>
                  {
                    CarnetOrdreStore.orderList.filter(item=>item.sideLabel==="BUY").map((orders,index)=>{
                        return (
                          <BkbLineTable isBlurLine={index%2===0 ? true : false} style={ACHAT}>
                      <Text style={CELL_COTATION_VALUE_STYLE}>{orders.quantityDecimalNumber}</Text>
                      <Text style={CELL_COTATION_VALUE_STYLE} >{orders.quantity}</Text>
                      <Text style={CELL_COTATION_VALUE_STYLE} >{orders.executionPrice}</Text>
                      </BkbLineTable>
                        );
                    })
                  }
               
                  
                  </ScrollView>
                  </View>
    
    
                <View style={{marginRight:metrics.heightPercentageToDP(1)}}>
                <BkbLineTable  isBlurLine={true} style={VENTE}>
                    <Text style={ACHAT_VENTE_TEXT} >{staticsStrings.vente}</Text>
                </BkbLineTable>
                  {/* nombre qte cours vente */}
    
                <BkbLineTable style={ACHAT}>
                      <Text style={CELL_COTATION_STYLE}>{staticsStrings.nombre}</Text>
                      <Text style={CELL_COTATION_STYLE} >{staticsStrings.qte}</Text>
                      <Text style={CELL_COTATION_STYLE} >{staticsStrings.cours}</Text>
    
                  </BkbLineTable>
                  {/* map data of vente */}
    
                  <ScrollView>
                  {
                    CarnetOrdreStore.orderList.filter(item=>item.sideLabel==="SELL").map((orders,index)=>{
                        return (
                          <BkbLineTable isBlurLine={index%2===0 ? true : false} style={ACHAT}>
                      <Text style={CELL_COTATION_VALUE_STYLE}>{orders.quantityDecimalNumber}</Text>
                      <Text style={CELL_COTATION_VALUE_STYLE} >{orders.quantity}</Text>
                      <Text style={CELL_COTATION_VALUE_STYLE} >{orders.executionPrice}</Text>
                      </BkbLineTable>
                        );
                    })
                  }
               
                  
                  </ScrollView>
               
                </View>
                
    
    
              </View>
              </BkbTransparentCard>
              
            }
            {
              // graphique
             
              tab===2 && 
    
           
         <BkbTransparentCard style={GRAPHIQUE} hexaColorCard={undefined} opacityColorCard={undefined}>
          <ScrollView>
            {/* Tabs Graphique  */}
          <View style={TABS}>
          <ScrollView  horizontal={true}>
    
            <TouchableOpacity 
            style={TAB_STYLE_GRAPHIC}
            onPress={()=>{setTabGraphic(0)}}><Text style={tabGraphic===0 ? isTabStyleGraphic : isNotTabStyleGraphic}>{staticsStrings.aujourdhui}</Text>
            {
            tabGraphic===0 ? (<View style={underlineTabGraphic}/> ):(<View style={underlineTabGraphicInvisible}/> )
          }
            </TouchableOpacity>
    
            <TouchableOpacity  onPress={()=>{setTabGraphic(1)}}><Text style={tabGraphic===1 ? isTabStyleGraphic : isNotTabStyleGraphic} >{staticsStrings.six_mois}</Text>
            {
            tabGraphic===1 ? (<View style={underlineTabGraphic}/> ):(<View style={underlineTabGraphicInvisible}/> )
          }
            </TouchableOpacity>
    
            <TouchableOpacity  
             style={TAB_STYLE_GRAPHIC}
            onPress={()=>{setTabGraphic(2)}}><Text style={tabGraphic===2 ? isTabStyleGraphic : isNotTabStyleGraphic}>{staticsStrings.un_an}</Text>
            {
            tabGraphic===2 ? (<View style={underlineTabGraphic}/> ):(<View style={underlineTabGraphicInvisible}/> )
          }
            </TouchableOpacity>
    
            <TouchableOpacity  
             style={TAB_STYLE_GRAPHIC}
            onPress={()=>{setTabGraphic(3)}}><Text style={tabGraphic===3 ? isTabStyleGraphic : isNotTabStyleGraphic}>{staticsStrings.trois_ans}</Text>
            {
            tabGraphic===3 ? (<View style={underlineTabGraphic}/> ):(<View style={underlineTabGraphicInvisible}/> )
          }
            </TouchableOpacity>
    
            <TouchableOpacity  
             style={TAB_STYLE_GRAPHIC}
            onPress={()=>{setTabGraphic(4)}}><Text style={tabGraphic===4 ? isTabStyleGraphic : isNotTabStyleGraphic}>{staticsStrings.cinq_ans}</Text>
            {
            tabGraphic===4 ? (<View style={underlineTabGraphic}/> ):(<View style={underlineTabGraphicInvisible}/> )
            }
            </TouchableOpacity>
            </ScrollView>
    
             </View>
    
                <BkbLineChart dataSet={getDataSetGraphic(tabGraphic)} labelsSet={getLabelsGraphic(tabGraphic)} toolTipText1={"25 Mai 2020"} toolTipText2={" MAD"}/>
            
            </ScrollView>     
         </BkbTransparentCard>
            }
            {
              // statistiques
              tab===3 && 
              <BkbTransparentCard style={STATISTICS_STYLE} hexaColorCard={undefined} opacityColorCard={undefined}>
                 <View style={FILTER_VALUES}>
                  <ScrollView horizontal={true}>
                  <TouchableOpacity onPress={()=>{setTabStatistic(0)}} style={tabStatistic===0 ? FILTER_TABBED : FILTER_NOTABBED}>
                  <Text style={FILTER_TEXT}>{staticsStrings.signaletique}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{setTabStatistic(1)}} style={tabStatistic===1 ? FILTER_TABBED : FILTER_NOTABBED}>
                  <Text style={FILTER_TEXT}>{staticsStrings.dividendes}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{setTabStatistic(2)}} style={tabStatistic===2 ? FILTER_TABBED : FILTER_NOTABBED}>
                  <Text style={FILTER_TEXT}>{staticsStrings.performance}</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>{setTabStatistic(3)}} style={tabStatistic===3 ? FILTER_TABBED : FILTER_NOTABBED}>
                  <Text style={FILTER_TEXT}>{staticsStrings.volatilité}</Text>
                </TouchableOpacity>
                  </ScrollView>
                 </View>
    
                 {
                   tabStatistic===0 && 
                   <View style={{justifyContent:"center",alignItems:'center'}}>
                     <BkbLineTable isBlurLine={true}>
                     <Text style={CELL_TABLE_STYLE}>hello</Text>
                     <Text  style={TITRE_TABLE_STYLE}>hello</Text>
                     </BkbLineTable>
                     <BkbLineTable isBlurLine={false}>
                     <Text style={CELL_TABLE_STYLE}>hello</Text>
                     <Text  style={TITRE_TABLE_STYLE}>hello</Text>
                     </BkbLineTable>
                     <BkbLineTable isBlurLine={true}>
                     <Text style={CELL_TABLE_STYLE}>hello</Text>
                     <Text  style={TITRE_TABLE_STYLE}>hello</Text>
                     </BkbLineTable>
                     <BkbLineTable isBlurLine={false}>
                     <Text style={TITRE_TABLE_STYLE}>Actionnariat</Text>
                     <Text  style={TITRE_TABLE_STYLE}>hello</Text>
                     </BkbLineTable>
                      {/* donalts Pie chart  */}
                      {/* <BkbPieChart/>
                      <BkbDonutChart dataSetDonut={pieData}/> */}


                      

                   </View>
    
                   
                 }
    
                 {
                   tabStatistic===1 && 
                   <View style={{justifyContent:"center",alignItems:'center'}}>
                     <BkbLineTable style={BKB_LINE_TABLE} isBlurLine={true}>
                     <Text style={TITRE_TABLE_STYLE}>{staticsStrings.annee}</Text>
                     <Text style={TITRE_TABLE_STYLE}>{staticsStrings.date_echatement}</Text>
                     <Text style={TITRE_TABLE_STYLE}>{staticsStrings.date_paiement}</Text>
                     <Text style={TITRE_TABLE_STYLE}>{staticsStrings.montant_net}</Text>
                     </BkbLineTable>
                     <BkbLineTable style={BKB_LINE_TABLE}  isBlurLine={false}>
                     <Text style={CELL_DIVIDENDES_VALUE_STYLE}>2019</Text>
                     <Text  style={CELL_DIVIDENDES_VALUE_STYLE}>11/11/1111</Text>
                     <Text  style={CELL_DIVIDENDES_VALUE_STYLE}>11/11/1111</Text>
                     <Text  style={CELL_DIVIDENDES_VALUE_STYLE}>34,85</Text>
    
                     </BkbLineTable>
                     <BkbLineTable style={BKB_LINE_TABLE}  isBlurLine={true}>
                     <Text style={CELL_DIVIDENDES_VALUE_STYLE}>2019</Text>
                     <Text  style={CELL_DIVIDENDES_VALUE_STYLE}>11/11/1111</Text>
                     <Text  style={CELL_DIVIDENDES_VALUE_STYLE}>11/11/1111</Text>
                     <Text  style={CELL_DIVIDENDES_VALUE_STYLE}>34,85</Text>
    
                     </BkbLineTable>
                    
                    
                   </View>
    
                   
                 }
                 {
                   tabStatistic===2 && 
                   <View style={{justifyContent:"center",alignItems:'center'}}>
                     <BkbLineTable style={BKB_LINE_TABLE}  isBlurLine={false}>
                     <Text style={CELL_DIVIDENDES_VALUE_STYLE}>{staticsStrings.ytd_percent}</Text>
                    <TouchableOpacity onPress={()=>setTabPerformance(0)}>
                      <Text  style={[ tabPerformance===0?CELL_DIVIDENDES_VALUE_STYLE_WITH_BLUR:CELL_DIVIDENDES_VALUE_STYLE]}>{staticsStrings.quatre_sem}</Text></TouchableOpacity>
                      <TouchableOpacity onPress={()=>setTabPerformance(1)}>
                    <Text  style={[ tabPerformance===1?CELL_DIVIDENDES_VALUE_STYLE_WITH_BLUR:CELL_DIVIDENDES_VALUE_STYLE]}>{staticsStrings.douze_sem}</Text>
                      </TouchableOpacity> 
    
                      <TouchableOpacity onPress={()=>setTabPerformance(2)}>
                    <Text  style={[ tabPerformance===2?CELL_DIVIDENDES_VALUE_STYLE_WITH_BLUR:CELL_DIVIDENDES_VALUE_STYLE]}>{staticsStrings.vingt_six}</Text>
    
                    </TouchableOpacity >
                    <TouchableOpacity onPress={()=>setTabPerformance(3)}>
                    <Text  style={[ tabPerformance===3?CELL_DIVIDENDES_VALUE_STYLE_WITH_BLUR:CELL_DIVIDENDES_VALUE_STYLE]}>{staticsStrings.cinquante_deux}</Text>
    
                    </TouchableOpacity>
    
                     </BkbLineTable>
                     <BkbLineTable style={BKB_LINE_TABLE}  isBlurLine={true}>
                     <Text style={CELL_PERFORMANCE_VALUE_STYLE}>{staticsStrings.perf_percent}</Text>
                     <Text  style={CELL_PERFORMANCE_VALUE_STYLE}>{staticsStrings.plus_haut}</Text>
                     <Text  style={CELL_PERFORMANCE_VALUE_STYLE}>{staticsStrings.date_plus_haut}</Text>
                     <Text  style={CELL_PERFORMANCE_VALUE_STYLE}>{staticsStrings.plus_bas}</Text>
                     <Text  style={CELL_PERFORMANCE_VALUE_STYLE}>{staticsStrings.date_plus_bas}</Text>
    
                     </BkbLineTable>
                     <BkbLineTable style={BKB_LINE_TABLE}  isBlurLine={false}>
                     <Text style={CELL_DIVIDENDES_VALUE_STYLE}>{"+23,2"}</Text>
                     <Text  style={CELL_DIVIDENDES_VALUE_STYLE}>{"22,22"}</Text>
                     <Text  style={CELL_DIVIDENDES_VALUE_STYLE}>{"22,22"}</Text>
                     <Text  style={CELL_DIVIDENDES_VALUE_STYLE}>{"22,22"}</Text>
                     <Text  style={CELL_DIVIDENDES_VALUE_STYLE}>{"22,22"}</Text>
    
                     </BkbLineTable>
                    
                    
                   </View>
    
                   
                 }
    
              {
                   tabStatistic===3 && 
                   <View style={{justifyContent:"center",alignItems:'center'}}>
                     <ScrollView>
                     <BkbLineTable style={BKB_LINE_TABLE}  isBlurLine={false}>
                     <Text style={CELL_DIVIDENDES_VALUE_STYLE}>{staticsStrings.ytd_percent}</Text>
                     <TouchableOpacity onPress={()=>setTabVolatilite(0)}>
                     <Text style={[ tabVolatilite===0?CELL_DIVIDENDES_VALUE_STYLE_WITH_BLUR:CELL_DIVIDENDES_VALUE_STYLE]}>{staticsStrings.quatre_sem}</Text>
    
                     </TouchableOpacity>
                     <TouchableOpacity onPress={()=>setTabVolatilite(1)}>
                     <Text  style={[ tabVolatilite===1?CELL_DIVIDENDES_VALUE_STYLE_WITH_BLUR:CELL_DIVIDENDES_VALUE_STYLE]}>{staticsStrings.douze_sem}</Text>
    
                     </TouchableOpacity>
                      <TouchableOpacity onPress={()=>setTabVolatilite(2)}>
                      <Text  style={[ tabVolatilite===2?CELL_DIVIDENDES_VALUE_STYLE_WITH_BLUR:CELL_DIVIDENDES_VALUE_STYLE]}>{staticsStrings.vingt_six}</Text>
    
                      </TouchableOpacity>
    
                      <TouchableOpacity onPress={()=>setTabVolatilite(3)}>
                      <Text  style={[ tabVolatilite===3?CELL_DIVIDENDES_VALUE_STYLE_WITH_BLUR:CELL_DIVIDENDES_VALUE_STYLE]}>{staticsStrings.cinquante_deux}</Text>
    
                      </TouchableOpacity>
                     </BkbLineTable>
                     <BkbLineTable style={BKB_LINE_TABLE}  isBlurLine={true}>
                     <Text style={CELL_PERFORMANCE_VALUE_STYLE}>{staticsStrings.volatilité}</Text>
                     <Text  style={CELL_PERFORMANCE_VALUE_STYLE}>{staticsStrings.quantite_KDH}</Text>
                     <Text  style={CELL_PERFORMANCE_VALUE_STYLE}>{staticsStrings.volume_moyen_dh}</Text>
                     <Text  style={CELL_PERFORMANCE_VALUE_STYLE}>{staticsStrings.rotation_du_capital_percent}</Text>
    
                     </BkbLineTable>
                     <BkbLineTable style={BKB_LINE_TABLE}  isBlurLine={false}>
                     <Text style={CELL_DIVIDENDES_VALUE_STYLE}>{"+23,2%"}</Text>
                     <Text  style={CELL_DIVIDENDES_VALUE_STYLE}>{"22,22"}</Text>
                     <Text  style={CELL_DIVIDENDES_VALUE_STYLE}>{"22,22"}</Text>
                     <Text  style={CELL_DIVIDENDES_VALUE_STYLE}>{"223,22"}</Text>
    
                     </BkbLineTable>
                    
                      <BkbDonutChartCustom dataSet={[10,80,5,10]}/>
                     </ScrollView>
                    
                   </View>
    
                   
                 }
    
    
              </BkbTransparentCard>
            }
            <Modal  
                backdropColor={color.palette.BKbBackDropColor}
    
            style={{}}
            isVisible={modalAchat}>
          <Pressable onPress={()=>{setModalAchat(false)}} style={{right:metrics.widthPercentageToDP(4),alignSelf:"flex-end",top:metrics.heightPercentageToDP(-4)}}>
            <Image  source={x_annuler}/>
            </Pressable>
            <View style={{backgroundColor:'rgba(63,106,145,0.99)',borderRadius:metrics.heightPercentageToDP(1),
            padding:metrics.heightPercentageToDP(1),
            width:metrics.heightPercentageToDP(44.5),
    
            }}>
          
          <CarnetOrdreModal 
          side={side}
          ValeurOdre={ValuesStore.plainInventoryElementList[props.route.params.indexTitre].assetLabel} AcheterPress={() => {
                  setModalAchat(false)
                } } date={0} 
                assetCode={ValuesStore.plainInventoryElementList[props.route.params.indexTitre].assetCode} 
                QuotationCodePlace={102} withQuote={0} marketCode={ValuesStore.plainInventoryElementList[props.route.params.indexTitre].marketCode}/>
            </View>
    
            {
              CarnetOrdreModalSore.message_error===0 &&
              <BkbTransparentCard    gradientColor={"#FB8A8A"}    style={GRADIENT_CARD} gradient={true} hexaColorCard={undefined} opacityColorCard={undefined}>
              <Text style={{fontFamily:"Sora-Bold"}}>{staticsStrings.avertissement}</Text>
                <Text style={GRADIENT_TEXT}>{staticsStrings.message_avertissement}</Text>
              
              </BkbTransparentCard>
            }
            {
              CarnetOrdreModalSore.message_error===1 &&
              <BkbTransparentCard    gradientColor={"#FB8A8A"}    style={GRADIENT_CARD} gradient={true} hexaColorCard={undefined} opacityColorCard={undefined}>
              <Text style={{fontFamily:"Sora-Bold"}}>{staticsStrings.controle_des_couvertures}</Text>
                <Text style={GRADIENT_TEXT}>{staticsStrings.couverture_especes_insuffisante}</Text>
              
              </BkbTransparentCard>
            }
            {
              CarnetOrdreModalSore.message_error===3 &&
              <BkbTransparentCard     gradientColor={"#FB8A8A"}    style={GRADIENT_CARD} gradient={true} hexaColorCard={undefined} opacityColorCard={undefined}>
              <Text style={{fontFamily:"Sora-Bold"}}>{staticsStrings.controle_des_couvertures}</Text>
                <Text style={GRADIENT_TEXT}>{staticsStrings.couverture_titres_insuffisantes}</Text>
              
              </BkbTransparentCard>
            }
    
    
    
          </Modal>
           
     
          </ScrollView>)
      }
    </BkbScreen>
  )
})

const HEADER_STYLE : ViewStyle={
  borderBottomEndRadius:metrics.heightPercentageToDP(1.5),
      borderBottomStartRadius:metrics.heightPercentageToDP(1.5),
      height:metrics.heightPercentageToDP(13),
}

//tabs styles
const TABS: ViewStyle = {
  flexDirection:'row',
  // justifyContent:'space-around',
  height:metrics.heightPercentageToDP(5),
  // paddingHorizontal:metrics.heightPercentageToDP(1),

}
const underlineTab :ViewStyle={
  height:metrics.heightPercentageToDP(0.25),
  backgroundColor:color.palette.white,
  borderRadius:metrics.heightPercentageToDP(2),
  marginTop:metrics.heightPercentageToDP(0.5),
}

const TAB_STYLE : ViewStyle={
  width:metrics.heightPercentageToDP(19),
  // marginLeft:metrics.heightPercentageToDP(1),
  // marginRight:metrics.heightPercentageToDP(3),

 
}
const TAB_STYLE_GRAPHIC : ViewStyle={
  width:metrics.heightPercentageToDP(20),
  // marginLeft:metrics.heightPercentageToDP(1),
  // marginRight:metrics.heightPercentageToDP(3),

 
}
//styles of textes
const isTabStyle : TextStyle={
  fontFamily:'Sora-ExtraBold',
  textAlign:'center',
  // width:metrics.heightPercentageToDP(15),



}
const isNotTabStyle : TextStyle={
  fontFamily:'Sora-Light',
  textAlign:'center'

  
}
const TITRE_HEADER : ViewStyle={
  alignSelf:'flex-start',
  padding:metrics.heightPercentageToDP(1.3),
}
const TITRE_TEXT : TextStyle={
  fontFamily:'Sora-Bold',
  fontSize:metrics.heightPercentageToDP(1.8),
}


const TITRE_TABLE_STYLE : TextStyle={
  width:metrics.heightPercentageToDP(11.5),
  fontFamily:'Sora-Bold',
  fontSize:metrics.heightPercentageToDP(1.5),
  textAlign:'left'
}
const CELL_TABLE_STYLE : TextStyle={
  //maxWidth:metrics.heightPercentageToDP(15),
  width:metrics.heightPercentageToDP(14),
  fontSize:metrics.heightPercentageToDP(1.3),
  fontFamily:"Sora-Regular",
  textAlign:'left',
 
}
const COLUMN_TABLE_STYLE : TextStyle={
  maxWidth:metrics.heightPercentageToDP(9),  
  textAlign:'left',
  fontSize:metrics.heightPercentageToDP(1.3),

}
const COTATION_CONTAINER : ViewStyle={
  marginHorizontal:metrics.heightPercentageToDP(2),
  borderColor:'transparent',
  height:metrics.heightPercentageToDP(50)
}
//styles of carnet d ordres
const CARNET_STYLE : ViewStyle={
    flexDirection:'row'
}
const ACHAT : ViewStyle={
  width:metrics.heightPercentageToDP(20),
  justifyContent:"center"
}
const VENTE : ViewStyle={
  width:metrics.heightPercentageToDP(20),
  height:metrics.heightPercentageToDP(4.7),
  justifyContent:"center"

}
const ACHAT_VENTE_TEXT : TextStyle={
  fontFamily:'Sora-Bold'
}
const CELL_COTATION_STYLE : TextStyle={
  ...CELL_TABLE_STYLE,
  width:metrics.heightPercentageToDP(6.5),
  textAlign:"center"

}
const CELL_COTATION_VALUE_STYLE : TextStyle={
  ...CELL_TABLE_STYLE,
  width:metrics.heightPercentageToDP(6.5),
  textAlign:"center",
  fontFamily:'Sora-Bold'


}

//styles of graphique 
const GRAPHIQUE : ViewStyle={
  marginRight:metrics.heightPercentageToDP(1),
  marginLeft:metrics.heightPercentageToDP(1),

  backgroundColor:"#ffffff10",
  borderColor:"transparent",
  height:metrics.heightPercentageToDP(50)
}
const underlineTabGraphic :ViewStyle={
  height:metrics.heightPercentageToDP(0.25),
  backgroundColor:color.palette.BKBBlueColor1,
  borderRadius:metrics.heightPercentageToDP(2),
  marginTop:metrics.heightPercentageToDP(0.5),

}
const underlineTabGraphicInvisible :ViewStyle={
  height:metrics.heightPercentageToDP(0.25),
  backgroundColor:"#ffffff20",
  // borderRadius:metrics.heightPercentageToDP(2),
  marginTop:metrics.heightPercentageToDP(0.5),
  // width:metrics.heightPercentageToDP(13)
}
const isTabStyleGraphic : TextStyle={
  fontFamily:'Sora-ExtraBold',
  textAlign:'center',
  color:color.palette.BKBBlueColor1


}
const isNotTabStyleGraphic : TextStyle={
  fontFamily:'Sora-Light',
  textAlign:'center',
  color:color.palette.white


}
//styles of statistics

const STATISTICS_STYLE : ViewStyle={
  marginHorizontal:metrics.heightPercentageToDP(1.5),
  borderColor:"transparent",
  height:metrics.heightPercentageToDP(100),
  
}
//styles of filter
const FILTER_VALUES : ViewStyle={
  flexDirection:'row',
  marginVertical:metrics.heightPercentageToDP(2),
}
const FILTER_TABBED: ViewStyle={
  marginHorizontal:metrics.heightPercentageToDP(1),
  borderRadius:metrics.heightPercentageToDP(3),
  padding:metrics.heightPercentageToDP(0.5),
  width:metrics.heightPercentageToDP(15),
  backgroundColor:color.palette.BKBBlueColor


}
const FILTER_NOTABBED: ViewStyle={
  marginHorizontal:metrics.heightPercentageToDP(1),
  borderRadius:metrics.heightPercentageToDP(3),
  padding:metrics.heightPercentageToDP(0.5),
  width:metrics.heightPercentageToDP(13),
  backgroundColor:"#FFFFFF35"
  
}
const FILTER_TEXT : TextStyle={
  fontFamily:'Sora-Bold',
  fontSize:metrics.heightPercentageToDP(1.5),
  textAlign:"center",

}
const BKB_LINE_TABLE : ViewStyle={
  width:metrics.heightPercentageToDP(44)
}
const CELL_DIVIDENDES_VALUE_STYLE : TextStyle={
  ...CELL_TABLE_STYLE,
  width:metrics.heightPercentageToDP(6.5),
  textAlign:"center",
  fontFamily:'Sora-Regular'


}
const CELL_PERFORMANCE_VALUE_STYLE : TextStyle={
  ...CELL_TABLE_STYLE,
  width:metrics.heightPercentageToDP(6.5),
  textAlign:"center",
  fontFamily:'Sora-Bold'


}
const CELL_STATISTICS_BLUR : TextStyle={
  backgroundColor:'#ffffff65',borderRadius:metrics.heightPercentageToDP(0.8),padding:metrics.heightPercentageToDP(0.5)
}
const CELL_DIVIDENDES_VALUE_STYLE_WITH_BLUR : TextStyle={
  ...CELL_DIVIDENDES_VALUE_STYLE,
  ...CELL_STATISTICS_BLUR,


}

const GRADIENT_CARD : ViewStyle={
  height:metrics.heightPercentageToDP(15),
  padding:metrics.heightPercentageToDP(2),
  width:metrics.heightPercentageToDP(38),
  marginTop:metrics.heightPercentageToDP(1),
}
const GRADIENT_TEXT : TextStyle={
  textAlign:"center",
  fontSize:metrics.heightPercentageToDP(1.69),
  fontFamily:"Sora-Regular"
}