import React, { FC, useEffect, useState } from "react"
import { observer } from "mobx-react-lite"
import {TextStyle, View, ViewStyle ,Image, Pressable} from "react-native"
import { BkbButton, BkbCardTransactionDetails, BkbInput, BkbModal, BkbPicker, BkbScreen, BkbTransparentCard, CarnetOrdreModal, Text } from "../../components"
import { color } from "../../theme"
import metrics from "../../theme/metrics"
import staticsStrings from "../../localData/staticStrings.json"
import { NavigationInjectedProps } from "react-navigation"
import { useStores } from "../../models"
import { useIsFocused } from "@react-navigation/native"
import GlobalFunctions from "../../classes/GlobalFunctions"
import Modal from "react-native-modal";

//images & icons
const x_annuler=require("../../../assets/images/x_annuler.png")
const before_annuler=require("../../../assets/images/before_annuler.png")
const after_annuler=require("../../../assets/images/after_annuler.png")

export interface TransactionsDetailsProps extends NavigationInjectedProps<{}> {
  route
}
export const TransactionsDetailsScreen :FC<TransactionsDetailsProps> = observer((props)=> {
  const {ProfileUserStore,CarnetOrdreStore,CancelOrderStore,CarnetOrdreModalSore,ModalStore} = useStores()
  const isFocused = useIsFocused();
  const [editOrder, setEditOrder]=useState(false);
  const [cancelOrder, setCancelOrder]=useState(false);
  const [confirmOrder, setconfirmOrder]=useState(false);
  //modal order start 
  const [valeurOrder, setValeurOrder]=useState("");
  const [assetCodeOrder, setAssetCodeOrder]=useState("");
  const [marketCodeOrder, setMarketCodeOrder]=useState("");
  const [dateOrder, setDateOrder]=useState(0);
  const [orderInternalReference, setOrderInternalReference]=useState("");
  
  //modal order end
  const [editOrderModal, setEditOrderModal]=useState(false);
  const globalFunctions=new GlobalFunctions();
  const GetOrderList=async ()=>{
    await  CarnetOrdreStore.GetOrderListBKB(ProfileUserStore.login,ProfileUserStore.contextId,ProfileUserStore.accountCode)
 
   }
  const CancelOrderFunction=async (orderInternal,issuerCodeFunc)=>{
    await CancelOrderStore.CancelOrderFunc(ProfileUserStore.login,ProfileUserStore.contextId,ProfileUserStore.accountCode,orderInternal,issuerCodeFunc)
  }
   useEffect(()=>{
    GetOrderList()
   },[isFocused])
  return (
    <BkbScreen headerShow
    goBackButton ={true}
    navigation={props.navigation}
    styleParent={{height:metrics.heightPercentageToDP(13),
      borderBottomEndRadius:metrics.heightPercentageToDP(1.5),
      borderBottomStartRadius:metrics.heightPercentageToDP(1.5),

    }}
    >
      <View style={CONTAINER_TITLE} >
        <Text style={TITLE}>{staticsStrings.carnet_dordres}</Text>
      </View>
      {
        CarnetOrdreStore.orderList.filter(item=>
          item.internalReference===props.route.params.internalReference 
          && item.assetLabel===props.route.params.assetLabel
          && item.assetCode===props.route.params.assetCode
          && item.statusLabel===props.route.params.statusLabel

          ).map((orders,index)=>{
            return(
              <BkbCardTransactionDetails 
              key={index}
              onPress={() => { setEditOrder(!editOrder); 
                setValeurOrder(orders.assetLabel);
                setOrderInternalReference(orders.internalReference)
                setAssetCodeOrder(orders.assetCode)
                setMarketCodeOrder(orders.marketCode)
              } } 
              creation={globalFunctions.getMaxdate(orders.creationDate) + " "+ globalFunctions.getHeure(orders.creationHour)} 
              quantite={orders.quantity===null ? "":orders.quantity} 
              modalite={orders.orderModalityLabel} 
              cours_traite={orders.orderModalityLabel} 
              date_de_validite={globalFunctions.getMaxdate(orders.validityDate)} 
              id_carnet={orders.assetCode} 
              name_carnet={orders.assetLabel} 
              type_titre={orders.sideLabel} 
              status_carnet={globalFunctions.translateStatusLabelFrench(orders.statusLabel)}/>

            );
        })
        
      }
      {
        editOrder ===true && 
        <View style={MODIFIER_ANNULER_ORDRE}>
        <Text style={{fontFamily:"Sora-Bold"}}>{staticsStrings.message_modifier_annuler_ordre}</Text>
        <View style={BUTTONS_ROW}>
        <BkbButton style={MODIFIER_BTN} ButtonType={"white"} ButtonText={staticsStrings.modifier} onPress={()=>{
          // setEditOrderModal(true)
          ModalStore.setModalToggle(true)

          }}/>
        <BkbButton style={ANNUMER_BTN} TextStyle={{textAlign:'center',fontFamily:"Sora-Bold",marginRight:metrics.heightPercentageToDP(1.9)}} ButtonType={"transparent"} ButtonText={staticsStrings.annuler} onPress={()=>{setCancelOrder(true)}}/>

        </View>
      </View>
      }
      
      {/* modal annuler modifier 1*/}
      <BkbModal  style={CANCEL_POP_UP} isVisibleState={cancelOrder}>
        <Pressable onPress={()=>{setCancelOrder(false)}} style={{right:metrics.widthPercentageToDP(4),alignSelf:"flex-end",top:metrics.heightPercentageToDP(-1)}}>
        <Image  source={x_annuler}/>
        </Pressable>

        <Image style={{alignSelf:'center'}} source={before_annuler}/>

          <Text style={{fontFamily:"Sora-ExtraBold",textAlign:"center",fontSize:metrics.heightPercentageToDP(2.5)}}>{valeurOrder}</Text>
          <Text style={{fontFamily:"Sora-Regular",textAlign:"center",margin:metrics.heightPercentageToDP(1)}}>{staticsStrings.message_modifier_annuler_valeur}</Text>
         
        <View style={BUTTONS_ROW}>
        <BkbButton style={MODIFIER_BTN} ButtonType={"white"}  TextStyle={{fontSize:metrics.heightPercentageToDP(1.7)}} ButtonText={staticsStrings.oui_confirmer} onPress={async ()=>{
          setCancelOrder(false);
         await  CancelOrderFunction(orderInternalReference,"W");
         if(CancelOrderStore.cancelDone===true){
          setconfirmOrder(true)
         }
          

          }}/>
        <BkbButton style={ANNUMER_BTN} TextStyle={{textAlign:'center',fontFamily:"Sora-Bold",marginRight:metrics.heightPercentageToDP(1.9),fontSize:metrics.heightPercentageToDP(1.7)}} ButtonType={"transparent"} ButtonText={staticsStrings.non} onPress={()=>{setCancelOrder(false);}}/>

        </View>
      </BkbModal>

      {/* modal annuler modifier 2 */}
      <BkbModal  style={CANCEL_POP_UP} isVisibleState={confirmOrder}>
        <Pressable onPress={()=>{
          setconfirmOrder(false)
          }} style={{right:metrics.widthPercentageToDP(4),alignSelf:"flex-end",top:metrics.heightPercentageToDP(-1)}}>
        <Image  source={x_annuler}/>
        </Pressable>

        <Image style={{alignSelf:'center'}} source={after_annuler}/>

          <Text style={{fontFamily:"Sora-ExtraBold",textAlign:"center",fontSize:metrics.heightPercentageToDP(2.5)}}>{valeurOrder}</Text>
          <Text style={{fontFamily:"Sora-Regular",textAlign:"center",margin:metrics.heightPercentageToDP(1)}}>{staticsStrings.message_annuler_succes}</Text>
         
        
      </BkbModal>

      {/* modal order edit  */}

      <Modal  
            backdropColor={color.palette.BKbBackDropColor}

        
        isVisible={ModalStore.modalToggle}>
          
        <View style={{
          // backgroundColor:'#ffffff60',
          backgroundColor:"rgba(63,106,145,0.91)",
          borderRadius:metrics.heightPercentageToDP(1),
        padding:metrics.heightPercentageToDP(1),
        width:metrics.heightPercentageToDP(44.5)
        }}>
      
      <CarnetOrdreModal ValeurOdre={valeurOrder} AcheterPress={() => {
      
            // setEditOrderModal(false)
          } } side={0} date={0} assetCode={assetCodeOrder} QuotationCodePlace={1} withQuote={1} marketCode={marketCodeOrder}/>
        </View>
       

        {
          CarnetOrdreModalSore.message_error===0 &&
          <BkbTransparentCard        gradientColor={"#FB8A8A"}          style={GRADIENT_CARD} gradient={true} hexaColorCard={undefined} opacityColorCard={undefined}>
          <Text style={{fontFamily:"Sora-Bold"}}>{staticsStrings.avertissement}</Text>
            <Text style={GRADIENT_TEXT}>{staticsStrings.message_avertissement}</Text>
          
          </BkbTransparentCard>
        }
        {
          CarnetOrdreModalSore.message_error===1 &&
          <BkbTransparentCard          gradientColor={"#FB8A8A"}          style={GRADIENT_CARD} gradient={true} hexaColorCard={undefined} opacityColorCard={undefined}>
          <Text style={{fontFamily:"Sora-Bold"}}>{staticsStrings.controle_des_couvertures}</Text>
            <Text style={GRADIENT_TEXT}>{staticsStrings.couverture_especes_insuffisante}</Text>
          
          </BkbTransparentCard>
        }
        {
          CarnetOrdreModalSore.message_error===3 &&
          <BkbTransparentCard   gradientColor={"#FB8A8A"} style={GRADIENT_CARD} gradient={true} hexaColorCard={undefined} opacityColorCard={undefined}>
          <Text style={{fontFamily:"Sora-Bold"}}>{staticsStrings.controle_des_couvertures}</Text>
            <Text style={GRADIENT_TEXT}>{staticsStrings.couverture_titres_insuffisantes}</Text>
          
          </BkbTransparentCard>
        }

      </Modal>
        
      

    </BkbScreen>
  )
})
//styles
const CONTAINER_TITLE : ViewStyle={
  margin:metrics.heightPercentageToDP(2)
}
const TITLE : TextStyle={
  fontFamily:'Sora-Bold'
}

const MODIFIER_ANNULER_ORDRE : ViewStyle={
  // backgroundColor:"#ffffff40",
  backgroundColor:"rgba(63,106,145,0.7)",

  marginTop:metrics.heightPercentageToDP(20),
  alignSelf:'center',
  padding:metrics.heightPercentageToDP(2),
  borderRadius:metrics.heightPercentageToDP(2),
  height:metrics.heightPercentageToDP(16),
  alignContent:'center',
  // alignItems:"center",

}
const MODIFIER_BTN : ViewStyle={
  width:metrics.heightPercentageToDP(16)
}
const ANNUMER_BTN : ViewStyle={
  width:metrics.heightPercentageToDP(16),
  justifyContent:"center",

}
const BUTTONS_ROW : ViewStyle={
  flexDirection:'row-reverse',
  margin:metrics.heightPercentageToDP(1.7),
  justifyContent:"space-around"
}
//POP UP style
const CANCEL_POP_UP : ViewStyle={
  marginBottom:metrics.heightPercentageToDP(35),
  
}
const GRADIENT_CARD : ViewStyle={
  height:metrics.heightPercentageToDP(15),
  padding:metrics.heightPercentageToDP(2),
  width:metrics.heightPercentageToDP(38),
  marginTop:metrics.heightPercentageToDP(1),
}
const GRADIENT_TEXT : TextStyle={
  textAlign:"center",
  fontSize:metrics.heightPercentageToDP(1.69),
  fontFamily:"Sora-Regular"
}