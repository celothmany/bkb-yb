import React, { FC, useState } from "react"
import { observer } from "mobx-react-lite"
import { ScrollView, TextStyle, TouchableOpacity, View, ViewStyle } from "react-native"
import { BkbIndiceCard, BkbLineChart, BkbScreen, BkbTransparentCard, Text } from "../../components"
import staticsStrings from "../../localData/staticStrings.json"
import { color } from "../../theme"
import { NavigationInjectedProps } from "react-navigation"
import metrics from "../../theme/metrics"



export interface IndicesProps extends NavigationInjectedProps<{}> {
  route
}
export const IndicesScreen :FC<IndicesProps>= observer((props)=> {
  const [tab,setTab]=useState(0);
  const [tabGraphic,setTabGraphic]=useState(0);


  //get data set for graphic
  const getDataSetGraphic=(filterData :number)=>{
    if(filterData===0){
      //aujourd'hui
      return [Math.random() * 10,Math.random() * 10,Math.random() * 10,Math.random() * 10,Math.random() * 10,Math.random() * 10]
    }
    if(filterData===1){
      //6 mois
      return [100,200,172,420,246,689]
    }
    if(filterData===2){
      // 1 an
      return [110,223,90,130,341,600,555]
    }
    if(filterData===3){
      //3 ans
      return [110,203,90,132,341,600,505]
    }
    if(filterData===4){
      //5 ans
      return [110,203,190,132,341,231,585]
    }
   
  }
  //get labels for graphic 

  const getLabelsGraphic=(filterLabel :number)=>{
    if(filterLabel===0){
      //aujourd'hui
      return ["Jan","Fév","Mars","Avr","Mai","Juin"]
    }
    if(filterLabel===1){
      //6 mois
      return ["Jan","Fév","Mars","Avr","Mai","Juin"]
    }
    if(filterLabel===2){
      // 1 an
      return ["Jan","Fév","Mars","Avr","Mai","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
    }
    if(filterLabel===3){
      // 3 ans
      return ["Jan","Fév","Mars","Avr","Mai","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
    }
    if(filterLabel===4){
      // 5 ans
      return ["Jan","Fév","Mars","Avr","Mai","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
    }
   
    
  }
  return (
    <BkbScreen
    navigation={props.navigation}
    headerShow={true}
    goBackButton={true}
    >
{/*       
      <View style={HEADER}>
          <Text style={{fontFamily:"Sora-Bold"}}>{staticsStrings.Indices}</Text>
      </View>
      <View style={TABS} >
        <TouchableOpacity style={TAB_CONTAINER}  onPress={()=>{setTab(0)}}>
        <Text style ={tab===0 ? TEXT_isTab : TEXT_isNotTab}>{staticsStrings.MASI}</Text>
        <View  style={tab===0 ? underLine_isTab : underLine_isNotTab}/>
        </TouchableOpacity>
      
        <TouchableOpacity style={TAB_CONTAINER} onPress={()=>{setTab(1)}}>
        <Text style ={tab===1 ? TEXT_isTab : TEXT_isNotTab}  >{staticsStrings.MSI_20}</Text>
        <View  style={tab===1 ? underLine_isTab : underLine_isNotTab}/>

        </TouchableOpacity>

      
      </View>

      {
        tab===0 &&
        <ScrollView style={{padding:metrics.heightPercentageToDP(0)}}  >
        <BkbIndiceCard assetStatus={"Achat"}/>

          <BkbTransparentCard hexaColorCard={undefined} opacityColorCard={undefined} style={GRAPHIQUE}>
          <Text style={{alignSelf:'flex-start',margin:metrics.heightPercentageToDP(1),fontFamily:"Sora-Bold"}}>{staticsStrings.graphique}</Text>
                        <ScrollView  horizontal={true} style={{paddingHorizontal:metrics.heightPercentageToDP(1)}}>

              <TouchableOpacity 
              style={TAB_STYLE_GRAPHIC}
              onPress={()=>{setTabGraphic(0)}}><Text style={tabGraphic===0 ? isTabStyleGraphic : isNotTabStyleGraphic}>{staticsStrings.aujourdhui}</Text>
              {
              tabGraphic===0 ? (<View style={underlineTabGraphic}/> ):(<View style={underlineTabGraphicInvisible}/> )
              }
              </TouchableOpacity>

              <TouchableOpacity  onPress={()=>{setTabGraphic(1)}}><Text style={tabGraphic===1 ? isTabStyleGraphic : isNotTabStyleGraphic} >{staticsStrings.six_mois}</Text>
              {
              tabGraphic===1 ? (<View style={underlineTabGraphic}/> ):(<View style={underlineTabGraphicInvisible}/> )
              }
              </TouchableOpacity>

              <TouchableOpacity  
              style={TAB_STYLE_GRAPHIC}
              onPress={()=>{setTabGraphic(2)}}><Text style={tabGraphic===2 ? isTabStyleGraphic : isNotTabStyleGraphic}>{staticsStrings.un_an}</Text>
              {
              tabGraphic===2 ? (<View style={underlineTabGraphic}/> ):(<View style={underlineTabGraphicInvisible}/> )
              }
              </TouchableOpacity>

              <TouchableOpacity  
              style={TAB_STYLE_GRAPHIC}
              onPress={()=>{setTabGraphic(3)}}><Text style={tabGraphic===3 ? isTabStyleGraphic : isNotTabStyleGraphic}>{staticsStrings.trois_ans}</Text>
              {
              tabGraphic===3 ? (<View style={underlineTabGraphic}/> ):(<View style={underlineTabGraphicInvisible}/> )
              }
              </TouchableOpacity>

              <TouchableOpacity  
              style={TAB_STYLE_GRAPHIC}
              onPress={()=>{setTabGraphic(4)}}><Text style={tabGraphic===4 ? isTabStyleGraphic : isNotTabStyleGraphic}>{staticsStrings.cinq_ans}</Text>
              {
              tabGraphic===4 ? (<View style={underlineTabGraphic}/> ):(<View style={underlineTabGraphicInvisible}/> )
              }
              </TouchableOpacity>
              </ScrollView>
                <ScrollView horizontal={true} >
                <BkbLineChart dataSet={getDataSetGraphic(tabGraphic)} labelsSet={getLabelsGraphic(tabGraphic)} toolTipText1={"25 Mai 2020"} toolTipText2={" MAD"}/>

                </ScrollView>
          </BkbTransparentCard>
        </ScrollView>
      }
      {
        tab===1 &&
        <ScrollView style={{padding:metrics.heightPercentageToDP(0)}}  >

        <BkbIndiceCard assetStatus={"Vente"}/>
          <BkbTransparentCard hexaColorCard={undefined} opacityColorCard={undefined} style={GRAPHIQUE}>
          <Text style={{alignSelf:'flex-start',margin:metrics.heightPercentageToDP(1),fontFamily:"Sora-Bold"}}>{staticsStrings.graphique}</Text>
                        <ScrollView  horizontal={true} style={{paddingHorizontal:metrics.heightPercentageToDP(1)}}>

              <TouchableOpacity 
              style={TAB_STYLE_GRAPHIC}
              onPress={()=>{setTabGraphic(0)}}><Text style={tabGraphic===0 ? isTabStyleGraphic : isNotTabStyleGraphic}>{staticsStrings.aujourdhui}</Text>
              {
              tabGraphic===0 ? (<View style={underlineTabGraphic}/> ):(<View style={underlineTabGraphicInvisible}/> )
              }
              </TouchableOpacity>

              <TouchableOpacity  onPress={()=>{setTabGraphic(1)}}><Text style={tabGraphic===1 ? isTabStyleGraphic : isNotTabStyleGraphic} >{staticsStrings.six_mois}</Text>
              {
              tabGraphic===1 ? (<View style={underlineTabGraphic}/> ):(<View style={underlineTabGraphicInvisible}/> )
              }
              </TouchableOpacity>

              <TouchableOpacity  
              style={TAB_STYLE_GRAPHIC}
              onPress={()=>{setTabGraphic(2)}}><Text style={tabGraphic===2 ? isTabStyleGraphic : isNotTabStyleGraphic}>{staticsStrings.un_an}</Text>
              {
              tabGraphic===2 ? (<View style={underlineTabGraphic}/> ):(<View style={underlineTabGraphicInvisible}/> )
              }
              </TouchableOpacity>

              <TouchableOpacity  
              style={TAB_STYLE_GRAPHIC}
              onPress={()=>{setTabGraphic(3)}}><Text style={tabGraphic===3 ? isTabStyleGraphic : isNotTabStyleGraphic}>{staticsStrings.trois_ans}</Text>
              {
              tabGraphic===3 ? (<View style={underlineTabGraphic}/> ):(<View style={underlineTabGraphicInvisible}/> )
              }
              </TouchableOpacity>

              <TouchableOpacity  
              style={TAB_STYLE_GRAPHIC}
              onPress={()=>{setTabGraphic(4)}}><Text style={tabGraphic===4 ? isTabStyleGraphic : isNotTabStyleGraphic}>{staticsStrings.cinq_ans}</Text>
              {
              tabGraphic===4 ? (<View style={underlineTabGraphic}/> ):(<View style={underlineTabGraphicInvisible}/> )
              }
              </TouchableOpacity>
              </ScrollView>
                <ScrollView horizontal={true} >
                <BkbLineChart dataSet={getDataSetGraphic(tabGraphic)} labelsSet={getLabelsGraphic(tabGraphic)} toolTipText1={"25 Mai 2020"} toolTipText2={" MAD"}/>

                </ScrollView>
          </BkbTransparentCard>
        </ScrollView>
      } */}
      
    </BkbScreen>
  )
})

//styles 
const HEADER: ViewStyle = {
  // backgroundColor:'#ffffff20',
  backgroundColor:'rgba(63,106,145,0.7)',
  flexDirection:'row',
  justifyContent:'space-around',
  height:metrics.heightPercentageToDP(4.5),
  borderBottomEndRadius:metrics.heightPercentageToDP(2),
  borderBottomStartRadius:metrics.heightPercentageToDP(2),
  paddingHorizontal:metrics.heightPercentageToDP(1),
  paddingTop:metrics.heightPercentageToDP(1),

}
const TABS : ViewStyle={
  marginTop:metrics.heightPercentageToDP(1),
  flexDirection:'row',
  // justifyContent:"space-around",
  alignSelf:'center',
  marginHorizontal:metrics.heightPercentageToDP(1),
  // width:metrics.heightPercentageToDP(50)
}
const TEXT_isTab: TextStyle={
  fontFamily:"Sora-Regular",
}
const TEXT_isNotTab: TextStyle={
    color:"#ffffff85"

}
const underLine_isTab:ViewStyle={
  height:metrics.heightPercentageToDP(0.5),
  backgroundColor:color.palette.white,
  borderRadius:metrics.heightPercentageToDP(3),
  marginTop:metrics.heightPercentageToDP(0.5),
  width:metrics.heightPercentageToDP(22)
}

const underLine_isNotTab:ViewStyle={
  height:metrics.heightPercentageToDP(0.5),
  backgroundColor:"#ffffff85",
  borderRadius:metrics.heightPercentageToDP(3),
  

  marginTop:metrics.heightPercentageToDP(0.7),
  width:metrics.heightPercentageToDP(22)
}

const TAB_CONTAINER : ViewStyle={
  alignItems:"center",
  alignSelf:'center',
  alignContent:'center',
}

const TAB_STYLE_GRAPHIC : ViewStyle={
  width:metrics.heightPercentageToDP(20),
  // marginLeft:metrics.heightPercentageToDP(1),
  // marginRight:metrics.heightPercentageToDP(3),

 
}
const underlineTabGraphic :ViewStyle={
  height:metrics.heightPercentageToDP(0.25),
  backgroundColor:color.palette.BKBBlueColor1,
  borderRadius:metrics.heightPercentageToDP(2),
  marginTop:metrics.heightPercentageToDP(0.5),

}
const underlineTabGraphicInvisible :ViewStyle={
  height:metrics.heightPercentageToDP(0.25),
  backgroundColor:"#ffffff20",
  // borderRadius:metrics.heightPercentageToDP(2),
  marginTop:metrics.heightPercentageToDP(0.5),
  // width:metrics.heightPercentageToDP(13)
}
const isTabStyleGraphic : TextStyle={
  fontFamily:'Sora-ExtraBold',
  textAlign:'center',
  color:color.palette.BKBBlueColor1


}
const isNotTabStyleGraphic : TextStyle={
  fontFamily:'Sora-Light',
  textAlign:'center',
  color:color.palette.white


}

//styles of graphic
const GRAPHIQUE : ViewStyle={
  marginTop:metrics.heightPercentageToDP(1.5),
  marginHorizontal:metrics.heightPercentageToDP(1),
  alignSelf:'center',
  height:metrics.heightPercentageToDP(55),
  width:metrics.heightPercentageToDP(45)
}