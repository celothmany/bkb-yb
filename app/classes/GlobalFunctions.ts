import moment from "moment";
import staticsStrings from "../localData/staticStrings.json"

/**this file is a helper one for global functions that are repeated many time on the project BKB */
class GlobalFunctions{
    wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
      }
     ParseFloat(str,val) {
       /**parse a string to float */
        str = str.toString();
        str = str.slice(0, (str.indexOf(".")) + val + 1); 
        return Number(str);   
     
      }
      getMaxdate=(date)=>{
        /**convert bkbdate to normal date */

        var date = date.toString()
        var year=date.substring(0,4)
        var month=date.substring(4,6)
        var day=date.substring(6,8)
        return day + "/"+month+"/"+year
    }
    convertDateToBkbDate=(date: Date)=>{
      let year=date.getFullYear()
      let month=this.addDigit(date.getMonth()+1)
      let day=this.addDigit(date.getDate())
      return  year.toString()+ month.toString()+day.toString()
      // return this.addDigit(date.getMonth())
    }
    getHeure=(time)=>{
      var time = time.toString()
      if(time.length===6){
       
      }
      if(time.length===5){
        time = "0"+time;
    
      }
      if(time.length===4){
        time = "00"+time;
        
      }
      var hour=time.substring(0,2)
      var min=time.substring(2,4)
      var sec=time.substring(5,6)
      return hour + ":" + min 
      // return time
  }
  translateTypeOrdre=(code : string)=>{
    /**translating the abrevaition of stocks to full names */
    if(code==="LIM"){
      return "Limite"
    }
    if(code==="ASD"){
      return "Stop"
    }
    if(code==="ASD"){
      return "Stop"
    }
    if(code==="AM"){
      return "Au marché"
    }
    if(code==="AM"){
      return "Au marché"
    }
    if(code==="APD"){
      return "Stop Limite"
    }
    if(code==="APD"){
      return "Stop Limite"
    }
    if(code==="SG"){
      return "Stop Glissant"
    }

    if(code==="MIT"){
      return "Stop inverse"
    }
    if(code==="ATP"){
      return "A tout prix"
    }
  }
  translateConditions=(code : string)=>{
        /**translating the abrevaition of stocks to full names */

    if(code==="MINFILL"){
      return "Minimum Fill"
    }
    if(code==="ICEBERG"){
      return "Quantité dévoilée"
    }
    if(code==="FOK"){
      return "Fill or Kill"
    }
    if(code==="IOC"){
      return "Immediate Or Cancel Fill"
    }

  }
  translateStatusLabelEnglish=(statusLabel:String)=>{
        /**translating the names of filters of transactions from french to english */

    if(statusLabel===staticsStrings.en_attente){
      return "PENDING"
    }
    if(statusLabel===staticsStrings.execute){
      return "EXECUTED"
    }
   if(statusLabel===staticsStrings.en_attente_annulation){
     return "WAITING FOR CANCELLATION"
   }
   if(statusLabel===staticsStrings.annule){
     return "CANCELLED"
   }
   if(statusLabel===staticsStrings.expire){
     return "FELL"
   }
   if(statusLabel===staticsStrings.en_anomalie){
     return "ANOMALY"
   }
   if(statusLabel===staticsStrings.execute_partiellement){
     return "PARTLY EXECUTED"
   }
   if(statusLabel===staticsStrings.comptablise){
     return "ACCOUNTED"
   }
   if(statusLabel===staticsStrings.ordre_execute_annule){
     return "EXECUTED ORDER CANCELLED"
   }
   if(statusLabel===staticsStrings.annulation_refusée){
     return "CANCELLATION REJECTED"
   }
   if(statusLabel===staticsStrings.rejetee){
     return "REJECTED"
   }
   if(statusLabel===staticsStrings.modifié){
     return "ORIGINAL ORDER CHANGED"
   }
   if(statusLabel===staticsStrings.non_acquitté){
     return "NOT ACKNOWLEDGED"
   }
   if(statusLabel===staticsStrings.execute_non_comptabilisée){
     return "ECECUTED NOT ACCOUNTED"
   }
  }
  translateStatusLabelFrench=(statusLabel:String)=>{
       /**translating the names of filters of transactions from english to french */

    if(statusLabel==="PENDING"){
      return staticsStrings.en_attente
    }
    if(statusLabel==="EXECUTED"){
      return  staticsStrings.execute
    }
   if(statusLabel==="WAITING FOR CANCELLATION"){
     return staticsStrings.en_attente_annulation
   }
   if(statusLabel==="CANCELLED"){
     return  staticsStrings.annule
   }
   if(statusLabel==="FELL"){
     return staticsStrings.expire
   }
   if(statusLabel==="ANOMALY"){
     return staticsStrings.en_anomalie
   }
   if(statusLabel===staticsStrings.execute_partiellement){
     return "PARTLY EXECUTED"
   }
   if(statusLabel==="ACCOUNTED"){
     return  staticsStrings.comptablise
   }
   if(statusLabel==="EXECUTED ORDER CANCELLED"){
     return staticsStrings.ordre_execute_annule
   }
   if(statusLabel==="CANCELLATION REJECTED"){
     return  staticsStrings.annulation_refusée
   }
   if(statusLabel==="REJECTED"){
     return  staticsStrings.rejetee
   }
   if(statusLabel==="ORIGINAL ORDER CHANGED"){
     return  staticsStrings.modifié
   }
   if(statusLabel=== "NOT ACKNOWLEDGED"){
     return staticsStrings.non_acquitté
   }
   if(statusLabel==="ECECUTED NOT ACCOUNTED"){
     return  staticsStrings.execute_non_comptabilisée
   }
  }
   translateNumberToStatusLabel=(tabNumber: number)=>{
            /**translating the tab number to transactions name to french */

    if(tabNumber===1){
      return staticsStrings.en_attente
    }
    if(tabNumber===2){
     return staticsStrings.execute
   }
   if(tabNumber===3){
     return staticsStrings.en_attente_annulation
   }
   if(tabNumber===4){
     return staticsStrings.annule
   }
   if(tabNumber===5){
     return staticsStrings.expire
   }
   if(tabNumber===6){
     return staticsStrings.en_anomalie
   }
   if(tabNumber===7){
     return staticsStrings.execute_partiellement
   }
   if(tabNumber===8){
     return staticsStrings.comptablise
   }
   if(tabNumber===9){
     return staticsStrings.ordre_execute_annule
   }
   if(tabNumber===10){
     return staticsStrings.annulation_refusée
   }
   if(tabNumber===11){
     return staticsStrings.rejetee
   }
   if(tabNumber===12){
     return staticsStrings.modifié
   }
   if(tabNumber===13){
     return staticsStrings.non_acquitté
   }
   if(tabNumber===14){
     return staticsStrings.execute_non_comptabilisée
   }
 
  }
  translateAchatVente=(translate)=>{
    if(translate==="BUY"){
      return "Achat"
    }
    if(translate==="SELL"){
      return "Vente"
    }
    }

  translateValidity=(validityNumber: number)=>{
      if(validityNumber===0){
        return "Jour"
      }
      if(validityNumber===1){
        return "Terme"
      }
      if(validityNumber===2){
        return "Autre"
      }
      if(validityNumber===3){
        return "Semaine"
      }
      if(validityNumber===4){
        return "Infini"
      }
      if(validityNumber===5){
        return "GIT"
      }
      }
    addDigit=(dateEnter:number)=>{
      /**for adding the zero  digit to the month of BKBdate */
      var newDateEnter= dateEnter.toString();
        if(newDateEnter==="0" ||newDateEnter==="1" ||newDateEnter==="2" ||newDateEnter==="3" ||newDateEnter==="4" ||newDateEnter==="5" || newDateEnter==="6" ||newDateEnter==="7" ||newDateEnter==="8" ||newDateEnter==="9" )
        {
          return "0"+newDateEnter
        }
        else return newDateEnter
    }
  makedateForFilter=(newDate:number)=>{
           /**convert bkb date to date as format yyyy-mm-dd */

    var date = newDate.toString();
    var year = date.substring(0,4)
    var month = date.substring(4,6)
    var day = date.substring(6,8)
    return year + "-"+ month + "-"+day
  }
  subtractMonths=(numOfMonths, date = new Date()) =>{
         /**this function for substracting a number of months from current date */

    date.setMonth(date.getMonth() - numOfMonths);
  
    return date;
  }
  

  TransactionFilterDateBKB = (creationDate,creationHour,numOfMonths)=>{
    //creation Date
    var date = creationDate.toString();
    var year = date.substring(0,4)
    var month = date.substring(4,6)
    var day = date.substring(6,8)
    //creation Hour
    var newHour = creationHour.toString();
    var hour = newHour.substring(0,2)
    var minute = newHour.substring(2,4)
    var second = newHour.substring(4,6)
    var normalDate= new Date(year,month,day,hour,minute,second)
    const mois_debut = this.subtractMonths(numOfMonths, new Date())
    const mois_fin = new Date()
    if (normalDate > mois_debut && normalDate <mois_fin ){
      return true 
    }
    else {
      return false
    }
  


  }

  createObjectDateFromBKB=(creationDate)=>{
    /**convert bkb date to object date  */
    //creation Date
    var date = creationDate.toString();
    var year = date.substring(0,4)
    var month = date.substring(4,6)
    var day = date.substring(6,8)
    var normalDate= new Date(year,month-1,day,)

    return normalDate
  }

  filterDateRange=(dateCurrent: number,creationHour,executionDate,filterDate : number )=>{
     /**this function for filetring the orderList of Transactions and display them by feltering the creation date and creation hour */
    var now = new Date();
    var  today =now.getFullYear() + "-"+ this.addDigit(now.getMonth())+ "-" + this.addDigit(now.getDate());
    let en_cours=new Date(new Date().getTime() - (0 * 24 * 60 * 60 * 1000));
    let J_J_1=new Date(new Date().getTime() - (1 * 24 * 60 * 60 * 1000));
    let Veille=new Date(new Date().getTime() - (1 * 24 * 60 * 60 * 1000));
    let un_mois=new Date(new Date().getTime() - (31 * 24 * 60 * 60 * 1000));
    let trois_mois=new Date(new Date().getTime() - (3*31 * 24 * 60 * 60 * 1000));
    let six_mois=new Date(new Date().getTime() - (6*31 * 24 * 60 * 60 * 1000));
    let un_an=new Date(new Date().getTime() - (12*31 * 24 * 60 * 60 * 1000));
    let last7Days=new Date(new Date().getTime() - (7 * 24 * 60 * 60 * 1000));
    const newDateCurrent=this.addDigit(dateCurrent)
    var year=un_an.getFullYear() + "-"+ this.addDigit(un_an.getMonth())+ "-" + this.addDigit(un_an.getDate())
    if(filterDate===0){
      /**the transactions are "en cours " if the executionDate and executionHour is zero is zero ( a valider )  */
      if (executionDate===0){
        return true 
      }
      else{
        return true
      }
      // var en_cours_filter=en_cours.getFullYear() + "-"+ this.addDigit(en_cours.getMonth())+ "-" + this.addDigit(en_cours.getDate())
      // return moment(newDateCurrent).isBetween(en_cours_filter,today)
    }
    if(filterDate===1){
        var yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
      if(this.createObjectDateFromBKB(dateCurrent).setHours(0,0,0,0)=== new Date().setHours(0,0,0,0) || this.createObjectDateFromBKB(dateCurrent).setHours(0,0,0,0)=== yesterday.setHours(0,0,0,0))
      {
        return true
      }
       else {
         return false
       }
      // var J_J_1_filter=J_J_1.getFullYear() + "-"+ this.addDigit(J_J_1.getMonth())+ "-" + this.addDigit(J_J_1.getDate())
      // return moment(newDateCurrent).isBetween(J_J_1_filter,today)
    }
    if(filterDate===2){
      var yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
      if(this.createObjectDateFromBKB(dateCurrent).setHours(0,0,0,0)=== yesterday.setHours(0,0,0,0))
      {
        return true
      }
       else {
         return false
       }

      // var Veille_filter=Veille.getFullYear() + "-"+ this.addDigit(Veille.getMonth())+ "-" + this.addDigit(Veille.getDate())
      // return moment(newDateCurrent).isBetween(Veille_filter,today)
    }
    if(filterDate===3){
      return this.TransactionFilterDateBKB(dateCurrent,creationHour,1)

      // var un_mois_filter=un_mois.getFullYear() + "-"+ this.addDigit(un_mois.getMonth())+ "-" + this.addDigit(un_mois.getDate())
      // return moment(newDateCurrent).isBetween(un_mois_filter,today)
    }
    if(filterDate===4){
      return this.TransactionFilterDateBKB(dateCurrent,creationHour,3)

      // var trois_mois_filter=trois_mois.getFullYear() + "-"+ this.addDigit(trois_mois.getMonth())+ "-" + this.addDigit(trois_mois.getDate())
      // return moment(newDateCurrent).isBetween(trois_mois_filter,today)
    }
    if(filterDate===5){
      return this.TransactionFilterDateBKB(dateCurrent,creationHour,6)

      // var six_mois_filter=six_mois.getFullYear() + "-"+ this.addDigit(six_mois.getMonth())+ "-" + this.addDigit(six_mois.getDate())
      // return moment(newDateCurrent).isBetween(six_mois_filter,today)
    }
    if(filterDate===6){

      return this.TransactionFilterDateBKB(dateCurrent,creationHour,12)

      // var un_an_filter=un_an.getFullYear() + "-"+ this.addDigit(un_an.getMonth())+ "-" + this.addDigit(un_an.getDate())
      // return moment(newDateCurrent).isBetween(un_an_filter,today)
    }

    }
}
export default GlobalFunctions;