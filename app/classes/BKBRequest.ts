
import { XMLParser } from 'fast-xml-parser';

class BKBRequest{
    
    parserXml = new XMLParser();
    header_open=``;
    envelope_open=`<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://definitions.patiotpm.com/publicws/service">`;
    envelope_close=`</soapenv:Envelope>`;
    header=`<soapenv:Header/>`;
    body_open=`<soapenv:Body>`;
    body_close=`</soapenv:Body>`;
    // header_open=``;
     createBodyRequest=(requestName,main)=>{
        const body_request_open=this.envelope_open+this.header+this.body_open;
        const body_request_close=this.body_close+this.envelope_close;
        const request_open=`<ser:${requestName}>`;
        const request_close=`</ser:${requestName}>`;
        const body_request_all=body_request_open+request_open+main+request_close+body_request_close;
        return body_request_all;

     };
     Response2Json=(responseXML,requestName)=>{
        const Xml2json= new XMLParser();       
        let ToJson=Xml2json.parse(responseXML);
        const ns2_requestName="ns2:"+requestName+"Response";
         return ToJson["soap:Envelope"]["soap:Body"][ns2_requestName]["return"]
        //return ToJson["soap:Envelope"]["soap:Body"]

     }
     
//    postXml=(login: any,pwd: any)=>{
//       fetch('https://recetteappmobile.bmcecapitalbourse.com/com.patiotpm.publicws.v2/services/TPMService/AuthenticationServicePort', {
//     method: 'POST',
//     headers: {
//       Accept: 'application/xml',
//       'Content-Type': 'text/xml'
      
//     },
//     body: `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://definitions.patiotpm.com/publicws/service">
//     <soapenv:Header/>
//     <soapenv:Body>
//        <ser:connectV1>
//           <loginCode>${login}</loginCode>
//           <password>${pwd}</password>
//           <emetteurs>W</emetteurs>
//        </ser:connectV1>
//     </soapenv:Body>
//  </soapenv:Envelope>`
//   })
//   .then((response) => response.text())
//   .then((textResponse) => {
//     let obj = this.parserXml.parse(textResponse);
//     if(obj["soap:Envelope"]["soap:Body"]["ns2:connectV1Response"]["return"]["resultCode"]===0)
//     {
//       console.log(obj["soap:Envelope"]["soap:Body"]["ns2:connectV1Response"]["return"]["user"].contextId)
//       // setContextId(obj["soap:Envelope"]["soap:Body"]["ns2:connectV1Response"]["return"]["user"].contextId)
//     }
     

//   })


// }
    
}
export default BKBRequest;





    
