import { ApisauceInstance, create, ApiResponse } from "apisauce"
import { getGeneralApiProblem } from "./api-problem"
import { ApiConfig, DEFAULT_API_CONFIG } from "./api-config"
import * as Types from "./api.types"
import BKBRequest from "../../classes/BKBRequest"
import { XMLParser } from "fast-xml-parser"

/**
 * Manages all requests to the API.
 */
export class Api {
  /**
   * The underlying apisauce instance which performs the requests.
   */
  apisauce: ApisauceInstance

  /**
   * Configurable options.
   */
  config: ApiConfig

  /**
   * Creates the api.
   *
   * @param config The configuration to use.
   */
  constructor(config: ApiConfig = DEFAULT_API_CONFIG) {
    this.config = config
  }

  /**
   * Sets up the API.  This will be called during the bootup
   * sequence and will happen before the first React component
   * is mounted.
   *
   * Be as quick as possible in here.
   */
  setup() {
    // construct the apisauce instance
    this.apisauce = create({
      baseURL: this.config.url,
      timeout: this.config.timeout,
      headers: {
        // Accept: "application/json",
        Accept: 'application/xml',
        'Content-Type': 'text/xml'
      },
    })
  }

  /**
   * Gets a list of users.
   */
  async getUsers(): Promise<Types.GetUsersResult> {
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.get(`/users`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    const convertUser = (raw) => {
      return {
        id: raw.id,
        name: raw.name,
      }
    }

    // transform the data into the format we are expecting
    try {
      const rawUsers = response.data
      const resultUsers: Types.User[] = rawUsers.map(convertUser)
      return { kind: "ok", users: resultUsers }
    } catch {
      return { kind: "bad-data" }
    }
  }

  /**
   * Gets a single user by ID
   */

  async getUser(id: string): Promise<Types.GetUserResult> {
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.get(`/users/${id}`)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }

    // transform the data into the format we are expecting
    try {
      const resultUser: Types.User = {
        id: response.data.id,
        name: response.data.name,
      }
      return { kind: "ok", user: resultUser }
    } catch {
      return { kind: "bad-data" }
    }
  }
  async LoginBKBApi(loginCode: string,password:string,emetteurs:string): Promise<Types.GetUserBKBResult> {

    //generate soap request 
    const connectV1Request=new BKBRequest();
    const main=`<loginCode>${loginCode}</loginCode>
    <password>${password}</password>
    <emetteurs>${emetteurs}</emetteurs>`
    const requestXML=connectV1Request.createBodyRequest("connectV1",main)
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.post(`https://recetteappmobile.bmcecapitalbourse.com/com.patiotpm.publicws.v2/services/TPMService/AuthenticationServicePort`,requestXML)
  
    const new_response=connectV1Request.Response2Json(response.data,"connectV1");
    // console.log("response here api",connectV1Request.Response2Json(response.data,"connectV1"))
    // console.log("new response",new_response)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }
    // transform the data into the format we are expecting
    try {
        if(new_response["detailCode"]===0  && new_response["resultCode"]===0){
          return { 
            status:response.status,
            result_code:new_response["resultCode"],
          detail_code:new_response["detailCode"],
          contextId:new_response["user"].contextId===undefined ? "": new_response["user"].contextId,
          login:new_response["user"].login===undefined ? "": new_response["user"].login
         }
        }else{
          return { 
            status:response.status,
            result_code:new_response["resultCode"],
          detail_code:new_response["detailCode"],
          contextId:"",
          login:""
         }
        }
       
     
       
    } catch {
      return { kind: "bad-data" }
    }
  }
  

  async DisconnectBKBApi(login: string,contextId:string): Promise<Types.GetUserBKBDisconnectResult> {

    //generate soap request 
    const disconnectV1Request=new BKBRequest();
    const main=`<user>
    <contextId>${contextId}</contextId>
    <login>${login}</login>
       </user>`
    const requestXML=disconnectV1Request.createBodyRequest("disconnectV1",main)
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.post(`https://recetteappmobile.bmcecapitalbourse.com/com.patiotpm.publicws.v2/services/TPMService/AuthenticationServicePort`,requestXML)
  
    const new_response=disconnectV1Request.Response2Json(response.data,"disconnectV1");


    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }
    // transform the data into the format we are expecting
    try {
      return { 
        status:response.status,
        result_code:new_response["resultCode"],
      detail_code:new_response["detailCode"],

     }
       
     
       
    } catch {
      return { kind: "bad-data" }
    }
  }

  async ListAccountBKBApi(login: string,contextId:string): Promise<Types.GetUserListAccountBKB> {

    //generate soap request 
    const listAccountV1Request=new BKBRequest();
    const main=` <user>
    <contextId>${contextId}</contextId>
        <login>${login}</login>
    </user>
    <login>${login}</login>
    `
    const requestXML=listAccountV1Request.createBodyRequest("listAccountV1",main)
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.post(`https://recetteappmobile.bmcecapitalbourse.com/com.patiotpm.publicws.v2/services/TPMService/AuthenticationServicePort`,requestXML)
  
    const new_response=listAccountV1Request.Response2Json(response.data,"listAccountV1");
    // console.log(new_response["accountList"])

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }
    // transform the data into the format we are expecting
    try {
      return { 
        status:response.status,
        result_code:new_response["resultCode"],
      detail_code:new_response["detailCode"],
      accountCode:new_response["accountList"]["accountCode"],
      accountLabel:new_response["accountList"]["accountLabel"],
      accountType:new_response["accountList"]["accountType"],
      mainAccount:new_response["accountList"]["mainAccount"],


     }
       
     
       
    } catch {
      return { kind: "bad-data" }
    }
  }
  async ChangePasswordAPI(login: string,contextId:string,loginCode:string,oldPassword:string,newPassword:string): Promise<Types.GetChangePasswordResult> {

    //generate soap request 
    const changePasswordV1=new BKBRequest();
    const main=` <user>
    <contextId>${contextId}</contextId>
        <login>${login}</login>
    </user>
    <loginCode>${loginCode}</loginCode>
    <oldPassword>${oldPassword}</oldPassword>
    <newPassword>${newPassword}</newPassword>
    `
    const requestXML=changePasswordV1.createBodyRequest("changePasswordV1",main)
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.post(`https://recetteappmobile.bmcecapitalbourse.com/com.patiotpm.publicws.v2/services/TPMService/AuthenticationServicePort`,requestXML)
  
    const new_response=changePasswordV1.Response2Json(response.data,"changePasswordV1");
    // console.log(new_response["accountList"])

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }
    // transform the data into the format we are expecting
    try {
      return { 
        status:response.status,
        result_code:new_response["resultCode"],
      detail_code:new_response["detailCode"],
      


     }
       
     
       
    } catch {
      return { kind: "bad-data" }
    }
  }
  async GetSythensisBKBApi(login: string,contextId:string,accountCode:number,synthesisType:number): Promise<Types.GetSynthesisinRefResult> {

    //generate soap request 
    const getSynthesisInRefCurrencyV1Request=new BKBRequest();
      const main=`<user>
      <contextId>${contextId}</contextId>
      <login>${login}</login>
  </user>
  <accountCode>${accountCode}</accountCode>
  <synthesisType>${synthesisType}</synthesisType>`
    const requestXML=getSynthesisInRefCurrencyV1Request.createBodyRequest("getSynthesisInRefCurrencyV1",main)
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.post(`https://recetteappmobile.bmcecapitalbourse.com/com.patiotpm.publicws.v2/services/TPMService/PortfolioServicePort`,requestXML)
  
    const new_response=getSynthesisInRefCurrencyV1Request.Response2Json(response.data,"getSynthesisInRefCurrencyV1");
    // console.log(new_response)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }
    // transform the data into the format we are expecting
    try {
     if(new_response["resultCode"]===0 && new_response["detailCode"]===-1){
      return { 
        status:response.status,
        result_code:new_response["resultCode"],
      detail_code:new_response["detailCode"],
      accountingDate:new_response["accountingDate"],
      buyingPowerList:new_response["buyingPowerList"],
      currency:new_response["currency"],
      assetValuationAmount:new_response["assetValuationAmount"],
      cashValuationAmount:new_response["cashValuationAmount"],
      globalValuationAmount:new_response["globalValuationAmount"],
      bloquedValuationAmount:new_response["bloquedValuationAmount"],
      assetQuoteMaxDate:new_response["assetQuoteMaxDate"]
     }
     
     }
     else{
      return { 
        status:response.status,
        result_code:new_response["resultCode"],
      detail_code:new_response["detailCode"],
      accountingDate:1111,
      buyingPowerList:[],
      currency:"currency",
      assetValuationAmount:0,
      cashValuationAmount:0,
      globalValuationAmount:0,
      bloquedValuationAmount:0,
      assetQuoteMaxDate:"1111",
     }
       
    }
       
     
       
    } catch {
      return { kind: "bad-data" }
    }
  }

  async GetTitresBKBApi(login: string,contextId:string,accountCode:number,valuationType:number): Promise<Types.GetTitresResult> {

    //generate soap request 
    const getPlainInventoryV1Request=new BKBRequest();
      const main=`<user>
      <contextId>${contextId}</contextId>
      <login>${login}</login>
   </user>
  <accountCode>${accountCode}</accountCode>
  <valuationType>${valuationType}</valuationType>`
    const requestXML=getPlainInventoryV1Request.createBodyRequest("getPlainInventoryV1",main)
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.post(`https://recetteappmobile.bmcecapitalbourse.com/com.patiotpm.publicws.v2/services/TPMService/PortfolioServicePort`,requestXML)
  
    const new_response=getPlainInventoryV1Request.Response2Json(response.data,"getPlainInventoryV1");
    // console.log(new_response)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }
    // transform the data into the format we are expecting
    try {
     if(new_response["resultCode"]===0 && new_response["detailCode"]===-1){
      return { 
        status:response.status,
        result_code:new_response["resultCode"],
        detail_code:new_response["detailCode"],
        accountingDate:new_response["accountingDate"],
        maxAssetPriceDate:new_response["maxAssetPriceDate"],
        plainInventoryElementList:new_response["plainInventoryElementList"]
      
     }
     
     }
     else{
      return { 
        status:response.status,
        result_code:new_response["resultCode"],
      detail_code:new_response["detailCode"],
      accountingDate:1111,
      maxAssetPriceDate:1111,
      plainInventoryElementList:[]
      
     }
       
    }
       
     
       
    } catch {
      return { kind: "bad-data" }
    }
  }


  async GetValuesBKBApi(login: string,contextId:string,accountCode:number,valuationType:number): Promise<Types.GetValuesResult> {

    //generate soap request 
    const getPlainInventoryV1Request=new BKBRequest();
      const main=`<user>
      <contextId>${contextId}</contextId>
      <login>${login}</login>
   </user>
  <accountCode>${accountCode}</accountCode>
  <valuationType>${valuationType}</valuationType>`
    const requestXML=getPlainInventoryV1Request.createBodyRequest("getPlainInventoryV1",main)
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.post(`https://recetteappmobile.bmcecapitalbourse.com/com.patiotpm.publicws.v2/services/TPMService/PortfolioServicePort`,requestXML)
  
    const new_response=getPlainInventoryV1Request.Response2Json(response.data,"getPlainInventoryV1");
    // console.log(new_response["plainInventoryElementList"])

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }
    // transform the data into the format we are expecting
    try {
     if(new_response["resultCode"]===0 && new_response["detailCode"]===-1){
      return { 
        status:response.status,
        result_code:new_response["resultCode"],
        detail_code:new_response["detailCode"],
        accountingDate:new_response["accountingDate"],
        maxAssetPriceDate:new_response["maxAssetPriceDate"],
        plainInventoryElementList:new_response["plainInventoryElementList"],
        totalPendingPNL:new_response["totalPendingPNL"],
        totalRealizedPNL:new_response["totalRealizedPNL"],
        currency:"BKB",
        totalSecuritiesValuation:new_response["totalSecuritiesValuation"]

      
     }
     
     }
     else{
      return { 
        status:response.status,
        result_code:new_response["resultCode"],
      detail_code:new_response["detailCode"],
      accountingDate:1111,
      maxAssetPriceDate:1111,
      plainInventoryElementList:[],
      totalPendingPNL:1111,
      totalRealizedPNL:1111,
      currency:"BKB",
      totalSecuritiesValuation:1111


      
     }
       
    }
       
     
       
    } catch {
      return { kind: "bad-data" }
    }
  }


  async GetOrderListBKBApi(login: string,contextId:string,accountCode:number): Promise<Types.GetOrderListResult> {

    //generate soap request 
    const getOrderBookV2Request=new BKBRequest();
      const main=`<user>
      <contextId>${contextId}</contextId>
      <login>${login}</login>
   </user>
  <accountCode>${accountCode}</accountCode>`
    const requestXML=getOrderBookV2Request.createBodyRequest("getOrderBookV2",main)
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.post(`https://recetteappmobile.bmcecapitalbourse.com/com.patiotpm.publicws.v2/services/TPMService/OrderServicePort`,requestXML)
  
    const new_response=getOrderBookV2Request.Response2Json(response.data,"getOrderBookV2");
    // console.log(new_response["orderList"])

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }
    // transform the data into the format we are expecting
    try {
     if(new_response["resultCode"]===0 && new_response["detailCode"]===-1){
      return { 
        status:response.status,
        result_code:new_response["resultCode"],
        detail_code:new_response["detailCode"],
        orderList:new_response["orderList"]

      
     }
     
     }
     else{
      return { 
        status:response.status,
        result_code:new_response["resultCode"],
        detail_code:new_response["detailCode"],
        orderList:[]
      
     }
       
    }
       
     
       
    } catch {
      return { kind: "bad-data" }
    }
  }
  
  async CancelOrderAPI(login: string,contextId:string,accountCode:number,orderInternalReference:string,issuerCode:string): Promise<Types.GetCancelOrder> {

    //generate soap request 
    const cancelOrderByInternalRefV1=new BKBRequest();
      const main=`<user>
        <contextId>${contextId}</contextId>
        <login>${login}</login>
    </user>
    <accountCode>${accountCode}</accountCode>
    <orderInternalReference>${orderInternalReference}</orderInternalReference>
    <issuerCode>${issuerCode}</issuerCode>`
    const requestXML=cancelOrderByInternalRefV1.createBodyRequest("cancelOrderByInternalRefV1",main)
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.post(`https://recetteappmobile.bmcecapitalbourse.com/com.patiotpm.publicws.v2/services/TPMService/OrderServicePort`,requestXML)
  
    const new_response=cancelOrderByInternalRefV1.Response2Json(response.data,"cancelOrderByInternalRefV1");
    // console.log(new_response["orderList"])

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }
    // transform the data into the format we are expecting
    try {
     if(new_response["resultCode"]===0 && new_response["detailCode"]===-1){
      return { 
        status:response.status,
        result_code:new_response["resultCode"],
        detail_code:new_response["detailCode"],
    

      
     }
     
     }
     else{
      return { 
        status:response.status,
        result_code:new_response["resultCode"],
        detail_code:new_response["detailCode"],
      
     }
       
    }
       
     
       
    } catch {
      return { kind: "bad-data" }
    }
  }



  async getSingleAssetV1API(login: string,contextId:string,accountCode:number,assetCode:string,quotationPlaceCode:number,withQuote:number): Promise<Types.GetSignleAssetV1Result> {

    //generate soap request 
    const getSingleAssetV1=new BKBRequest();
      const main=`<user>
              <contextId>${contextId}</contextId>
              <login>${login}</login>
          </user>

        <accountCode>${accountCode}</accountCode>
        <assetCode>${assetCode}</assetCode>
        <quotationPlaceCode>${quotationPlaceCode}</quotationPlaceCode>
        <withQuote>${withQuote}</withQuote>`
    const requestXML=getSingleAssetV1.createBodyRequest("getSingleAssetV1",main)
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.post(`https://recetteappmobile.bmcecapitalbourse.com/com.patiotpm.publicws.v2/services/TPMService/AssetServicePort`,requestXML)
  
    const new_response=getSingleAssetV1.Response2Json(response.data,"getSingleAssetV1");
    // console.log(new_response["marketList"]["validityList"][0])

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }
    // transform the data into the format we are expecting
    try {
     if(new_response["resultCode"]===0 && new_response["detailCode"]===-1){
      return { 
        status:response.status,
        result_code:new_response["resultCode"],
        detail_code:new_response["detailCode"],
        validityList:new_response["marketList"],
        // validityList:[],
        conditionList:new_response["conditionList"],
        modalityList:new_response["modalityList"]
    

      
     }
     
     }
     else{
      return { 
        status:response.status,
        result_code:new_response["resultCode"],
        detail_code:new_response["detailCode"],
        validityList:[],
        conditionList:[],
        modalityList:[]
      
     }
       
    }
       
     
       
    } catch {
      return { kind: "bad-data" }
    }
  }



  async ValidateOrdreAPI(login: string,contextId:string,accountCode:number,assetCode:string,quotationPlaceCode:number,marketCode:string,modalityCode : string ,firstLimit : number,quantity : number,validityDate : number ,validityType : number,secondExecutionConditionCode : number,side :number): Promise<Types.GetValidateOrderResult> {

    //generate soap request 
    const validateCreateOrderWithAlertsV1=new BKBRequest();
      const main=`<user>
              <contextId>${contextId}</contextId>
              <login>${login}</login>
          </user>


        <orderToPlace>
          <accountCode>${accountCode}</accountCode>
          <assetCode>${assetCode}</assetCode>
        <issuerCode>W</issuerCode>
          <marketCode>${marketCode}</marketCode>
          <modalityCode>${modalityCode}</modalityCode>
          <firstLimit>${firstLimit}</firstLimit>
          <quantity>${quantity}</quantity>
          <side>${side}</side>
          <quotationPlaceCode>${quotationPlaceCode}</quotationPlaceCode>
          <validityDate>${validityDate}</validityDate>
          <validityType>${validityType}</validityType>
          <secondExecutionConditionCode>${secondExecutionConditionCode}</secondExecutionConditionCode>
        </orderToPlace>`
    const requestXML=validateCreateOrderWithAlertsV1.createBodyRequest("validateCreateOrderWithAlertsV1",main)
    // make the api call
    const response: ApiResponse<any> = await this.apisauce.post(`https://recetteappmobile.bmcecapitalbourse.com/com.patiotpm.publicws.v2/services/TPMService/OrderServicePort`,requestXML)
  
    const new_response=validateCreateOrderWithAlertsV1.Response2Json(response.data,"validateCreateOrderWithAlertsV1");
    console.log(new_response)

    // the typical ways to die when calling an api
    if (!response.ok) {
      const problem = getGeneralApiProblem(response)
      if (problem) return problem
    }
    // transform the data into the format we are expecting
    try {
      return { 
        status:response.status,
        result_code:new_response["resultCode"],
        detail_code:new_response["detailCode"],
        detail:new_response["detailCode"]
       
      
     }
       
     
       
    } catch {
      return { kind: "bad-data" }
    }
  }
}
