import { GeneralApiProblem } from "./api-problem"
import { Character } from "../../models/character/character"

export interface User {
  id: number
  name: string
}

export type GetUsersResult = { kind: "ok"; users: User[] } | GeneralApiProblem
export type GetUserResult = { kind: "ok"; user: User } | GeneralApiProblem
export type GetUserBKBResult = {status : number, result_code: any,detail_code:any, contextId:any,login:any } | GeneralApiProblem
export type GetUserBKBDisconnectResult = {status : number, result_code: any,detail_code:any} | GeneralApiProblem
export type GetChangePasswordResult = {status : number, result_code: any,detail_code:any} | GeneralApiProblem
export type GetUserListAccountBKB = {status : number, result_code: any,detail_code:any,accountCode:any,accountLabel:any,accountType:any,mainAccount:any} | GeneralApiProblem
export type GetSynthesisinRefResult = {status : number, result_code: any,detail_code:any,accountingDate:any,
  buyingPowerList:any,
  currency:any,
  assetValuationAmount:any,
  cashValuationAmount:any,
  globalValuationAmount:any,
  bloquedValuationAmount:any,
  assetQuoteMaxDate:any
} | GeneralApiProblem
export type GetTitresResult = {status : number, result_code: any,detail_code:any,accountingDate:any,
  maxAssetPriceDate:any,
  plainInventoryElementList:any
} | GeneralApiProblem
export type GetValuesResult = {status : number, result_code: any,detail_code:any,accountingDate:any,
  maxAssetPriceDate:any,
  plainInventoryElementList:any,
  totalPendingPNL:any,
  totalRealizedPNL:any,
  currency:any,
  totalSecuritiesValuation:any


} | GeneralApiProblem
export type GetOrderListResult = {status : number, result_code: any,detail_code:any,orderList:any} | GeneralApiProblem
export type GetCancelOrder = {status : number, result_code: any,detail_code:any} | GeneralApiProblem
export type GetSignleAssetV1Result = {status : number, result_code: any,detail_code:any,validityList:any,modalityList:any,conditionList:any} | GeneralApiProblem
export type GetValidateOrderResult = {status : number, result_code: any,detail_code:any,detail : any} | GeneralApiProblem





export type GetCharactersResult = { kind: "ok"; characters: Character[] } | GeneralApiProblem
export type GetCharacterResult = { kind: "ok"; character: Character } | GeneralApiProblem


// currency: types.optional(types.string,"BKB"),
//     assetValuationAmount:types.optional(types.number,0.0),
//     cashValuationAmount:types.optional(types.number,0.0),
//     globalValuationAmount :types.optional(types.number,0.0),
//     bloquedValuationAmount:types.optional(types.number,0.0),
//     assetQuoteMaxDate:types.optional(types.string,""),